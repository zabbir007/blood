$(function (){

    $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });


    $("#editOutdoorDonor").click(function(e){

    	$(this).hide();
    	$("#updateOutdoorDonor").show();
      $("#showSex6").show();
      $("#showSex5").show();
      $("#showSex4").hide();
    	$("#backOutdoorDonor").show();
    	$(':input').prop('readonly', false);
    	$("#printOutdoorDonor").hide();

    	e.preventDefault();
    });

    $(".backOutdoorDonor").click(function(){
    	location.reload();
    });

     $(".deleteOutdoorDonor").click(function(e){

               
                var element=$(this);
                var id = element.attr("id");
                var APP_URL = $('meta[name="_base_url"]').attr('content');

               
                
                 swal({
                  title: "আপনি কি নিশ্চিত?",
                  text: "মুছে ফেলা হলে, আপনি এই রেকর্ড পুনরুদ্ধার করতে পারবেন না!",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
                })
                .then((willDelete) => {
                  if (willDelete) {

                        
                         jQuery.ajax({
                            url: APP_URL+'/user/delete-outdoor-donor',
                            method: 'post',
                            data:{id:id},
                            success: function(result){
                             
                              
                                location.href = APP_URL+"/user/create-outdoor-donor";
                            },
                              error: function() {
                                alert('Error occurs!');
                             }
                        });



                    swal("প্রমাণ!  আপনার রেকর্ড মুছে ফেলা হয়েছে!!", {
                      icon: "success",
                    });
                  } else {
                    swal("আপনার রেকর্ড নিরাপদ!");
                  }
                });

                e.preventDefault();
            })
    
  




});