 $(function(){

    $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });


  
  //user district check start here....
    $("[name='districtName']").keyup(function(){

      var APP_URL = $('meta[name="_base_url"]').attr('content');

      

      var district=$(this).val();

      
  
         jQuery.ajax({
              url: APP_URL+'/admin/check-district',
              method: 'post',
              data:{district:district},
              success: function(result){
                var checkEmail = JSON.parse(result);

                if(checkEmail=="Found"){
                   swal({
                        title: "This District Allready Used",
                        icon: "warning",
                        dangerMode: true,
                      })

                   $("#btnDistrictSave").hide();
                }else{
                  $("#btnDistrictSave").show();
                }
                
              },
                error: function() {
                  alert('Error occurs!');
               }
          });
      });
//user district check end here....



    
   $("select[name='regionId']").change(function(){
              var id=$.trim($("select[name='regionId']").val());
              if(id==''){
                $('#districtNameField').hide();
                $('#btnDistrictSave').hide();
              }
              else{
                    $('#districtNameField').show();
                    $('#btnDistrictSave').show();
                  }
               });

        
$(".btnDistrictDelete").click(function(){

               
                 var element=$(this);
                 var id = element.attr("id");
                 var page=$("[name='page']").val();
                 var APP_URL = $('meta[name="_base_url"]').attr('content');
                 swal({
                  title: "Are you sure?",
                  text: "Once deleted, you will not be able to recover this  file!",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
                })
                .then((willDelete) => {
                  if (willDelete) {

                        
                         jQuery.ajax({
                            url: APP_URL+'/admin/delete-district',
                            method: 'post',
                            data:{id:id,page:page},
                            beforeSend: function() {
                              $('#loadingImg').show();
                            },
                            success: function(result){

                              $('#loadingImg').hide();
                               var result = JSON.parse(result);
                               if(result=="success"){
                                       location.reload(true);
                               }
                            },
                              error: function() {
                                alert('Error occurs!');
                             }
                        });



                    swal("Poof! Your  file has been deleted!", {
                      icon: "success",
                    });
                  } 
                  else {
                    swal("Your  file is safe!");
                  }
                });
            })


   
   //Search District

   $("#searchDistrictBox").keyup(function(){
        var searchText=$.trim($(this).val());

        var APP_URL = $('meta[name="_base_url"]').attr('content');

        $("#districtTable").hide();
        $("#paginationLink").hide();

        if($.trim(searchText)){

              jQuery.ajax({
              url: APP_URL+'/admin/search-district',
              method: 'post',
              data:{searchText:searchText},
               beforeSend: function() {
              $('#loadingImg').show();
            },
              success: function(result){
                  $('#loadingImg').hide();
                 $("#searchDistrictTable").find("tr:gt(0)").remove();
                  //console.log(result);
                  var info = JSON.parse(result);

                  var data="";

                  
                   $.each(info, function (key, val) {
                  
                  
                    data+='<tr/><td style="border: 2px solid #F7F7F4; text-align: center;">'+val.districtName+'</td><td style="border: 2px solid #F7F7F4; text-align: center;">'+val.regionName+'</td><td style="border: 2px solid #F7F7F4; text-align: center;"><a href="'+APP_URL+'/admin/edit-district/'+val.id+'/1'+'" class="action-icon"><i class="mdi mdi-square-edit-outline"></i></a><a id="'+val.id+'"  class="action-icon btnDistrictSearchDelete"><i class="mdi mdi-delete"></i></a></td></tr/>';
                   
                 

                
                   
              });

                   
                  $("#searchDistrictTable").show().append(data).on('click','.btnDistrictSearchDelete', function () {

                     var element=$(this);
                    var id = element.attr("id");
                    var del_id = element.attr("id");

                 var APP_URL = $('meta[name="_base_url"]').attr('content');
                
                 swal({
                  title: "Are you sure?",
                  text: "Once deleted, you will not be able to recover this record!",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
                })
                .then((willDelete) => {
                  if (willDelete) {

                        
                         jQuery.ajax({
                            url: APP_URL+'/admin/delete-district',
                            method: 'post',
                            data:{id:del_id},
                             beforeSend: function() {
                              $('#loadingImg').show();
                            },
                            success: function(result){

                               $('#loadingImg').hide();
                               var result = JSON.parse(result);
                               if(result=="success"){
                                       location.reload(true);
                                }
                            },
                              error: function() {
                                alert('Error occurs!');
                             }
                        });



                    swal("Poof! Your  record has been deleted!", {
                      icon: "success",
                    });
                  } 
                  else {
                    swal("Your  record is safe!");
                  }
                });

                
                   
                  });
              },
                error: function() {
                  alert('Error occurs!');
               }
          });

        }else{

        $("#districtTable").show();
        $("#paginationLink").show();
        $("#searchDistrictTable").hide();

        }

        

   });





 })