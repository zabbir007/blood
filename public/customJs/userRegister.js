 $(function(){

 	$.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });

//user email check start here....
 		$("[name='email']").keyup(function(){

 			var APP_URL = $('meta[name="_base_url"]').attr('content');

 			

 			var userEmail=$(this).val();

 			
  
	       jQuery.ajax({
	            url: APP_URL+'/admin/check-user-email',
	            method: 'post',
	            data:{userEmail:userEmail},
	            success: function(result){
	            	var checkEmail = JSON.parse(result);

	            	if(checkEmail=="Found"){
	            		 swal({
			                  title: "This Email Allready Used",
			                  icon: "warning",
			                  dangerMode: true,
			                })

	            		 $("#signUp").hide();
            		}else{
            			$("#signUp").show();
            		}
	              
	            },
	              error: function() {
	                alert('Error occurs!');
	             }
	        });
 	    });
//user email check end here....

//user phone check start here...
        $("[name='phone']").keyup(function(){

 			var APP_URL = $('meta[name="_base_url"]').attr('content');

 			

 			var userPhone=$(this).val();

 			
  
	       jQuery.ajax({
	            url: APP_URL+'/admin/check-user-phone',
	            method: 'post',
	            data:{userPhone:userPhone},
	            success: function(result){
	            	var checkPhone = JSON.parse(result);

	            	if(checkPhone=="Found"){
	            		 swal({
			                  title: "This Number Allready Used",
			                  icon: "warning",
			                  dangerMode: true,
			                })

	            		 $("#signUp").hide();
            		}else{
            			$("#signUp").show();
            		}
	              
	            },
	              error: function() {
	                alert('Error occurs!');
	             }
	        });
 	    });
//user phone check end here.....




//search user start here...

$(".btnUserDelete").click(function(){

               
                var element=$(this);
                var id = element.attr("id");
                
                 var APP_URL = $('meta[name="_base_url"]').attr('content');
                 swal({
                  title: "Are you sure?",
                  text: "Once deleted, you will not be able to recover this  file!",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
                })
                .then((willDelete) => {
                  if (willDelete) {

                        
                         jQuery.ajax({
                            url: APP_URL+'/admin/delete-user',
                            method: 'post',
                            data:{id:id},
                             beforeSend: function() {
                              $('#loadingImg').show();
                            },
                            success: function(result){
                               $('#loadingImg').hide();
                                var result = JSON.parse(result);
                               if(result=="success"){
                                       location.reload(true);
                                }
                                
                            },
                              error: function() {
                                alert('Error occurs!');
                             }
                        });



                    swal("Poof! Your  file has been deleted!", {
                      icon: "success",
                    });
                  } 
                  else {
                    swal("Your  file is safe!");
                  }
                });
            })

//Active Indoor Donor

 $(".btnUserActive").click(function(){

               
                var element=$(this);
                var id = element.attr("id");
                
                 var APP_URL = $('meta[name="_base_url"]').attr('content');
                 swal({
                  title: "Are you sure?",
                  text: "Once Active, you will not be able to recover this  file!",
                  icon: "success",
                  buttons: true,
                  dangerMode: false,
                })
                .then((willDelete) => {
                  if (willDelete) {

                        
                         jQuery.ajax({
                            url: APP_URL+'/admin/active-user',
                            method: 'post',
                            data:{id:id},
                             beforeSend: function() {
                              $('#loadingImg').show();
                            },
                            success: function(result){
                               $('#loadingImg').hide();
                                var result = JSON.parse(result);
                               if(result=="success"){
                                       location.reload(true);
                                }
                                
                            },
                              error: function() {
                                alert('Error occurs!');
                             }
                        });



                    swal("Poof! This User Active Now!", {
                      icon: "success",
                    });
                  } 
                  else {
                    swal("Your  file is safe!");
                  }
                });
            })


//Deactive Indoor Donor...
  $(".btnUserDeActive").click(function(){

               
                var element=$(this);
                var id = element.attr("id");
                
                 var APP_URL = $('meta[name="_base_url"]').attr('content');
                 swal({
                  title: "Are you sure?",
                  text: "Once Deactive, you will not be able to recover this  file!",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
                })
                .then((willDelete) => {
                  if (willDelete) {

                        
                         jQuery.ajax({
                            url: APP_URL+'/user/deactive-user',
                            method: 'post',
                            data:{id:id},
                             beforeSend: function() {
                              $('#loadingImg').show();
                            },
                            success: function(result){
                               $('#loadingImg').hide();
                                var result = JSON.parse(result);
                               if(result=="success"){
                                       location.reload(true);
                                }
                                
                            },
                              error: function() {
                                alert('Error occurs!');
                             }
                        });



                    swal("Poof! This User Deactive Now !", {
                      icon: "success",
                    });
                  } 
                  else {
                    swal("Your  file is safe!");
                  }
                });
            })

//Search Indoor Donor..

   $("#searchUserBox").keyup(function(){
        var searchText=$.trim($(this).val());
        
        var APP_URL = $('meta[name="_base_url"]').attr('content');
        console.log(searchText);
         console.log(APP_URL);
        $("#userTable").hide();
        $("#paginationLink").hide();

        if($.trim(searchText)){

              jQuery.ajax({
              url: APP_URL+'/admin/search-user',
              method: 'post',
              data:{searchText:searchText},
               beforeSend: function() {
                $('#loadingImg').show();
              },
              success: function(result){
                 $('#loadingImg').hide();
                 $("#searchUserTable").find("tr:gt(0)").remove();
                  //console.log(result);
                  var info = JSON.parse(result);

                  var data="";
                  var status="";
                  var statusShow="";
                  
                   $.each(info, function (key, val) {
                   
                   if (val.status=='1') {
                    status='<a href="#" id="'+val.id+'" class="action-icon btnUserDeActive"><span class="mdi mdi-close-circle"></span></a>';
                    statusShow=' <h5><span class="badge badge-success">Active</span></h5>';     
                   }else{
                    status=' <a href="#" id="'+val.id+'" class="action-icon btnUserActive"><span class="mdi mdi-checkbox-marked-circle"></span></a>';
                    statusShow='<h5><span class="badge badge-danger">DeActive</span></h5>';
                   }
                  
                    data+='<tr/><td style="border: 2px solid #F7F7F4; text-align: center;">'+val.name+'</td><td style="border: 2px solid #F7F7F4; text-align: center;">'+val.email+'</td><td style="border: 2px solid #F7F7F4; text-align: center;">'+val.phone+'</td><td style="border: 2px solid #F7F7F4; text-align: center;">'+val.branchName+'</td><td style="border: 2px solid #F7F7F4; text-align: center;">'+statusShow+'</td><td style="border: 2px solid #F7F7F4; text-align: center;">'+status+'<a id="'+val.id+'" class="action-icon btnUserDelete"><i class="mdi mdi-delete"></i></a></td></tr/>';
                   
                 

                
                   
              });

                   
                  $("#searchUserTable").show().append(data).on('click','.btnUserDelete', function () {


                    var element=$(this);
                    var id = element.attr("id");
                 var APP_URL = $('meta[name="_base_url"]').attr('content');
                
                 swal({
                  title: "Are you sure?",
                  text: "Once deleted, you will not be able to recover this record!",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
                })
                .then((willDelete) => {
                  if (willDelete) {

                        
                         jQuery.ajax({
                            url: APP_URL+'/admin/delete-user',
                            method: 'post',
                            data:{id:id},
                            success: function(result){
                               $('#loadingImg').hide();
                               var result = JSON.parse(result);
                               if(result=="success"){
                                       location.reload(true);
                                }
                            },
                              error: function() {
                                alert('Error occurs!');
                             }
                        });



                    swal("Poof! Your  record has been deleted!", {
                      icon: "success",
                    });
                  } 
                  else {
                    swal("Your  record is safe!");
                  }
                });

                
                   
                  });
//active indoor donor start here...
     //Active Indoor Donor

 $(".btnUserActive").click(function(){

               
                var element=$(this);
                var id = element.attr("id");
                
                 var APP_URL = $('meta[name="_base_url"]').attr('content');
                 swal({
                  title: "Are you sure?",
                  text: "Once Active, you will not be able to recover this  file!",
                  icon: "success",
                  buttons: true,
                  dangerMode: false,
                })
                .then((willDelete) => {
                  if (willDelete) {

                        
                         jQuery.ajax({
                            url: APP_URL+'/admin/active-user',
                            method: 'post',
                            data:{id:id},
                             beforeSend: function() {
                              $('#loadingImg').show();
                            },
                            success: function(result){
                               $('#loadingImg').hide();
                                var result = JSON.parse(result);
                               if(result=="success"){
                                       location.reload(true);
                                }
                                
                            },
                              error: function() {
                                alert('Error occurs!');
                             }
                        });



                    swal("Poof! This User Active Now!", {
                      icon: "success",
                    });
                  } 
                  else {
                    swal("Your  file is safe!");
                  }
                });
            })


//Deactive Indoor Donor...
  $(".btnUserDeActive").click(function(){

               
                var element=$(this);
                var id = element.attr("id");
                
                 var APP_URL = $('meta[name="_base_url"]').attr('content');
                 swal({
                  title: "Are you sure?",
                  text: "Once Deactive, you will not be able to recover this  file!",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
                })
                .then((willDelete) => {
                  if (willDelete) {

                        
                         jQuery.ajax({
                            url: APP_URL+'/admin/deactive-user',
                            method: 'post',
                            data:{id:id},
                             beforeSend: function() {
                              $('#loadingImg').show();
                            },
                            success: function(result){
                               $('#loadingImg').hide();
                                var result = JSON.parse(result);
                               if(result=="success"){
                                       location.reload(true);
                                }
                                
                            },
                              error: function() {
                                alert('Error occurs!');
                             }
                        });



                    swal("Poof! This User Deactive Now !", {
                      icon: "success",
                    });
                  } 
                  else {
                    swal("Your  file is safe!");
                  }
                });
            })

//active indoor donor End here...

              },
                error: function() {
                  alert('Error occurs!');
               }
          });

        }else{

        $("#userTable").show();
        $("#paginationLink").show();
        $("#searchUserTable").hide();

        }

        

   });



 })