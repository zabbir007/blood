 $(function(){

  $.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});
  console.log('jabbir');      
$(".btnTestReportDelete").click(function(){

               
                var element=$(this);
                var id = element.attr("id");
                console.log('ID: '+id);
               
                 var APP_URL = $('meta[name="_base_url"]').attr('content');
                 swal({
                  title: "Are you sure?",
                  text: "Once deleted, you will not be able to recover this  file!",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
                })
                .then((willDelete) => {
                  if (willDelete) {

                        
                         jQuery.ajax({
                            url: APP_URL+'/user/delete-test-report',
                            method: 'post',
                            data:{id:id,page:page},
                             beforeSend: function() {
                              $('#loadingImg').show();
                            },
                            success: function(result){
                               $('#loadingImg').hide();
                                var result = JSON.parse(result);
                               if(result=="success"){
                                       location.reload(true);
                                }
                                
                            },
                              error: function() {
                                alert('Error occurs!');
                             }
                        });



                    swal("Poof! Your  file has been deleted!", {
                      icon: "success",
                    });
                  } 
                  else {
                    swal("Your  file is safe!");
                  }
                });
            })


//Search Area

   $("#searchTestReportBoxJ").keyup(function(){
        var searchText=$.trim($(this).val());
        
        var APP_URL = $('meta[name="_base_url"]').attr('content');
        console.log(searchText);
         console.log(APP_URL);
        $("#testReportTable").hide();
        $("#paginationLink").hide();

        if($.trim(searchText)){

              jQuery.ajax({
              url: APP_URL+'/user/search-test-report',
              method: 'post',
              data:{searchText:searchText},
               beforeSend: function() {
                $('#loadingImg').show();
              },
              success: function(result){
                 $('#loadingImg').hide();
                 $("#searchTestReportTable").find("tr:gt(0)").remove();
                  //console.log(result);
                  var info = JSON.parse(result);

                  var data="";

                  
                   $.each(info, function (key, val) {
                  
                  
                    data+='<tr/><td style="border: 2px solid #F7F7F4; text-align: center;">'+val.testNumber+'</td><td style="border: 2px solid #F7F7F4; text-align: center;">'+val.name+'</td><td style="border: 2px solid #F7F7F4; text-align: center;">'+val.referance+'</td><td style="border: 2px solid #F7F7F4; text-align: center;"> <a href="'+APP_URL+'/user/view-test-report/'+val.id+'" class="action-icon"> <i class="mdi mdi-eye"></i></a><a href="'+APP_URL+'/user/edit-test-report/'+val.id+'/1'+'" class="action-icon"><i class="mdi mdi-square-edit-outline"></i></a><a id="'+val.id+'" class="action-icon btnAmountSearchDelete"><i class="mdi mdi-delete"></i></a></td></tr/>';
                   
                 

                
                   
              });

                   
                  $("#searchTestReportTable").show().append(data).on('click','.btnAmountSearchDelete', function () {


                    var element=$(this);
                    var id = element.attr("id");
                 var APP_URL = $('meta[name="_base_url"]').attr('content');
                
                 swal({
                  title: "Are you sure?",
                  text: "Once deleted, you will not be able to recover this record!",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
                })
                .then((willDelete) => {
                  if (willDelete) {

                        
                         jQuery.ajax({
                            url: APP_URL+'/user/delete-test-report',
                            method: 'post',
                            data:{id:id},
                            success: function(result){
                               $('#loadingImg').hide();
                               var result = JSON.parse(result);
                               if(result=="success"){
                                       location.reload(true);
                                }
                            },
                              error: function() {
                                alert('Error occurs!');
                             }
                        });



                    swal("Poof! Your  record has been deleted!", {
                      icon: "success",
                    });
                  } 
                  else {
                    swal("Your  record is safe!");
                  }
                });

                
                   
                  });
              },
                error: function() {
                  alert('Error occurs!');
               }
          });

        }else{

        $("#testReportTable").show();
        $("#paginationLink").show();
        $("#searchTestReportTable").hide();

        }

        

   });


   //search test 
   $("#searchTestBox").keyup(function(){
        var searchText=$.trim($(this).val());
        
        var APP_URL = $('meta[name="_base_url"]').attr('content');
        console.log(searchText);
         console.log(APP_URL);
        $("#showTest").hide();

        if($.trim(searchText)){

              jQuery.ajax({
              url: APP_URL+'/user/search-test-user',
              method: 'post',
              data:{searchText:searchText},
               beforeSend: function() {
                $('#loadingImg').show();
              },
              success: function(result){
                 $('#loadingImg').hide();
                 $("#searchShowTest").find("tr:gt(0)").remove();
                  //console.log(result);
                  var info = JSON.parse(result);

                  var data="";

                  
                   $.each(info, function (key, val) {
                  
                  
                    data+='<tr><td><input class="my-activity" type="checkbox"  name="test[]" value="'+val.id+'">Test Name :'+val.testName+' Test Amount : '+val.testAmount+'</td></tr>';
                   
                 

                
                   
              });

                   
                  $("#searchShowTest").show().append(data);
              },
                error: function() {
                  alert('Error occurs!');
               }
          });

        }else{

        $("#showTest").show();
        $("#searchShowTest").hide();

        }

        

   });

  

//Test cost count start here...
   $(".my-activity").click(function(event) {
       var APP_URL = $('meta[name="_base_url"]').attr('content');
        var total = 0;
        console.log(total);
        $("#showTotalAmount").empty();
          $("#paidTotalAmount").hide();
        $(".my-activity:checked").each(function() {
            
            $("#testAmountTable").show();
            $("#paidTotalAmount").show();
             var id= parseInt($(this).val());
             jQuery.ajax({
                        url: APP_URL+'/user/get-test-amount',
                        method: 'post',
                        data:{id:id},
                        success: function(result){
                         
                          var val = JSON.parse(result);
                          testAmount=parseInt(val.testAmount);
                          // console.log(testAmount);
                         total += testAmount;
                          
                         $("#showTotalAmount").empty();
                         var showTotalAmount='</br></br><span>Total Cost</span></br> <input type="text" readonly="" name="totalAmount" value="'+total+'" class="form-control mb-2">';
                         $("#showTotalAmount").append(showTotalAmount);
                         
                        },
                          error: function() {
                            alert('Error occurs!');
                         }
                    });
        });
        
        // if (total == 0) {
        //     $('#amount').val('');
        // } else {                
        //     $('#amount').val(total);
        // }

    });

//Test Cost count End Hete.....

//Total Paid amount Check.........
   $("#paidAmount").keyup(function(){
        $("#saveTestBtn").show();
        var totalPaidAmount=$.trim($(this).val());
         testTotalAmount=parseInt(totalPaidAmount);
         console.log(totalPaidAmount);
        var totalPaid=$.trim($("input[name='totalAmount']").val());
         totalPaidResult=parseInt(totalPaid);
        console.log(totalPaidResult);
        if (testTotalAmount!='') {
          if (totalPaidResult<testTotalAmount) { 
              $("#saveTestBtn").hide();
             swal({
                  title: "Not Accepted?",
                  text: "Total Paid Amount Geater Than Total Amount!",
                  icon: "warning",
                  dangerMode: true,
                })
          }
        }
      
     });
//Total Paid Amount Check.........




 })