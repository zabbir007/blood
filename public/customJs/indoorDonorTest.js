 $(function(){

  $.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});
  console.log('jabbir');      
$(".btnTestDelete").click(function(){

               
                var element=$(this);
                var id = element.attr("id");
                console.log('ID: '+id);
               
                 var APP_URL = $('meta[name="_base_url"]').attr('content');
                 swal({
                  title: "Are you sure?",
                  text: "Once deleted, you will not be able to recover this  file!",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
                })
                .then((willDelete) => {
                  if (willDelete) {

                        
                         jQuery.ajax({
                            url: APP_URL+'/user/delete-indoor-donor-test-report',
                            method: 'post',
                            data:{id:id,page:page},
                             beforeSend: function() {
                              $('#loadingImg').show();
                            },
                            success: function(result){
                               $('#loadingImg').hide();
                                var result = JSON.parse(result);
                               if(result=="success"){
                                       location.reload(true);
                                }
                                
                            },
                              error: function() {
                                alert('Error occurs!');
                             }
                        });



                    swal("Poof! Your  file has been deleted!", {
                      icon: "success",
                    });
                  } 
                  else {
                    swal("Your  file is safe!");
                  }
                });
            })


//Search Area

   $("#searchReportBox").keyup(function(){
        var searchText=$.trim($(this).val());
        
        var APP_URL = $('meta[name="_base_url"]').attr('content');
        console.log(searchText);
         console.log(APP_URL);
        $("#testTable").hide();
        $("#paginationLink").hide();

        if($.trim(searchText)){

              jQuery.ajax({
              url: APP_URL+'/user/search-indoor-donor-test-report',
              method: 'post',
              data:{searchText:searchText},
               beforeSend: function() {
                $('#loadingImg').show();
              },
              success: function(result){
                 $('#loadingImg').hide();
                 $("#searchTestTable").find("tr:gt(0)").remove();
                  //console.log(result);
                  var info = JSON.parse(result);

                  var data="";

                  
                   $.each(info, function (key, val) {
                  
                  
                    data+='<tr/><td style="border: 2px solid #F7F7F4; text-align: center;">'+val.id+'</td><td style="border: 2px solid #F7F7F4; text-align: center;">'+val.donorReg+'</td><td style="border: 2px solid #F7F7F4; text-align: center;">'+val.testDoctorName+'</td><td style="border: 2px solid #F7F7F4; text-align: center;"> <a href="'+APP_URL+'/user/view-indoor-donor-test-report/'+val.id+'" class="action-icon"> <i class="mdi mdi-eye"></i></a><a href="'+APP_URL+'/user/edit-indoor-donor-test-report/'+val.id+'/1'+'" class="action-icon"><i class="mdi mdi-square-edit-outline"></i></a><a id="'+val.id+'" class="action-icon btnAmountSearchDelete"><i class="mdi mdi-delete"></i></a></td></tr/>';
                   
                 

                
                   
              });

                   
                  $("#searchTestTable").show().append(data).on('click','.btnAmountSearchDelete', function () {


                    var element=$(this);
                    var id = element.attr("id");
                 var APP_URL = $('meta[name="_base_url"]').attr('content');
                
                 swal({
                  title: "Are you sure?",
                  text: "Once deleted, you will not be able to recover this record!",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
                })
                .then((willDelete) => {
                  if (willDelete) {

                        
                         jQuery.ajax({
                            url: APP_URL+'/user/delete-indoor-donor-test-report',
                            method: 'post',
                            data:{id:id},
                            success: function(result){
                               $('#loadingImg').hide();
                               var result = JSON.parse(result);
                               if(result=="success"){
                                       location.reload(true);
                                }
                            },
                              error: function() {
                                alert('Error occurs!');
                             }
                        });



                    swal("Poof! Your  record has been deleted!", {
                      icon: "success",
                    });
                  } 
                  else {
                    swal("Your  record is safe!");
                  }
                });

                
                   
                  });
              },
                error: function() {
                  alert('Error occurs!');
               }
          });

        }else{

        $("#testTable").show();
        $("#paginationLink").show();
        $("#searchTestTable").hide();

        }

        

   });





 })