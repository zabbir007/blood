 $(function(){
        
$(".btnPaymentDelete").click(function(){

                var page=$("[name='page']").val();
                var element=$(this);
                var id = element.attr("id");
                // console.log('ID: '+id);
                // console.log('page: '+page);
                 var APP_URL = $('meta[name="_base_url"]').attr('content');
                 swal({
                  title: "Are you sure?",
                  text: "Once deleted, you will not be able to recover this  file!",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
                })
                .then((willDelete) => {
                  if (willDelete) {

                        
                         jQuery.ajax({
                            url: APP_URL+'/user/delete-payment-type',
                            method: 'post',
                            data:{id:id,page:page},
                             beforeSend: function() {
                              $('#loadingImg').show();
                            },
                            success: function(result){
                               $('#loadingImg').hide();
                               var result = JSON.parse(result);
                              if(result=="success"){
                                       location.reload(true);
                                }
                                
                            },
                              error: function() {
                                alert('Error occurs!');
                             }
                        });



                    // swal("Poof! Your  file has been deleted!", {
                    //   icon: "success",
                    // });
                  } 
                  // else {
                  //   swal("Your  file is safe!");
                  // }
                });
            })

$(".btnLeagerDelete").click(function(){

                var page=$("[name='page']").val();
                var element=$(this);
                var id = element.attr("id");
                // console.log('ID: '+id);
                // console.log('page: '+page);
                 var APP_URL = $('meta[name="_base_url"]').attr('content');
                 swal({
                  title: "Are you sure?",
                  text: "Once deleted, you will not be able to recover this  file!",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
                })
                .then((willDelete) => {
                  if (willDelete) {

                        
                         jQuery.ajax({
                            url: APP_URL+'/user/delete-account-leager',
                            method: 'post',
                            data:{id:id,page:page},
                             beforeSend: function() {
                              $('#loadingImg').show();
                            },
                            success: function(result){
                               $('#loadingImg').hide();
                               var result = JSON.parse(result);
                              if(result=="success"){
                                       location.reload(true);
                                }
                                
                            },
                              error: function() {
                                alert('Error occurs!');
                             }
                        });



                    // swal("Poof! Your  file has been deleted!", {
                    //   icon: "success",
                    // });
                  } 
                  // else {
                  //   swal("Your  file is safe!");
                  // }
                });
            })


})