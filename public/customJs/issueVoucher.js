$(function (){

    $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });


    $("#editIssueVoucher").click(function(e){

    	$(this).hide();
    	$("#updateIssueVoucher").show();
      $("#updateMode").show();
      $("#editMode").hide();
    	$("#backIssueVoucher").show();
    	$(':input').prop('readonly', false);
    	$("#printIssueVoucher").hide();

    	e.preventDefault();
    });

    $(".backIssueVoucher").click(function(){
    	location.reload();
    });

     $(".deleteIssueVoucher").click(function(e){

               
                var element=$(this);
                var id = element.attr("id");
                var APP_URL = $('meta[name="_base_url"]').attr('content');

               
                
                 swal({
                  title: "আপনি কি নিশ্চিত?",
                  text: "মুছে ফেলা হলে, আপনি এই রেকর্ড পুনরুদ্ধার করতে পারবেন না!",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
                })
                .then((willDelete) => {
                  if (willDelete) {

                        
                         jQuery.ajax({
                            url: APP_URL+'/user/delete-issue-voucher',
                            method: 'post',
                            data:{id:id},
                            success: function(result){
                             
                              
                                location.href = APP_URL+"/user/create-issue-voucher";
                            },
                              error: function() {
                                alert('Error occurs!');
                             }
                        });



                    swal("প্রমাণ!  আপনার রেকর্ড মুছে ফেলা হয়েছে!!", {
                      icon: "success",
                    });
                  } else {
                    swal("আপনার রেকর্ড নিরাপদ!");
                  }
                });

                e.preventDefault();
            })
    
  




});