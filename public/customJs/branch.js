 $(function(){
  
  //Edit button modal data show start here...
 	$.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });

  
  //user branch check start here....
    $("[name='areaName']").keyup(function(){

      var APP_URL = $('meta[name="_base_url"]').attr('content');

      

      var branch=$(this).val();

      
  
         jQuery.ajax({
              url: APP_URL+'/admin/check-branch',
              method: 'post',
              data:{branch:branch},
              success: function(result){
                var checkEmail = JSON.parse(result);

                if(checkEmail=="Found"){
                   swal({
                        title: "This Branch Allready Used",
                        icon: "warning",
                        dangerMode: true,
                      })

                   $("#btnAreaSave").hide();
                }else{
                  $("#btnAreaSave").show();
                }
                
              },
                error: function() {
                  alert('Error occurs!');
               }
          });
      });
//user branch check end here....






 	 $("select[name='regionId']").change(function(){
        var id=$.trim($("select[name='regionId']").val());

        if(id==''){
          $('#districtSelectBox').hide();
          $('#areaBox').hide();
          $('#btnAreaSave').hide();
        }else{


          var APP_URL = $('meta[name="_base_url"]').attr('content');
          $('#areaBox').hide();

         jQuery.ajax({
            url: APP_URL+'/admin/get-district',
            method: 'post',
            data:{id:id},
            beforeSend: function() {
              $('#loadingImg').show();
            },
            success: function(result){
                $('#loadingImg').hide();

              var district = JSON.parse(result);
              $("#districtId").empty();
              var options='<option value="">----Select----</option>';
              $('#districtSelectBox').show();

               $.each(district, function (key, val) {
                  
                  
                   //console.log(val.id+" "+val.districtName);

                 

                 options +='<option value='+val.id+'>'+val.districtName+'</option>';
                //options.append(option);

                   
              });
                $("#districtId").append(options);
          

            },
              error: function() {
                alert('Error occurs!');
             }
        });


        }

         



   });


   $("select[name='districtId']").change(function(){
          var id=$.trim($("select[name='districtId']").val());

         
          if(id==''){
            $("input[name='areaName']").val('');
            $('#areaBox').hide();
            $('#btnAreaSave').hide();
         }
        else{
          $('#btnAreaSave').show();
          $('#areaBox').show();
        }
     });




   $(".btnAreaDelete").click(function(){

               
                 var element=$(this);
                 var id = element.attr("id");
                 var APP_URL = $('meta[name="_base_url"]').attr('content');
                
                 swal({
                  title: "Are you sure?",
                  text: "Once deleted, you will not be able to recover this record!",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
                })
                .then((willDelete) => {
                  if (willDelete) {

                        
                         jQuery.ajax({
                            url: APP_URL+'/admin/delete-branch',
                            method: 'post',
                            data:{id:id},
                             beforeSend: function() {
                              $('#loadingImg').show();
                            },
                            success: function(result){
                               $('#loadingImg').hide();
                               var result = JSON.parse(result);
                               if(result=="success"){
                                       location.reload(true);
                                }
                                
                            },
                              error: function() {
                                alert('Error occurs!');
                             }
                        });



                    swal("Poof! Your  record has been deleted!", {
                      icon: "success",
                    });
                  } 
                  else {
                    swal("Your  record is safe!");
                  }
                });
            })





   //Search Area

   $("#searchAreaBox").keyup(function(){
        var searchText=$.trim($(this).val());

        var APP_URL = $('meta[name="_base_url"]').attr('content');

        $("#areaTable").hide();
        $("#paginationLink").hide();

        if($.trim(searchText)){

              jQuery.ajax({
              url: APP_URL+'/admin/search-branch',
              method: 'post',
              data:{searchText:searchText},
              beforeSend: function() {
              $('#loadingImg').show();
              },
              success: function(result){
                $('#loadingImg').hide();
                 $("#searchAreaTable").find("tr:gt(0)").remove();
                  //console.log(result);
                  var info = JSON.parse(result);

                  var data="";

                  
                   $.each(info, function (key, val) {
                  
                  
                    data+='<tr/><td>'+val.branchName+'</td><td>'+val.districtName+'</td><td>'+val.regionName+'</td><td><a href="'+APP_URL+'/admin/edit-branch/'+val.id+'/1'+'" class="action-icon"><i class="mdi mdi-square-edit-outline"></i></a><a  id="'+val.id+'" class="action-icon btnAreaSearchDelete"><i class="mdi mdi-delete"></i></a></td></tr/>';
                   
                 

                
                   
              });

                   
                  $("#searchAreaTable").show().append(data).on('click','.btnAreaSearchDelete', function () {


                  
                    var element=$(this);
                 var id = element.attr("id");
                 var APP_URL = $('meta[name="_base_url"]').attr('content');
                
                 swal({
                  title: "Are you sure?",
                  text: "Once deleted, you will not be able to recover this record!",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
                })
                .then((willDelete) => {
                  if (willDelete) {

                        
                         jQuery.ajax({
                            url: APP_URL+'/admin/delete-branch',
                            method: 'post',
                            data:{id:id},
                             beforeSend: function() {
                              $('#loadingImg').show();
                            },
                            success: function(result){
                               $('#loadingImg').hide();
                               var result = JSON.parse(result);
                                if(result=="success"){
                                       location.reload(true);
                               
                                }
                                
                            },
                              error: function() {
                                alert('Error occurs!');
                             }
                        });



                    swal("Poof! Your  record has been deleted!", {
                      icon: "success",
                    });
                  } 
                  else {
                    swal("Your  record is safe!");
                  }
                });

                
                   
                  });
              },
                error: function() {
                  alert('Error occurs!');
               }
          });

        }else{

        $("#areaTable").show();
        $("#paginationLink").show();
        $("#searchAreaTable").hide();

        }

        

   });






   });