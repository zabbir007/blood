 $(function(){

  $.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});
  console.log('jabbir');      
$(".btnIndoorDonorDelete").click(function(){

               
                var element=$(this);
                var id = element.attr("id");
                
                 var APP_URL = $('meta[name="_base_url"]').attr('content');
                 swal({
                  title: "Are you sure?",
                  text: "Once deleted, you will not be able to recover this  file!",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
                })
                .then((willDelete) => {
                  if (willDelete) {

                        
                         jQuery.ajax({
                            url: APP_URL+'/user/delete-in-indoor-donor',
                            method: 'post',
                            data:{id:id},
                             beforeSend: function() {
                              $('#loadingImg').show();
                            },
                            success: function(result){
                               $('#loadingImg').hide();
                                var result = JSON.parse(result);
                               if(result=="success"){
                                       location.reload(true);
                                }
                                
                            },
                              error: function() {
                                alert('Error occurs!');
                             }
                        });



                    swal("Poof! Your  file has been deleted!", {
                      icon: "success",
                    });
                  } 
                  else {
                    swal("Your  file is safe!");
                  }
                });
            })

//Active Indoor Donor

 $(".btnIndoorDonorActive").click(function(){

               
                var element=$(this);
                var id = element.attr("id");
                
                 var APP_URL = $('meta[name="_base_url"]').attr('content');
                 swal({
                  title: "Are you sure?",
                  text: "Once Active, you will not be able to recover this  file!",
                  icon: "success",
                  buttons: true,
                  dangerMode: false,
                })
                .then((willDelete) => {
                  if (willDelete) {

                        
                         jQuery.ajax({
                            url: APP_URL+'/user/active-indoor-donor',
                            method: 'post',
                            data:{id:id},
                             beforeSend: function() {
                              $('#loadingImg').show();
                            },
                            success: function(result){
                               $('#loadingImg').hide();
                                var result = JSON.parse(result);
                               if(result=="success"){
                                       location.reload(true);
                                }
                                
                            },
                              error: function() {
                                alert('Error occurs!');
                             }
                        });



                    swal("Poof! This Donor Active Now!", {
                      icon: "success",
                    });
                  } 
                  else {
                    swal("Your  file is safe!");
                  }
                });
            })


//Deactive Indoor Donor...
  $(".btnIndoorDonorDeActive").click(function(){

               
                var element=$(this);
                var id = element.attr("id");
                
                 var APP_URL = $('meta[name="_base_url"]').attr('content');
                 swal({
                  title: "Are you sure?",
                  text: "Once Deactive, you will not be able to recover this  file!",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
                })
                .then((willDelete) => {
                  if (willDelete) {

                        
                         jQuery.ajax({
                            url: APP_URL+'/user/deactive-indoor-donor',
                            method: 'post',
                            data:{id:id},
                             beforeSend: function() {
                              $('#loadingImg').show();
                            },
                            success: function(result){
                               $('#loadingImg').hide();
                                var result = JSON.parse(result);
                               if(result=="success"){
                                       location.reload(true);
                                }
                                
                            },
                              error: function() {
                                alert('Error occurs!');
                             }
                        });



                    swal("Poof! This Donor Deactive Now !", {
                      icon: "success",
                    });
                  } 
                  else {
                    swal("Your  file is safe!");
                  }
                });
            })

//Search Indoor Donor..

   $("#searchIndoorDonorBox").keyup(function(){
        var searchText=$.trim($(this).val());
        
        var APP_URL = $('meta[name="_base_url"]').attr('content');
        console.log(searchText);
         console.log(APP_URL);
        $("#indoorDonorTable").hide();
        $("#paginationLink").hide();

        if($.trim(searchText)){

              jQuery.ajax({
              url: APP_URL+'/user/search-in-indoor-donor',
              method: 'post',
              data:{searchText:searchText},
               beforeSend: function() {
                $('#loadingImg').show();
              },
              success: function(result){
                 $('#loadingImg').hide();
                 $("#searchIndoorDonorTable").find("tr:gt(0)").remove();
                  //console.log(result);
                  var info = JSON.parse(result);

                  var data="";
                  var status="";
                  var statusShow="";
                  
                   $.each(info, function (key, val) {
                   
                   if (val.status=='1') {
                    status='<a href="#" id="'+val.id+'" class="action-icon btnIndoorDonorDeActive"><span class="mdi mdi-close-circle"></span></a>';
                    statusShow=' <h5><span class="badge badge-success">Active</span></h5>';     
                   }else{
                    status=' <a href="#" id="'+val.id+'" class="action-icon btnIndoorDonorActive"><span class="mdi mdi-checkbox-marked-circle"></span></a>';
                    statusShow='<h5><span class="badge badge-danger">DeActive</span></h5>';
                   }
                  
                    data+='<tr/><td style="border: 2px solid #F7F7F4; text-align: center;">'+val.name+'</td><td style="border: 2px solid #F7F7F4; text-align: center;">'+val.age+'</td><td style="border: 2px solid #F7F7F4; text-align: center;">'+val.sex+'</td><td style="border: 2px solid #F7F7F4; text-align: center;">'+val.education+'</td><td style="border: 2px solid #F7F7F4; text-align: center;">'+val.phone+'</td><td style="border: 2px solid #F7F7F4; text-align: center;">'+val.nibondonNo+'</td><td style="border: 2px solid #F7F7F4; text-align: center;">'+statusShow+'</td><td style="border: 2px solid #F7F7F4; text-align: center;">'+status+'<a id="'+val.id+'" class="action-icon btnIndoorDonorDelete"><i class="mdi mdi-delete"></i></a></td></tr/>';
                   
                 

                
                   
              });

                   
                  $("#searchIndoorDonorTable").show().append(data).on('click','.btnIndoorDonorDelete', function () {


                    var element=$(this);
                    var id = element.attr("id");
                 var APP_URL = $('meta[name="_base_url"]').attr('content');
                
                 swal({
                  title: "Are you sure?",
                  text: "Once deleted, you will not be able to recover this record!",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
                })
                .then((willDelete) => {
                  if (willDelete) {

                        
                         jQuery.ajax({
                            url: APP_URL+'/user/delete-in-indoor-donor',
                            method: 'post',
                            data:{id:id},
                            success: function(result){
                               $('#loadingImg').hide();
                               var result = JSON.parse(result);
                               if(result=="success"){
                                       location.reload(true);
                                }
                            },
                              error: function() {
                                alert('Error occurs!');
                             }
                        });



                    swal("Poof! Your  record has been deleted!", {
                      icon: "success",
                    });
                  } 
                  else {
                    swal("Your  record is safe!");
                  }
                });

                
                   
                  });
//active indoor donor start here...
     //Active Indoor Donor

 $(".btnIndoorDonorActive").click(function(){

               
                var element=$(this);
                var id = element.attr("id");
                
                 var APP_URL = $('meta[name="_base_url"]').attr('content');
                 swal({
                  title: "Are you sure?",
                  text: "Once Active, you will not be able to recover this  file!",
                  icon: "success",
                  buttons: true,
                  dangerMode: false,
                })
                .then((willDelete) => {
                  if (willDelete) {

                        
                         jQuery.ajax({
                            url: APP_URL+'/user/active-indoor-donor',
                            method: 'post',
                            data:{id:id},
                             beforeSend: function() {
                              $('#loadingImg').show();
                            },
                            success: function(result){
                               $('#loadingImg').hide();
                                var result = JSON.parse(result);
                               if(result=="success"){
                                       location.reload(true);
                                }
                                
                            },
                              error: function() {
                                alert('Error occurs!');
                             }
                        });



                    swal("Poof! This Donor Active Now!", {
                      icon: "success",
                    });
                  } 
                  else {
                    swal("Your  file is safe!");
                  }
                });
            })


//Deactive Indoor Donor...
  $(".btnIndoorDonorDeActive").click(function(){

               
                var element=$(this);
                var id = element.attr("id");
                
                 var APP_URL = $('meta[name="_base_url"]').attr('content');
                 swal({
                  title: "Are you sure?",
                  text: "Once Deactive, you will not be able to recover this  file!",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
                })
                .then((willDelete) => {
                  if (willDelete) {

                        
                         jQuery.ajax({
                            url: APP_URL+'/user/deactive-indoor-donor',
                            method: 'post',
                            data:{id:id},
                             beforeSend: function() {
                              $('#loadingImg').show();
                            },
                            success: function(result){
                               $('#loadingImg').hide();
                                var result = JSON.parse(result);
                               if(result=="success"){
                                       location.reload(true);
                                }
                                
                            },
                              error: function() {
                                alert('Error occurs!');
                             }
                        });



                    swal("Poof! This Donor Deactive Now !", {
                      icon: "success",
                    });
                  } 
                  else {
                    swal("Your  file is safe!");
                  }
                });
            })

//active indoor donor End here...

              },
                error: function() {
                  alert('Error occurs!');
               }
          });

        }else{

        $("#amountTable").show();
        $("#paginationLink").show();
        $("#searchAmountTable").hide();

        }

        

   });





 })