$(function (){

    $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });


    $("#editReceiptVoucher").click(function(e){

    	$(this).hide();
    	$("#updateReceiptVoucher").show();
      $("#updateMode").show();
      $("#editMode").hide();
    	$("#backReceiptVoucher").show();
    	$(':input').prop('readonly', false);
    	$("#printReceiptVoucher").hide();

    	e.preventDefault();
    });

    $(".backReceiptVoucher").click(function(){
    	location.reload();
    });

     $(".deleteReceiptVoucher").click(function(e){

               
                var element=$(this);
                var id = element.attr("id");
                var APP_URL = $('meta[name="_base_url"]').attr('content');

               
                
                 swal({
                  title: "আপনি কি নিশ্চিত?",
                  text: "মুছে ফেলা হলে, আপনি এই রেকর্ড পুনরুদ্ধার করতে পারবেন না!",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
                })
                .then((willDelete) => {
                  if (willDelete) {

                        
                         jQuery.ajax({
                            url: APP_URL+'/user/delete-receipt-voucher',
                            method: 'post',
                            data:{id:id},
                            success: function(result){
                             
                              
                                location.href = APP_URL+"/user/create-receipt-voucher";
                            },
                              error: function() {
                                alert('Error occurs!');
                             }
                        });



                    swal("প্রমাণ!  আপনার রেকর্ড মুছে ফেলা হয়েছে!!", {
                      icon: "success",
                    });
                  } else {
                    swal("আপনার রেকর্ড নিরাপদ!");
                  }
                });

                e.preventDefault();
            })
    
  




});