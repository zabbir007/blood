 $(function(){

  $.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});
  console.log('jabbir');  


//user Test Name check start here....
    $("[name='testName']").keyup(function(){

      var APP_URL = $('meta[name="_base_url"]').attr('content');

      

      var test=$(this).val();

      
  
         jQuery.ajax({
              url: APP_URL+'/user/check-test-name',
              method: 'post',
              data:{test:test},
              success: function(result){
                var checkEmail = JSON.parse(result);

                if(checkEmail=="Found"){
                   swal({
                        title: "This Test Name Allready Used",
                        icon: "warning",
                        dangerMode: true,
                      })

                   $("#btnTest").hide();
                }else{
                  $("#btnTest").show();
                }
                
              },
                error: function() {
                  alert('Error occurs!');
               }
          });
      });
//user Test Name check end here....


$(".btnAmountDelete").click(function(){

                var page=$("[name='page']").val();
                var element=$(this);
                var id = element.attr("id");
                console.log('ID: '+id);
                console.log('page: '+page);
                 var APP_URL = $('meta[name="_base_url"]').attr('content');
                 swal({
                  title: "Are you sure?",
                  text: "Once deleted, you will not be able to recover this  file!",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
                })
                .then((willDelete) => {
                  if (willDelete) {

                        
                         jQuery.ajax({
                            url: APP_URL+'/user/delete-amount',
                            method: 'post',
                            data:{id:id,page:page},
                             beforeSend: function() {
                              $('#loadingImg').show();
                            },
                            success: function(result){
                               $('#loadingImg').hide();
                                var result = JSON.parse(result);
                               if(result=="success"){
                                       location.reload(true);
                                }
                                
                            },
                              error: function() {
                                alert('Error occurs!');
                             }
                        });



                    swal("Poof! Your  file has been deleted!", {
                      icon: "success",
                    });
                  } 
                  else {
                    swal("Your  file is safe!");
                  }
                });
            })


//Search Area

   $("#searchAmountBox").keyup(function(){
        var searchText=$.trim($(this).val());
        
        var APP_URL = $('meta[name="_base_url"]').attr('content');
        console.log(searchText);
         console.log(APP_URL);
        $("#amountTable").hide();
        $("#paginationLink").hide();

        if($.trim(searchText)){

              jQuery.ajax({
              url: APP_URL+'/user/search-amount',
              method: 'post',
              data:{searchText:searchText},
               beforeSend: function() {
                $('#loadingImg').show();
              },
              success: function(result){
                 $('#loadingImg').hide();
                 $("#searchAmountTable").find("tr:gt(0)").remove();
                  //console.log(result);
                  var info = JSON.parse(result);

                  var data="";

                  
                   $.each(info, function (key, val) {
                  
                  
                    data+='<tr/><td style="border: 2px solid #F7F7F4; text-align: center;">'+val.testName+'</td><td style="border: 2px solid #F7F7F4; text-align: center;">'+val.testAmount+'</td><td style="border: 2px solid #F7F7F4; text-align: center;"><a href="'+APP_URL+'/user/edit-amount/'+val.id+'/1'+'" class="action-icon"><i class="mdi mdi-square-edit-outline"></i></a><a id="'+val.id+'" class="action-icon btnAmountSearchDelete"><i class="mdi mdi-delete"></i></a></td></tr/>';
                   
                 

                
                   
              });

                   
                  $("#searchAmountTable").show().append(data).on('click','.btnAmountSearchDelete', function () {


                    var element=$(this);
                    var id = element.attr("id");
                 var APP_URL = $('meta[name="_base_url"]').attr('content');
                
                 swal({
                  title: "Are you sure?",
                  text: "Once deleted, you will not be able to recover this record!",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
                })
                .then((willDelete) => {
                  if (willDelete) {

                        
                         jQuery.ajax({
                            url: APP_URL+'/user/delete-amount',
                            method: 'post',
                            data:{id:id},
                            success: function(result){
                               $('#loadingImg').hide();
                               var result = JSON.parse(result);
                               if(result=="success"){
                                       location.reload(true);
                                }
                            },
                              error: function() {
                                alert('Error occurs!');
                             }
                        });



                    swal("Poof! Your  record has been deleted!", {
                      icon: "success",
                    });
                  } 
                  else {
                    swal("Your  record is safe!");
                  }
                });

                
                   
                  });
              },
                error: function() {
                  alert('Error occurs!');
               }
          });

        }else{

        $("#amountTable").show();
        $("#paginationLink").show();
        $("#searchAmountTable").hide();

        }

        

   });





 })