$(function(){
        
$(".btnEquipmentDelete").click(function(){

                var page=$("[name='page']").val();
                var element=$(this);
                var id = element.attr("id");
                // console.log('ID: '+id);
                // console.log('page: '+page);
                 var APP_URL = $('meta[name="_base_url"]').attr('content');
                 swal({
                  title: "Are you sure?",
                  text: "Once deleted, you will not be able to recover this  file!",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
                })
                .then((willDelete) => {
                  if (willDelete) {

                        
                         jQuery.ajax({
                            url: APP_URL+'/user/delete-equipment',
                            method: 'post',
                            data:{id:id,page:page},
                             beforeSend: function() {
                              $('#loadingImg').show();
                            },
                            success: function(result){
                               $('#loadingImg').hide();
                               var result = JSON.parse(result);
                              if(result=="success"){
                                       location.reload(true);
                                }
                                
                            },
                              error: function() {
                                alert('Error occurs!');
                             }
                        });



                    // swal("Poof! Your  file has been deleted!", {
                    //   icon: "success",
                    // });
                  } 
                  // else {
                  //   swal("Your  file is safe!");
                  // }
                });
            })


$("#addAnotherNewItem").click(function(e){
   var APP_URL = $('meta[name="_base_url"]').attr('content');
          
         jQuery.ajax({
            url: APP_URL+'/user/get-item-name',
            method: 'post',
            beforeSend: function() {
            
            },
            success: function(result){
               var leag = JSON.parse(result);
               var options='<select class="form-control parsley-validated" name="itemName[]" data-style="btn-secondary">';
                $.each(leag, function (key, val) {
                 options +='<option value='+val.itemName+'>'+val.itemName+'</option>';
              });
              options +='</select>';
           
    var quantity='<input id="quantity" required  data-parsley-required-message="Please Enter Quantity"  data-style="btn-secondary" style="border:0;margin-bottom: 0px; border-bottom: 1px  none; width:100%;margin-left:none;" type="text" name="quantity[]">';
    var rate='<input  required  data-parsley-required-message="Please Enter Rate"  data-style="btn-secondary" style="border:0;margin-bottom: 0px; border-bottom: 1px  none; width:100%;margin-left:none;" type="text" name="rate[]">';
    var amount='<input id="rate" class="rate" required  data-parsley-required-message="Please Enter Amount"  data-style="btn-secondary" style="border:0;margin-bottom: 0px; border-bottom: 1px  none; width:100%;margin-left:none;" type="text" name="amount[]">';
    var add = "<tr><td>"+ options +"</td><td>" + quantity + "</td><td>" + rate + "</td><td>"+ amount +"</td></tr>";
    $("table tbody").append(add);  
    
   $('.rate').keyup(function () {
 
    // initialize the sum (total price) to zero
    var sum = 0;
   
    $('.rate').each(function() {
        sum += Number($(this).val());
        console.log(sum);
    });
     
    // set the computed value to 'totalPrice' textbox
    $('#totalAmount').val(sum);
     
});

}
});   

 }); 





$('.rate').keyup(function () {
 
    // initialize the sum (total price) to zero
    var sum = 0;
   
    $('.rate').each(function() {
        sum += Number($(this).val());
        console.log(sum);
    });
     
    // set the computed value to 'totalPrice' textbox
    $('#totalAmount').val(sum);

});

 })