 $(function(){

  $.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});
  console.log('jabbir'); 

//user region check start here....
    $("[name='regionName']").keyup(function(){

      var APP_URL = $('meta[name="_base_url"]').attr('content');

      

      var region=$(this).val();

      
  
         jQuery.ajax({
              url: APP_URL+'/admin/check-region',
              method: 'post',
              data:{region:region},
              success: function(result){
                var checkEmail = JSON.parse(result);

                if(checkEmail=="Found"){
                   swal({
                        title: "This Region Allready Used",
                        icon: "warning",
                        dangerMode: true,
                      })

                   $("#btnRegionSave").hide();
                }else{
                  $("#btnRegionSave").show();
                }
                
              },
                error: function() {
                  alert('Error occurs!');
               }
          });
      });
//user region check end here....




$(".btnRegionDelete").click(function(){

                var page=$("[name='page']").val();
                var element=$(this);
                var id = element.attr("id");
                console.log('ID: '+id);
                console.log('page: '+page);
                 var APP_URL = $('meta[name="_base_url"]').attr('content');
                 swal({
                  title: "Are you sure?",
                  text: "Once deleted, you will not be able to recover this  file!",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
                })
                .then((willDelete) => {
                  if (willDelete) {

                        
                         jQuery.ajax({
                            url: APP_URL+'/admin/delete-region',
                            method: 'post',
                            data:{id:id,page:page},
                             beforeSend: function() {
                              $('#loadingImg').show();
                            },
                            success: function(result){
                               $('#loadingImg').hide();
                                var result = JSON.parse(result);
                               if(result=="success"){
                                       location.reload(true);
                                }
                                
                            },
                              error: function() {
                                alert('Error occurs!');
                             }
                        });



                    swal("Poof! Your  file has been deleted!", {
                      icon: "success",
                    });
                  } 
                  else {
                    swal("Your  file is safe!");
                  }
                });
            })


//Search Area

   $("#searchRegionBox").keyup(function(){
        var searchText=$.trim($(this).val());
        
        var APP_URL = $('meta[name="_base_url"]').attr('content');
        console.log(searchText);
         console.log(APP_URL);
        $("#regionTable").hide();
        $("#paginationLink").hide();

        if($.trim(searchText)){

              jQuery.ajax({
              url: APP_URL+'/admin/search-region',
              method: 'post',
              data:{searchText:searchText},
               beforeSend: function() {
                $('#loadingImg').show();
              },
              success: function(result){
                 $('#loadingImg').hide();
                 $("#searchRegionTable").find("tr:gt(0)").remove();
                  //console.log(result);
                  var info = JSON.parse(result);

                  var data="";

                  
                   $.each(info, function (key, val) {
                  
                  
                    data+='<tr/><td style="border: 2px solid #F7F7F4; text-align: center;">'+val.regionName+'</td><td style="border: 2px solid #F7F7F4; text-align: center;"><a href="'+APP_URL+'/admin/edit-region/'+val.id+'/1'+'" class="action-icon"><i class="mdi mdi-square-edit-outline"></i></a><a id="'+val.id+'" class="action-icon btnRegionSearchDelete"><i class="mdi mdi-delete"></i></a></td></tr/>';
                   
                 

                
                   
              });

                   
                  $("#searchRegionTable").show().append(data).on('click','.btnRegionSearchDelete', function () {


                    var element=$(this);
                    var id = element.attr("id");
                 var APP_URL = $('meta[name="_base_url"]').attr('content');
                
                 swal({
                  title: "Are you sure?",
                  text: "Once deleted, you will not be able to recover this record!",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
                })
                .then((willDelete) => {
                  if (willDelete) {

                        
                         jQuery.ajax({
                            url: APP_URL+'/admin/delete-region',
                            method: 'post',
                            data:{id:id},
                            success: function(result){
                               $('#loadingImg').hide();
                               var result = JSON.parse(result);
                               if(result=="success"){
                                       location.reload(true);
                                }
                            },
                              error: function() {
                                alert('Error occurs!');
                             }
                        });



                    swal("Poof! Your  record has been deleted!", {
                      icon: "success",
                    });
                  } 
                  else {
                    swal("Your  record is safe!");
                  }
                });

                
                   
                  });
              },
                error: function() {
                  alert('Error occurs!');
               }
          });

        }else{

        $("#regionTable").show();
        $("#paginationLink").show();
        $("#searchRegionTable").hide();

        }

        

   });





 })