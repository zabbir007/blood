$(function (){

    $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });


    $("#editIndoorDonor").click(function(e){

    	$(this).hide();
    	$("#updateIndoorDonor").show();
      $("#updateMode").show();
      $("#showSex2").show();
      $("#showSex3").show();
      $("#editMode").hide();
      $("#showSex1").hide();
    	$("#backIndoorDonor").show();
    	$(':input').prop('readonly', false);
    	$("#printIndoorDonor").hide();

    	e.preventDefault();
    });

    $(".backIndoorDonor").click(function(){
    	location.reload();
    });

     $(".deleteIndoorDonor").click(function(e){

               
                var element=$(this);
                var id = element.attr("id");
                var APP_URL = $('meta[name="_base_url"]').attr('content');

               
                
                 swal({
                  title: "আপনি কি নিশ্চিত?",
                  text: "মুছে ফেলা হলে, আপনি এই রেকর্ড পুনরুদ্ধার করতে পারবেন না!",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
                })
                .then((willDelete) => {
                  if (willDelete) {

                        
                         jQuery.ajax({
                            url: APP_URL+'/user/delete-indoor-donor',
                            method: 'post',
                            data:{id:id},
                            success: function(result){
                             
                              
                                location.href = APP_URL+"/user/create-indoor-donor";
                            },
                              error: function() {
                                alert('Error occurs!');
                             }
                        });



                    swal("প্রমাণ!  আপনার রেকর্ড মুছে ফেলা হয়েছে!!", {
                      icon: "success",
                    });
                  } else {
                    swal("আপনার রেকর্ড নিরাপদ!");
                  }
                });

                e.preventDefault();
            })
    
  
//Test cost count start here...
   $(".my-activity").click(function(event) {
       var APP_URL = $('meta[name="_base_url"]').attr('content');
        var total = 0;
        $(".my-activity:checked").each(function() {
            
            $("#testAmountTable").show();
             var id= parseInt($(this).val());
             jQuery.ajax({
                        url: APP_URL+'/user/get-test-amount',
                        method: 'post',
                        data:{id:id},
                        success: function(result){
                         
                          var val = JSON.parse(result);
                          testAmount=parseInt(val.testAmount);
                          // console.log(testAmount);
                         total += testAmount;
                          console.log(total);
                         
                        },
                          error: function() {
                            alert('Error occurs!');
                         }
                    });
        });
        
        // if (total == 0) {
        //     $('#amount').val('');
        // } else {                
        //     $('#amount').val(total);
        // }

    });

//Test Cost count End Hete.....



});