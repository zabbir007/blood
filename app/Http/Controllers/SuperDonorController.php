<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Mail;
use Session;
if(!isset($_SESSION)) 
{ 
    session_start(); 
} 
class SuperDonorController extends Controller
{
   
   public function __construct()
    {
        $this->middleware('checkDonorLogin');
    }
   public function donorDashboard()
    {
    	return view('donor.dashboard');
    }
   public function donorLogOut()
    {
        //Session::flush(); //destroy all session
        Session::put('name','');
        Session::put('donorId','');
        return redirect()->route('donorLogin');
    }

}
