<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Mail;
use Session;
use File;

if (!isset($_SESSION)) {
    session_start();
}
class PatientController extends Controller
{
   

   public function __construct()
    {
        $this->middleware('checkPatient');
    }
    public function patientRegister()
    {
        return view('patient.patientRegister');
    }
  //patient register start here..
     public function savePatient(Request $request)
    {

            $data=array();
            $data['name']=$request->name;
            $data['email']=$request->email;
            $data['phone']=$request->phone;
            $data['password']=md5($request->password);
            $data['branchId']=$request->branchId;
            $image=$request->file('image');

            // $checkEmailAndPhone=DB::table('merchants')
            //          ->get();
            // foreach($checkEmailAndPhone as $checkEP){
            //     //check phone number..
            //     if ($checkEP->phone == $request->merchantPhone) {
            //         Session::put('messageWarning','This Phone Number Allready Used!!');
            //         return redirect()->back();

            //     }
            //     //check email
            //     if ($checkEP->email ==$request->merchantEmail) {
            //         Session::put('messageWarning','This Email Allready Used!!');
            //         return redirect()->back();
            //     }
            // }
            // if ($request->merchantPassword!=$request->confirmMerchantPassword) {
            //     Session::put('messageWarning','Confirm Password not match!!');
            //     return redirect()->back(); 
            // }
        
            if ($image) {
            $image_name=str_random(20);
            $ext=strtolower($image->getClientOriginalExtension());

            $permited  = array('jpg', 'jpeg', 'png', 'pdf');
            $fileSize = $request->file('image')->getClientSize();

            if($fileSize>524288){
                 Session::put('messageWarning','Please upload file less than 512 KB');
                  return redirect()->back();
            }elseif (in_array($ext, $permited) === false){

                Session::put('messageWarning','You can only upload '.implode(', ', $permited));
                  return redirect()->back();
            }

            $image_full_name=$image_name.'.'.$ext;
            $upload_path='patient/img/';
            $image_url=$upload_path.$image_full_name;
            $success=$image->move($upload_path,$image_full_name);
            if ($success) {
                
                $data['image']=$image_url;
                 
                $patientRegister=DB::table('patient')
                    ->insert($data); 
                if ($patientRegister) {
                    Session::put('message','Registration Done successfully!!');
                    return redirect()->route('patientRegister');
                }
                else{
                     Session::put('messageWarning','Registration Failed!!');
                    return redirect()->route('patientRegister');
                }
                   
                              
        
            }
          }else{

               
                $patientRegister=DB::table('patient')
                    ->insert($data); 
                if ($patientRegister) {
                    Session::put('message','Registration Done successfully!!');
                    return redirect()->route('patientRegister');
                }
                else{
                     Session::put('messageWarning','Registration Failed!!');
                    return redirect()->route('patientRegister');
                }
          }
                
        }
  //patient register end here....

  //patient login..
    public function patientLogin()
    {
        return view('patient.patientLogin');
    }
   //patient sign in
     public function patientSignIn(Request $request){
        // dd($request);
        // exit();

        $validate_array['email'] =  'required';
        $customMessages['email.required']  ='Please Enter Your Email';
        $validate_array['password'] =  'required|min:8|max:16';
        $customMessages['password.required']  ='Please Enter Your Password';
        $customMessages['password.min']  ='Password must be between 8 to 16 character';
        $customMessages['password.max']  ='Password must be between 8 to 16 character';


        $this->validate($request, $validate_array, $customMessages);

        
        $email=$request->email;
        $password=md5($request->password);
        


        $result = DB::table('patient')
                    ->where('email', $email)
                    ->where('password', $password)
                    ->first();

                    if ($result) {
                        
                        Session::put('patientId',$result->id);
                        Session::put('patientName',$result->name);
                        
                        return redirect()->route('patientDashboard');
                    }else{
                        Session::put('message','Email Or Password Invalid');
                        return redirect()->route('patientSignIn');
                    }

    } 

}
