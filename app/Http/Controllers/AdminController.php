<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Mail;
use Session;
use File;

if (!isset($_SESSION)) {
    session_start();
}
class AdminController extends Controller
{
    
    public function __construct()
    {
         
        $this->middleware('checkAdminLogin');
    } 
   
    public function login()
    {

        return view('admin.login');
    }
   public function adminSignIn(Request $request){
    	// dd($request);
    	// exit();

        $validate_array['email'] =  'required';
        $customMessages['email.required']  ='Please Enter Your Email';
        $validate_array['password'] =  'required|min:8|max:16';
        $customMessages['password.required']  ='Please Enter Your Password';
        $customMessages['password.min']  ='Password must be between 8 to 16 character';
        $customMessages['password.max']  ='Password must be between 8 to 16 character';


        $this->validate($request, $validate_array, $customMessages);

    	
    	$email=$request->email;
    	$password=md5($request->password);
    	


		$result = DB::table('admin')
					->where('email', $email)
					->where('password', $password)
					->first();

					if ($result) {
						Session::put('email',$result->email);
						Session::put('adminId',$result->id);
						
						return redirect()->route('adminDashboard');
					}else{
						Session::put('message','Email Or Password Invalid');
						return redirect()->route('adminLogin');
					}

    }
}
