<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Mail;
use Session;
if(!isset($_SESSION)) 
{ 
    session_start(); 
} 
class SuperPatientController extends Controller
{
   public function __construct()
    {
        $this->middleware('checkPatientLogin');
    }
   public function patientDashboard()
    {
    	return view('patient.dashboard');
    }
   public function patientLogOut()
    {
        
        Session::put('name','');
        Session::put('patientId','');
        return redirect()->route('patientLogin');
    }
}
