<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\BarcodeGenerator;
use App\Http\Controllers\BarcodeGeneratorPNG;
use DB;
use Mail;
use Session;
use File;


if (!isset($_SESSION)) {
    session_start();
}
class SuperUserController extends Controller
{
   public function __construct()
    {
       //call user message or mail function.
        $this->UserMessageOrEmail();
      //end call user or email function  
        $this->middleware('checkUser');
    }
  //send user messeage start here...
  public function UserMessageOrEmail(){
    $userMessageInfo=DB::table('patients')
                      ->get();
    $curdate=strtotime(date('Y-m-d'));
    foreach ($userMessageInfo as $userMessagedate){

      if ($userMessagedate->messageDate!='') {
       
        $userMessagedateCheck=strtotime($userMessagedate->messageDate);
        if ($curdate=$userMessagedateCheck) {
           // $id= $userMessagedate->id;
           // echo $id;
        }
      }
   
    }
  }
  //send user message end here....
   public function dashboard()
    {
     
     $activeIndoorDonorInfo = DB::table('indoor_donor as ind')
                      ->where('ind.status','=','1')
                      ->count();
     $activeOutdoorDonorInfo = DB::table('outdoor_donor as out')
                      ->where('out.status','=','1')
                      ->count();
      $activeNormalPatientInfo = DB::table('patients as pat')
                      ->where('pat.status','=','1')
                      ->where('pat.type','=','1')
                      ->count();
      $activeThalassemiaPatientInfo = DB::table('patients as pat')
                      ->where('pat.status','=','1')
                      ->where('pat.type','=','2')
                      ->count();
      $indoorDonorPendingReport=DB::table('store_test as sto')
                      ->where('sto.testReport','!=','')
                      ->count();
      $outdoorDonorPendingReport=DB::table('store_outdoor_test as sto_out')
                      ->where('sto_out.testReport','!=','')
                      ->count();
      $pendingTestReport=DB::table('all_test as all')
                      ->where('all.testReport','!=','')
                      ->count();
    	return view('user.dashboard',compact('activeIndoorDonorInfo','activeOutdoorDonorInfo','activeNormalPatientInfo','activeThalassemiaPatientInfo','indoorDonorPendingReport','outdoorDonorPendingReport','pendingTestReport'));
    }

  public function totalBloodReservation(){

     $userId=Session::get('userId');
     $receiptOnInfo = DB::table('receipt_voucher as rec')
                      ->where('rec.group','=','on')
                      ->where('rec.userId',$userId)
                      ->count();
      $issueOnInfo = DB::table('issue_voucher as iss')
                      ->where('iss.group','=','on')
                      ->where('iss.userId',$userId)
                      ->count();
      $countOn=($receiptOnInfo-$issueOnInfo);



      $receiptOpInfo = DB::table('receipt_voucher as rec')
                      ->where('rec.group','=','op')
                      ->where('rec.userId',$userId)
                      ->count();
      $issueOpInfo = DB::table('issue_voucher as iss')
                      ->where('iss.group','=','op')
                      ->where('iss.userId',$userId)
                      ->count();
      $countOp=($receiptOpInfo-$issueOpInfo);


      $receiptabpInfo = DB::table('receipt_voucher as rec')
                      ->where('rec.group','=','abp')
                      ->where('rec.userId',$userId)
                      ->count();
      $issueabpInfo = DB::table('issue_voucher as iss')
                      ->where('iss.group','=','abp')
                      ->where('iss.userId',$userId)
                      ->count();
      $countabp=($receiptabpInfo-$issueabpInfo);

      $receiptabnInfo = DB::table('receipt_voucher as rec')
                      ->where('rec.group','=','abn')
                      ->where('rec.userId',$userId)
                      ->count();
      $issueabnInfo = DB::table('issue_voucher as iss')
                      ->where('iss.group','=','abn')
                      ->where('iss.userId',$userId)
                      ->count();
      $countabn=($receiptabnInfo-$issueabnInfo);

      $receiptapInfo = DB::table('receipt_voucher as rec')
                      ->where('rec.group','=','ap')
                      ->where('rec.userId',$userId)
                      ->count();
      $issueapInfo = DB::table('issue_voucher as iss')
                      ->where('iss.group','=','ap')
                      ->where('iss.userId',$userId)
                      ->count();
      $countap=($receiptapInfo-$issueapInfo);

      $receiptanInfo = DB::table('receipt_voucher as rec')
                      ->where('rec.group','=','an')
                      ->where('rec.userId',$userId)
                      ->count();
      $issueanInfo = DB::table('issue_voucher as iss')
                      ->where('iss.group','=','an')
                      ->where('iss.userId',$userId)
                      ->count();
      $countan=($receiptanInfo-$issueanInfo);

      $receiptbpInfo = DB::table('receipt_voucher as rec')
                      ->where('rec.group','=','bp')
                      ->where('rec.userId',$userId)
                      ->count();
      $issuebpInfo = DB::table('issue_voucher as iss')
                      ->where('iss.group','=','bp')
                      ->where('iss.userId',$userId)
                      ->count();
      $countbp=($receiptbpInfo-$issuebpInfo);

      $receiptbnInfo = DB::table('receipt_voucher as rec')
                      ->where('rec.group','=','bn')
                      ->where('rec.userId',$userId)
                      ->count();
      $issuebnInfo = DB::table('issue_voucher as iss')
                      ->where('iss.group','=','bn')
                      ->where('iss.userId',$userId)
                      ->count();
      $countbn=($receiptbnInfo-$issuebnInfo);

    return view('user.totalBloodReservation',compact('countOn','countOp','countabp','countabn','countap','countan','countbp','countbn'));
      
  }

  //show profile class start here...
  public function showAccount()
    {
        $userId = Session::get('userId');
        $userInfo = DB::table('users as use')
                    ->where('id', $userId)
                    ->first();
        
        return view('user.profile', compact('userInfo'));
    }
//show profile class end here...   


   public function userLogOut()
    {
        //Session::flush(); //destroy all session
        Session::put('email','');
        Session::put('userId','');
        Session::put('userImage','');
        Session::put('userName','');
        return redirect()->route('login');
    }
  //donor request..
    public function donorRequest(){
        return view('user.donorRequest');
    }
  //Add amount
     public function showAmount()
        {
         
            $showAmount=DB::table('test_amount')
                 ->orderBy('test_amount.testName','ASC')
                 ->paginate(5);
            return view('user.showAmount',compact('showAmount'));
        }
    public function createAmount()
    {
        
        return view('user.createAmount');
    }
   public function saveAmount( Request $request)
    {
        $data=array();
        $data['testName']=$request->testName;
        $data['testAmount']=$request->testAmount;
        $result=DB::table('test_amount')->insert($data);
        if ($result) {
          Session::put('message','Amount Save successfully!!');

          return redirect()->back();
          
        }else{
            Session::put('message','Amount Save Failed !!!');
            return redirect()->back();
        }
       
    }

  public function checkTestName(Request $request){

        $userTest=$request->test;

        $userTestCheck=DB::table('test_amount')
                                ->where('testName',$userTest)
                                ->select('test_amount.testName')
                                ->first();

  

        if(isset($userTestCheck->testName)){
                 
                echo json_encode("Found");
        }else{
            echo json_encode("Not Found");
        }
    }
  
  public function editAmount($id,$page)
    {
        
         $singleAmountInfo = DB::table('test_amount')
                            ->Where('id', $id)
                            ->first();
         return view('user.editAmount',compact('singleAmountInfo','page'));
        
    } 
  public function updateAmount(Request $request)
    {
        
            $id=$request->id;
           $page=$request->page;


         $data=array();
         $data['testName']=$request->testName;
         $data['testAmount']=$request->testAmount;
        

         DB::table('test_amount')
            ->where('id',$id)
            ->update($data);

                           

        Session::put('message','Amount Update successfully!!');

        return redirect('user/show-amount?page='.$page);
    }
    public function serachAmount(Request $request){

        $serch=$request->searchText;

         $amountInfo=DB::table('test_amount')
                    ->Where('testName', 'like', '%' .$serch. '%')
                    ->OrWhere('testAmount', 'like', '%' .$serch. '%')
                    ->select('testName','testAmount','id')
                    ->orderBy('testName','ASC')
                    ->limit(10)
                    ->get();

         
        echo json_encode($amountInfo);
       
    }
   public function deleteAmount(Request $request)
    {
       
        $id=intval($request->id);
        $result=DB::table('test_amount')
              ->where('id', $id)
              ->delete();
           if ($result) {
                        echo json_encode('success');
                        exit();
                    }
    }

//add indoor donor
   public function createIndoorDonor()
    {
        
        return view('user.createIndoorDonor');
    } 
     public function addIndoorDonor(Request $request)
    {
      $orderNumberGenerate= mt_rand(10000000, 99999999);
      $generatorPNG = new BarcodeGeneratorPNG();
        file_put_contents('barCodeImg/'.$orderNumberGenerate.'.png', $generatorPNG->getBarcode($orderNumberGenerate, $generatorPNG::TYPE_CODE_128));
      $barCodeImg=$orderNumberGenerate.'.png';
      
      $data=array();
      $data['name']=$request->name;
      $data['userId']=Session::get('userId');
      $data['fatherOrHusband']=$request->fatherOrHusband;
      $data['age']=$request->age;
      $data['sex']=$request->sex;
      $data['education']=$request->education;
      $data['address1']=$request->address1;
      $data['address2']=$request->address2;
      $data['phone']=$request->phone;
      $data['address3']=$request->address3;
      $data['address4']=$request->address4;
      $data['phone2']=$request->phone2;
      $data['description']=$request->description;
      $data['bloodGroup']=$request->bloodGroup;
      $data['date']=$request->date;
      $data['barCodeImage']='barCodeImg/'.$barCodeImg;
      $data['nibondonNo']=$orderNumberGenerate;
      $id=DB::table('indoor_donor')
                         ->insertGetId($data);


       Session::put('message','Donor Save successfully !!');

      return redirect()->route('showIndoorDonor',$id);

    } 

  public function showIndoorDonor($id){

         $indoorDonorInfo=DB::table('indoor_donor')
                          ->where('id',$id)
                          ->first();

        return view('user.showIndoorDonor',compact('indoorDonorInfo'));
     }
     public function showSearchIndoorDonor($id){

         $indoorDonorInfo=DB::table('indoor_donor')
                          ->where('nibondonNo',$id)
                          ->OrWhere('phone',$id)
                          ->OrWhere('phone2',$id)
                          ->first();

        return view('user.showIndoorDonor',compact('indoorDonorInfo'));
     }
   public function updateIndoorDonor(Request $request){
      $id=$request->id;
     $outdoorDOnorInfo = DB::table('indoor_donor')
                            ->where('id', $id)
                            ->first();
      $data=array();
      $data['name']=$request->name;
      $data['fatherOrHusband']=$request->fatherOrHusband;
      $data['age']=$request->age;
     if($request->sex !=null){
            $data['sex']=$request->sex;
        }else{
           
            $data['sex']=$outdoorDOnorInfo->sex;
        }
      $data['phone']=$request->phone;
      if($request->bloodGroup !=null){
            $data['bloodGroup']=$request->bloodGroup;
        }else{
           
            $data['bloodGroup']=$outdoorDOnorInfo->bloodGroup;
        }
      $data['userId']=Session::get('userId');
      $data['education']=$request->education;
      $data['address1']=$request->address1;
      $data['address2']=$request->address2;
      $data['address3']=$request->address3;
      $data['address4']=$request->address4;
      $data['phone2']=$request->phone2;
      $data['description']=$request->description;
      $data['date']=$request->date;
      DB::table('indoor_donor')
                ->where('id',$id)
                         ->update($data);


       
      Session::put('message','Donor Update Successfully !!');
      return redirect()->route('showIndoorDonor',$id);
   }
  public function searchIndoorDonor(Request $request){
         $id=$request->id;
        $showIndoorDonorInfo=DB::table('indoor_donor')
                        ->where('nibondonNo',$id)
                        ->OrWhere('phone',$id)
                        ->OrWhere('phone2',$id)
                        ->first();
        if($showIndoorDonorInfo){
            return redirect()->route('showSearchIndoorDonor',$id);
        }else{
            Session::put('messageWarning','Donor Not Found !');
            return redirect()->back();
        }
    }
  public function deleteIndoorDonor(Request $request){
         $id=$request->id;

         DB::table('indoor_donor')
                    ->where('id', $id)
                    ->delete();
    }




//add indoor donor English

   public function createIndoorDonorEnglish()
    {
        
        return view('user.createIndoorDonorEnglish');
    } 
     public function addIndoorDonorEnglish(Request $request)
    {
      $orderNumberGenerate= mt_rand(10000000, 99999999);
      $generatorPNG = new BarcodeGeneratorPNG();
        file_put_contents('barCodeImg/'.$orderNumberGenerate.'.png', $generatorPNG->getBarcode($orderNumberGenerate, $generatorPNG::TYPE_CODE_128));
      $barCodeImg=$orderNumberGenerate.'.png';
      
      $data=array();
      $data['name']=$request->name;
      $data['userId']=Session::get('userId');
      $data['fatherOrHusband']=$request->fatherOrHusband;
      $data['age']=$request->age;
      $data['sex']=$request->sex;
      $data['education']=$request->education;
      $data['address1']=$request->address1;
      $data['address2']=$request->address2;
      $data['phone']=$request->phone;
      $data['address3']=$request->address3;
      $data['address4']=$request->address4;
      $data['phone2']=$request->phone2;
      $data['description']=$request->description;
      $data['bloodGroup']=$request->bloodGroup;
      $data['date']=$request->date;
      $data['barCodeImage']='barCodeImg/'.$barCodeImg;
      $data['nibondonNo']=$orderNumberGenerate;
      $id=DB::table('indoor_donor')
                         ->insertGetId($data);


       Session::put('message','Donor Save successfully !!');

      return redirect()->route('showIndoorDonorEnglish',$id);

    } 

  public function showIndoorDonorEnglish($id){

         $indoorDonorInfo=DB::table('indoor_donor')
                          ->where('id',$id)
                          ->first();

        return view('user.showIndoorDonorEnglish',compact('indoorDonorInfo'));
     }
     public function showSearchIndoorDonorEnglish($id){

         $indoorDonorInfo=DB::table('indoor_donor')
                          ->where('nibondonNo',$id)
                          ->OrWhere('phone',$id)
                          ->OrWhere('phone2',$id)
                          ->first();

        return view('user.showIndoorDonorEnglish',compact('indoorDonorInfo'));
     }
   public function updateIndoorDonorEnglish(Request $request){
      $id=$request->id;
     $outdoorDOnorInfo = DB::table('indoor_donor')
                            ->where('id', $id)
                            ->first();
      $data=array();
      $data['name']=$request->name;
      $data['fatherOrHusband']=$request->fatherOrHusband;
      $data['age']=$request->age;
     if($request->sex !=null){
            $data['sex']=$request->sex;
        }else{
           
            $data['sex']=$outdoorDOnorInfo->sex;
        }
      $data['phone']=$request->phone;
      if($request->bloodGroup !=null){
            $data['bloodGroup']=$request->bloodGroup;
        }else{
           
            $data['bloodGroup']=$outdoorDOnorInfo->bloodGroup;
        }
      $data['userId']=Session::get('userId');
      $data['education']=$request->education;
      $data['address1']=$request->address1;
      $data['address2']=$request->address2;
      $data['address3']=$request->address3;
      $data['address4']=$request->address4;
      $data['phone2']=$request->phone2;
      $data['description']=$request->description;
      $data['date']=$request->date;
      DB::table('indoor_donor')
                ->where('id',$id)
                         ->update($data);


       
      Session::put('message','Donor Update Successfully !!');
      return redirect()->route('showIndoorDonorEnglish',$id);
   }
  public function searchIndoorDonorEnglish(Request $request){
         $id=$request->id;
        $showIndoorDonorInfo=DB::table('indoor_donor')
                        ->where('nibondonNo',$id)
                        ->OrWhere('phone',$id)
                        ->OrWhere('phone2',$id)
                        ->first();
        if($showIndoorDonorInfo){
            return redirect()->route('showSearchIndoorDonorEnglish',$id);
        }else{
            Session::put('messageWarning','Donor Not Found !');
            return redirect()->back();
        }
    }
  public function deleteIndoorDonorEnglish(Request $request){
         $id=$request->id;

         DB::table('indoor_donor')
                    ->where('id', $id)
                    ->delete();
    }



//Add Outdoor Donor...
     public function createOutdoorDonor()
    {
        
        return view('user.createOutdoorDonor');
    } 
     public function addOutdoorDonor(Request $request)
    {
      $orderNumberGenerate= mt_rand(10000000, 99999999);
      $generatorPNG = new BarcodeGeneratorPNG();
        file_put_contents('barCodeImg/'.$orderNumberGenerate.'.png', $generatorPNG->getBarcode($orderNumberGenerate, $generatorPNG::TYPE_CODE_128));
      $barCodeImg=$orderNumberGenerate.'.png';

      $data=array();
      $data['name']=$request->name;
      $data['age']=$request->age;
      $data['sex']=$request->sex;
      $data['phone']=$request->phone;
      $data['bloodGroup']=$request->bloodGroup;
      $data['date']=$request->date;
      $data['barCodeImage']='barCodeImg/'.$barCodeImg;
      $data['nibondonNo']=$orderNumberGenerate;
      $id=DB::table('outdoor_donor')
                         ->insertGetId($data);


       Session::put('message','Donor Save successfully !!');

      return redirect()->route('showOutdoorDonor',$id);

    } 

  public function showOutdoorDonor($id){

         $outdoorDonorInfo=DB::table('outdoor_donor')
                          ->where('id',$id)
                          ->first();

        return view('user.showOutdoorDonor',compact('outdoorDonorInfo'));
     }
     public function showSearchOutdoorDonor($id){

         $outdoorDonorInfo=DB::table('outdoor_donor')
                          ->where('nibondonNo',$id)
                          ->orWhere('phone',$id)
                          ->first();

        return view('user.showOutdoorDonor',compact('outdoorDonorInfo'));
     }
   public function updateOutdoorDonor(Request $request){
      $id=$request->id;
      $outdoorDOnorInfo = DB::table('outdoor_donor')
                              ->where('id', $id)
                              ->first();
      $data=array();
      $data['name']=$request->name;
      $data['age']=$request->age;
      if($request->sex !=null){
            $data['sex']=$request->sex;
        }else{
           
            $data['sex']=$outdoorDOnorInfo->sex;
        }
      $data['phone']=$request->phone;
      if($request->bloodGroup !=null){
            $data['bloodGroup']=$request->bloodGroup;
        }else{
           
            $data['bloodGroup']=$outdoorDOnorInfo->bloodGroup;
        }
      $data['date']=$request->date;
      DB::table('outdoor_donor')
                ->where('id',$id)
                         ->update($data);


       
      Session::put('message','Donor Update Successfully !!');
      return redirect()->route('showOutdoorDonor',$id);
   }
  public function searchOutdoorDonor(Request $request){
         $id=$request->id;
        $showOutdoorDonorInfo=DB::table('outdoor_donor')
                        ->where('nibondonNo',$id)
                        ->orWhere('phone',$id)
                        ->first();
        if($showOutdoorDonorInfo){
            return redirect()->route('showSearchOutdoorDonor',$id);
        }else{
            Session::put('messageWarning','Donor Not Found !');
            return redirect()->back();
        }
    }
  public function deleteOutdoorDonor(Request $request){
         $id=$request->id;

         DB::table('outdoor_donor')
                    ->where('id', $id)
                    ->delete();
    }


//Show Outdoor Test .....
     public function showOutdoorTest($id){

         $outdoorDonorInfo=DB::table('outdoor_donor')
                          ->where('nibondonNo',$id)
                          ->first();
         $testAmount=DB::table('test_amount')
                          ->get();

        return view('user.showOutdoorTest',compact('outdoorDonorInfo','testAmount'));
     }

  public function getOutdoorTestAmount(Request $request)
    {
       
      

        $id=intval($request->id);
        $result=DB::table('test_amount')
              ->where('id', $id)
              ->first();
           if ($result) {
                        echo json_encode($result);
                       
                    }
    }
  public function saveOutdoorTestAmount(Request $request){

    $totalAmount=$request->totalAmount;
     $paidAmount=$request->paidAmount;
     if ($totalAmount<$paidAmount) {
      Session::put('messageWarning','Total Paid Amount Geater Than Total Amount !!');
      return redirect()->back();
     }

      $checkTestId=$request->test;
      if ($checkTestId=='') {
       Session::put('messageWarning','Please Select At-Least One Test !!');

      return redirect()->back();
      }

     $data=array();
     $data['testId']=implode(', ', $request->test);
     $data['donorReg']=$request->nibondonNo;
     $data['totalAmount']=$request->totalAmount;
     $data['paidAmount']=$request->paidAmount;
     $data['testDoctorName']='Not Test By Doctor';
     $id=DB::table('store_outdoor_test')
                         ->insertGetId($data);


       Session::put('message','Test Information Save Successfully !!');

       return redirect()->route('showOutdoorTestAmount',$id);

  }
 public function showOutdoorTestAmount($id){

   $testAmountInfo=DB::table('store_outdoor_test')
                  ->where('id',$id)
                  ->first();
   $totalAmount=$testAmountInfo->totalAmount;
   $paidAmount=$testAmountInfo->paidAmount;

   $outdoorDonorInfo=DB::table('outdoor_donor')
                          ->where('nibondonNo',$testAmountInfo->donorReg)
                          ->first();

        
    $amount=array();
    $amountApplication=  explode(',', $testAmountInfo->testId);

        foreach($amountApplication as $amountApp){
            $amount[]=$amountApp;

            // $testAmountInfo=DB::table('test_amount')
            //       ->where('id',$amountApp)
            //       ->get();
            // echo "<pre/>";
            // print_r($testAmountInfo);
           

        }

    return view('user.showOutdoorTestAmount',compact('outdoorDonorInfo','amount','testAmountInfo','totalAmount','paidAmount'));
   
 }


//Show Test .....
     public function showTest($id){

         $indoorDonorInfo=DB::table('indoor_donor')
                          ->where('nibondonNo',$id)
                          ->first();
         $testAmount=DB::table('test_amount')
                          ->get();

        return view('user.showTest',compact('indoorDonorInfo','testAmount'));
     }

  public function getTestAmount(Request $request)
    {
       
      

        $id=intval($request->id);
        $result=DB::table('test_amount')
              ->where('id', $id)
              ->first();
           if ($result) {
                        echo json_encode($result);
                       
                    }
    }
  public function saveTestAmount(Request $request){
     
     $totalAmount=$request->totalAmount;
     $paidAmount=$request->paidAmount;
     if ($totalAmount<$paidAmount) {
      Session::put('messageWarning','Total Paid Amount Geater Than Total Amount !!');
      return redirect()->back();
     }

      $checkTestId=$request->test;
      if ($checkTestId=='') {
       Session::put('messageWarning','Please Select At-Least One Test !!');

      return redirect()->back();
      }

     $data=array();
     $data['testId']=implode(', ', $request->test);
     $data['userId']=Session::get('userId');
     $data['donorReg']=$request->nibondonNo;
     $data['totalAmount']=$request->totalAmount;
     $data['paidAmount']=$request->paidAmount;
     $data['remainingAmount']=($request->totalAmount-$request->paidAmount);
     $data['testDoctorName']='Not Test By Doctor';
     $id=DB::table('store_test')
                         ->insertGetId($data);


       Session::put('message','Test Information Save Successfully !!');

       return redirect()->route('showTestAmount',$id);

  }
 public function showTestAmount($id){

   $testAmountInfo=DB::table('store_test')
                  ->where('id',$id)
                  ->first();
   $totalAmount=$testAmountInfo->totalAmount;
   $paidAmount=$testAmountInfo->paidAmount;

   $indoorDonorInfo=DB::table('indoor_donor')
                          ->where('nibondonNo',$testAmountInfo->donorReg)
                          ->first();

        
    $amount=array();
    $amountApplication=  explode(',', $testAmountInfo->testId);

        foreach($amountApplication as $amountApp){
            $amount[]=$amountApp;

            // $testAmountInfo=DB::table('test_amount')
            //       ->where('id',$amountApp)
            //       ->get();
            // echo "<pre/>";
            // print_r($testAmountInfo);
           

        }

    return view('user.showTestAmount',compact('indoorDonorInfo','amount','testAmountInfo','totalAmount','paidAmount'));
   
 }

//Indoor Donor Test Report ....

 public function showIndoorDonorTestReport()
 {
  
  $userId=Session::get('userId');
  $showTestReport=DB::table('store_test')
                  ->where('store_test.userId',$userId)
                  ->orderBy('store_test.id','DESC')
                  ->paginate(5);

  return view('user.showTestReport',compact('showTestReport'));               

 }

 //edit test report
 public function editIndoorDonorTestReport($id,$page){
  $testAmountInfo=DB::table('store_test')
                  ->where('id',$id)
                  ->first();
        
    $amount=array();
    $amountApplication=  explode(',', $testAmountInfo->testId);

        foreach($amountApplication as $amountApp){
            $amount[]=$amountApp;

            // $testAmountInfo=DB::table('test_amount')
            //       ->where('id',$amountApp)
            //       ->get();
            // echo "<pre/>";
            // print_r($testAmountInfo);
           

        }

    return view('user.editIndoorDonorTestReport',compact('amount','id'));
 }

 public function updateIndoorDonorTestReport(Request $request)
 {
    $data=array();
    $id=$request->id;
    $data['testDoctorName']=$request->doctorName;
    $data['testReport']=implode(', ', $request->report);
     DB::table('store_test as st')
            ->where('st.id',$id)
             ->update($data);
     Session::put('message','Update Successfully !!');

     return redirect()->back();
    
 }
 public function viewIndoorDonorTestReport($id)
 {
   
   $userId=Session::get('userId');
   $companyName=DB::table('company_details')
                    ->where('company_details.active',1)
                    ->orderBy('id','DESC')
                    ->first();
   $branchName=DB::table('users as use')
                    ->join('branch as bra','use.branchId','=','bra.id')
                    ->select('bra.branchName')
                    ->where('use.id',$userId)
                    ->first();
   $testAmountInfo=DB::table('store_test')
                  ->where('id',$id)
                  ->first();
    $doctorName= $testAmountInfo->testDoctorName;

   $indoorDonorInfo=DB::table('indoor_donor')
                          ->where('nibondonNo',$testAmountInfo->donorReg)
                          ->first();

        
    $amount=array();
    $amountApplication=  explode(',', $testAmountInfo->testId);
    $testReport=  explode(',', $testAmountInfo->testReport);
        foreach($amountApplication as $amountApp){
            $amount[]=$amountApp;

            // $testAmountInfo=DB::table('test_amount')
            //       ->where('id',$amountApp)
            //       ->get();
            // echo "<pre/>";
            // print_r($testAmountInfo);
           

        }

    return view('user.viewIndoorDonorTestReport',compact('indoorDonorInfo','amount','testAmountInfo','testReport','doctorName','companyName','branchName'));

 }
  public function searchIndoorDonorTestReport(Request $request){

        $serch=$request->searchText;

         $amountInfo=DB::table('store_test')
                    ->Where('donorReg', 'like', '%' .$serch. '%')
                    ->OrWhere('id', 'like', '%' .$serch. '%')
                     ->OrWhere('testDoctorName', 'like', '%' .$serch. '%')
                    ->select('donorReg','testDoctorName','id')
                    ->orderBy('donorReg','ASC')
                    ->limit(10)
                    ->get();

         
        echo json_encode($amountInfo);
       
    }

  public function deleteIndoorDonorTestReport(Request $request)
    {
       
        $id=intval($request->id);
        $result=DB::table('store_test')
              ->where('id', $id)
              ->delete();
           if ($result) {
                        echo json_encode('success');
                        exit();
                    }
    }


//Outdoor Donor Test Report ....

 public function showOutdoorDonorTestReport()
 {
  
  $showTestReport=DB::table('store_outdoor_test')
                  ->orderBy('store_outdoor_test.id','DESC')
                  ->paginate(5);

  return view('user.showOutdoorTestReport',compact('showTestReport'));               

 }


 public function editOutdoorDonorTestReport($id,$page){
  $testAmountInfo=DB::table('store_outdoor_test')
                  ->where('id',$id)
                  ->first();
        
    $amount=array();
    $amountApplication=  explode(',', $testAmountInfo->testId);

        foreach($amountApplication as $amountApp){
            $amount[]=$amountApp;

            // $testAmountInfo=DB::table('test_amount')
            //       ->where('id',$amountApp)
            //       ->get();
            // echo "<pre/>";
            // print_r($testAmountInfo);
           

        }

    return view('user.editOutdoorDonorTestReport',compact('amount','id'));
 }

 public function updateOutdoorDonorTestReport(Request $request)
 {
    $data=array();
    $id=$request->id;
    $data['testDoctorName']=$request->doctorName;
    $data['testReport']=implode(', ', $request->report);
     DB::table('store_outdoor_test as st')
            ->where('st.id',$id)
             ->update($data);
     Session::put('ss_message','Update Successfully !!');

     return redirect()->back();
    
 }
 public function viewOutdoorDonorTestReport($id)
 {
   
   $testAmountInfo=DB::table('store_outdoor_test')
                  ->where('id',$id)
                  ->first();
    $doctorName= $testAmountInfo->testDoctorName;

   $outdoorDonorInfo=DB::table('outdoor_donor')
                          ->where('nibondonNo',$testAmountInfo->donorReg)
                          ->first();

        
    $amount=array();
    $amountApplication=  explode(',', $testAmountInfo->testId);
    $testReport=  explode(',', $testAmountInfo->testReport);
        foreach($amountApplication as $amountApp){
            $amount[]=$amountApp;

            // $testAmountInfo=DB::table('test_amount')
            //       ->where('id',$amountApp)
            //       ->get();
            // echo "<pre/>";
            // print_r($testAmountInfo);
           

        }

    return view('user.viewOutdoorDonorTestReport',compact('outdoorDonorInfo','amount','testAmountInfo','testReport','doctorName'));

 }
  public function searchOutdoorDonorTestReport(Request $request){

        $serch=$request->searchText;

         $amountInfo=DB::table('store_outdoor_test')
                    ->Where('donorReg', 'like', '%' .$serch. '%')
                    ->OrWhere('id', 'like', '%' .$serch. '%')
                     ->OrWhere('testDoctorName', 'like', '%' .$serch. '%')
                    ->select('donorReg','testDoctorName','id')
                    ->orderBy('donorReg','ASC')
                    ->limit(10)
                    ->get();

         
        echo json_encode($amountInfo);
       
    }

  public function deleteOutdoorDonorTestReport(Request $request)
    {
       
        $id=intval($request->id);
        $result=DB::table('store_outdoor_test')
              ->where('id', $id)
              ->delete();
           if ($result) {
                        echo json_encode('success');
                        exit();
                    }
    }


//Show All Indoor Donor Start Here..
    public function showAllIndoorDonor()
    {
      $indoorDonorInfo=DB::table('indoor_donor')
                      ->paginate(5);
       return view('user.showAllIndoorDonor',compact('indoorDonorInfo'));
    }
   public function deleteInIndoorDonor(Request $request)
    {
       
        $id=intval($request->id);
        $result=DB::table('indoor_donor')
              ->where('id', $id)
              ->delete();
           if ($result) {
                        echo json_encode('success');
                        exit();
                    }
    }
  public function deactiveIndoorDonor(Request $request)
    {
       
        $id=intval($request->id);
        $data=array();
        $data['status']='0';
        $result=DB::table('indoor_donor as ind')
                ->where('ind.id',$id)
                 ->update($data);
           if ($result) {
                        echo json_encode('success');
                        exit();
                    }
    }
  public function activeIndoorDonor(Request $request)
    {
       
        $id=intval($request->id);
        $data=array();
        $data['status']='1';
        $result=DB::table('indoor_donor as ind')
                ->where('ind.id',$id)
                 ->update($data);
           if ($result) {
                        echo json_encode('success');
                        exit();
                    }
    }

  public function searchInIndoorDonor(Request $request){

        $serch=$request->searchText;

         $indoorInfo=DB::table('indoor_donor as idor')
                    ->Where('idor.name', 'like', '%' .$serch. '%')
                    ->OrWhere('idor.phone', 'like', '%' .$serch. '%')
                     ->OrWhere('idor.nibondonNo', 'like', '%' .$serch. '%')
                    ->select('idor.*')
                    ->limit(10)
                    ->get();

         
        echo json_encode($indoorInfo);
       
    }
//Show All Indoor Donor End Here..


//Show All Outdoor Donor Start Here..
    public function showAllOutdoorDonor()
    {
      $outdoorDonorInfo=DB::table('outdoor_donor')
                      ->paginate(5);
       return view('user.showAllOutdoorDonor',compact('outdoorDonorInfo'));
    }
   public function deleteInOutdoorDonor(Request $request)
    {
       
        $id=intval($request->id);
        $result=DB::table('outdoor_donor')
              ->where('id', $id)
              ->delete();
           if ($result) {
                        echo json_encode('success');
                        exit();
                    }
    }
  public function deactiveOutdoorDonor(Request $request)
    {
       
        $id=intval($request->id);
        $data=array();
        $data['status']='0';
        $result=DB::table('outdoor_donor as ind')
                ->where('ind.id',$id)
                 ->update($data);
           if ($result) {
                        echo json_encode('success');
                        exit();
                    }
    }
  public function activeOutdoorDonor(Request $request)
    {
       
        $id=intval($request->id);
        $data=array();
        $data['status']='1';
        $result=DB::table('outdoor_donor as ind')
                ->where('ind.id',$id)
                 ->update($data);
           if ($result) {
                        echo json_encode('success');
                        exit();
                    }
    }

  public function searchInOutdoorDonor(Request $request){

        $serch=$request->searchText;

         $indoorInfo=DB::table('outdoor_donor as idor')
                    ->Where('idor.name', 'like', '%' .$serch. '%')
                    ->OrWhere('idor.phone', 'like', '%' .$serch. '%')
                     ->OrWhere('idor.nibondonNo', 'like', '%' .$serch. '%')
                    ->select('idor.*')
                    ->limit(10)
                    ->get();

         
        echo json_encode($indoorInfo);
       
    }
//Show All Outdoor Donor End Here..


//All test start here...
    public function addTest()
    {

       $testAmount=DB::table('test_amount')
                          ->get();
       return view('user.addTestUser',compact('testAmount'));
    }
   public function saveTest(Request $request)
    {
      
     $totalAmount=$request->totalAmount;
     $paidAmount=$request->paidAmount;
     if ($totalAmount<$paidAmount) {
      Session::put('messageWarning','Total Paid Amount Geater Than Total Amount !!');
      return redirect()->back();
     }

      $checkTestId=$request->test;
      if ($checkTestId=='') {
       Session::put('messageWarning','Please Select At-Least One Test !!');

      return redirect()->back();
      }else{
       
      $orderNumberGenerate= mt_rand(10000000, 99999999);
      $generatorPNG = new BarcodeGeneratorPNG();
        file_put_contents('barCodeImg/'.$orderNumberGenerate.'.png', $generatorPNG->getBarcode($orderNumberGenerate, $generatorPNG::TYPE_CODE_128));
      $barCodeImg=$orderNumberGenerate.'.png';

      $data=array();
      $data['date']=$request->date;
      $data['name']=$request->name;
      $data['userId']=Session::get('userId');
      $data['age']=$request->age;
      $data['sex']=$request->sex;
      $data['group']=$request->group;
      $data['totalAmount']=$request->totalAmount;
      $data['paidAmount']=$request->paidAmount;
      $data['remainingAmount']=($request->totalAmount-$request->paidAmount);
      $data['referance']=$request->referance;
      $data['ward']=$request->ward;
      $data['bed']=$request->bed;
      $data['testId']=implode(', ', $request->test);
      $data['barCodeImage']='barCodeImg/'.$barCodeImg;
      $data['testNumber']=$orderNumberGenerate;
      $id=DB::table('all_test')
                         ->insertGetId($data);


       Session::put('message','Test Save successfully !!');

      return redirect()->route('showSaveTest',$id);
    }

    } 

  public function showSaveTest($id){

         $allTestInfo=DB::table('all_test')
                          ->where('id',$id)
                          ->first();
         $amount=array();
         $amountApplication=  explode(',', $allTestInfo->testId);

        foreach($amountApplication as $amountApp){
            $amount[]=$amountApp;

            // $testAmountInfo=DB::table('test_amount')
            //       ->where('id',$amountApp)
            //       ->get();
            // echo "<pre/>";
            // print_r($testAmountInfo);
           

        }

    return view('user.showSaveTest',compact('allTestInfo','amount'));
     }
  public function searchTest(Request $request){
        
         $id=$request->id;
         $allTestInfo=DB::table('all_test')
                          ->where('testNumber',$id)
                          ->first();
          if($allTestInfo){
             $amount=array();
               $amountApplication=  explode(',', $allTestInfo->testId);

              foreach($amountApplication as $amountApp){
                  $amount[]=$amountApp;

                  // $testAmountInfo=DB::table('test_amount')
                  //       ->where('id',$amountApp)
                  //       ->get();
                  // echo "<pre/>";
                  // print_r($testAmountInfo);
                 

              }

          return view('user.showSaveTest',compact('allTestInfo','amount'));
        }else{
            Session::put('messageWarning','Test Not Found !');
            return redirect()->back();
        }
        
     }

  public function testReport()
     {
      
      $id=Session::get('userId'); 
      $showAllTestReport=DB::table('all_test')
                      ->where('all_test.userId',$id)
                      ->orderBy('all_test.id','DESC')
                      ->paginate(5);

      return view('user.showAllTestReport',compact('showAllTestReport'));               

     }
   public function editTestReport($id,$page){
  $testAmountInfo=DB::table('all_test')
                  ->where('id',$id)
                  ->first();
        
    $amount=array();
    $amountApplication=  explode(',', $testAmountInfo->testId);

        foreach($amountApplication as $amountApp){
            $amount[]=$amountApp;

            // $testAmountInfo=DB::table('test_amount')
            //       ->where('id',$amountApp)
            //       ->get();
            // echo "<pre/>";
            // print_r($testAmountInfo);
           

        }

    return view('user.editTestReport',compact('amount','id'));
 }

 public function updateTestReport(Request $request)
 {
    $data=array();
    $id=$request->id;
    $data['testReport']=implode(', ', $request->testReport);
     DB::table('all_test as st')
            ->where('st.id',$id)
             ->update($data);
     Session::put('message','Update Successfully !!');

     return redirect()->back();
    
 }
 public function viewTestReport($id)
 {
   
   $userId=Session::get('userId');
   $companyName=DB::table('company_details')
                    ->where('company_details.active',1)
                    ->orderBy('id','DESC')
                    ->first();
   $branchName=DB::table('users as use')
                    ->join('branch as bra','use.branchId','=','bra.id')
                    ->select('bra.branchName')
                    ->where('use.id',$userId)
                    ->first();
   // echo "<pre/>";
   // print_r($branchName);  
   // exit();             
   $testAmountInfo=DB::table('all_test')
                  ->where('id',$id)
                  ->first();
        
    $amount=array();
    $amountApplication=  explode(',', $testAmountInfo->testId);
    $testReport=  explode(',', $testAmountInfo->testReport);
        foreach($amountApplication as $amountApp){
            $amount[]=$amountApp;

            // $testAmountInfo=DB::table('test_amount')
            //       ->where('id',$amountApp)
            //       ->get();
            // echo "<pre/>";
            // print_r($testAmountInfo);
           

        }

    return view('user.viewTestReport',compact('amount','testAmountInfo','testReport','companyName','branchName'));

 }
  public function searchTestReport(Request $request){

        $serch=$request->searchText;

         $indoorInfo=DB::table('all_test')
                    ->Where('name', 'like', '%' .$serch. '%')
                    ->OrWhere('referance', 'like', '%' .$serch. '%')
                     ->OrWhere('testNumber', 'like', '%' .$serch. '%')
                    ->limit(10)
                    ->get();

         
        echo json_encode($indoorInfo);
       
    }

  public function deleteTestReport(Request $request)
    {
       
        $id=intval($request->id);
        $result=DB::table('all_test')
              ->where('id', $id)
              ->delete();
           if ($result) {
                        echo json_encode('success');
                        exit();
                    }
    }

public function searchTestUser(Request $request){

        $serch=$request->searchText;

         $testInfo=DB::table('test_amount')
                    ->Where('testName', 'like', '%' .$serch. '%')
                    ->OrWhere('testAmount', 'like', '%' .$serch. '%')
                    ->limit(10)
                    ->get();

         
        echo json_encode($testInfo);
       
    }

//All test end Here.....


//Create Patient....
  //add indoor donor
   public function createPatient()
    {
        
        return view('patient.createPatient');
    } 
     public function addPatient(Request $request)
    {
      $data=array();
      $durationTimeCount=$request->durationTime;
      if ($durationTimeCount !='') {
        $effectiveDate = strtotime("+$durationTimeCount months", strtotime(date("Y-m-d")));
        $messageDate = date("Y-m-d", $effectiveDate);
        $data['messageDate']=$messageDate;
      }
      $orderNumberGenerate= mt_rand(10000000, 99999999);
      $generatorPNG = new BarcodeGeneratorPNG();
        file_put_contents('barCodeImg/'.$orderNumberGenerate.'.png', $generatorPNG->getBarcode($orderNumberGenerate, $generatorPNG::TYPE_CODE_128));
      $barCodeImg=$orderNumberGenerate.'.png';

     
      $data['name']=$request->name;
      $data['userId']=Session::get('userId');
       $data['status']='1';
      $data['age']=$request->age;
      $data['gender']=$request->gender;
      $data['educationQualification']=$request->educationQualification;
      $data['email']=$request->email;
      $data['mobile']=$request->mobile;
      $data['address1']=$request->address1;
      $data['address2']=$request->address2;
      $data['address3']=$request->address3;
      $data['address4']=$request->address4;
      $data['bloodGroup']=$request->bloodGroup;
      $data['requestedBloodGroup']=$request->requestedBloodGroup;
      $data['type']=$request->type;
      $data['durationTime']=$request->durationTime;
      $data['bloodNeedDate']=$request->bloodNeedDate;
      $data['date']=$request->date;
      $data['barCodeImage']='barCodeImg/'.$barCodeImg;
      $data['nibondonNo']=$orderNumberGenerate;
      $id=DB::table('patients')
                         ->insertGetId($data);


       Session::put('message','Patient Save successfully !!');

      return redirect()->route('showPatient',$id);

    } 

  public function showPatient($id){

         $patientInfo=DB::table('patients')
                          ->where('id',$id)
                          ->first();

        return view('patient.showPatient',compact('patientInfo'));
     }
     public function showSearchPatient($id){

         $patientInfo=DB::table('patients')
                          ->where('nibondonNo',$id)
                          ->first();

        return view('patient.showPatient',compact('patientInfo'));
     }
   public function updatePatient(Request $request){
      $id=$request->id;
      $data=array();
      $durationTimeCount=$request->durationTime;
      if ($durationTimeCount !='') {
        $effectiveDate = strtotime("+$durationTimeCount months", strtotime(date("Y-m-d")));
        $messageDate = date("Y-m-d", $effectiveDate);
        $data['messageDate']=$messageDate;
      }
      $data['name']=$request->name;
      $data['age']=$request->age;
      $data['userId']=Session::get('userId');
      $data['gender']=$request->gender;
      $data['educationQualification']=$request->educationQualification;
      $data['email']=$request->email;
      $data['mobile']=$request->mobile;
      $data['address1']=$request->address1;
      $data['address2']=$request->address2;
      $data['address3']=$request->address3;
      $data['address4']=$request->address4;
      $data['bloodGroup']=$request->bloodGroup;
      $data['requestedBloodGroup']=$request->requestedBloodGroup;
      $data['type']=$request->type;
      $data['durationTime']=$request->durationTime;
      $data['bloodNeedDate']=$request->bloodNeedDate;
      $data['date']=$request->date;
      DB::table('patients')
                ->where('id',$id)
                         ->update($data);


       
      Session::put('message','Patient Update Successfully !!');
      return redirect()->route('showPatient',$id);
   }
  public function searchPatient(Request $request){
         $id=$request->id;
        $showPatientInfo=DB::table('patients')
                        ->where('nibondonNo',$id)
                        ->first();
        if($showPatientInfo){
            return redirect()->route('showSearchPatient',$id);
        }else{
            Session::put('messageWarning','Patient Not Found !');
            return redirect()->back();
        }
    }
  public function deletePatient(Request $request){
         $id=$request->id;

         DB::table('patients')
                    ->where('id', $id)
                    ->delete();
    }

//Show Normal Patient Start Here..
    public function showAllNormalPatient()
    {
      $userId=Session::get('userId');
      $patientInfo=DB::table('patients')
                      ->where('patients.type','=','1')
                      ->where('patients.userId',$userId)
                      ->paginate(5);
      // echo "<pre/>";
      // print_r($patientInfo);
      // exit();
       return view('patient.showAllNormalPatient',compact('patientInfo'));
    }
   public function deleteAllNormalPatient(Request $request)
    {
       
        $id=intval($request->id);
        $result=DB::table('patients')
              ->where('id', $id)
              ->delete();
           if ($result) {
                        echo json_encode('success');
                        exit();
                    }
    }
  public function deactiveAllNormalPatient(Request $request)
    {
       
        $id=intval($request->id);
        $data=array();
        $data['status']='0';
        $result=DB::table('patients as pat')
                ->where('pat.id',$id)
                 ->update($data);
           if ($result) {
                        echo json_encode('success');
                        exit();
                    }
    }
  public function activeAllNormalPatient(Request $request)
    {
       
        $id=intval($request->id);
        $data=array();
        $data['status']='1';
        $result=DB::table('patients as pat')
                ->where('pat.id',$id)
                 ->update($data);
           if ($result) {
                        echo json_encode('success');
                        exit();
                    }
    }

  public function searchAllInNormalPatient(Request $request){
        
        $userId=Session::get('userId');
        $search=$request->searchText;
        $type=1;
        $patientInfo=DB::Select('select pat.* From patients as pat where pat.type="'.$type.'" AND pat.userId="'.$userId.'" AND (pat.name like "%'.$search.'%" OR pat.mobile like "%'.$search.'%" OR pat.nibondonNo like "%'.$search.'%" OR pat.bloodNeedDate like "%'.$search.'%" OR pat.email like "%'.$search.'%")');

         
        echo json_encode($patientInfo);
       
    }
//Show Normal Patient End Here..


//Show Thalassemia Patient Start Here..
    public function showAllThalassemiaPatient()
    {
      $userId=Session::get('userId');
      $patientInfo=DB::table('patients')
                      ->where('patients.type','=','2')
                      ->where('patients.userId',$userId)
                      ->paginate(5);
       return view('patient.showAllThalassemiaPatient',compact('patientInfo'));
    }
   public function deleteAllThalassemiaPatient(Request $request)
    {
       
        $id=intval($request->id);
        $result=DB::table('patients')
              ->where('id', $id)
              ->delete();
           if ($result) {
                        echo json_encode('success');
                        exit();
                    }
    }
  public function deactiveAllThalassemiaPatient(Request $request)
    {
       
        $id=intval($request->id);
        $data=array();
        $data['status']='0';
        $result=DB::table('patients as pat')
                ->where('pat.id',$id)
                 ->update($data);
           if ($result) {
                        echo json_encode('success');
                        exit();
                    }
    }
  public function activeAllThalassemiaPatient(Request $request)
    {
       
        $userId=Session::get('userId');
        $id=intval($request->id);
        $data=array();
        $data['status']='1';
        $result=DB::table('patients as pat')
                ->where('pat.id',$id)
                ->where('pat.userId',$userId)
                ->update($data);
           if ($result) {
                        echo json_encode('success');
                        exit();
                    }
    }

  public function searchAllInThalassemiaPatient(Request $request){

        $search=$request->searchText;
        $userId=Session::get('userId');
        $type=2;
          $patientInfo=DB::Select('select pat.* From patients as pat where pat.type="'.$type.'" AND pat.userId="'.$userId.'" AND (pat.name like "%'.$search.'%" OR pat.mobile like "%'.$search.'%" OR pat.nibondonNo like "%'.$search.'%" OR pat.messageDate like "%'.$search.'%" OR pat.email like "%'.$search.'%")');

         
        echo json_encode($patientInfo);
       
    }
//Show Thalassemia Patient End Here..


//Issue Voucher Start Here...
  
  public function createIssueVoucher()
    {
      
      $userId=Session::get('userId'); 
      $receiptOnInfo = DB::table('receipt_voucher as rec')
                      ->where('rec.group','=','on')
                      ->where('rec.userId',$userId)
                      ->count();
      $issueOnInfo = DB::table('issue_voucher as iss')
                      ->where('iss.group','=','on')
                      ->where('iss.userId',$userId)
                      ->count();
      $countOn=($receiptOnInfo-$issueOnInfo);



      $receiptOpInfo = DB::table('receipt_voucher as rec')
                      ->where('rec.group','=','op')
                      ->where('rec.userId',$userId)
                      ->count();
      $issueOpInfo = DB::table('issue_voucher as iss')
                      ->where('iss.group','=','op')
                      ->where('iss.userId',$userId)
                      ->count();
      $countOp=($receiptOpInfo-$issueOpInfo);


      $receiptabpInfo = DB::table('receipt_voucher as rec')
                      ->where('rec.group','=','abp')
                      ->where('rec.userId',$userId)
                      ->count();
      $issueabpInfo = DB::table('issue_voucher as iss')
                      ->where('iss.group','=','abp')
                      ->where('iss.userId',$userId)
                      ->count();
      $countabp=($receiptabpInfo-$issueabpInfo);

      $receiptabnInfo = DB::table('receipt_voucher as rec')
                      ->where('rec.group','=','abn')
                      ->where('rec.userId',$userId)
                      ->count();
      $issueabnInfo = DB::table('issue_voucher as iss')
                      ->where('iss.group','=','abn')
                      ->where('iss.userId',$userId)
                      ->count();
      $countabn=($receiptabnInfo-$issueabnInfo);

      $receiptapInfo = DB::table('receipt_voucher as rec')
                      ->where('rec.group','=','ap')
                      ->where('rec.userId',$userId)
                      ->count();
      $issueapInfo = DB::table('issue_voucher as iss')
                      ->where('iss.group','=','ap')
                      ->where('iss.userId',$userId)
                      ->count();
      $countap=($receiptapInfo-$issueapInfo);

      $receiptanInfo = DB::table('receipt_voucher as rec')
                      ->where('rec.group','=','an')
                      ->where('rec.userId',$userId)
                      ->count();
      $issueanInfo = DB::table('issue_voucher as iss')
                      ->where('iss.group','=','an')
                      ->where('iss.userId',$userId)
                      ->count();
      $countan=($receiptanInfo-$issueanInfo);

      $receiptbpInfo = DB::table('receipt_voucher as rec')
                      ->where('rec.group','=','bp')
                      ->where('rec.userId',$userId)
                      ->count();
      $issuebpInfo = DB::table('issue_voucher as iss')
                      ->where('iss.group','=','bp')
                      ->where('iss.userId',$userId)
                      ->count();
      $countbp=($receiptbpInfo-$issuebpInfo);

      $receiptbnInfo = DB::table('receipt_voucher as rec')
                      ->where('rec.group','=','bn')
                      ->where('rec.userId',$userId)
                      ->count();
      $issuebnInfo = DB::table('issue_voucher as iss')
                      ->where('iss.group','=','bn')
                      ->where('iss.userId',$userId)
                      ->count();
      $countbn=($receiptbnInfo-$issuebnInfo);
        return view('user.createIssueVoucher',compact('countOn','countOp','countabp','countabn','countap','countan','countbp','countbn'));
    } 
     public function addIssueVoucher(Request $request)
    {
      $orderNumberGenerate= mt_rand(10000000, 99999999);
      
      $data=array();
      $data['date']=$request->date;
      $data['name']=$request->name;
      $data['userId']=Session::get('userId');
      $data['referance']=$request->referance;
      $data['ward']=$request->ward;
      $data['bed']=$request->bed;
      $data['group']=$request->group;
      $data['bagNumber']=$request->bagNumber;
      $data['transfusion']=$request->transfusion;
      $data['donorNumber']=$request->donorNumber;
      $data['receiptNumber']=$request->receiptNumber;
      $data['receiverName']=$request->receiverName;
      $data['receiverAdd']=$request->receiverAdd;
      $data['receiverTel']=$request->receiverTel;
      $data['receiverEm']=$request->receiverEm;
      $data['voucherNumber']=$orderNumberGenerate;
      $id=DB::table('issue_voucher')
                         ->insertGetId($data);


       Session::put('message','Issue Voucher Save successfully !!');

      return redirect()->route('showIssueVoucher',$id);

    } 

  public function showIssueVoucher($id){
         $userId=Session::get('userId');
         $companyName=DB::table('company_details')
                          ->where('company_details.active',1)
                          ->orderBy('id','DESC')
                          ->first();
         $branchName=DB::table('users as use')
                          ->join('branch as bra','use.branchId','=','bra.id')
                          ->select('bra.branchName')
                          ->where('use.id',$userId)
                          ->first();

         $issueVoucherInfo=DB::table('issue_voucher')
                          ->where('id',$id)
                          ->first();

        return view('user.showIssueVoucher',compact('issueVoucherInfo','companyName','branchName'));
     }
     public function showSearchIssueVoucher($id){
         $userId=Session::get('userId');
         $companyName=DB::table('company_details')
                          ->where('company_details.active',1)
                          ->orderBy('id','DESC')
                          ->first();
         $branchName=DB::table('users as use')
                          ->join('branch as bra','use.branchId','=','bra.id')
                          ->select('bra.branchName')
                          ->where('use.id',$userId)
                          ->first();
         $issueVoucherInfo=DB::table('issue_voucher')
                          ->where('voucherNumber',$id)
                          ->first();

        return view('user.showIssueVoucher',compact('issueVoucherInfo','companyName','branchName'));
     }
   public function updateIssueVoucher(Request $request){
      $id=$request->id;
      $data=array();
      $data['date']=$request->date;
      $data['name']=$request->name;
      $data['userId']=Session::get('userId');
      $data['referance']=$request->referance;
      $data['ward']=$request->ward;
      $data['bed']=$request->bed;
      $data['group']=$request->group;
      $data['bagNumber']=$request->bagNumber;
      $data['transfusion']=$request->transfusion;
      $data['donorNumber']=$request->donorNumber;
      $data['receiptNumber']=$request->receiptNumber;
      $data['receiverName']=$request->receiverName;
      $data['receiverAdd']=$request->receiverAdd;
      $data['receiverTel']=$request->receiverTel;
      $data['receiverEm']=$request->receiverEm;
      DB::table('issue_voucher')
                ->where('id',$id)
                         ->update($data);


       
      Session::put('message',' Issue Voucher Update Successfully !!');
      return redirect()->route('showIssueVoucher',$id);
   }
  public function searchIssueVoucher(Request $request){
         $id=$request->id;
        $showIssueVoucherInfo=DB::table('issue_voucher')
                        ->where('voucherNumber',$id)
                        ->first();
        if($showIssueVoucherInfo){
            return redirect()->route('showSearchIssueVoucher',$id);
        }else{
            Session::put('messageWarning','Issue Voucher Not Found !');
            return redirect()->back();
        }
    }
  public function deleteIssueVoucher(Request $request){
         $id=$request->id;

         DB::table('issue_voucher')
                    ->where('id', $id)
                    ->delete();
    }

  
//Issue Voucher End Here.....

//Receipt Voucher Start Here...
  
  public function createReceiptVoucher()
    {
        
        return view('user.createReceiptVoucher');
    } 
     public function addReceiptVoucher(Request $request)
    {
      $orderNumberGenerate= mt_rand(10000000, 99999999);
      
      $data=array();
      $data['date']=$request->date;
      $data['userId']=Session::get('userId');
      $data['referance']=$request->referance;
      $data['ward']=$request->ward;
      $data['bed']=$request->bed;
      $data['group']=$request->group;
      $data['bagNumber']=$request->bagNumber;
      $data['transfusion']=$request->transfusion;
      $data['voucherNumber']=$orderNumberGenerate;
      $id=DB::table('receipt_voucher')
                         ->insertGetId($data);


       Session::put('message','Receipt Voucher Save successfully !!');

      return redirect()->route('showReceiptVoucher',$id);

    } 

  public function showReceiptVoucher($id){
         
         $userId=Session::get('userId');
         $companyName=DB::table('company_details')
                          ->where('company_details.active',1)
                          ->orderBy('id','DESC')
                          ->first();
         $branchName=DB::table('users as use')
                          ->join('branch as bra','use.branchId','=','bra.id')
                          ->select('bra.branchName')
                          ->where('use.id',$userId)
                          ->first();
         $issueVoucherInfo=DB::table('receipt_voucher')
                          ->where('id',$id)
                          ->first();

        return view('user.showReceiptVoucher',compact('issueVoucherInfo','companyName','branchName'));
     }
     public function showSearchReceiptVoucher($id){
         
         $userId=Session::get('userId');
         $companyName=DB::table('company_details')
                          ->where('company_details.active',1)
                          ->orderBy('id','DESC')
                          ->first();
         $branchName=DB::table('users as use')
                          ->join('branch as bra','use.branchId','=','bra.id')
                          ->select('bra.branchName')
                          ->where('use.id',$userId)
                          ->first();
         $issueVoucherInfo=DB::table('receipt_voucher')
                          ->where('voucherNumber',$id)
                          ->first();

        return view('user.showReceiptVoucher',compact('issueVoucherInfo','companyName','branchName'));
     }
   public function updateReceiptVoucher(Request $request){
      $id=$request->id;
      $data=array();
      $data['date']=$request->date;
      $data['userId']=Session::get('userId');
      $data['referance']=$request->referance;
      $data['ward']=$request->ward;
      $data['bed']=$request->bed;
      $data['group']=$request->group;
      $data['bagNumber']=$request->bagNumber;
      $data['transfusion']=$request->transfusion;
      DB::table('receipt_voucher')
                ->where('id',$id)
                         ->update($data);


       
      Session::put('message',' Receipt Voucher Update Successfully !!');
      return redirect()->route('showReceiptVoucher',$id);
   }
  public function searchReceiptVoucher(Request $request){
         $id=$request->id;
        $showIssueVoucherInfo=DB::table('receipt_voucher')
                        ->where('voucherNumber',$id)
                        ->first();
        if($showIssueVoucherInfo){
            return redirect()->route('showSearchReceiptVoucher',$id);
        }else{
            Session::put('messageWarning','Receipt Voucher Not Found !');
            return redirect()->back();
        }
    }
  public function deleteReceiptVoucher(Request $request){
         $id=$request->id;

         DB::table('receipt_voucher')
                    ->where('id', $id)
                    ->delete();
    }

  
//Receipt Voucher End Here.....


//Show Money Receipt

     public function showMoneyReceipt($id){

       $userId=Session::get('userId');
       $companyName=DB::table('company_details')
                        ->where('company_details.active',1)
                        ->orderBy('id','DESC')
                        ->first();
       $branchName=DB::table('users as use')
                        ->join('branch as bra','use.branchId','=','bra.id')
                        ->select('bra.branchName')
                        ->where('use.id',$userId)
                        ->first();

        $allTestInfo=DB::table('all_test')
                          ->where('testNumber',$id)
                          ->first();
        
        if ($allTestInfo) {
             $amount=array();
              $amountApplication=  explode(',', $allTestInfo->testId);

              foreach($amountApplication as $amountApp){
                  $amount[]=$amountApp;

              }
        $totalAmount=$allTestInfo->totalAmount;
        $paidAmount=$allTestInfo->paidAmount;
        // echo $totalAmount;
        // echo $paidAmount;
        // exit();

         $moneyReceiptInfo=DB::table('issue_voucher as isv')
                          ->join('all_test as all','all.testNumber','=','isv.donorNumber')
                          ->select('isv.*','all.*')
                          ->first();

        return view('user.showMoneyReceipt',compact('moneyReceiptInfo','allTestInfo','amount','totalAmount','paidAmount','companyName','branchName'));
      }else{

        Session::put('messageWarning','Test Report Not Found !');
            return redirect()->back();
      }
   
}


/* Store Equipment Start Here */

      public function showEquipment()
        {
            
          
        
            $row=DB::table('equipment')
                 ->orderBy('equipment.id','DESC')
                 ->paginate(10);
            return view('user.showEquipment',compact('row'));
        }
     public function createEquipment()
    {
      
      $userId=Session::get('userId');
      $equipmentInfo=DB::table('users as use')
                         ->join('branch as bra','use.branchId','=','bra.id')
                         ->select('bra.branchName')
                         ->where('use.id',$userId)
                         ->first();
      $currentDateTime = date('d-m-Y');
      
      $itemInfo=DB::table('item_name')
                        ->where('item_name.userId',$userId)
                        ->get();
      // echo "<pre/>";
      // print_r($paymentVouchaerInfo);
      // exit();
      return view('user.createEquipment',compact('equipmentInfo','currentDateTime','itemInfo')); 
    }
     public function saveEquipment( Request $request)
    {

      $data=array();
      $data['itemGroup']=$request->itemGroup;
      $data['totalAmount']=$request->totalAmount;
      $data['date']=$request->date;
      $data['userId']=Session::get('userId');
      $data['branchName']=$request->branchName;
      $data['balanceType']=$request->balanceType;
      $data['itemName']=implode(', ', $request->itemName);
      $data['quantity']=implode(', ', $request->quantity);
      $data['rate']=implode(', ', $request->rate);
      $data['amount']=implode(', ', $request->amount);
      // echo "<pre/>";
      // print_r($data);
      // exit();
        $result=DB::table('equipment')->insert($data);
        if ($result) {
          Session::put('message','Store Equipment Save successfully!!');

          return redirect()->back();
          
        }else{
            Session::put('message','Store Equipment Save Failed !!!');
            return redirect()->back();
        }
       
    } 
    public function editEquipment($id,$page)
    {
        
         $singleEquipmentInfo = DB::table('equipment')
                            ->Where('id', $id)
                            ->first();
         return view('user.editEquipment',compact('singleEquipmentInfo','page'));
        
    }
     public function updateEquipment(Request $request)
    {
        
      $id=$request->id;
      $page=$request->page;
      $data=array();
      $quantityCount=$request->quantity;
      if ($quantityCount!='' && $quantityCount!='0') {
       $data['quantity']=$request->quantity;
       $increaseEquipmentPrice=($request->equipmentPrice)* $quantityCount;
       $totalPrice=($increaseEquipmentPrice + $request->additionalPrice);
      }else{
        $data['quantity']=0;
        $totalPrice=($request->equipmentPrice + $request->additionalPrice);
      }
      
        $data['equipmentName']=$request->equipmentName;
        $data['equipmentPrice']=$request->equipmentPrice;
        $data['additionalPrice']=$request->additionalPrice;
        $data['remark']=$request->remark;
        $data['totalPrice']=$totalPrice;
        $result=DB::table('equipment')
                      ->where('id',$id)
                      ->update($data);

         

                           

        Session::put('message','Store Equipment Update successfully!!');

        return redirect('user/show-equipment?page='.$page);
    }
     public function deleteEquipment(Request $request)
    {
        $id=intval($request->id);
        $result=DB::table('equipment')
              ->where('id', $id)
              ->delete();
           if ($result) {
                        echo json_encode('success');
                        exit();
                    }
    }                       
                          
/* Store Equipment End Here */


/* Account Module Start Here */

public function getLeager(Request $request){
  $userId=Session::get('userId');

  $paymentType=DB::table('account_leager')
                    ->where('account_leager.userId',$userId)
                    ->get(); 
   echo json_encode($paymentType);
}

public function getItemName(Request $request){
  $userId=Session::get('userId');

  $paymentType=DB::table('item_name')
                    ->where('item_name.userId',$userId)
                    ->get(); 
   echo json_encode($paymentType);
}

//payment vouchaer start here
public function addPaymentVouchaer(){

  $userId=Session::get('userId');
  $paymentVouchaerInfo=DB::table('users as use')
                           ->join('branch as bra','use.branchId','=','bra.id')
                           ->select('bra.branchName')
                           ->where('use.id',$userId)
                           ->first();
  $currentDateTime = date('d-m-Y');
  $randomNumber=rand ( 10000 , 99999 );

  $paymentType=DB::table('account_leager')
                    ->where('account_leager.userId',$userId)
                    ->get();
  // echo "<pre/>";
  // print_r($paymentVouchaerInfo);
  // exit();
  return view('user.addPaymentVouchaer',compact('paymentVouchaerInfo','currentDateTime','randomNumber','paymentType'));
}

public function showPaymentVouchaer(){
  
  $userId=Session::get('userId');
  $paymentVouchaerInfo=DB::table('payment_vouchare')
                            ->where('payment_vouchare.userId',$userId)
                            ->paginate(10);
  return view('user.showPaymentVouchaer',compact('paymentVouchaerInfo'));

}

public function viewPaymentVouchaer($id)
{

  $userId=Session::get('userId');
   $companyName=DB::table('company_details')
                    ->where('company_details.active',1)
                    ->orderBy('id','DESC')
                    ->first();
   $branchName=DB::table('users as use')
                    ->join('branch as bra','use.branchId','=','bra.id')
                    ->select('bra.branchName')
                    ->where('use.id',$userId)
                    ->first();
  $singleInfo=DB::table('payment_vouchare as pay')
                    ->where('pay.id',$id)
                    ->first();
  // echo "<pre/>";
  // print_r($singleInfo);
  // exit();
  if ($singleInfo) {
      
        $sll=array();
        $slInfo=  explode(',', $singleInfo->sl);
            foreach($slInfo as $slI){
                $sll[]=$slI;
            }

        $amount=array();
        $amountInfo=  explode(',', $singleInfo->amount);
            foreach($amountInfo as $amountI){
                $amount[]=$amountI;
            }

        $leager=array();
        $leagerInfo=  explode(',', $singleInfo->leager);
            foreach($leagerInfo as $leagerI){
                $leager[]=$leagerI;
            }
  }

  return view('user.viewPaymentVouchaer',compact('singleInfo','sll','amount','leager','companyName','branchName'));

}

public function savePaymentVouchaer(Request $request){
  
  $data=array();
  $data['userId']=Session::get('userId');
  $data['vouchareNo']=$request->vouchaerNo;
  $data['date']=$request->date;
  $data['branchName']=$request->branchName;
  $data['paymentTypeId']=$request->paymentTypeId;
  $data['totalAmount']=$request->totalAmount;
  $data['taka']=$request->taka;
  $data['narration']=$request->narration;
  $data['sl']=implode(', ', $request->sl);
  $data['leager']=implode(', ', $request->leager);
  $data['amount']=implode(', ', $request->amount);
  // echo "<pre/>";
  // print_r($data);
  // exit();
  $insertPaymentVouchaer=DB::table('payment_vouchare')
                                ->insert($data);

  if($insertPaymentVouchaer){
            Session::put('message','Payment Voucher Insert Successfully');
            return redirect()->back();
        }else{
            Session::put('messageWarning','Payment Voucher Insert Failed');
            return redirect()->back();
        }

}
//payment vouchaer end here



//receipt vouchaer start here
public function addReceiptVouchaer(){

  $userId=Session::get('userId');
  $paymentVouchaerInfo=DB::table('users as use')
                           ->join('branch as bra','use.branchId','=','bra.id')
                           ->select('bra.branchName')
                           ->where('use.id',$userId)
                           ->first();
  $currentDateTime = date('d-m-Y');
  $randomNumber=rand ( 10000 , 99999 );

   $paymentType=DB::table('account_leager')
                    ->where('account_leager.userId',$userId)
                    ->get();
  // echo "<pre/>";
  // print_r($paymentVouchaerInfo);
  // exit();
  return view('user.addReceiptVouchaer',compact('paymentVouchaerInfo','currentDateTime','randomNumber','paymentType'));
}

public function showReceiptVouchaer(){
  
  $userId=Session::get('userId');
  $paymentVouchaerInfo=DB::table('receipt_vouchare')
                            ->where('receipt_vouchare.userId',$userId)
                            ->paginate(10);
  return view('user.showReceiptVouchaer',compact('paymentVouchaerInfo'));

}

public function viewReceiptVouchaer($id)
{

  $userId=Session::get('userId');
   $companyName=DB::table('company_details')
                    ->where('company_details.active',1)
                    ->orderBy('id','DESC')
                    ->first();
   $branchName=DB::table('users as use')
                    ->join('branch as bra','use.branchId','=','bra.id')
                    ->select('bra.branchName')
                    ->where('use.id',$userId)
                    ->first();
  $singleInfo=DB::table('receipt_vouchare as pay')
                    ->where('pay.id',$id)
                    ->first();
  // echo "<pre/>";
  // print_r($singleInfo);
  // exit();
  if ($singleInfo) {
      
        $sll=array();
        $slInfo=  explode(',', $singleInfo->sl);
            foreach($slInfo as $slI){
                $sll[]=$slI;
            }

        $amount=array();
        $amountInfo=  explode(',', $singleInfo->amount);
            foreach($amountInfo as $amountI){
                $amount[]=$amountI;
            }

        $leager=array();
        $leagerInfo=  explode(',', $singleInfo->leager);
            foreach($leagerInfo as $leagerI){
                $leager[]=$leagerI;
            }
  }

  return view('user.viewReceiptVouchaer',compact('singleInfo','sll','amount','leager','companyName','branchName'));

}

public function saveReceiptVouchaer(Request $request){
  
  $data=array();
  $data['userId']=Session::get('userId');
  $data['vouchareNo']=$request->vouchaerNo;
  $data['date']=$request->date;
  $data['branchName']=$request->branchName;
  $data['paymentTypeId']=$request->paymentTypeId;
  $data['totalAmount']=$request->totalAmount;
  $data['taka']=$request->taka;
  $data['narration']=$request->narration;
  $data['sl']=implode(', ', $request->sl);
  $data['leager']=implode(', ', $request->leager);
  $data['amount']=implode(', ', $request->amount);

  $insertPaymentVouchaer=DB::table('receipt_vouchare')
                                ->insert($data);

  if($insertPaymentVouchaer){
            Session::put('message','Receipt Voucher Insert Successfully');
            return redirect()->back();
        }else{
            Session::put('messageWarning','Receipt Voucher Insert Failed');
            return redirect()->back();
        }

}
//receipt vouchaer end here


public function showStatistics(){
  
  $total_amount=0;
  $userId=Session::get('userId');

  $totalEaringAmount=DB::table('all_test')
                        ->where('all_test.userId',$userId)
                        ->get();
  
  // echo "<pre/>";
  // print_r($totalEaringAmount);
  // exit();
  foreach ($totalEaringAmount as $totalEarning) {

     $total_amount += $totalEarning->totalAmount;
   }

   echo $total_amount;
}

/* Account Module End Here */

/*Payment Type start Here*/
      
      public function showPaymentType()
        {
            
          
        
            $row=DB::table('payment_type')
                 ->orderBy('payment_type.id','DESC')
                 ->paginate(4);
            return view('user.showPaymentType',compact('row'));
        }
     public function createPaymentType()
    {
        
        return view('user.createPaymentType');
    }
     public function savePaymentType( Request $request)
    {

        $data=array();
        $data['paymentName']=$request->paymentName;
        $data['userId']=Session::get('userId');
        $result=DB::table('payment_type')->insert($data);
        if ($result) {
          Session::put('message','Payment Type Save successfully!!');

          return redirect()->back();
          
        }else{
            Session::put('message','Payment Type Save Failed !!!');
            return redirect()->back();
        }
       
    } 
    public function editPaymentType($id,$page)
    {
        
         $singlePaymentInfo = DB::table('payment_type')
                            ->Where('id', $id)
                            ->first();
         return view('user.editPaymentType',compact('singlePaymentInfo','page'));
        
    }
     public function updatePaymentType(Request $request)
    {
        
            $id=$request->id;
           $page=$request->page;


         $data=array();
         $data['paymentName']=$request->paymentName;
        

         DB::table('payment_type')
            ->where('id',$id)
            ->update($data);

                           

        Session::put('message','Payment Type Update successfully!!');

        return redirect('user/show-payment-type?page='.$page);
    }
     public function deletePaymentType(Request $request)
    {
        $id=intval($request->id);
        $result=DB::table('payment_type')
              ->where('id', $id)
              ->delete();
           if ($result) {
                        echo json_encode('success');
                        exit();
                    }
    }                       
                          

/*Payment Type End Here*/



/*Account Leager start Here*/
      
      public function showAccountLeager()
        {
            
          
        
            $row=DB::table('account_leager')
                 ->orderBy('account_leager.id','DESC')
                 ->paginate(4);
            return view('user.showAccountLeager',compact('row'));
        }
     public function createAccountLeager()
    {
        
        $accountGroup=DB::table('payment_type')
                             ->get();
        return view('user.createAccountLeager',compact('accountGroup'));
    }
     public function saveAccountLeager( Request $request)
    {

        $data=array();
        $data['accountGroup']=$request->accountGroup;
        $data['accountLeager']=$request->accountLeager;
        $data['userId']=Session::get('userId');
        $result=DB::table('account_leager')->insert($data);
        if ($result) {
          Session::put('message','Account Leager Save successfully!!');

          return redirect()->back();
          
        }else{
            Session::put('message','Account Leager Save Failed !!!');
            return redirect()->back();
        }
       
    } 
    public function editAccountLeager($id,$page)
    {
        
         $accountGroup=DB::table('payment_type')
                             ->get();
         $singlePaymentInfo = DB::table('account_leager')
                            ->Where('id', $id)
                            ->first();
         return view('user.editAccountLeager',compact('singlePaymentInfo','page','accountGroup'));
        
    }
     public function updateAccountLeager(Request $request)
    {
        
            $id=$request->id;
           $page=$request->page;


         $data=array();
        $data['accountGroup']=$request->accountGroup;
        $data['accountLeager']=$request->accountLeager;
        $data['userId']=Session::get('userId');
        

         DB::table('account_leager')
            ->where('id',$id)
            ->update($data);

                           

        Session::put('message','Account Leager Update successfully!!');

        return redirect('user/show-account-leager?page='.$page);
    }
     public function deleteAccountLeager(Request $request)
    {
        $id=intval($request->id);
        $result=DB::table('account_leager')
              ->where('id', $id)
              ->delete();
           if ($result) {
                        echo json_encode('success');
                        exit();
                    }
    }                       
                          

/*Account Leager End Here*/


/*Item Name start Here*/
      
      public function showItemName()
        {
            
          
        
            $row=DB::table('item_name')
                 ->orderBy('item_name.id','DESC')
                 ->paginate(4);
            // echo "<pre/>";
            // print_r($row);
            // exit();
            return view('user.showItemName',compact('row'));
        }
     public function createItemName()
    {
        
        $accountGroup=DB::table('item')
                             ->get();
        return view('user.createItemName',compact('accountGroup'));
    }
     public function saveItemName( Request $request)
    {

        $data=array();
        $data['itemGroup']=$request->itemGroup;
        $data['itemName']=$request->itemName;
        $data['userId']=Session::get('userId');
        $result=DB::table('item_name')->insert($data);
        if ($result) {
          Session::put('message','Item Name Save successfully!!');

          return redirect()->back();
          
        }else{
            Session::put('message','Item Name Save Failed !!!');
            return redirect()->back();
        }
       
    } 
    public function editItemName($id,$page)
    {
        
         $accountGroup=DB::table('item')
                             ->get();
         $singlePaymentInfo = DB::table('item_name')
                            ->Where('id', $id)
                            ->first();
         return view('user.editItemName',compact('singlePaymentInfo','page','accountGroup'));
        
    }
     public function updateItemName(Request $request)
    {
        
            $id=$request->id;
           $page=$request->page;


         $data=array();
        $data['itemGroup']=$request->itemGroup;
        $data['itemName']=$request->itemName;
        $data['userId']=Session::get('userId');
        

         DB::table('item_name')
            ->where('id',$id)
            ->update($data);

                           

        Session::put('message','Item Name Update successfully!!');

        return redirect('user/show-item-name?page='.$page);
    }
     public function deleteItemName(Request $request)
    {
        $id=intval($request->id);
        $result=DB::table('item_name')
              ->where('id', $id)
              ->delete();
           if ($result) {
                        echo json_encode('success');
                        exit();
                    }
    }                       
                          

/*Item Name End Here*/

/*Item start Here*/
      
  public function showItem()
    {
    
        $row=DB::table('item')
             ->orderBy('item.id','DESC')
             ->paginate(4);
        return view('user.showItem',compact('row'));
    }
    
  public function createItem()
    {
        
        return view('user.createItem');
    }
     public function saveItem( Request $request)
    {

        $data=array();
        $data['itemName']=$request->itemName;
        $data['userId']=Session::get('userId');
        $result=DB::table('item')->insert($data);
        if ($result) {
          Session::put('message','Item Save successfully!!');

          return redirect()->back();
          
        }else{
            Session::put('message','Item Save Failed !!!');
            return redirect()->back();
        }
       
    } 
    public function editItem($id,$page)
    {
        
         $singlePaymentInfo = DB::table('item')
                            ->Where('id', $id)
                            ->first();
         return view('user.editItem',compact('singlePaymentInfo','page'));
        
    }
     public function updateItem(Request $request)
    {
        
            $id=$request->id;
           $page=$request->page;


         $data=array();
         $data['itemName']=$request->itemName;
        

         DB::table('item')
            ->where('id',$id)
            ->update($data);

                           

        Session::put('message','Item Update successfully!!');

        return redirect('user/show-item?page='.$page);
    }
     public function deleteItem(Request $request)
    {
        $id=intval($request->id);
        $result=DB::table('item')
              ->where('id', $id)
              ->delete();
           if ($result) {
                        echo json_encode('success');
                        exit();
                    }
    }                       
                          
/*Payment Type End Here*/


/* Stock Voucher Entry Start Here */

  public function createStockVoucherEntry(){
      $userId=Session::get('userId');
      $stockInfo=DB::table('users as use')
                     ->join('branch as bra','use.branchId','=','bra.id')
                     ->select('bra.branchName')
                     ->where('use.id',$userId)
                     ->first();
      $accountLeager=DB::table('account_leager')
                          ->where('account_leager.userId',$userId)
                          ->get();
      $itemName=DB::table('item_name')
                     ->where('item_name.userId',$userId)
                     ->get();
      $currentDateTime = date('d-m-Y');
      $randomNumber=rand ( 10000 , 99999 );

      return view('user.createStockVoucherEntry',compact('stockInfo','currentDateTime','randomNumber','accountLeager','itemName'));
  }


  public function showStockVoucherEntry(){
  
  $userId=Session::get('userId');
  $paymentVouchaerInfo=DB::table('stock_voucher_entry')
                            ->where('stock_voucher_entry.userId',$userId)
                            ->paginate(10);
  return view('user.showStockVoucherEntry',compact('paymentVouchaerInfo'));

}

public function viewStockVoucherEntry($id)
{

  $userId=Session::get('userId');
   $companyName=DB::table('company_details')
                    ->where('company_details.active',1)
                    ->orderBy('id','DESC')
                    ->first();
   $branchName=DB::table('users as use')
                    ->join('branch as bra','use.branchId','=','bra.id')
                    ->select('bra.branchName')
                    ->where('use.id',$userId)
                    ->first();
  $singleInfo=DB::table('stock_voucher_entry as pay')
                    ->select('pay.*')
                    ->where('pay.id',$id)
                    ->first();
  // echo "<pre/>";
  // print_r($singleInfo);
  // exit();
  if ($singleInfo) {
      
        $sll=array();
        $slInfo=  explode(',', $singleInfo->sl);
            foreach($slInfo as $slI){
                $sll[]=$slI;
            }

        $amount=array();
        $amountInfo=  explode(',', $singleInfo->amount);
            foreach($amountInfo as $amountI){
                $amount[]=$amountI;
            }
        $itemName=array();
        $itemNameInfo=  explode(',', $singleInfo->itemName);
            foreach($itemNameInfo as $itemNameI){
                $itemName[]=$itemNameI;
            }

        $quantity=array();
        $quantityInfo=  explode(',', $singleInfo->quantity);
            foreach($quantityInfo as $quantityI){
                $quantity[]=$quantityI;
            }

        $rate=array();
        $rateInfo=  explode(',', $singleInfo->rate);
            foreach($rateInfo as $rateI){
                $rate[]=$rateI;
            }
  }

  return view('user.viewStockVoucherEntry',compact('singleInfo','sll','amount','itemName','quantity','rate','companyName','branchName'));

}

  public function saveStockVoucherEntry(Request $request){
    // stick voucher save here..

    $data=array();
    $data['userId']=Session::get('userId');
    $data['invoiceNumber']=$request->invoiceNumber;
    $data['date']=$request->date;
    $data['branchName']=$request->branchName;
    $data['balanceType']=$request->balanceType;
    $data['partyName']=$request->partyName;
    $data['sl']=implode(', ', $request->sl);
    $data['itemName']=implode(', ', $request->itemName);
    $data['quantity']=implode(', ', $request->quantity);
    $data['rate']=implode(', ', $request->rate);
    $data['amount']=implode(', ', $request->amount);
    $data['totalAmount']=$request->totalAmount;
    $data['taka']=$request->taka;
    $data['narration']=$request->narration;
    // echo "<pre/>";
    // print_r($data);
    // exit();
    $result=DB::table('stock_voucher_entry')->insert($data);
      if ($result) {
        Session::put('message','Stock Voucher Save Successfully!!');

        return redirect()->back();
        
      }else{
          Session::put('message','Stock Voucher Save Failed !!!');
          return redirect()->back();
      }

  }
/* Stock Voucher Entry End Here */

/* Donor Recozation Start Here */

  public function showDonorRecozation(){

    $donorRecozationInfo=DB::table('donor_recozation')
                                ->paginate(10);
    return view('user.showDonorRecozation',compact('donorRecozationInfo'));
  }

  public function createDonorRecozation(){
    return view('user.createDonorRecozation');
  }

  public function viewDonorRecozation($id){
    $userId=Session::get('userId');
   $companyName=DB::table('company_details')
                    ->where('company_details.active',1)
                    ->orderBy('id','DESC')
                    ->first();
   $branchName=DB::table('users as use')
                    ->join('branch as bra','use.branchId','=','bra.id')
                    ->select('bra.branchName')
                    ->where('use.id',$userId)
                    ->first();
    $donorRecozation=DB::table('donor_recozation')
                          ->where('donor_recozation.id',$id)
                          ->first();
    return view('user.viewDonorRecozation',compact('donorRecozation','companyName','branchName'));
  }
  public function saveDonorRecozation(Request $request){
    $data=array();
    $data['userId']=Session::get('userId');
    $data['name']=$request->name;
    $data['date']=$request->date;
    $data['age']=$request->age;
    $data['group']=$request->group;
    $data['referance']=$request->referance;
    $data['phone']=$request->phone;
    $data['bag']=$request->bag;
    $data['sex']=$request->sex;

    $result=DB::table('donor_recozation')->insert($data);
        if ($result) {
          Session::put('message','Recozation Donor Save successfully!!');

          return redirect()->back();
          
        }else{
            Session::put('message','Recozation Donor Save Failed !!!');
            return redirect()->back();
        }

  }

   public function deleteDonorRecozation(Request $request)
    {
        $id=intval($request->id);
        $result=DB::table('donor_recozation')
              ->where('id', $id)
              ->delete();
           if ($result) {
                        echo json_encode('success');
                        exit();
                    }
    }        

/* Donor Recozation End Here */



/* Journal Vouchare */
public function showJournalVouchaer(){
  
  $userId=Session::get('userId');
  $journalVouchaerInfo=DB::table('journal_vouchaer')
                            ->where('journal_vouchaer.userId',$userId)
                            ->paginate(10);
  return view('user.showJournalVouchaer',compact('journalVouchaerInfo'));

}

public function addJournalVouchaer(){
  $userId=Session::get('userId');
  $currentDateTime = date('d-m-Y');
  $accountLeager=DB::table('account_leager')
                      ->where('account_leager.userId',$userId)
                      ->get();
   $randomNumber=rand ( 10000 , 99999 );
   $equipmentInfo=DB::table('users as use')
                         ->join('branch as bra','use.branchId','=','bra.id')
                         ->select('bra.branchName')
                         ->where('use.id',$userId)
                         ->first();
  return view('user.createJournalVouchaer',compact('accountLeager','currentDateTime','equipmentInfo','randomNumber'));

}

public function viewJournalVouchaer($id)
{
  $userId=Session::get('userId');
   $companyName=DB::table('company_details')
                    ->where('company_details.active',1)
                    ->orderBy('id','DESC')
                    ->first();
   $branchName=DB::table('users as use')
                    ->join('branch as bra','use.branchId','=','bra.id')
                    ->select('bra.branchName')
                    ->where('use.id',$userId)
                    ->first();
  $singleInfo=DB::table('journal_vouchaer as pay')
                    ->where('pay.id',$id)
                    ->first();
  // echo "<pre/>";
  // print_r($singleInfo);
  // exit();
  if ($singleInfo) {
      
        $drl=array();
        $slInfo=  explode(',', $singleInfo->dr);
            foreach($slInfo as $drI){
                $drl[]=$drI;
            }

        $cr=array();
        $crInfo=  explode(',', $singleInfo->cr);
            foreach($crInfo as $crI){
                $cr[]=$crI;
            }

        $leagerName=array();
        $leagerNameInfo=  explode(',', $singleInfo->leagerName);
            foreach($leagerNameInfo as $leagerNameI){
                $leagerName[]=$leagerNameI;
            }
  }

  return view('user.viewJournalVouchaer',compact('singleInfo','drl','cr','leagerName','companyName','branchName'));

}

public function saveJournalVouchaer(Request $request){
  
  $data=array();
  $data['userId']=Session::get('userId');
  $data['vouchaerNo']=$request->vouchaerNo;
  $data['taka']=$request->taka;
  $data['narration']=$request->narration;
  $data['date']=$request->date;
  $data['branchName']=$request->branchName;
  $data['drAmount']=$request->drAmount;
  $data['crAmount']=$request->crAmount;
  $data['leagerName']=implode(', ', $request->leagerName);
  $data['dr']=implode(', ', $request->dr);
  $data['cr']=implode(', ', $request->cr);
  // echo "<pre/>";
  // print_r($data);
  // exit();
  $insertPaymentVouchaer=DB::table('journal_vouchaer')
                                ->insert($data);

  if($insertPaymentVouchaer){
            Session::put('message','Journal Voucher Insert Successfully');
            return redirect()->back();
        }else{
            Session::put('messageWarning','Journal Voucher Insert Failed');
            return redirect()->back();
        }

}
//Journal vouchaer end here



   
}
