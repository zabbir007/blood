<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Mail;
use Session;
use File;

if (!isset($_SESSION)) {
    session_start();
}

class SuperAdminController extends Controller
{
    
    public function __construct()
    {
         
        $this->middleware('checkAdmin');
    }
   public function dashboard()
    {
    	return view('admin.dashboard');
    }
   public function adminLogOut()
    {
        //Session::flush(); //destroy all session
        Session::put('email','');
        Session::put('adminId','');
        return redirect()->route('adminLogin');
    }
//add user start here..
    public function adminAddUser()
    {
        $showBranch = DB::table('branch')
                      ->get();
        return view('admin.addUser',compact('showBranch'));
       
    }

    public function adminSaveUser(Request $request)
    {

            $data=array();
            $data['name']=$request->name;
            $data['email']=$request->email;
            $data['phone']=$request->phone;
            $data['password']=md5($request->password);
            $data['branchId']=$request->branchId;
            $image=$request->file('image');

            // $checkEmailAndPhone=DB::table('merchants')
            //          ->get();
            // foreach($checkEmailAndPhone as $checkEP){
            //     //check phone number..
            //     if ($checkEP->phone == $request->merchantPhone) {
            //         Session::put('messageWarning','This Phone Number Allready Used!!');
            //         return redirect()->back();

            //     }
            //     //check email
            //     if ($checkEP->email ==$request->merchantEmail) {
            //         Session::put('messageWarning','This Email Allready Used!!');
            //         return redirect()->back();
            //     }
            // }
            // if ($request->merchantPassword!=$request->confirmMerchantPassword) {
            //     Session::put('messageWarning','Confirm Password not match!!');
            //     return redirect()->back(); 
            // }
        
            if ($image) {
            $image_name=str_random(20);
            $ext=strtolower($image->getClientOriginalExtension());

            $permited  = array('jpg', 'jpeg', 'png', 'pdf');
            $fileSize = $request->file('image')->getClientSize();

            if($fileSize>524288){
                 Session::put('messageWarning','Please upload file less than 512 KB');
                  return redirect()->back();
            }elseif (in_array($ext, $permited) === false){

                Session::put('messageWarning','You can only upload '.implode(', ', $permited));
                  return redirect()->back();
            }

            $image_full_name=$image_name.'.'.$ext;
            $upload_path='user/img/';
            $image_url=$upload_path.$image_full_name;
            $success=$image->move($upload_path,$image_full_name);
            if ($success) {
                
                $data['image']=$image_url;
                 
                $userRegister=DB::table('users')
                    ->insert($data); 
                if ($userRegister) {
                    Session::put('message','Registration Done successfully!!');
                    return redirect()->route('adminAddUser');
                }
                else{
                     Session::put('messageWarning','Registration Failed!!');
                    return redirect()->route('adminAddUser');
                }
                   
                              
        
            }
          }else{

               
                $userRegister=DB::table('users')
                    ->insert($data); 
                if ($userRegister) {
                    Session::put('message','Registration Done successfully!!');
                    return redirect()->route('adminAddUser');
                }
                else{
                     Session::put('messageWarning','Registration Failed!!');
                    return redirect()->route('adminAddUser');
                }
          }
                
        }

    public function adminShowAllUser()
    {
      $userInfo=DB::table('users as use')
                ->join('branch as bran','bran.id','=','use.branchId')
                ->paginate(5);

       return view('admin.showAllUser',compact('userInfo'));
    }

   public function deleteUser(Request $request)
    {
       
        $id=intval($request->id);
        $result=DB::table('users')
              ->where('id', $id)
              ->delete();
           if ($result) {
                        echo json_encode('success');
                        exit();
                    }
    }
  public function deactiveUser(Request $request)
    {
       
        $id=intval($request->id);
        $data=array();
        $data['status']='0';
        $result=DB::table('users as use')
                ->where('use.id',$id)
                 ->update($data);
           if ($result) {
                        echo json_encode('success');
                        exit();
                    }
    }
  public function activeUser(Request $request)
    {
       
        $id=intval($request->id);
        $data=array();
        $data['status']='1';
        $result=DB::table('users as use')
                ->where('use.id',$id)
                 ->update($data);
           if ($result) {
                        echo json_encode('success');
                        exit();
                    }
    }

  public function searchUser(Request $request){

        $serch=$request->searchText;

         $userInfo=DB::table('users as use')
                    ->join('branch as bran','bran.id','=','use.branchId')
                    ->Where('use.name', 'like', '%' .$serch. '%')
                    ->OrWhere('use.phone', 'like', '%' .$serch. '%')
                    ->OrWhere('use.email', 'like', '%' .$serch. '%')
                    ->select('use.*','bran.branchName')
                    ->limit(10)
                    ->get();

         
        echo json_encode($userInfo);
       
    }



    public function checkUserEmail(Request $request){

        $userEmail=$request->userEmail;

        $userEmailCheck=DB::table('users')
                                ->where('email',$userEmail)
                                ->select('users.email')
                                ->first();

  

        if(isset($userEmailCheck->email)){
                 
                echo json_encode("Found");
        }else{
            echo json_encode("Not Found");
        }
    }

    public function checkUserPhone(Request $request){

        $userPhone=$request->userPhone;

        $userPhoneCheck=DB::table('users')
                              ->where('phone',$userPhone)
                              ->select('users.phone')
                              ->first();

  

        if(isset($userPhoneCheck->phone)){
                 
                echo json_encode("Found");
        }else{
            echo json_encode("Not Found");
        }
    }
//add user end here...

//Region start here...

    public function checkRegion(Request $request){

        $region=$request->region;

        $regionCheck=DB::table('regions')
                    ->where('regionName',$region)
                    ->select('regions.regionName')
                    ->first();

  

        if(isset($regionCheck->regionName)){
                 
                echo json_encode("Found");
        }else{
            echo json_encode("Not Found");
        }
    }
   public function showRegion()
    {
        
      
    
        $showRegion=DB::table('regions')
             ->orderBy('regions.regionName','ASC')
             ->paginate(5);
        return view('admin.showRegion',compact('showRegion'));
    }
    public function createRegion()
    {
        
        return view('admin.createRegion');
    }

   public function saveRegion( Request $request)
    {
        $data=array();
        $data['regionName']=$request->regionName;
        $result=DB::table('regions')->insert($data);
        if ($result) {
          Session::put('message','Region Save successfully!!');

          return redirect()->back();
          
        }else{
            Session::put('message','Region Save Failed !!!');
            return redirect()->back();
        }
       
    } 

   public function editRegion($id,$page)
    {
        
         $singleRegionInfo = DB::table('regions')
                            ->Where('id', $id)
                            ->first();
         return view('admin.editRegion',compact('singleRegionInfo','page'));
        
    }
    public function updateRegion(Request $request)
    {
        
            $id=$request->id;
           $page=$request->page;


         $data=array();
         $data['regionName']=$request->regionName;
        

         DB::table('regions')
            ->where('id',$id)
            ->update($data);

                           

        Session::put('message','Region Update successfully!!');

        return redirect('admin/show-region?page='.$page);
    }
   public function serachRegion(Request $request){

        $serch=$request->searchText;

         $regionInfo=DB::table('regions')
                    ->Where('regionName', 'like', '%' .$serch. '%')
                    ->select('regionName','id')
                    ->orderBy('regionName','ASC')
                    ->limit(10)
                    ->get();

         
        echo json_encode($regionInfo);
       
    }
   public function deleteRegion(Request $request)
    {
       
        $id=intval($request->id);
        $result=DB::table('regions')
              ->where('id', $id)
              ->delete();
           if ($result) {
                        echo json_encode('success');
                        exit();
                    }
    }
//Region End Here....

//District Start Here..
    
    public function checkDistrict(Request $request){

        $district=$request->district;

        $districtCheck=DB::table('districts')
                        ->where('districtName',$district)
                        ->select('districts.districtName')
                        ->first();

  

        if(isset($districtCheck->districtName)){
                 
                echo json_encode("Found");
        }else{
            echo json_encode("Not Found");
        }
    }

 public function showDistrict()
    {
        
        $districtResult=DB::table('regions as r')
                    ->join('districts as d', 'r.id','=','d.regionId')
                    ->select('r.regionName','d.districtName','d.id')
                    ->orderBy('r.regionName','ASC')
                    ->paginate(5);                 
        return view('admin.showDistrict', compact('districtResult'));
    }
   public function createDistrict()
    {
        
        $regionResult=DB::table('regions')->get();
        return view('admin.createDistrict', compact('regionResult'));
    }
   public function saveDistrict( Request $request)
    {
        
        $data=array();
        $data['regionId']=$request->regionId;
        $data['districtName']=$request->districtName;
        $result=DB::table('districts')->insert($data);
        if ($result) {
               Session::put('message','District Save successfully!!');
               return redirect()->back();
        }else{
             Session::put('message','District Save Failed!!');
             return redirect()->back();
        }
    } 
  public function editDistrict($id,$page)
    {
        
        $singleDistrictInfo = DB::table('districts')
                            ->Where('id', $id)
                            ->first();
         $regionResult=DB::table('regions')->get();
         return view('admin.editDistrict',compact('singleDistrictInfo','page','regionResult'));
    }
  public function updateDistrict(Request $request)
    {
        
        $id=$request->id;
        $page=$request->page;


         $data=array();
         $data['districtName']=$request->districtName;
         $data['regionId']=$request->regionId;

         DB::table('districts')
                            ->where('id',$id)
                            ->update($data);

                           

        Session::put('message','District Update Successfully!!');

        return redirect('admin/show-district?page='.$page);

    }
  public function serachDistrict(Request $request){

        $search=$request->searchText;

         $areaInfo=DB::table('regions as r')
                    ->join('districts as d', 'r.id','=','d.regionId')
                    ->Where('r.regionName', 'like', '%' .$search. '%')
                    ->orWhere('d.districtName', 'like', '%' .$search. '%')
                    ->select('r.regionName','d.districtName','d.id')
                    ->orderBy('d.districtName','ASC')
                    ->limit(10)
                    ->get();

         
        echo json_encode($areaInfo);
       
    }
  public function deleteDistrict(Request $request)
    {
       
        $id=intval($request->id);
        $result=DB::table('districts')
              ->where('id', $id)
              ->delete();
           if ($result) {
                        echo json_encode('success');
                        exit();
                    }
    }
//District End Here...

//Branch Start Here..

    public function checkBranch(Request $request){

        $branch=$request->branch;

        $branchCheck=DB::table('branch')
                    ->where('branchName',$branch)
                    ->select('branch.branchName')
                    ->first();

  

        if(isset($branchCheck->branchName)){
                 
                echo json_encode("Found");
        }else{
            echo json_encode("Not Found");
        }
    }


    public function showBranch()
    {
    
    
        
      $branchInfo=DB::table('regions as r')
                    ->join('districts as d', 'r.id','=','d.regionId')
                    ->join('branch as b', 'd.id','=','b.districtId')
                    ->select('r.regionName','d.districtName','b.*')
                    ->orderBy('r.regionName','ASC')
                    ->paginate(5);


        return view('admin.showBranch',compact('branchInfo'));
    }
    public function createBranch()
    {
        
        $regionInfo=DB::table('regions')->get();
        return view('admin.createBranch', compact('regionInfo'));
    }
    public function saveBranch( Request $request)
    {
        $data=array();
        $data['branchName']=$request->areaName;
        $data['districtId']=$request->districtId;
        $result=DB::table('branch')
                    ->insert($data);
                    
       Session::put('message','Branch Save Successfully !!!');

        return redirect()->back();
    } 
   public function editBranch($areaId,$page)
    {
       
        $singleAreaInfo=DB::table('branch')
                    ->where('id',$areaId)
                    ->first();

        $districtId=$singleAreaInfo->districtId;

        $districtInfo=DB::table('districts')
                    ->where('id',$districtId)
                    ->first();

        $regionId=$districtInfo->regionId;

        $districtInfoForSelectedRegion=DB::table('districts')
                    ->where('regionId',$regionId)
                    ->get();

        $regionInfo=DB::table('regions')->get();

        

        return view('admin.editBranch',compact('singleAreaInfo','page','districtId','districtInfoForSelectedRegion','regionInfo','regionId','areaId'));
    }
   
   public function updateBranch(Request $request)
    {
        $id=$request->id;

        $page=$request->page;
        $areaInfo = DB::table('branch')
                            ->where('id', $id)
                            ->first();
        $data=array();
        if($request->districtId !=null){
            $data['districtId']=$request->districtId;
        }else{
           
            $data['districtId']=$areaInfo->districtId;
        }
        if($request->areaName !=null){
            $data['branchName']=$request->areaName;
        }else{
           
            $data['branchName']=$areaInfo->branchName;
        }
        
        DB::table('branch')
                ->where('id', $id)
                ->update($data);
        Session::put('message','Branch Update Successfully!!');

        return redirect('admin/show-branch?page='.$page);
       
    }
    public function getDistrict(Request $request){

        $id=intval($request->id);

       $districtInfo=DB::table('districts')
                            ->where('regionId',$id)
                            ->get();
        echo json_encode($districtInfo);
    }
    public function serachBranch(Request $request){

        $search=$request->searchText;

         $areaInfo=DB::table('regions as r')
                    ->join('districts as d', 'r.id','=','d.regionId')
                    ->join('branch as b', 'd.id','=','b.districtId')
                    ->Where('r.regionName', 'like', '%' .$search. '%')
                    ->orWhere('d.districtName', 'like', '%' .$search. '%')
                    ->orWhere('b.branchName', 'like', '%' .$search. '%')
                    ->select('r.regionName','d.districtName','b.branchName','b.id')
                    ->orderBy('r.regionName','ASC')
                    ->limit(10)
                    ->get();

         
        echo json_encode($areaInfo);
       
    }

   public function deleteBranch(Request $request)
    {
       
        $id=intval($request->id);
        $result=DB::table('branch')
              ->where('id', $id)
              ->delete();
           if ($result) {
                        echo json_encode('success');
                        exit();
                    }
    }
//Branch End Here....


/*Company Details start Here*/
      
      public function showCompanyDetails()
        {
            
          
        
            $row=DB::table('company_details')
                 ->orderBy('company_details.id','DESC')
                 ->paginate(4);
            return view('admin.showCompanyDetails',compact('row'));
        }
     public function createCompanyDetails()
    {
        
        return view('admin.createCompanyDetails');
    }
     public function saveCompanyDetails( Request $request)
    {

        $data=array();
        $data['companyName']=$request->companyName;
        $data['description']=$request->companyDetails;
        $data['active']=$request->active;
        $result=DB::table('company_details')->insert($data);
        if ($result) {
          Session::put('message','Company Details Save successfully!!');

          return redirect()->back();
          
        }else{
            Session::put('message','Company Details Save Failed !!!');
            return redirect()->back();
        }
       
    } 
    public function editCompanyDetails($id,$page)
    {
        
         $singleDetailsInfo = DB::table('company_details')
                            ->Where('id', $id)
                            ->first();
         return view('admin.editCompanyDetails',compact('singleDetailsInfo','page'));
        
    }
     public function updateCompanyDetails(Request $request)
    {
        
            $id=$request->id;
           $page=$request->page;


        $data=array();
        $data['companyName']=$request->companyName;
        $data['description']=$request->companyDetails;
        $data['active']=$request->active;

         DB::table('company_details')
            ->where('id',$id)
            ->update($data);

                           

        Session::put('message','Company Details Update successfully!!');

        return redirect('admin/show-company-details?page='.$page);
    }
     public function deleteCompanyDetails(Request $request)
    {
        $id=intval($request->id);
        $result=DB::table('company_details')
              ->where('id', $id)
              ->delete();
           if ($result) {
                        echo json_encode('success');
                        exit();
                    }
    }                       
                          

/*Payment Type End Here*/



}
