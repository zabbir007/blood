<?php

namespace App\Http\Middleware;

use Closure;
use Session;
if(!isset($_SESSION)) 
{ 
    session_start(); 
}

class DonorMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
       $donorId=Session::get('donorId');
       


        if($donorId){
            
            return redirect()->route('donorDashboard');
            
        }

        return $next($request);
    }
}
