<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




//user module start here.....

Route::prefix('user')->group(function () {

Route::get('show-account', 'SuperUserController@showAccount')->name('showAccount');
Route::get('login', 'UserController@login')->name('login');
Route::get('dashboard', 'SuperUserController@dashboard')->name('dashboard');
Route::post('signin', 'UserController@userSignIn')->name('userSignIn');
Route::get('user-log-logout', 'SuperUserController@userLogOut')->name('userLogOut');

//donor request
Route::get('show-donor-request', 'SuperUserController@donorRequest')->name('donorRequest');

//Add Test Amount
Route::post('search-test-user', 'SuperUserController@searchTestUser')->name('searchTestUser');
Route::get('create-amount', 'SuperUserController@createAmount')->name('createAmount');
Route::post('save-amount', 'SuperUserController@saveAmount')->name('saveAmount');
Route::get('show-amount', 'SuperUserController@showAmount')->name('showAmount');
Route::get('edit-amount/{id}/{page}', 'SuperUserController@editAmount')->name('editAmount');
Route::post('update-amount', 'SuperUserController@updateAmount')->name('updateAmount');
Route::post('delete-amount', 'SuperUserController@deleteAmount')->name('deleteAmount');
Route::post('search-amount', 'SuperUserController@serachAmount')->name('serachAmount');

//Add Indoor Donor..
Route::get('create-indoor-donor', 'SuperUserController@createIndoorDonor')->name('createIndoorDonor');
Route::post('add-indoor-donor', 'SuperUserController@addIndoorDonor')->name('addIndoorDonor');
Route::post('search-indoor-donor', 'SuperUserController@searchIndoorDonor')->name('searchIndoorDonor');
Route::get('show-indoor-donor/{id}','SuperUserController@showIndoorDonor')->name('showIndoorDonor');
Route::get('show-search-indoor-donor/{id}','SuperUserController@showSearchIndoorDonor')->name('showSearchIndoorDonor');
Route::post('update-indoor-donor', 'SuperUserController@updateIndoorDonor')->name('updateIndoorDonor');
Route::post('delete-indoor-donor', 'SuperUserController@deleteIndoorDonor')->name('deleteIndoorDonor');


//Add Indoor Donor English..
Route::get('create-indoor-donor-english', 'SuperUserController@createIndoorDonorEnglish')->name('createIndoorDonorEnglish');
Route::post('add-indoor-donor-english', 'SuperUserController@addIndoorDonorEnglish')->name('addIndoorDonorEnglish');
Route::post('search-indoor-donor-english', 'SuperUserController@searchIndoorDonorEnglish')->name('searchIndoorDonorEnglish');
Route::get('show-indoor-donor-english/{id}','SuperUserController@showIndoorDonorEnglish')->name('showIndoorDonorEnglish');
Route::get('show-search-indoor-donor-english/{id}','SuperUserController@showSearchIndoorDonorEnglish')->name('showSearchIndoorDonorEnglish');
Route::post('update-indoor-donor-english', 'SuperUserController@updateIndoorDonorEnglish')->name('updateIndoorDonorEnglish');
Route::post('delete-indoor-donor-english', 'SuperUserController@deleteIndoorDonorEnglish')->name('deleteIndoorDonorEnglish');



//Add Outdoor Donor..
Route::get('create-outdoor-donor', 'SuperUserController@createOutdoorDonor')->name('createOutdoorDonor');
Route::post('add-outdoor-donor', 'SuperUserController@addOutdoorDonor')->name('addOutdoorDonor');
Route::post('search-outdoor-donor', 'SuperUserController@searchOutdoorDonor')->name('searchOutdoorDonor');
Route::get('show-outdoor-donor/{id}','SuperUserController@showOutdoorDonor')->name('showOutdoorDonor');
Route::get('show-search-outdoor-donor/{id}','SuperUserController@showSearchOutdoorDonor')->name('showSearchOutdoorDonor');
Route::post('update-outdoor-donor', 'SuperUserController@updateOutdoorDonor')->name('updateOutdoorDonor');
Route::post('delete-outdoor-donor', 'SuperUserController@deleteOutdoorDonor')->name('deleteOutdoorDonor');


//show test
Route::get('show-test/{id}','SuperUserController@showTest')->name('showTest');
Route::post('get-test-amount','SuperUserController@getTestAmount')->name('getTestAmount');
Route::post('save-test-amount','SuperUserController@saveTestAmount')->name('saveTestAmount');
Route::get('show-test-amount/{id}','SuperUserController@showTestAmount')->name('showTestAmount');

//Show Test For Outdoor Donor..
Route::get('show-outdoor-test/{id}','SuperUserController@showOutdoorTest')->name('showOutdoorTest');
Route::post('get-outdoor-test-amount','SuperUserController@getOutdoorTestAmount')->name('getOutdoorTestAmount');
Route::post('save-outdoor-test-amount','SuperUserController@saveOutdoorTestAmount')->name('saveOutdoorTestAmount');
Route::get('show-outdoor-test-amount/{id}','SuperUserController@showOutdoorTestAmount')->name('showOutdoorTestAmount');


//Indoor Donor Test Report..
Route::get('show-indoor-donor-test-report', 'SuperUserController@showIndoorDonorTestReport')->name('showIndoorDonorTestReport');
Route::get('edit-indoor-donor-test-report/{id}/{page}', 'SuperUserController@editIndoorDonorTestReport')->name('editIndoorDonorTestReport');
Route::get('view-indoor-donor-test-report/{id}', 'SuperUserController@viewIndoorDonorTestReport')->name('viewIndoorDonorTestReport');
Route::post('update-indoor-donor-test-report', 'SuperUserController@updateIndoorDonorTestReport')->name('updateIndoorDonorTestReport');
Route::post('search-indoor-donor-test-report', 'SuperUserController@searchIndoorDonorTestReport')->name('searchIndoorDonorTestReport');
Route::post('delete-indoor-donor-test-report', 'SuperUserController@deleteIndoorDonorTestReport')->name('deleteIndoorDonorTestReport');


//Outdoor Donor Test Report...
Route::get('show-outdoor-donor-test-report', 'SuperUserController@showOutdoorDonorTestReport')->name('showOutdoorDonorTestReport');
Route::get('edit-outdoor-donor-test-report/{id}/{page}', 'SuperUserController@editOutdoorDonorTestReport')->name('editOutdoorDonorTestReport');
Route::get('view-outdoor-donor-test-report/{id}', 'SuperUserController@viewOutdoorDonorTestReport')->name('viewOutdoorDonorTestReport');
Route::post('update-outdoor-donor-test-report', 'SuperUserController@updateOutdoorDonorTestReport')->name('updateOutdoorDonorTestReport');
Route::post('search-outdoor-donor-test-report', 'SuperUserController@searchOutdoorDonorTestReport')->name('searchOutdoorDonorTestReport');
Route::post('delete-outdoor-donor-test-report', 'SuperUserController@deleteOutdoorDonorTestReport')->name('deleteOutdoorDonorTestReport');

//Show Indoor Donor Start Here..
Route::get('show-all-indoor-donor', 'SuperUserController@showAllIndoorDonor')->name('showAllIndoorDonor');
Route::post('delete-in-indoor-donor', 'SuperUserController@deleteInIndoorDonor')->name('deleteInIndoorDonor');
Route::post('active-indoor-donor', 'SuperUserController@activeIndoorDonor')->name('activeIndoorDonor');
Route::post('deactive-indoor-donor', 'SuperUserController@deactiveIndoorDonor')->name('deactiveIndoorDonor');
Route::post('search-in-indoor-donor', 'SuperUserController@searchInIndoorDonor')->name('searchInIndoorDonor');

//Show Outdoor Donor Start Here..
Route::get('show-all-outdoor-donor', 'SuperUserController@showAllOutdoorDonor')->name('showAllOutdoorDonor');
Route::post('delete-in-outdoor-donor', 'SuperUserController@deleteInOutdoorDonor')->name('deleteInOutdoorDonor');
Route::post('active-outdoor-donor', 'SuperUserController@activeOutdoorDonor')->name('activeOutdoorDonor');
Route::post('deactive-outdoor-donor', 'SuperUserController@deactiveOutdoorDonor')->name('deactiveOutdoorDonor');
Route::post('search-in-outdoor-donor', 'SuperUserController@searchInOutdoorDonor')->name('searchInOutdoorDonor');

//Show Normal Patient Start Here..
Route::get('show-all-normal-patient', 'SuperUserController@showAllNormalPatient')->name('showAllNormalPatient');
Route::post('delete-all-normal-patient', 'SuperUserController@deleteAllNormalPatient')->name('deleteAllNormalPatient');
Route::post('active-all-normal-patient', 'SuperUserController@activeAllNormalPatient')->name('activeAllNormalPatient');
Route::post('deactive-all-normal-patient', 'SuperUserController@deactiveAllNormalPatient')->name('deactiveAllNormalPatient');
Route::post('search-all-in-normal-patient', 'SuperUserController@searchAllInNormalPatient')->name('searchAllInNormalPatient');

//Show Thalassemia Patient Start Here..
Route::get('show-all-thalassemia-patient', 'SuperUserController@showAllThalassemiaPatient')->name('showAllThalassemiaPatient');
Route::post('delete-all-thalassemia-patient', 'SuperUserController@deleteAllThalassemiaPatient')->name('deleteAllThalassemiaPatient');
Route::post('active-all-thalassemia-patient', 'SuperUserController@activeAllThalassemiaPatient')->name('activeAllThalassemiaPatient');
Route::post('deactive-all-thalassemia-patient', 'SuperUserController@deactiveAllThalassemiaPatient')->name('deactiveAllThalassemiaPatient');
Route::post('search-all-in-thalassemia-patient', 'SuperUserController@searchAllInThalassemiaPatient')->name('searchAllInThalassemiaPatient');


//All Test Start Here....
Route::get('add-test', 'SuperUserController@addTest')->name('addTest');
Route::post('save-test', 'SuperUserController@saveTest')->name('saveTest');
Route::get('show-save-test/{id}','SuperUserController@showSaveTest')->name('showSaveTest');
Route::post('search-test', 'SuperUserController@searchTest')->name('searchTest');
Route::get('test-report', 'SuperUserController@testReport')->name('testReport');
Route::get('edit-test-report/{id}/{page}', 'SuperUserController@editTestReport')->name('editTestReport');
Route::get('view-test-report/{id}', 'SuperUserController@viewTestReport')->name('viewTestReport');
Route::post('update-test-report', 'SuperUserController@updateTestReport')->name('updateTestReport');
Route::post('search-test-report', 'SuperUserController@searchTestReport')->name('searchTestReport');
Route::post('delete-test-report', 'SuperUserController@deleteTestReport')->name('deleteTestReport');
Route::post('check-test-name', 'SuperUserController@checkTestName')->name('checkTestName');
//All Test End Here....


//Add Patient..
Route::get('create-patient', 'SuperUserController@createPatient')->name('createPatient');
Route::post('add-patient', 'SuperUserController@addPatient')->name('addPatient');
Route::post('search-patient', 'SuperUserController@searchPatient')->name('searchPatient');
Route::get('show-patient/{id}','SuperUserController@showPatient')->name('showPatient');
Route::get('show-search-patient/{id}','SuperUserController@showSearchPatient')->name('showSearchPatient');
Route::post('update-patient', 'SuperUserController@updatePatient')->name('updatePatient');
Route::post('delete-patient', 'SuperUserController@deletePatient')->name('deletePatient');


//Add Issue Voucher..
Route::get('create-issue-voucher', 'SuperUserController@createIssueVoucher')->name('createIssueVoucher');
Route::post('add-issue-voucher', 'SuperUserController@addIssueVoucher')->name('addIssueVoucher');
Route::post('search-issue-voucher', 'SuperUserController@searchIssueVoucher')->name('searchIssueVoucher');
Route::get('show-issue-voucher/{id}','SuperUserController@showIssueVoucher')->name('showIssueVoucher');
Route::get('show-search-issue-voucher/{id}','SuperUserController@showSearchIssueVoucher')->name('showSearchIssueVoucher');
Route::post('update-issue-voucher', 'SuperUserController@updateIssueVoucher')->name('updateIssueVoucher');
Route::post('delete-issue-voucher', 'SuperUserController@deleteIssueVoucher')->name('deleteIssueVoucher');

//Add Receipt Voucher
Route::get('create-receipt-voucher', 'SuperUserController@createReceiptVoucher')->name('createReceiptVoucher');
Route::post('add-receipt-voucher', 'SuperUserController@addReceiptVoucher')->name('addReceiptVoucher');
Route::post('search-receipt-voucher', 'SuperUserController@searchReceiptVoucher')->name('searchReceiptVoucher');
Route::get('show-receipt-voucher/{id}','SuperUserController@showReceiptVoucher')->name('showReceiptVoucher');
Route::get('show-search-receipt-voucher/{id}','SuperUserController@showSearchReceiptVoucher')->name('showSearchReceiptVoucher');
Route::post('update-receipt-voucher', 'SuperUserController@updateReceiptVoucher')->name('updateReceiptVoucher');
Route::post('delete-receipt-voucher', 'SuperUserController@deleteReceiptVoucher')->name('deleteReceiptVoucher');

//Total Blood Reservation
Route::get('total-blood-reservation', 'SuperUserController@totalBloodReservation')->name('totalBloodReservation');

//show Money Receipt
Route::get('show-money-receipt/{id}','SuperUserController@showMoneyReceipt')->name('showMoneyReceipt');

/* Add Store Equipment Start Here */

Route::get('create-equipment', 'SuperUserController@createEquipment')->name('createEquipment');
Route::post('save-equipment', 'SuperUserController@saveEquipment')->name('saveEquipment');
Route::get('show-equipment', 'SuperUserController@showEquipment')->name('showEquipment');
Route::get('edit-equipment/{id}/{page}', 'SuperUserController@editEquipment')->name('editEquipment');
Route::post('update-equipment', 'SuperUserController@updateEquipment')->name('updateEquipment');
Route::post('delete-equipment', 'SuperUserController@deleteEquipment')->name('deleteEquipment');

/* Add Store Equipment End Here */


/* Account Module Start Here */
Route::get('show-payment-vouchaer','SuperUserController@showPaymentVouchaer')->name('showPaymentVouchaer');
Route::get('add-payment-vouchaer','SuperUserController@addPaymentVouchaer')->name('addPaymentVouchaer');
Route::get('view-payment-vouchaer/{id}','SuperUserController@viewPaymentVouchaer')->name('viewPaymentVouchaer');
Route::post('save-payment-vouchaer','SuperUserController@savePaymentVouchaer')->name('savePaymentVouchaer');

Route::get('show-receipt-vouchaer','SuperUserController@showReceiptVouchaer')->name('showReceiptVouchaer');
Route::get('add-receipt-vouchaer','SuperUserController@addReceiptVouchaer')->name('addReceiptVouchaer');
Route::get('view-receipt-vouchaer/{id}','SuperUserController@viewReceiptVouchaer')->name('viewReceiptVouchaer');
Route::post('save-receipt-vouchaer','SuperUserController@saveReceiptVouchaer')->name('saveReceiptVouchaer');
//statistics show Here.
Route::get('show-statistics','SuperUserController@showStatistics')->name('showStatistics');

/* Account Module End Here */

/* Pyment Type Start**/
    Route::get('create-payment-type', 'SuperUserController@createPaymentType')->name('createPaymentType');
    Route::post('save-payment-type', 'SuperUserController@savePaymentType')->name('savePaymentType');
    Route::get('show-payment-type', 'SuperUserController@showPaymentType')->name('showPaymentType');
    Route::get('edit-payment-type/{id}/{page}', 'SuperUserController@editPaymentType')->name('editPaymentType');
    Route::post('update-payment-type', 'SuperUserController@updatePaymentType')->name('updatePaymentType');
    Route::post('delete-payment-type', 'SuperUserController@deletePaymentType')->name('deletePaymentType');
/* Payment Type End**/

/* Account Leager Start**/
    Route::get('create-account-leager', 'SuperUserController@createAccountLeager')->name('createAccountLeager');
    Route::post('save-account-leager', 'SuperUserController@saveAccountLeager')->name('saveAccountLeager');
    Route::get('show-account-leager', 'SuperUserController@showAccountLeager')->name('showAccountLeager');
    Route::get('edit-account-leager/{id}/{page}', 'SuperUserController@editAccountLeager')->name('editAccountLeager');
    Route::post('update-account-leager', 'SuperUserController@updateAccountLeager')->name('updateAccountLeager');
    Route::post('delete-account-leager', 'SuperUserController@deleteAccountLeager')->name('deleteAccountLeager');
/* Account Leager End**/
    Route::post('get-leager', 'SuperUserController@getLeager')->name('getLeager');


/* Item Start */
    Route::get('create-item', 'SuperUserController@createItem')->name('createItem');
    Route::post('save-item', 'SuperUserController@saveItem')->name('saveItem');
    Route::get('show-item', 'SuperUserController@showItem')->name('showItem');
    Route::get('edit-item/{id}/{page}', 'SuperUserController@editItem')->name('editItem');
    Route::post('update-item', 'SuperUserController@updateItem')->name('updateItem');
    Route::post('delete-item', 'SuperUserController@deleteItem')->name('deleteItem');
/* Item End**/

/* Item Start */
    Route::get('create-item-name', 'SuperUserController@createItemName')->name('createItemName');
    Route::post('save-item-name', 'SuperUserController@saveItemName')->name('saveItemName');
    Route::get('show-item-name', 'SuperUserController@showItemName')->name('showItemName');
    Route::get('edit-item-name/{id}/{page}', 'SuperUserController@editItemName')->name('editItemName');
    Route::post('update-item-name', 'SuperUserController@updateItemName')->name('updateItemName');
    Route::post('delete-item-name', 'SuperUserController@deleteItemName')->name('deleteItemName');
/* Item End**/
    Route::post('get-item-name', 'SuperUserController@getItemName')->name('getItemName');
   
/* Stock Voucher Entry Start Here */
    Route::get('show-stock-voucher-entry','SuperUserController@showStockVoucherEntry')->name('showStockVoucherEntry');
    
    Route::get('view-stock-voucher-entry/{id}','SuperUserController@viewStockVoucherEntry')->name('viewStockVoucherEntry');

    Route::get('create-stock-voucher-entry','SuperUserController@createStockVoucherEntry')->name('createStockVoucherEntry');
    Route::post('save-stock-voucher-entry','SuperUserController@saveStockVoucherEntry')->name('saveStockVoucherEntry');
/* Stock Voucher Entry End Here */

/* Donor Recozation Start Here */
Route::get('create-donor-recozation', 'SuperUserController@createDonorRecozation')->name('createDonorRecozation');
Route::post('save-donor-recozation', 'SuperUserController@saveDonorRecozation')->name('saveDonorRecozation');
Route::get('show-donor-recozation', 'SuperUserController@showDonorRecozation')->name('showDonorRecozation');
Route::get('view-donor-recozation/{id}', 'SuperUserController@viewDonorRecozation')->name('viewDonorRecozation');
Route::post('delete-donor-recozation', 'SuperUserController@deleteDonorRecozation')->name('deleteDonorRecozation');
/* Donor Recozation End Here */

/* journal */

Route::get('show-journal-vouchaer','SuperUserController@showJournalVouchaer')->name('showJournalVouchaer');
Route::get('add-journal-vouchaer','SuperUserController@addJournalVouchaer')->name('addJournalVouchaer');
Route::get('view-journal-vouchaer/{id}','SuperUserController@viewJournalVouchaer')->name('viewJournalVouchaer');
Route::post('save-journal-vouchaer','SuperUserController@saveJournalVouchaer')->name('saveJournalVouchaer');

Route::get('show-stock-report','SuperUserController@showStockReport')->name('showStockReport');
Route::get('view-stock-report','SuperUserController@viewStockReport')->name('viewStockReport');


});


//user module end here.....




//admin module start here.....

Route::prefix('admin')->group(function () {


/* Company Details Start */
Route::get('create-company-details', 'SuperAdminController@createCompanyDetails')->name('createCompanyDetails');
Route::post('save-company-details', 'SuperAdminController@saveCompanyDetails')->name('saveCompanyDetails');
Route::get('show-company-details', 'SuperAdminController@showCompanyDetails')->name('showCompanyDetails');
Route::get('edit-company-details/{id}/{page}', 'SuperAdminController@editCompanyDetails')->name('editCompanyDetails');
Route::post('update-company-details', 'SuperAdminController@updateCompanyDetails')->name('updateCompanyDetails');
Route::post('delete-company-details', 'SuperAdminController@deleteCompanyDetails')->name('deleteCompanyDetails');
/* Company Details End */

Route::get('login', 'AdminController@login')->name('adminLogin');
Route::get('dashboard', 'SuperAdminController@dashboard')->name('adminDashboard');
Route::post('signin', 'AdminController@adminSignIn')->name('adminSignIn');
Route::get('admin-log-logout', 'SuperAdminController@adminLogOut')->name('adminLogOut');

/*Add User start Here..*/
Route::get('admin-add-user', 'SuperAdminController@adminAddUser')->name('adminAddUser');
Route::post('admin-save-user', 'SuperAdminController@adminSaveUser')->name('adminSaveUser');
Route::get('admin-show-user', 'SuperAdminController@adminShowAllUser')->name('adminShowAllUser');
Route::post('delete-user', 'SuperAdminController@deleteUser')->name('deleteUser');
Route::post('active-user', 'SuperAdminController@activeUser')->name('activeUser');
Route::post('deactive-user', 'SuperAdminController@deactiveUser')->name('deactiveUser');
Route::post('search-user', 'SuperAdminController@searchUser')->name('searchUser');


Route::post('check-user-email', 'SuperAdminController@checkUserEmail')->name('checkUserEmail');
Route::post('check-user-phone', 'SuperAdminController@checkUserPhone')->name('checkUserPhone');

/*Add User End Here..*/

/*Add Address Start here...*/

//Region
Route::get('create-region', 'SuperAdminController@createRegion')->name('createRegion');
Route::post('save-region', 'SuperAdminController@saveRegion')->name('saveRegion');
Route::get('show-region', 'SuperAdminController@showRegion')->name('showRegion');
Route::get('edit-region/{id}/{page}', 'SuperAdminController@editRegion')->name('editRegion');
Route::post('update-region', 'SuperAdminController@updateRegion')->name('updateRegion');
Route::post('delete-region', 'SuperAdminController@deleteRegion')->name('deleteRegion');
Route::post('search-region', 'SuperAdminController@serachRegion')->name('serachRegion');
Route::post('check-region', 'SuperAdminController@checkRegion')->name('checkRegion');

//district
Route::get('create-district', 'SuperAdminController@createDistrict')->name('createDistrict');
Route::post('save-district', 'SuperAdminController@saveDistrict')->name('saveDistrict');
Route::get('show-district', 'SuperAdminController@showDistrict')->name('showDistrict');
Route::get('edit-district/{id}/{page}', 'SuperAdminController@editDistrict')->name('editDistrict');
Route::post('update-district', 'SuperAdminController@updateDistrict')->name('updateDistrict');
Route::post('delete-district', 'SuperAdminController@deleteDistrict')->name('deleteDistrict');
Route::post('search-district', 'SuperAdminController@serachDistrict')->name('serachDistrict'); 
Route::post('check-district', 'SuperAdminController@checkDistrict')->name('checkDistrict');
/*Add Address End here...*/

/* Add Branch Start Here....*/
Route::get('create-branch', 'SuperAdminController@createBranch')->name('createBranch');
Route::post('save-branch', 'SuperAdminController@saveBranch')->name('saveBranch');
Route::get('show-branch', 'SuperAdminController@showBranch')->name('showBranch');
Route::get('edit-branch/{id}/{page}', 'SuperAdminController@editBranch')->name('editBranch');
Route::post('update-branch', 'SuperAdminController@updateBranch')->name('updateBranch');
Route::post('delete-branch', 'SuperAdminController@deleteBranch')->name('deleteBranch');
Route::post('get-district', 'SuperAdminController@getDistrict')->name('getDistrict');
Route::post('search-branch', 'SuperAdminController@serachBranch')->name('serachBranch');
Route::post('check-branch', 'SuperAdminController@checkBranch')->name('checkBranch');
/* Add Branch End Here...*/

});

//admin module end here.....



//donor module start here.....

Route::prefix('donor')->group(function () {

Route::get('register', 'DonorController@donorRegister')->name('donorRegister');
Route::post('save-donor', 'DonorController@saveDonor')->name('saveDonor');
Route::get('dashboard', 'SuperDonorController@donorDashboard')->name('donorDashboard');
Route::get('login', 'DonorController@donorLogin')->name('donorLogin');
Route::post('signin', 'DonorController@donorSignIn')->name('donorSignIn');
Route::get('donor-logout', 'SuperDonorController@donorLogOut')->name('donorLogOut');



});

//donor module end here.....


//patient module start here.....

Route::prefix('patient')->group(function () {

Route::get('register', 'PatientController@patientRegister')->name('patientRegister');
Route::post('save-patient', 'PatientController@savePatient')->name('savePatient');
Route::get('dashboard', 'SuperPatientController@patientDashboard')->name('patientDashboard');
Route::get('login', 'PatientController@patientLogin')->name('patientLogin');
Route::post('signin', 'PatientController@patientSignIn')->name('patientSignIn');
Route::get('patient-logout', 'SuperPatientController@patientLogOut')->name('patientLogOut');



});

//patient module end here.....
