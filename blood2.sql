-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 04, 2020 at 08:33 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `blood2`
--

-- --------------------------------------------------------

--
-- Table structure for table `account_leager`
--

CREATE TABLE `account_leager` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `userId` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `accountGroup` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `accountLeager` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `account_leager`
--

INSERT INTO `account_leager` (`id`, `userId`, `accountGroup`, `accountLeager`, `created_at`, `updated_at`) VALUES
(2, '1', 'Bank', 'dhaka bank', NULL, NULL),
(3, '1', 'Bank', 'dhaka bank1', NULL, NULL),
(4, '1', 'Bank', 'dhaka bank2', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `email`, `password`, `created_at`, `updated_at`) VALUES
(1, 'zabbirhossain727@gmail.com', '25d55ad283aa400af464c76d713c07ad', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `all_test`
--

CREATE TABLE `all_test` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `userId` int(255) DEFAULT NULL,
  `date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `age` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sex` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `group` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `referance` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ward` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bed` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `testId` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remainingAmount` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `totalAmount` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `paidAmount` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `testReport` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `testNumber` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `barCodeImage` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `all_test`
--

INSERT INTO `all_test` (`id`, `userId`, `date`, `name`, `age`, `sex`, `group`, `referance`, `ward`, `bed`, `testId`, `remainingAmount`, `totalAmount`, `paidAmount`, `testReport`, `testNumber`, `barCodeImage`, `created_at`, `updated_at`) VALUES
(1, 2, '12\\12\\2019', 'Zabbir Hossain', '23', 'male', '', 'dhaka clinic', '3', '12', '1, 3', NULL, '', '', 'positive, positive', '956984', 'barCodeImg/956984.png', NULL, NULL),
(2, 2, '12\\12\\2019', 'Zabbir Hossain', '23', 'male', '', 'dhaka clinic', '3', '3', '4', NULL, '', '', NULL, '88203254', 'barCodeImg/88203254.png', NULL, NULL),
(3, 2, '12\\12\\2019', 'Zabbir Hossain', '23', 'male', '', 'dhaka clinic', '3', '23', '1, 2', NULL, '', '', NULL, '89307554', 'barCodeImg/89307554.png', NULL, NULL),
(4, 2, '12\\12\\2019', 'Zabbir Hossain', '23', 'male', '', 'dhaka clinic', '3', '34', '1, 2', NULL, '60', '55', NULL, '85564082', 'barCodeImg/85564082.png', NULL, NULL),
(5, NULL, '12\\12\\2019', 'Zabbir Hossain', '23', 'male', '', 'dhaka clinic', '3', '45', '1, 3', NULL, '80', '45', 'positive, positive', '60644697', 'barCodeImg/60644697.png', NULL, NULL),
(6, NULL, '12\\12\\2019', 'Zabbir Hossain', '23', 'male', '', 'dhaka clinic', '3', '34', '1, 3, 4', NULL, '122', '120', NULL, '73669845', 'barCodeImg/73669845.png', NULL, NULL),
(7, NULL, '12\\12\\2019', 'sdfg', 'sdf', 'sdf', '', 'sdf', '32', '34', '1, 2', NULL, '60', '50', NULL, '60895262', 'barCodeImg/60895262.png', NULL, NULL),
(8, NULL, '12\\12\\2019', 'Zabbir Hossain', '23', 'Male', NULL, 'dhaka clinic', '3', 'dhaka', '1, 2', NULL, '60', '45', NULL, '35465282', 'barCodeImg/35465282.png', NULL, NULL),
(9, 1, '12\\12\\2019', 'Zabbir Hossain', '23', 'Male', 'bp', 'dhaka clinic', '3', 'dhaka', '1, 3', '10', '30', '20', NULL, '57070017', 'barCodeImg/57070017.png', NULL, NULL),
(10, 1, '12\\12\\2019', 'Zabbir Hossain', '23', 'Male', 'ap', 'dhaka clinic', '3', '34', '1, 4', '10', '30', '20', 'positive, positive', '68869285', 'barCodeImg/68869285.png', NULL, NULL),
(11, 1, '12\\12\\2019', 'Zabbir Hossain', '23', 'Male', 'abp', 'asdfs', '3', '4', '1, 3', '30', '30', NULL, NULL, '22389283', 'barCodeImg/22389283.png', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `branch`
--

CREATE TABLE `branch` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `districtId` int(11) NOT NULL,
  `branchName` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `branch`
--

INSERT INTO `branch` (`id`, `districtId`, `branchName`, `created_at`, `updated_at`) VALUES
(1, 1, 'Mirpu-10 ward-11', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `company_details`
--

CREATE TABLE `company_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `companyName` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `company_details`
--

INSERT INTO `company_details` (`id`, `companyName`, `description`, `active`, `created_at`, `updated_at`) VALUES
(1, 'Dhaka Red Blood Company', 'We Provide Blood', '1', NULL, NULL),
(2, 'Tomattos', 'We Provide Blood', '0', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `districts`
--

CREATE TABLE `districts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `regionId` int(11) NOT NULL,
  `districtName` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `districts`
--

INSERT INTO `districts` (`id`, `regionId`, `districtName`, `created_at`, `updated_at`) VALUES
(1, 3, 'Mirpur', NULL, NULL),
(2, 3, 'Tangail', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `donor`
--

CREATE TABLE `donor` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `branchId` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `donor_recozation`
--

CREATE TABLE `donor_recozation` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `userId` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `age` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sex` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `group` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `referance` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bag` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `donor_recozation`
--

INSERT INTO `donor_recozation` (`id`, `userId`, `date`, `name`, `age`, `sex`, `group`, `referance`, `phone`, `bag`, `created_at`, `updated_at`) VALUES
(2, '1', '2019-07-04', 'zabbir Hossain', '23', 'Male', 'op', 'dhaka clinic', '01933722564', '3', NULL, NULL),
(3, '1', '2019-07-09', 'zabbir Hossain', '23', 'Male', 'on', 'dhaka clinic', '1933722564', '01933722564', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `equipment`
--

CREATE TABLE `equipment` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `branchName` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `balanceType` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `userId` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `itemGroup` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `itemName` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantity` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rate` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `totalAmount` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `equipment`
--

INSERT INTO `equipment` (`id`, `branchName`, `balanceType`, `date`, `userId`, `itemGroup`, `itemName`, `quantity`, `rate`, `amount`, `totalAmount`, `created_at`, `updated_at`) VALUES
(4, 'Mirpu-10 ward-11', 'opening', '13-07-2019', '1', NULL, 'Table, Table', '1, 1', '12, 12', '12, 12', '24', NULL, NULL),
(5, 'Mirpu-10 ward-11', 'opening', '13-07-2019', '1', NULL, 'Table, Table', '1, 1', '12, 12', '12, 12', '24', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `indoor_donor`
--

CREATE TABLE `indoor_donor` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `userId` int(255) DEFAULT NULL,
  `fatherOrHusband` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `age` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sex` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `education` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address4` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `barCodeImage` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nibondonNo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bloodGroup` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(20) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `indoor_donor`
--

INSERT INTO `indoor_donor` (`id`, `name`, `userId`, `fatherOrHusband`, `age`, `sex`, `education`, `address1`, `address2`, `phone`, `address3`, `address4`, `phone2`, `description`, `date`, `barCodeImage`, `nibondonNo`, `bloodGroup`, `status`, `created_at`, `updated_at`) VALUES
(1, 'zabbir', 1, 'asdf', '343', 'asdas', 'asdas', 'asdas', 'asdf', '23423423432', 'asdfas', 'asdas', '23142', 'asdas', '12\\12\\2018', 'barCodeImg/137989.png', '137989', 'on', 1, NULL, NULL),
(2, 'sdf', 1, 'sdf', 'sdf', 'sdf', 'sdf', 'sdf', 'sdf', '2346575665', 'dfsd', 'dsf', '6575675675656', 'fdgdf', '12\\12\\2019', 'barCodeImg/807202.png', '807202', 'on', 1, NULL, NULL),
(3, 'sf', 2, 'dsf', '34', 'sad', 'sad', 'sad', 'sad', '1933722564', 'asdfas', 'fsd', '01933722564', 'zabbir', '12\\12\\2019', 'barCodeImg/715131.png', '715131', 'on', 1, NULL, NULL),
(4, 'zabbir', 3, 'fazlur', '23', 'male', 'sad', 'sad jhjh', 'dhaka', '01933722564', 'fgdg', NULL, '01933722564', 'yes', '12\\12\\2019', 'barCodeImg/183019.png', '183019', 'bp', 1, NULL, NULL),
(5, 'zabbir Hossain', NULL, 'sdasd', '23', 'male', 'sad', 'moghbazer', 'Dhaka', '1933722564', 'asdfas', NULL, '01933722564', 'zabbir', '12\\12\\2019', 'barCodeImg/404311.png', '404311', 'bn', 1, NULL, NULL),
(6, 'zabbir Hossain', NULL, 'sdf', '23', 'Male', 'sad', 'moghbazer', 'Dhaka', '1933722564', 'asdfas', NULL, '01933722564', 'zabbir', '12\\12\\2019', 'barCodeImg/14190821.png', '14190821', 'bp', 1, NULL, NULL),
(7, 'dfsa', NULL, 'asds', 'asd', 'Male', 'asd', 'asd', 'asd', '1933722564', 'asd', 'asd', '01933722564', 'asd', '12\\12\\2019', 'barCodeImg/74709212.png', '74709212', 'on', 1, NULL, NULL),
(8, 'zabbir Hossain', 1, 'fc', '23', 'Male', 'sad', 'moghbazer, Dhaka, Dhaka, Dhaka, Dhaka, Dhaka', 'Dhaka', '1933722564', 'asd', 'asdas', '01933722544', 'zabbir', '2019-06-27', 'barCodeImg/49628805.png', '49628805', 'on', 1, NULL, NULL),
(9, 'zabbir Hossain', 1, 'fc', '23', 'Male', 'sad', 'moghbazer, Dhaka, Dhaka, Dhaka, Dhaka, Dhaka', 'Dhaka', '1933722564', 'asdfas', NULL, '01933722564', 'zabbir', '12\\12\\2019', 'barCodeImg/78365363.png', '78365363', 'bp', 1, NULL, NULL),
(10, 'zabbir Hossain', 1, 'fc', '23', 'Male', 'sad', 'moghbazer, Dhaka, Dhaka, Dhaka, Dhaka, Dhaka', '01933722564', '1933722234', 'moghbazer, Dhaka, Dhaka, Dhaka, Dhaka, Dhaka', '01933722564', '01933722564', 'zabbir', '2019-06-30', 'barCodeImg/38083691.png', '38083691', 'op', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `issue_voucher`
--

CREATE TABLE `issue_voucher` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `voucherNumber` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `userId` int(255) DEFAULT NULL,
  `referance` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ward` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bed` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `group` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bagNumber` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `transfusion` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `donorNumber` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `receiptNumber` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `receiverName` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `receiverAdd` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `receiverTel` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `receiverEm` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `issue_voucher`
--

INSERT INTO `issue_voucher` (`id`, `voucherNumber`, `date`, `name`, `userId`, `referance`, `ward`, `bed`, `group`, `bagNumber`, `transfusion`, `donorNumber`, `receiptNumber`, `receiverName`, `receiverAdd`, `receiverTel`, `receiverEm`, `created_at`, `updated_at`) VALUES
(2, '63390007', '12\\12\\2019', 'zabbir Hossain', 1, 'sdfsdf', '3', '4', 'on', '1', '5', '60644696', '23432df32', 'joy', 'dhaka, dhaka', '722564', 'zabbirhossain727@gmail.com', NULL, NULL),
(3, '95124335', NULL, 'zabbir Hossain', 1, 'zabbir Hossain', '3', '34', 'abn', '1', '23', '57070017', '23432df32', 'sdf sdf', 'dhaka, dhaka', '722564', 'zabbirhossain727@gmail.com', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `item`
--

CREATE TABLE `item` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `userId` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `itemName` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `item`
--

INSERT INTO `item` (`id`, `userId`, `itemName`, `created_at`, `updated_at`) VALUES
(2, '1', 'Table', NULL, NULL),
(3, '1', 'Chair', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `item_name`
--

CREATE TABLE `item_name` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `userId` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `itemGroup` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `itemName` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `item_name`
--

INSERT INTO `item_name` (`id`, `userId`, `itemGroup`, `itemName`, `created_at`, `updated_at`) VALUES
(1, '1', 'Chair', 'Chair2', NULL, NULL),
(2, '1', 'Table', 'Table', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `journal_vouchaer`
--

CREATE TABLE `journal_vouchaer` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `userId` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `branchName` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vouchaerNo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `taka` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `narration` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `leagerName` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dr` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cr` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `drAmount` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `crAmount` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `journal_vouchaer`
--

INSERT INTO `journal_vouchaer` (`id`, `date`, `userId`, `branchName`, `vouchaerNo`, `taka`, `narration`, `leagerName`, `dr`, `cr`, `drAmount`, `crAmount`, `created_at`, `updated_at`) VALUES
(2, '15-07-2019', '1', 'Mirpu-10 ward-11', '15839', 'Twelve Taka Only', 'sdfsa', 'dhaka bank, dhaka', '12, 0', '0, 12', '12', '12', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(23, '2019_03_31_081553_create_admin_table', 1),
(24, '2019_04_08_105951_create_users_table', 1),
(25, '2019_04_08_135059_create_donor_table', 1),
(26, '2019_04_09_055932_create_patient_table', 1),
(27, '2019_04_09_085511_create_regions_table', 1),
(28, '2019_04_09_085523_create_districts_table', 1),
(29, '2019_04_09_085538_create_branch_table', 1),
(30, '2019_04_10_053805_create_test_amount_table', 1),
(31, '2019_04_11_061229_create_indoor_donor_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `outdoor_donor`
--

CREATE TABLE `outdoor_donor` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `age` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sex` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `barCodeImage` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nibondonNo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bloodGroup` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `outdoor_donor`
--

INSERT INTO `outdoor_donor` (`id`, `name`, `age`, `sex`, `phone`, `barCodeImage`, `nibondonNo`, `bloodGroup`, `date`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Zabbir Hossain', '23', 'male', '01933722564', 'barCodeImg/315430.png', '315430', NULL, '12\\12\\2019', 1, NULL, NULL),
(2, NULL, '23', 'Male', '01933722564', 'barCodeImg/70444498.png', '70444498', 'abp', '12\\12\\2019', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `patient`
--

CREATE TABLE `patient` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `branchId` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `patients`
--

CREATE TABLE `patients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `userId` int(255) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `age` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `educationQualification` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address4` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bloodGroup` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `requestedBloodGroup` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `durationTime` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bloodNeedDate` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nibondonNo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `barCodeImage` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `messageDate` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `patients`
--

INSERT INTO `patients` (`id`, `userId`, `name`, `age`, `gender`, `educationQualification`, `email`, `mobile`, `address1`, `address2`, `address3`, `address4`, `bloodGroup`, `requestedBloodGroup`, `type`, `durationTime`, `bloodNeedDate`, `date`, `nibondonNo`, `barCodeImage`, `status`, `messageDate`, `created_at`, `updated_at`) VALUES
(1, 1, 'zabbir', '23', 'male', 'pass', 'zabbirhossain727@gmail.com', '0193', 'sdfsdf', 'dfsdf', 'sdf', 'sdf', 'op', 'bn', '1', '2', '12/1/2019', '12\\12\\2019', '83537980', 'barCodeImg/83537980.png', '1', '', NULL, NULL),
(2, 1, 'zabbir', '23', 'male', 'pass', 'zabbirhossain727@gmail.com', '01933', 'moghbazer', 'Dhaka', 'dfsd', 'sadf', 'on', 'abn', '2', '3', '', '12\\12\\2019', '52248598', 'barCodeImg/52248598.png', '1', '2019-06-23', NULL, NULL),
(3, 2, 'zabbir Hossain', '23', 'sdf sdf', 'pass', 'zabbirhossain727@gmail.com', '1933722564', 'dhaka', 'dhaka', 'asdfas', NULL, 'on', 'on', '2', '3', NULL, '12\\12\\2019', '23928299', 'barCodeImg/23928299.png', '1', '2019-07-23', NULL, NULL),
(4, 1, 'zabbir Hossain', '23', 'zabbir Hossain', 'pass', 'zabbirhossain727@gmail.com', '1933722564', 'moghbazer, Dhaka, Dhaka, Dhaka, Dhaka, Dhaka', 'Dhaka', 'asdfas', NULL, 'abn', 'on', '1', NULL, '12/1/2019', '12\\12\\2019', '57815357', 'barCodeImg/57815357.png', '1', NULL, NULL, NULL),
(5, 1, 'zabbir Hossain', '23', 'zabbir Hossain', 'pass', 'zabbirhossain727@gmail.com', '1933722564', 'moghbazer, Dhaka, Dhaka, Dhaka, Dhaka, Dhaka', 'Dhaka', 'asd', NULL, 'abn', 'on', '2', '2', NULL, '12\\12\\2019', '22894303', 'barCodeImg/22894303.png', '1', '2019-08-27', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `payment_type`
--

CREATE TABLE `payment_type` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `userId` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `paymentName` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `payment_type`
--

INSERT INTO `payment_type` (`id`, `userId`, `paymentName`, `created_at`, `updated_at`) VALUES
(4, '1', 'Bank', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `payment_vouchare`
--

CREATE TABLE `payment_vouchare` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `userId` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vouchareNo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `branchName` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `paymentTypeId` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sl` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `leager` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `totalAmount` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `taka` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `narration` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `payment_vouchare`
--

INSERT INTO `payment_vouchare` (`id`, `userId`, `vouchareNo`, `date`, `branchName`, `paymentTypeId`, `sl`, `leager`, `amount`, `totalAmount`, `taka`, `narration`, `created_at`, `updated_at`) VALUES
(1, '1', '97213', '01-07-2019', 'Mirpu-10 ward-11', '4', '1, 2', 'entertainment, entertainment', '200, 200', '400', 'Four Hundrad taka only', 'aigulo kina hoica', NULL, NULL),
(2, '1', '70719', '02-07-2019', 'Mirpu-10 ward-11', '4', '1, 2', 'entertainment, blood test', '200, 100', '300', 'Three Hundrad taka only', 'aigulo kina hoica', NULL, NULL),
(3, '1', '92404', '11-07-2019', 'Mirpu-10 ward-11', NULL, '1, 2', 'dhaka bank, dhaka', '12, 12', '24', 'Twenty Four Taka Only', 'sad', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `receipt_vouchare`
--

CREATE TABLE `receipt_vouchare` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `userId` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vouchareNo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `branchName` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `paymentTypeId` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sl` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `leager` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `totalAmount` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `taka` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `narration` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `receipt_vouchare`
--

INSERT INTO `receipt_vouchare` (`id`, `userId`, `vouchareNo`, `date`, `branchName`, `paymentTypeId`, `sl`, `leager`, `amount`, `totalAmount`, `taka`, `narration`, `created_at`, `updated_at`) VALUES
(1, '1', '44828', '01-07-2019', 'Mirpu-10 ward-11', '4', '1', 'blood test', '1000', '1000', 'one thousand taka only', 'blood test kora aita ana hoica', NULL, NULL),
(2, '1', '64880', '11-07-2019', 'Mirpu-10 ward-11', NULL, '1, 2, 3', 'dhaka bank, dhaka, dhaka', '12, 12, 12', '36', 'Thirty Six Taka Only', 'sadsa', NULL, NULL),
(3, '1', '53570', '11-07-2019', 'Mirpu-10 ward-11', NULL, '1, 2, 3', 'dhaka bank1, dhaka, dhaka', '12, 12, 12', '36', 'Thirty Six Taka Only', 'sdfsad', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `receipt_voucher`
--

CREATE TABLE `receipt_voucher` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `userId` int(255) DEFAULT NULL,
  `voucherNumber` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `referance` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ward` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bed` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `group` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bagNumber` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `transfusion` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `receipt_voucher`
--

INSERT INTO `receipt_voucher` (`id`, `userId`, `voucherNumber`, `date`, `referance`, `ward`, `bed`, `group`, `bagNumber`, `transfusion`, `created_at`, `updated_at`) VALUES
(2, 1, '123123123', '12312', '12312', '123', '123', 'on', '123', '1231', NULL, NULL),
(3, 1, '12312', '123', '123', '123', '123', 'on', '1231', '1231', NULL, NULL),
(4, 1, '46373030', '12\\12\\2019', 'dhaka clinic', '3', '3', 'abn', '1', '23', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `regions`
--

CREATE TABLE `regions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `regionName` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `regions`
--

INSERT INTO `regions` (`id`, `regionName`, `created_at`, `updated_at`) VALUES
(1, 'Rangpur', NULL, NULL),
(3, 'dhaka', NULL, NULL),
(4, 'sdfgds', NULL, NULL),
(5, 'asdfsaf', NULL, NULL),
(6, 'asdfsa', NULL, NULL),
(7, 'sadfsaf', NULL, NULL),
(8, 'asdfas', NULL, NULL),
(9, 'asdfasd', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `stock_voucher_entry`
--

CREATE TABLE `stock_voucher_entry` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `userId` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `invoiceNumber` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `balanceType` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `branchName` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `partyName` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sl` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `itemName` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantity` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rate` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `totalAmount` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `taka` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `narration` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `stock_voucher_entry`
--

INSERT INTO `stock_voucher_entry` (`id`, `userId`, `invoiceNumber`, `balanceType`, `date`, `branchName`, `partyName`, `sl`, `itemName`, `quantity`, `rate`, `amount`, `totalAmount`, `taka`, `narration`, `created_at`, `updated_at`) VALUES
(2, '1', '82444', 'stockin', '13-07-2019', 'Mirpu-10 ward-11', 'dhaka bank', '1, 2', 'Table, Table', '1, 1', '12, 12', '12, 12', '24', 'Twenty Four Taka Only', 'sadfasd', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `store_outdoor_test`
--

CREATE TABLE `store_outdoor_test` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `donorReg` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `testId` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `totalAmount` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `paidAmount` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `testReport` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `testDoctorName` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `store_outdoor_test`
--

INSERT INTO `store_outdoor_test` (`id`, `donorReg`, `testId`, `totalAmount`, `paidAmount`, `testReport`, `testDoctorName`, `created_at`, `updated_at`) VALUES
(1, '315430', '1, 2', NULL, NULL, 'Negative, positive', 'Zabbir', NULL, NULL),
(2, '315430', '1, 2', '60', '56', NULL, 'Not Test By Doctor', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `store_test`
--

CREATE TABLE `store_test` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `userId` int(255) DEFAULT NULL,
  `donorReg` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `testId` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remainingAmount` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `totalAmount` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `paidAmount` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `testReport` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `testDoctorName` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `store_test`
--

INSERT INTO `store_test` (`id`, `userId`, `donorReg`, `testId`, `remainingAmount`, `totalAmount`, `paidAmount`, `testReport`, `testDoctorName`, `created_at`, `updated_at`) VALUES
(3, 2, '137989', '1, 2, 3', NULL, NULL, NULL, 'positive, Negative, Negative', 'Saiful', NULL, NULL),
(4, 2, '807202', '1, 2', NULL, NULL, NULL, NULL, 'Not Test By Doctor', NULL, NULL),
(5, 2, '137989', '1, 3', NULL, NULL, NULL, NULL, 'Not Test By Doctor', NULL, NULL),
(6, 3, '137989', '1, 2, 3', NULL, NULL, NULL, NULL, 'Not Test By Doctor', NULL, NULL),
(7, 2, '715131', '1, 3', NULL, NULL, NULL, 'Negative, Negative', 'Zabbir', NULL, NULL),
(8, NULL, '183019', '1, 3', NULL, NULL, NULL, 'Negative, positive', 'Zabbir', NULL, NULL),
(9, NULL, '404311', '1, 3', NULL, NULL, NULL, NULL, 'Not Test By Doctor', NULL, NULL),
(11, NULL, '137989', '1, 4', NULL, '62', '61', NULL, 'Not Test By Doctor', NULL, NULL),
(12, 1, '404311', '1, 4', '30', '30', '', 'Negative, positive', 'Zabbir', NULL, NULL),
(13, 1, '74709212', '1, 2, 3', '10', '30', '20', 'positive, positive, positive', 'Zabbir', NULL, NULL),
(14, 1, '49628805', '1, 3, 4', '10', '30', '20', NULL, 'Not Test By Doctor', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `test_amount`
--

CREATE TABLE `test_amount` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `testName` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `testAmount` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `test_amount`
--

INSERT INTO `test_amount` (`id`, `testName`, `testAmount`, `created_at`, `updated_at`) VALUES
(1, 'Hbs', '20', NULL, NULL),
(2, 'HCB', '40', NULL, NULL),
(3, 'HIV', '660', NULL, NULL),
(4, 'hh', '42', NULL, NULL),
(5, 'Hipo', '120', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `branchId` int(11) NOT NULL,
  `status` int(255) NOT NULL DEFAULT '1',
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `phone`, `password`, `branchId`, `status`, `image`, `created_at`, `updated_at`) VALUES
(1, 'zabbir', 'zabbirhossain727@gmail.com', '01933722564', '25d55ad283aa400af464c76d713c07ad', 1, 1, 'user/img/lxBbcb4HnWZrJ1Z7cgru.png', NULL, NULL),
(2, 'Zabbir', 'zabbirhossain721@gmail.com', '01675518778', '25d55ad283aa400af464c76d713c07ad', 1, 1, 'user/img/lxBbcb4HnWZrJ1Z7cgru.png', NULL, NULL),
(3, 'zabbir', 'zabbirhossain009@gmail.com', '01876543567', '25d55ad283aa400af464c76d713c07ad', 1, 1, 'user/img/lxBbcb4HnWZrJ1Z7cgru.png', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account_leager`
--
ALTER TABLE `account_leager`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `all_test`
--
ALTER TABLE `all_test`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `branch`
--
ALTER TABLE `branch`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_details`
--
ALTER TABLE `company_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `districts`
--
ALTER TABLE `districts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `donor`
--
ALTER TABLE `donor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `donor_recozation`
--
ALTER TABLE `donor_recozation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `equipment`
--
ALTER TABLE `equipment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `indoor_donor`
--
ALTER TABLE `indoor_donor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `issue_voucher`
--
ALTER TABLE `issue_voucher`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `item`
--
ALTER TABLE `item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `item_name`
--
ALTER TABLE `item_name`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `journal_vouchaer`
--
ALTER TABLE `journal_vouchaer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `outdoor_donor`
--
ALTER TABLE `outdoor_donor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `patient`
--
ALTER TABLE `patient`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `patients`
--
ALTER TABLE `patients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_type`
--
ALTER TABLE `payment_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_vouchare`
--
ALTER TABLE `payment_vouchare`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `receipt_vouchare`
--
ALTER TABLE `receipt_vouchare`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `receipt_voucher`
--
ALTER TABLE `receipt_voucher`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `regions`
--
ALTER TABLE `regions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stock_voucher_entry`
--
ALTER TABLE `stock_voucher_entry`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `store_outdoor_test`
--
ALTER TABLE `store_outdoor_test`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `store_test`
--
ALTER TABLE `store_test`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `test_amount`
--
ALTER TABLE `test_amount`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `account_leager`
--
ALTER TABLE `account_leager`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `all_test`
--
ALTER TABLE `all_test`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `branch`
--
ALTER TABLE `branch`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `company_details`
--
ALTER TABLE `company_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `districts`
--
ALTER TABLE `districts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `donor`
--
ALTER TABLE `donor`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `donor_recozation`
--
ALTER TABLE `donor_recozation`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `equipment`
--
ALTER TABLE `equipment`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `indoor_donor`
--
ALTER TABLE `indoor_donor`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `issue_voucher`
--
ALTER TABLE `issue_voucher`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `item`
--
ALTER TABLE `item`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `item_name`
--
ALTER TABLE `item_name`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `journal_vouchaer`
--
ALTER TABLE `journal_vouchaer`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `outdoor_donor`
--
ALTER TABLE `outdoor_donor`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `patient`
--
ALTER TABLE `patient`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `patients`
--
ALTER TABLE `patients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `payment_type`
--
ALTER TABLE `payment_type`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `payment_vouchare`
--
ALTER TABLE `payment_vouchare`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `receipt_vouchare`
--
ALTER TABLE `receipt_vouchare`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `receipt_voucher`
--
ALTER TABLE `receipt_voucher`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `regions`
--
ALTER TABLE `regions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `stock_voucher_entry`
--
ALTER TABLE `stock_voucher_entry`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `store_outdoor_test`
--
ALTER TABLE `store_outdoor_test`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `store_test`
--
ALTER TABLE `store_test`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `test_amount`
--
ALTER TABLE `test_amount`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
