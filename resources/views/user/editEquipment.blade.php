@extends('layouts.user')

@section('title') Edit Store Equipment @endsection

@section('content')


<div class="row" style="margin-top: 20px;">
        <div class="col-lg-6 offset-3">
        	<a href="{{route('showEquipment')}}">
        	<button class="btn btn-primary" type="submit">Show All Equipment</button>
        	</a>
            <div class="card" style="margin-top: 10px;">
                <div class="card-body">
                	<h4 class="header-title" style="text-align: center">Edit Store Equipment</h4>
                    <form class="parsley-examples"  method="post" action="{{route('updateEquipment')}}" novalidate>
                       @csrf

                        
                        
                      <div class="form-group">
                            <label>Equipment Name</label>
                            <div>
                        		<input type="hidden" name="id" value="{{$singleEquipmentInfo->id}}" />
                        		<input type="hidden" name="page" value="{{$page}}" />
                                <input type="text" name="equipmentName" value="{{$singleEquipmentInfo->equipmentName}}" class="form-control parsley-validated" required
                                        data-parsley-required-message="Please Enter Equipment Name"   placeholder="Equipment Name"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Equipment Price</label>
                            <div>
                        
                                <input type="number" name="equipmentPrice" value="{{$singleEquipmentInfo->equipmentPrice}}" class="form-control parsley-validated" required
                                        data-parsley-required-message="Please Enter Equipment Price"   placeholder="Equipment Price"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Additional Price</label>
                            <div>
                        
                                <input type="number" name="additionalPrice" value="{{$singleEquipmentInfo->additionalPrice}}" class="form-control parsley-validated" required
                                        data-parsley-required-message="Please Enter Additional Price"   placeholder="Additional Price"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Quantity</label>
                            <div>
                        
                                <input type="number" name="quantity" value="{{$singleEquipmentInfo->quantity}}" class="form-control parsley-validated" required
                                        data-parsley-required-message="Please Enter Quantity"   placeholder="Quantity"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Remark</label>
                            <div>
                        
                                <textarea type="text" name="remark"  class="form-control parsley-validated" required
                                        data-parsley-required-message="Please Enter Your Remark"  placeholder="Remark">{{$singleEquipmentInfo->remark}}</textarea>
                            </div>
                        </div>
                        <button  class="btn btn-success" type="submit">Update</button>
                    </form>
                    
                </div> <!-- end card-body-->
            </div> <!-- end card-->
        </div> <!-- end col-->
    </div>


@endsection