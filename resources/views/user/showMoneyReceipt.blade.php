@extends('layouts.user')




@section('title') 

 
        Money Receipt
   

@endsection


@section('content')

<style type="text/css">
	.indoorDonor{
		
		background-color: #fff;
	}


	@media print {

	  body * {
	  	margin-top: 5px;
	    visibility: hidden;
	  }
	  
	  #printInvoice, #printInvoice * {
	    visibility: visible;
	  }
	  #notPrint, #notPrint *{
	  	visibility: hidden;
	  }
	  #searchTestBox, #searchTestBox *{
	  	visibility: hidden;
	  }
	  #searchIndoorDonor, #searchIndoorDonor *{
	  	visibility: hidden;
	  }
	  #printInvoice {
	    position: absolute;
	    left: 0;
	    top: 0;
	  }



	    
	 


	}

    #total {
      text-align:right;
   }

   #table {
      border:1px solid red;
      border-collapse:separate;
   }

   #table th, #table td {
      border:2px solid #000;
   }

</style>

<div id="printInvoice">
	 <div class="container ">

	 	<div id="searchIndoorDonor" style="margin-left: 50px; margin-top: 5px;">
	 		 @if($errors->any())
                    
                <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">
                   

                           <ul>
                               @foreach($errors->all() as $error)
                                    <li>{{$error}}</li>
                               @endforeach
                           </ul>
                       
                   
                </div>
                 @endif

                  <?php 
                    $messageWarning=Session::get('messageWarning');
                    if($messageWarning){

                        ?>
                        <div style="margin-top: 40px;" class="alert alert-warning alert-dismissible bg-warning text-white border-0 fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <?php
                                echo $messageWarning;
                                Session::put('messageWarning','');
                            ?>
                        </div>
                        <?php
                    
                }
                ?>

	 	</div>
<div class="row" style="margin-top: 5%;margin-left:5px;" >
<table border="0" width="100%"> 
	   <thead> 
	    <tr> 
	     <th style="width:100%">
		    <div class="row">
			  <div class="col-md-4"><img class="logo" height="80" width="100" src="{{asset('user/img/1.png')}}"></div>
			  <div class="col-md-7">
			  	<h5 style="color: red;text-align: left;">{{$companyName->companyName}}</h5>
                <h5>Branch : {{$branchName->branchName}}</h5>
                <h5>{{$companyName->description}}</h5>
			  </div>
			  <div class="col-md-1"><img style="margin-left: -35px;" class="logo" height="80" width="100" src="{{asset('user/img/2.png')}}"></div>
			</div>
	     </th> 
	   </tr> 
	   <tr> 
	    <th><hr style="color:#000080"/></th> 
	   </tr> 
	  </thead> 
	  <tbody> 
	    <tr> 
	      <td width="100%"> 
	<div class="col-12 ">

			<div class="indoorDonor">
			    <br/>
			    <div class="col-12" >
					
					<span style="margin-left: 600px;color: red;">Receipt number:</span>
					<span><input type="text" name="" readonly="" value="{{$moneyReceiptInfo->receiptNumber}}" value="" style=" height: 48px;width: 180px;"></span>
				</div>
			    <br/><br/>
				<div class="col-12">					
				
						<span style="margin-left: 600px;">Date:</span> 
						<span >
						<input name="date" readonly="" value="{{$moneyReceiptInfo->date}}" style="border:0; border-bottom: 1px  dotted; width:20%" type="text"  >
					    </span>
					   
				</div>
				<br/><br/>
				<div class="col-12">					
				
						<span style="margin-left: 3px;">Name Of Patient :</span> 
						<span>
						<input name="name" required readonly="" value="{{$moneyReceiptInfo->name}}" data-parsley-required-message="Please Enter Patient Name"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:86%" type="text"  >
					    </span>
				</div>
			    </br>
				<div class="col-12">					
				
						<span style="margin-left: 3px;">Name of the hospital/clinic:</span> 
						<span>
						<input name="referance" required readonly="" value="{{$moneyReceiptInfo->referance}}" data-parsley-required-message="Please Enter Hospital Or Clinic Name"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:80%" type="text"  >
					    </span>
				</div>
				<br/>
				<div class="col-12">					
				
						<span style="margin-left: 3px;">Ward/cabin number:</span> 
						<span>
						<input name="ward" required readonly="" value="{{$moneyReceiptInfo->ward}}" data-parsley-required-message="Please Enter Ward No"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:30%" type="text"  >
					    </span>
					    <span style="margin-left: 3px;">Bed number:</span> 
						<span>
						<input name="bed" required readonly="" value="{{$moneyReceiptInfo->bed}}" data-parsley-required-message="Please Enter Bed Number"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:30%" type="text"  >
					    </span>
				</div>
				
				
				<br/>
				<div class="col-12">					
				
						<span style="margin-left: 3px;">Blood group:</span> 
						<span>
						<input name="group" required readonly="" value="{{$moneyReceiptInfo->group}}" data-parsley-required-message="Please Enter Blood group"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:30%" type="text"  >
					    </span>
					    <span style="margin-left: 3px;">Number of blood bag:</span> 
						<span>
						<input name="bagNumber" required readonly="" value="{{$moneyReceiptInfo->bagNumber}}"  data-parsley-required-message="Please Enter Number of blood bag"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:30%" type="text"  >
					    </span>
				</div>


				<br/>
                <br/>
                <br/>
                <div class="col-12">
		            <div class="card">
		               
		                <div class="card-body">
		                   <div class="form-group position-relative mb-3">
		                        <label>Test Required :<span class="text-danger">*</span></label>
		                       <?php
		                            $firstAmount=0;
				                    $pkgCount=count($amount);
				                    for($i=0;$i<$pkgCount;$i++){
				                       
				                        $testAmountInfo=DB::table('test_amount')
										                  ->where('id',$amount[$i])
										                  ->get();
											foreach($testAmountInfo as $te){
									           echo $te->testName.' , ';
									           $firstAmount += $te->testAmount;
									        }
									         
				                    }
			                  
				            	?>

				            	<table id="table">
								  <thead>
								   <tr>
								     <th>Test Name</th>
								     <th>Amount</th>
								   </tr>
								  </thead>
								  <tbody>
								  	<?php
								  	 $pkgCount=count($amount);
				                    for($i=0;$i<$pkgCount;$i++){
				                       
				                        $testAmountInfo=DB::table('test_amount')
										                  ->where('id',$amount[$i])
										                  ->get();
                                      foreach($testAmountInfo as $te){
								  	?>
								   <tr>
								     <td><?php echo $te->testName; ?></td>
								     <td><?php echo $te->testAmount; ?></td>
								   </tr>
								   <?php
                                     }
                                    }
								   ?>
								  </tbody>
								  <tfoot>
								    <tr>
								      <th id="total" colspan="3">Total Amount:</th>
								      <td>{{$firstAmount.'  Taka'}}</td>
								     </tr>
								     <tr>
								      <th id="total" colspan="3">Total Paid Amount:</th>
								      <td>{{$paidAmount}}</td>
								     </tr>
								     <tr>
								      <th id="total" colspan="3">Total Remaining Amount:</th>
								      <td>{{$totalAmount-$paidAmount}}</td>
								     </tr>
								   
								   </tfoot>
								</table>
		                       
		                        
		                    </div>

		                
		                </div>
		            </div> <!-- end card-box-->
		        </div> <!-- end col -->

				
			</div>

		
	   
	</div> <!-- end col -->
	</td> 
   </tr>
 </tbody> 
</table>

	
	<div id="notPrint" style="margin-left: 10px; margin-top: 30px;">
        <button type="button"  class="btn btn-info printNewVotar"  onclick="printPage()">Print</button>
            	
 	</div>

</div>

 
<!-- end row --> 
</div>
</div>


 <script>
    
function printPage() {
  window.print();

}
</script>
@endsection