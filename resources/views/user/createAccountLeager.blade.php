@extends('layouts.user')

@section('title') Create Payment Leager @endsection

@section('content')
   <div class="row" style="margin-top: 20px;">
        <div class="col-lg-6 offset-3">
        	<a href="{{route('showAccountLeager')}}">
        	<button class="btn btn-primary" type="submit">Show All Account Leager</button>
        	</a>
            <div class="card" style="margin-top: 10px;">
                <div class="card-body">
                	<h4 class="header-title" style="text-align: center">Add Account Leager</h4>
                    <form class="parsley-examples"  method="post" action="{{route('saveAccountLeager')}}" novalidate>
                    	<?php 
                            $message=Session::get('message');
                            if($message){

                                ?>
                                <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <?php
                                        echo $message;
                                        Session::put('message','');
                                    ?>
                                </div>
                                <?php
                            
                        }
                        ?>
                        @if($errors->any())
                                    
                                    <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">
                                       

                                               <ul>
                                                   @foreach($errors->all() as $error)
                                                        <li>{{$error}}</li>
                                                   @endforeach
                                               </ul>
                                           
                                       
                                    </div>
                                    @endif
                       @csrf
                        <div class="form-group">
                            <label>Select Account Group</label>
                            <div>
                                <select class="selectpicker" name="accountGroup" data-style="btn-secondary">
                                    @foreach($accountGroup as $account) 
                                    <option value="{{$account->paymentName}}">{{$account->paymentName}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                      <div class="form-group">
                            <label>Name</label>
                            <div>
                        
                                <input type="text" name="accountLeager"  class="form-control parsley-validated" required
                                        data-parsley-required-message="Please Enter Account Leager"   placeholder="Account Leager"/>
                            </div>
                        </div>
                        <button class="btn btn-success" type="submit">Add</button>
                    </form>
                    
                </div> <!-- end card-body-->
            </div> <!-- end card-->
        </div> <!-- end col-->
    </div>

@endsection