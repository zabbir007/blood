@extends('layouts.user')
@section('title') 
    Create Payment Vouchaer
@endsection
@section('content')

<div id="printInvoice">
	<div class="container">
		<div class="row" style="margin-top: 5%;">
            <div class="col-md-12">
            	    <?php 
                    $message=Session::get('message');
                    if($message){
                        ?>
                        <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <?php
                                echo $message;
                                Session::put('message','');
                            ?>
                        </div>
                    <?php
                    }
                    ?>

                    <?php 
                        $message=Session::get('messageWarning');
                        if($message){
                            ?>
                        <div class="alert alert-warning alert-dismissible bg-warning text-white border-0 fade show" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            <?php
                                echo $message;
                                Session::put('messageWarning','');
                            ?>
                        </div>
                    <?php
                    }
                    ?>
            	<form class="parsley-examples"  method="post" action="{{route('saveStockVoucherEntry')}}" novalidate>
            		@csrf
            	<div class="card">
                    <div class="card-header">
                    	<div class="row">
                    		<div class="col-md-8">
                    		</div>
                    		<div class="col-md-4">
                                <div class="row">
                                    <div class="col-md-6">
                                        <span style="">Invoice No :</span>
                                    </div>
                                    <div class="col-md-6">
                                        <span><input type="text" name="invoiceNumber" value="{{$randomNumber}}" readonly=""></span>
                                    </div>
                                </div><br>
                                <div class="row">
                                    <div class="col-md-6">
                                        <span style="">Date :</span>
                                    </div>
                                    <div class="col-md-6">
                                        <span> <input type="text" readonly="" name="date" value="{{$currentDateTime}}"></span>
                                    </div>
                                </div>
                    			<br>
                                <div class="row">
                                    <div class="col-md-6">
                                        <span style="">Branch Name :</span>
                                    </div>
                                    <div class="col-md-6">
                                        <span><input type="text" readonly="" name="branchName" value="{{$stockInfo->branchName}}"></span>
                                    </div>
                                </div>
                    			<br>
                                <div class="row">
                                    <div class="col-md-6">
                                        <span style="">Party Name/Cash/Bank :</span>
                                    </div>
                                    <div class="col-md-6">
                                        <select name="partyName" required="">
                                            @foreach($accountLeager as $account)
                                            <option value="{{$account->accountLeager}}">
                                                {{$account->accountLeager}}
                                            </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                
                    		</div>
                    	</div>
                        <div class="row">
                            <div class="col-md-6">
                                
                            </div>
                            <div class="col-md-6">
                                <span>Balance Type</span>
                                <span>
                                    <select class="form-control parsley-validated" name="balanceType" data-style="btn-secondary">
                                        <option value="stockin">Stock In</option>
                                        <option value="stockout">Stock Out</option>
                                    </select>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered" style="width: 100%;" id="addRow">
							<thead>
							  <tr>
							    <th style="text-align: center;">SL</th>
							    <th style="text-align: center;">Item Name</th>
                                <th style="text-align: center;">Quantity</th>
                                <th style="text-align: center;">Rate</th>
							    <th style="text-align: center;">Amount</th>
							  </tr>
							</thead>
							<tbody>
							  <tr>
							    <td width="20%">
							    	<input required  data-parsley-required-message="Please Enter Serial Number"  data-style="btn-secondary" style="border:0;margin-bottom: 0px; border-bottom: 1px  none; width:100%;margin-left:none;" type="text" name="sl[]">
							    </td>
							    <td width="20%">
							    	<!-- <input required  data-parsley-required-message="Please Enter Item Name"  data-style="btn-secondary" style="border:0;margin-bottom: 0px; border-bottom: 1px  none; width:100%;margin-left:none;" type="text" name="itemName[]"> -->
                                    <select class="form-control parsley-validated" name="itemName[]" data-style="btn-secondary">
                                        @foreach($itemName as $item) 
                                       <option value="{{$item->itemName}}">
                                            {{$item->itemName}}
                                        </option>
                                        @endforeach
                                    </select>
							    </td>
							    <td width="20%">
							    	<input required  data-parsley-required-message="Please Enter Quantity"  data-style="btn-secondary" style="border:0;margin-bottom: 0px; border-bottom: 1px  none; width:100%;margin-left:none;" type="text" name="quantity[]">
							    </td>
                                <td width="20%">
                                    <input required  data-parsley-required-message="Please Enter Rate"  data-style="btn-secondary" style="border:0;margin-bottom: 0px; border-bottom: 1px  none; width:100%;margin-left:none;" type="text" name="rate[]">
                                </td>
                                <td width="20%">
                                    <input required class="rate" data-parsley-required-message="Please Enter Amount"  data-style="btn-secondary" style="border:0;margin-bottom: 0px; border-bottom: 1px  none; width:100%;margin-left:none;" type="text" name="amount[]">
                                </td>
							   
							  </tr>
							  <button class="btn-success" type="button" style="margin-bottom: 4px;" id="addAnotherNewStock">Add More</button>
							</tbody>
							<tfoot>
							    <tr>
							      <th id="total" colspan="4">Total :</th>
							      <td><input id="totalAmount" required  data-parsley-required-message="Please Enter Amount"  data-style="btn-secondary" style="border:0;margin-bottom: 0px; border-bottom: 1px  none; width:100%;margin-left:none;" type="text" name="totalAmount"></td>
							    </tr>
						    </tfoot>
						</table>
						<div class="row">
	                		<div class="col-md-6">
	                			<span>Taka :</span>
	                			<span>
	                				<input type="text" id="taka" name="taka"  class="form-control parsley-validated" required
                                        data-parsley-required-message="Please Enter Amount in Word"   placeholder="Amount In Word"/>
	                			</span>
	                		</div>
	                		<div class="col-md-6">
	                		</div>
	                	</div>
	                	<br>
	                	<div class="row">
	                		<div class="col-md-6">
	                			<span>Narration :</span>
	                			<span>
	                				<input type="text" name="narration"  class="form-control parsley-validated" required
                                        data-parsley-required-message="Please Enter Narration"   placeholder="Narration"/>
	                			</span>
	                		</div>
	                		<div class="col-md-6">
	                		</div>
	                	</div>
	                	<div style="margin-top: 2%;">
	                		<button class="btn btn-success" type="submit">Add</button>
	                	</div>
	                	 
                    </div>
                </div> <!-- end card-box-->
                </form>
            </div>
        </div>
	</div>
</div>
@endsection