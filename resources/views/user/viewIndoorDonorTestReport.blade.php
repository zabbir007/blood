@extends('layouts.user')




@section('title') 

 
        Show Test Report
   

@endsection


@section('content')

<style type="text/css">
	.indoorDonor{
		
		background-color: #fff;
	}


	@media print {

	  body * {
	  	margin-top: 5px;
	    visibility: hidden;
	  }

	  #footer {
    display: block; 
    position: fixed; 
    bottom: 0;
  } 
	  
	  #printInvoice, #printInvoice * {
	    visibility: visible;
	  }
	  #notPrint, #notPrint *{
	  	visibility: hidden;
	  }
	  #searchIndoorDonor, #searchIndoorDonor *{
	  	visibility: hidden;
	  }
	  #printInvoice {
	    position: absolute;
	    left: 0;
	    top: 0;
	  }





	}

</style>

<div id="printInvoice">
	 <div class="container ">

<div class="row" style="margin-top: 5%;margin-left:5px;" >
	<table border="0" width="100%"> 
	   <thead> 
	    <tr> 
	     <th style="width:100%">
		    <div class="row">
			  <div class="col-md-4"><img class="logo" height="80" width="100" src="{{asset('user/img/1.png')}}"></div>
			  <div class="col-md-7">
			  	<h5 style="color: red;text-align: left;">{{$companyName->companyName}}</h5>
                <h5>Branch : {{$branchName->branchName}}</h5>
                <h5>{{$companyName->description}}</h5>
			  </div>
			  <div class="col-md-1"><img style="margin-left: -35px;" class="logo" height="80" width="100" src="{{asset('user/img/2.png')}}"></div>
			</div>
	     </th> 
	   </tr> 
	   <tr> 
	    <th><hr style="color:#000080"/></th> 
	   </tr> 
	  </thead> 
	  <tbody> 
	    <tr> 
	      <td width="100%"> 
	<div class="col-12 ">
		<form class="parsley-examples" action="{{route('saveTestAmount')}}"  method="post"  >
			@csrf

			<div class="indoorDonor">
				<p style="text-align: right;">Test No- {{$testAmountInfo->id}}</p>
				<br/>
				<div class="col-12">					
				
						<span style="margin-left: 3px;">1.Donor Name </span> 
						<span>
						<input name="name" required  data-parsley-required-message="Please Enter Donor Name"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:82%" type="text" value="{{$indoorDonorInfo->name}}" >
					    </span>
				</div>
				<br/>
				<div class="col-12">					
				
						<span style="margin-left: 3px;">2.Donor Registration Number</span> 
						<span>
						<input name="nibondonNo" required  data-parsley-required-message="দয়া করে পিতা/ স্বামীর নাম লিখুন"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:74%" type="text" value="{{$indoorDonorInfo->nibondonNo}}" >
					    </span>
				</div>

				<br/>
				<div class="col-12">					
				
						<span style="margin-left: 3px;">3.Age </span> 
						<span>
						<input name="age" required  data-parsley-required-message="দয়া করে বয়স লিখুন"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:30%" type="text" value="{{$indoorDonorInfo->age}}" >
					    </span>
					    <span style="margin-left: 3px;">4.Gender </span> 
						<span>
						<input name="sex" required  data-parsley-required-message="দয়া করে লিঙ্গ লিখুন"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:30%" type="text" value="{{$indoorDonorInfo->sex}}" >
					    </span>
				</div>
               <br/>
               <br/>
               <br/>
                <div class="col-12">
		            <div class="card" >
		               
		                <div class="card-body">
		                   <div class="form-group position-relative mb-3">
		                        <label>Test Required :<span class="text-danger">*</span></label>
		                       <?php
		                            $firstAmount=0;
				                    $pkgCount=count($amount);
				                    for($i=0;$i<$pkgCount;$i++){
				                       
				                        $testAmountInfo=DB::table('test_amount')
										                  ->where('id',$amount[$i])
										                  ->get();
											foreach($testAmountInfo as $te){
									           echo $te->testName.' , ';
									           $firstAmount += $te->testAmount;
									        }
									         
				                    }
			                  
				            	?>
		                       
		                        
		                    </div>
		                   <h2 style="text-align: center;">Report</h2>
		                    
		                    <div class="row">
							    <div class="col-sm-6">
							     <?php
                                    $pkgCount=count($amount);
				                    for($i=0;$i<$pkgCount;$i++){
				                       
				                        $testAmountInfo=DB::table('test_amount')
										                  ->where('id',$amount[$i])
										                  ->get();
											foreach($testAmountInfo as $te){
									           
                                               ?>  
									           
										        <h4 style="margin-left: 370px;">{{$te->testName}}</h4>
										       <?php

									           
									        }
									        
									         
				                    }
							     ?>
							    </div>
							    <div class="col-sm-6">
							      <?php
		                           
				                    
				                    foreach($testReport as $test){
									        ?>  
									           
									        <h4 style="margin-left: 50px;">{{$test}}</h4>
									        <?php
									        }
			                  
				            	?>
							    </div>
							 </div>
					    <h1 style="text-align: center;">(All Test Done By {{$doctorName}})</h1>
		                       
				            </br>
                        
		                 <strong>Total Amount:</strong> <h5>{{$firstAmount.'  Taka'}}</h5>
		                 </br></br></br>
		                <div class="row" style="padding: 2px 2px 1px 10px;">
							<div class="col-4">
							 
								<span style="margin-left: 10px;">
									<p style="text-align:center;border-bottom: 1px solid #000000; width: 60%;"></p>
									<span style="margin-left:15px;">Technologist's Signature</span>
								</span>
				
							</div>
							
							<div class="col-4">					
							 															
							</div>

							<div class="col-4" style="text-align: center;">
								<span style="margin-left: 10px;">
									<p style="text-align:center;border-bottom: 1px solid #000000; width: 60%;"></p>
									<span style="margin-left:-123px;">Doctor's Signature</span>
								</span>
							</div>	
						</div>	
		                </div>
		            </div> <!-- end card-box-->
		        </div> <!-- end col -->

			
				
				

			</div>



		
	   
	</div> <!-- end col -->
    </td> 
   </tr>
 </tbody> 
</table>	

	
	<div id="notPrint" style="margin-left: 10px; margin-top: 30px;">
      <a href="{{url()->previous()}}" type="button"  class="btn btn-success printJobNgo" id="notPrint">Back</a>     	
      <!--  <button  class="btn btn-success" id="" type="submit" >Save</button> -->
        <button type="button"  class="btn btn-info printNewVotar"  onclick="printPage()">Print</button>
        <a href="{{route('createIndoorDonor')}}" class="btn btn-success  waves-effect waves-light ">Add New Indoor Donor</a>
        <a href=""></a>
            	
 	</div>

 	</form>

</div>

 
<!-- end row --> 
</div>
</div>


 <script>
    
function printPage() {
  window.print();

}
</script>
@endsection