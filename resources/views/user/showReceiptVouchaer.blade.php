@extends('layouts.user')

@section('title')  All Receipt Vouchaer @endsection

@section('content')

<style>
::placeholder {
  color: white;
  opacity: 0.6; /* Firefox */
}

:-ms-input-placeholder { /* Internet Explorer 10-11 */
 color: white;
}

::-ms-input-placeholder { /* Microsoft Edge */
 color: white;
}
.searchBox{
  margin-left: 20%;width:60%;border:1px solid #cce6ff;border-radius:5px;padding: 5px;background-color: #001a33;color:white;
}
</style>
            <div class="row">
                <div class="col-12">
                                
                    <div class="row">
                        <div class="col-lg-12">
                                
                            <div style="margin-top: 20px;">
                                <a href="{{route('addReceiptVouchaer')}}">
                                 <button type="button" class="btn btn-success  waves-effect waves-light ">Add New Receipt Vouchaer</button>
                                </a> 
                            </div>
                               <br/> 
                                <?php 
                                        $message=Session::get('message');
                                        if($message){

                                            ?>
                                            <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                                <?php
                                                    echo $message;
                                                    Session::put('message','');
                                                ?>
                                            </div>
                                            <?php
                                        
                                    }
                                    ?>              

                                <div class="card-box">
                                    <h4 class="header-title" style="text-align: center">All Receipt Vouchaer</h4>
                                    

                                    <div class="table-responsive">
                                        <table class="table mb-0" id="paymentTable">
                                            <thead class="thead-dark">
                                            <tr>
                                                 <th style="text-align: center;">Vouchaer Number</th>
                                                 <th style="text-align: center;">Data</th>
                                                 <th style="text-align: center;">Branch Name</th>
                                                 <th style="text-align: center;">Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                             @foreach($paymentVouchaerInfo as $payment)
                                            <tr>
                                                <td style="text-align: center;">{{$payment->vouchareNo}}</td>
                                                <td style="text-align: center;">{{$payment->date}}</td>
                                                <td style="text-align: center;">{{$payment->branchName}}</td>
                                                <td style="text-align: center;">
                                                    <a href="{{route('viewReceiptVouchaer',[$payment->id])}}" class="btn btn-warning  waves-effect waves-light">view</a>
                                                </td>
                                            </tr>
                                             @endforeach 
                                            </tbody>
                                        </table>
                                    </div> <!-- end table-responsive-->
                                  <br/>
                                   <div id="paginationLink">
                                      {{ $paymentVouchaerInfo->links() }}
                                   </div>
                                </div> <!-- end card-box -->
                                </div> <!-- end card -->
                            </div><!-- end col-->
                        </div>
                        <!-- end row-->



@endsection