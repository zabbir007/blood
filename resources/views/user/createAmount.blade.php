@extends('layouts.user')

@section('title') Create Amount @endsection

@section('content')
   <div class="row" style="margin-top: 20px;">
        <div class="col-lg-6 offset-3">
        	<a href="{{route('showAmount')}}">
        	<button class="btn btn-success" type="submit">Show All Amount</button>
        	</a>
            <div class="card" style="margin-top: 10px;">
                <div class="card-body">
                	<h4 class="header-title" style="text-align: center">Add Amount</h4>
                    <form class="parsley-examples"  method="post" action="{{route('saveAmount')}}" novalidate>
                    	<?php 
                            $message=Session::get('message');
                            if($message){

                                ?>
                                <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <?php
                                        echo $message;
                                        Session::put('message','');
                                    ?>
                                </div>
                                <?php
                            
                        }
                        ?>
                        @if($errors->any())
                                    
                                    <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">
                                       

                                               <ul>
                                                   @foreach($errors->all() as $error)
                                                        <li>{{$error}}</li>
                                                   @endforeach
                                               </ul>
                                           
                                       
                                    </div>
                                    @endif
                       @csrf
                    <div class="form-group">
                        <label>Test Name</label>
                        <div class="mt-3">             
                            <input type="text" name="testName"  class="form-control parsley-validated" required
                                    data-parsley-required-message="Please Enter Test Name"   placeholder="Test Name"/>
                        </div>
                           
                    </div>
                    <div class="form-group">
                        <label>Test Amount</label>
                        <div class="mt-3">             
                            <input type="text" name="testAmount"  class="form-control parsley-validated" required
                                        data-parsley-required-message="Please Enter Test Amount"   placeholder="Test Amount"/>
                        </div>
                           
                    </div>
                        <button class="btn btn-success" type="submit" id="btnTest">Add</button>
                    </form>
                    
                </div> <!-- end card-body-->
            </div> <!-- end card-->
        </div> <!-- end col-->
    </div>

@endsection