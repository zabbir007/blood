@extends('layouts.user')




@section('title') 

 
        Create Issue Voucher
   

@endsection


@section('content')

<style type="text/css">
	.indoorDonor{
		
		background-color: #fff;
	}


	@media print {

	  body * {
	  	margin-top: 15px;
	    visibility: hidden;
	  }
	  
	  #printInvoice, #printInvoice * {
	    visibility: visible;
	  }
	  #notPrint, #notPrint *{
	  	visibility: hidden;
	  }
	  #searchTestBox, #searchTestBox *{
	  	visibility: hidden;
	  }
	  #searchIndoorDonor, #searchIndoorDonor *{
	  	visibility: hidden;
	  }
	  #printInvoice {
	    position: absolute;
	    left: 0;
	    top: 0;
	  }



	    
	 


	}

</style>

<div id="printInvoice">
	 <div class="container ">

	 	<div id="searchIndoorDonor" style="margin-left: 50px; margin-top: 5px;">
	 		 @if($errors->any())
                    
                <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">
                   

                           <ul>
                               @foreach($errors->all() as $error)
                                    <li>{{$error}}</li>
                               @endforeach
                           </ul>
                       
                   
                </div>
                 @endif

                  <?php 
                    $messageWarning=Session::get('messageWarning');
                    if($messageWarning){

                        ?>
                        <div style="margin-top: 40px;" class="alert alert-warning alert-dismissible bg-warning text-white border-0 fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <?php
                                echo $messageWarning;
                                Session::put('messageWarning','');
                            ?>
                        </div>
                        <?php
                    
                }
                ?>

	 		<form class="parsley-examples" action="{{route('searchIssueVoucher')}}"  method="post"  >
				@csrf
				<div class="row">
					<div class="col-6">
						<input type="text" class="form-control" name="id" style="width:90%"  required  data-parsley-required-message="please Enter Voucher Number" placeholder="please Enter Voucher Number" />
				
					</div>
					<div class="col-3">
						
						<button  class="btn btn-success"  type="submit" >Search  Voucher</button>
						
					</div>
					
				</div>
	 		
	 		
	 		</form>
	 	</div>
<div class="row" style="margin-top: 5%;margin-left:5px;" >
	<div class="col-12 ">
		<form class="parsley-examples" action="{{route('addIssueVoucher')}}"  method="post"  >
			@csrf

			<div class="indoorDonor">
			    <br/>
			    <div class="col-12" >
					
					<span style="margin-left: 600px;color: red;">Issue voucher number:</span>
					<span><input type="text" name="" readonly="" style=" height: 48px;width: 180px;"></span>
				</div>
			    <br/><br/>
				<div class="col-12">					
				
						<span style="margin-left: 600px;">Date:</span> 
						<span >
						<input name="date" style="border:0; border-bottom: 1px  dotted; width:20%" type="text"  >
					    </span>
					   
				</div>
				<br/><br/>
				<div class="col-12">					
				
						<span style="margin-left: 3px;">Name Of Patient :</span> 
						<span>
						<input name="name" required  data-parsley-required-message="Please Enter Patient Name"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:86%" type="text"  >
					    </span>
				</div>
			    </br>
				<div class="col-12">					
				
						<span style="margin-left: 3px;">Name of the hospital/clinic:</span> 
						<span>
						<input name="referance" required  data-parsley-required-message="Please Enter Hospital Or Clinic Name"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:80%" type="text"  >
					    </span>
				</div>
				<br/>
				<div class="col-12">					
				
						<span style="margin-left: 3px;">Ward/cabin number:</span> 
						<span>
						<input name="ward" required  data-parsley-required-message="Please Enter Ward No"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:30%" type="text"  >
					    </span>
					    <span style="margin-left: 3px;">Bed number:</span> 
						<span>
						<input name="bed" required  data-parsley-required-message="Please Enter Bed Number"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:30%" type="text"  >
					    </span>
				</div>
				
				
				<br/>
				<div class="col-12">					
				
						<span style="margin-left: 3px;">Blood group:</span> 
						<span style="margin-left: 10px;">
						<input type="checkbox" value="op" name="group">O+
					    </span>
					    <span>{{'= '.$countOp}}</span>
					    <span>
						<input type="checkbox" value="on" name="group">O-
					    </span>
					     <span>{{'= '.$countOn}}</span>
					    <span style="margin-left: 20px;">
						<input type="checkbox" value="abp" name="group">AB+
					    </span>
					    <span>{{'= '.$countabp}}</span>
					    <span>
						<input type="checkbox" value="abn" name="group">AB-
					    </span>
					    <span>{{'= '.$countabn}}</span>
					    <span style="margin-left: 20px;">
						<input type="checkbox" value="ap" name="group">A+
					    </span>
					    <span>{{'= '.$countap}}</span>
					    <span>
						<input type="checkbox" value="an" name="group">A-
					    </span>
					    <span>{{'= '.$countan}}</span>
					    <span style="margin-left: 20px;">
						<input type="checkbox" value="bp" name="group">B+
					    </span>
					    <span>{{'= '.$countbp}}</span>
					    <span>
						<input type="checkbox" value="bn" name="group">B-
					    </span>
					    <span>{{'= '.$countbn}}</span>
					    <span style="margin-left: 3px;">Number of blood bag:</span> 
						<span>
						<input name="bagNumber" required  data-parsley-required-message="Please Enter Number of blood bag"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:10%" type="text"  >
					    </span>
				</div>


				<br/>
				<div class="col-12">					
				
						<span style="margin-left: 3px;">Number of transfusion set:</span> 
						<span>
						<input name="transfusion" required  data-parsley-required-message="Please Enter Number of transfusion set"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:30%" type="text"  >
					    </span>
					    <span style="margin-left: 3px;">Blood Test number:</span> 
						<span>
						<input name="donorNumber" style="border:0; border-bottom: 1px  dotted; width:30%" type="text"  >
					    </span>
				</div>
				
                <br/>
				<div class="col-12">					
					    <span style="margin-left: 3px;">Receipt number of the service charges (if applicable):</span> 
						<span>
						<input name="receiptNumber" style="border:0; border-bottom: 1px  dotted; width:30%" type="text"  >
					    </span>
				</div>

				<br/>
				<br/>
				<div style="border-bottom: 1px dotted black;"></div>
				<br/><br/><br/>
				<div class="col-12">					
				
						<span style="margin-left: 3px;">Name of the receiver:</span> 
						<span>
						<input name="receiverName" required  data-parsley-required-message="Please Enter Name of the receiver"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:30%" type="text"  >
					    </span>
					    <span style="margin-left: 3px;">Address of the receiver:</span> 
						<span>
						<input name="receiverAdd" required  data-parsley-required-message="Please Enter Address of the receiver"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:37%" type="text"  >
					    </span>
				</div>
				<br/>
				<div class="col-12">					
				
						<span style="margin-left: 3px;">Telephone number of the receiver:</span> 
						<span>
						<input name="receiverTel" required  data-parsley-required-message="Please Enter Telephone number of the receiver"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:30%" type="text"  >
					    </span>
					    <span style="margin-left: 3px;">Email Address of the receiver:</span> 
						<span>
						<input name="receiverEm" required  data-parsley-required-message="Please Enter Email Address of the receiver"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:25%" type="text"  >
					    </span>
				</div>
				<br/><br/>
				<br/><br/>

				<div class="col-12">					
				
						
					    <span style="margin-left: 627px;"> Signature:</span> 
						<span>
						<input name=""  style="border:0; border-bottom: 1px  dotted; width:20%" type="text"  >
					    </span>
					   
				</div>
				<br/><br/>

			</div>

		
	   
	</div> <!-- end col -->
	

	
	<div id="notPrint" style="margin-left: 10px; margin-top: 30px;">
            	
        <button  class="btn btn-success"  type="submit" id="saveTestBtn" >Save Voucher</button>
        <button type="button"  class="btn btn-info printNewVotar"  onclick="printPage()">Print</button>
            	
 	</div>

 	</form>

</div>

 
<!-- end row --> 
</div>
</div>


 <script>
    
function printPage() {
  window.print();

}
</script>
@endsection