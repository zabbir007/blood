@extends('layouts.user')




@section('title') 

 
        Create Donor Recozation
   

@endsection


@section('content')

<style type="text/css">
	.indoorDonor{
		
		background-color: #fff;
	}


	@media print {

	  body * {
	  	margin-top: 15px;
	    visibility: hidden;
	  }
	  
	  #printInvoice, #printInvoice * {
	    visibility: visible;
	  }
	  #notPrint, #notPrint *{
	  	visibility: hidden;
	  }
	  #searchTestBox, #searchTestBox *{
	  	visibility: hidden;
	  }
	  #searchIndoorDonor, #searchIndoorDonor *{
	  	visibility: hidden;
	  }
	  #printInvoice {
	    position: absolute;
	    left: 0;
	    top: 0;
	  }



	    
	 


	}

</style>

<div id="printInvoice">
	 <div class="container ">

	 	<div id="searchIndoorDonor" style="margin-left: 50px; margin-top: 5px;">
	 		 @if($errors->any())
                    
                <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">
                   

                           <ul>
                               @foreach($errors->all() as $error)
                                    <li>{{$error}}</li>
                               @endforeach
                           </ul>
                       
                   
                </div>
                 @endif

                  <?php 
                    $messageWarning=Session::get('messageWarning');
                    if($messageWarning){

                        ?>
                        <div style="margin-top: 40px;" class="alert alert-warning alert-dismissible bg-warning text-white border-0 fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <?php
                                echo $messageWarning;
                                Session::put('messageWarning','');
                            ?>
                        </div>
                        <?php
                    
                }
                ?>

                <?php 
                    $message=Session::get('message');
                    if($message){

                        ?>
                        <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <?php
                                echo $message;
                                Session::put('message','');
                            ?>
                        </div>
                        <?php
                    
                }
                ?>
	 	</div>
<div class="row" style="margin-top: 5%;margin-left:5px;" >
<table border="0" width="100%"> 
   <thead> 
    <tr> 
     <th style="width:100%">
	    <div class="row">
		  <div class="col-md-4"><img class="logo" height="80" width="100" src="{{asset('user/img/1.png')}}"></div>
		  <div class="col-md-7">
		  	<h5 style="color: red;text-align: left;">{{$companyName->companyName}}</h5>
            <h5>Branch : {{$branchName->branchName}}</h5>
            <h5>{{$companyName->description}}</h5>
		  </div>
		  <div class="col-md-1"><img style="margin-left: -35px;" class="logo" height="80" width="100" src="{{asset('user/img/2.png')}}"></div>
		</div>
     </th> 
   </tr> 
   <tr> 
    <th><hr style="color:#000080"/></th> 
   </tr> 
  </thead> 
  <tbody> 
    <tr> 
      <td width="100%"> 
	<div class="col-12 ">
		<form class="parsley-examples" action="{{route('saveDonorRecozation')}}"  method="post"  >
			@csrf

			<div class="indoorDonor">
			    <br/><br/>
				<div class="col-12">					
				
						<span style="margin-left: 600px;">Date:</span> 
						<span >
						<input name="date" style="border:0; border-bottom: 1px  dotted; width:20%" type="date" value="{{$donorRecozation->date}}" readonly >
					    </span>
					   
				</div>
				<br/><br/>
				<div class="col-12">					
				
						<span style="margin-left: 3px;">Name Of Patient :</span> 
						<span>
						<input name="name" required  data-parsley-required-message="Please Enter Patient Name"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:86%" type="text" value="{{$donorRecozation->name}}" readonly >
					    </span>
				</div>
				
				<br/>
				<div class="col-12">					
				
						<span style="margin-left: 3px;">Age :</span> 
						<span>
						<input name="age" required  data-parsley-required-message="Please Enter Age"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:30%" type="text" value="{{$donorRecozation->age}}" >
					    </span>
					    <span style="margin-left: 3px;">Sex :</span> 
						<span style="margin-left: 10px;">
						<input type="checkbox" value="Male" name="sex" <?php if($donorRecozation->sex=='Male'){echo "checked";} ?> >Male
					    </span>
					    <span>
						<input type="checkbox" value="Female" name="sex" <?php if($donorRecozation->sex=='Female'){echo "checked";} ?> >Female
					    </span>
				</div>
				<br/>
				<div class="col-12">					
				
						<span style="margin-left: 3px;">Needed Blood group:</span> 
						<span style="margin-left: 10px;">
						<input type="checkbox" value="op" name="group" <?php if($donorRecozation->group=='op'){echo "checked";} ?> >O+
					    </span>
					    <span>
						<input type="checkbox" value="on" name="group" <?php if($donorRecozation->group=='on'){echo "checked";} ?> >O-
					    </span>
					    <span style="margin-left: 20px;">
						<input type="checkbox" value="abp" name="group" <?php if($donorRecozation->group=='abp'){echo "checked";} ?> >AB+
					    </span>
					    <span>
						<input type="checkbox" value="abn" name="group" <?php if($donorRecozation->group=='abn'){echo "checked";} ?> >AB-
					    </span>
					    <span style="margin-left: 20px;">
						<input type="checkbox" value="ap" name="group" <?php if($donorRecozation->group=='ap'){echo "checked";} ?> >A+
					    </span>
					    <span>
						<input type="checkbox" value="an" name="group" <?php if($donorRecozation->group=='on'){echo "checked";} ?> >A-
					    </span>
					    <span style="margin-left: 20px;">
						<input type="checkbox" value="bp" name="group" <?php if($donorRecozation->group=='bp'){echo "checked";} ?> >B+
					    </span>
					    <span>
						<input type="checkbox" value="bn" name="group" <?php if($donorRecozation->group=='bn'){echo "checked";} ?> >B-
					    </span>
				</div>
			</br>
				<div class="col-12">					
				
						<span style="margin-left: 3px;">Refd.by/Hospital/Clinic :</span> 
						<span>
						<input name="referance" required  data-parsley-required-message="Please Enter Hospital Or Clinic Name"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:82%" type="text" value="{{$donorRecozation->referance}}" readonly >
					    </span>
				</div>
				<br/>
				<div class="col-12">					
				
						<span style="margin-left: 3px;">Phone :</span> 
						<span>
						<input name="phone" required  data-parsley-required-message="Please Enter Phone Number"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:30%" type="text" value="{{$donorRecozation->phone}}" readonly >
					    </span>
					    <span style="margin-left: 3px;">Amount Of Blood Bag :</span> 
						<span>
						<input name="bag" required  data-parsley-required-message="Please Enter Number Of Blood Bag"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:30%" type="text" value="{{$donorRecozation->bag}}" readonly >
					    </span>
				</div>
				<br/>
				<br/>

			</div>

		
	   
	</div> <!-- end col -->
	</td> 
   </tr>
 </tbody> 
</table>

	
	<div id="notPrint" style="margin-left: 10px; margin-top: 30px;">
       <a href="{{url()->previous()}}" type="button"  class="btn btn-success printJobNgo" id="notPrint">Back</a>     	
        <button type="button"  class="btn btn-info printLandPukurLig"  onclick="printPage()">Print</button>
       
            	
 	</div>

 	</form>

</div>

 
<!-- end row --> 
</div>
</div>


 <script>
    
function printPage() {
  window.print();

}
</script>
@endsection