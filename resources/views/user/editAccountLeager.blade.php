@extends('layouts.user')

@section('title') Edit Account Leager @endsection

@section('content')


<div class="row" style="margin-top: 20px;">
        <div class="col-lg-6 offset-3">
        	<a href="{{route('showAccountLeager')}}">
        	<button class="btn btn-primary" type="submit">Show All Account Leager</button>
        	</a>
            <div class="card" style="margin-top: 10px;">
                <div class="card-body">
                	<h4 class="header-title" style="text-align: center">Edit Payment Type</h4>
                    <form class="parsley-examples"  method="post" action="{{route('updateAccountLeager')}}" novalidate>
                       @csrf

                     
                      <div class="form-group">
                            <label>Select Account Group</label>
                            <div>
                                <select class="selectpicker" name="accountGroup" data-style="btn-secondary">
                                    @foreach($accountGroup as $account) 
                                    <option value="{{$account->paymentName}}">{{$account->paymentName}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div> 
                        
                      <div class="form-group">
                            <label>Account Leager</label>
                            <div>
                        		<input type="hidden" name="id" value="{{$singlePaymentInfo->id}}" />
                        		<input type="hidden" name="page" value="{{$page}}" />
                        		
                                <input type="text" name="accountLeager" value="{{$singlePaymentInfo->accountLeager}}"  class="form-control parsley-validated" required
                                        data-parsley-required-message="Please Enter Payment Type"   placeholder="Payment Type"/>
                            </div>
                        </div>
                        <button  class="btn btn-success" type="submit">Update</button>
                    </form>
                    
                </div> <!-- end card-body-->
            </div> <!-- end card-->
        </div> <!-- end col-->
    </div>


@endsection