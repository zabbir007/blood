@extends('layouts.user')

@section('title') Edit Amount @endsection

@section('content')


<div class="row" style="margin-top: 20px;">
        <div class="col-lg-6 offset-3">
        	<a href="{{route('showAmount')}}">
        	<button class="btn btn-success" type="submit">Show All Amount</button>
        	</a>
            <div class="card" style="margin-top: 10px;">
                <div class="card-body">
                	<h4 class="header-title" style="text-align: center">Edit Test Amount</h4>
                    <form class="parsley-examples"  method="post" action="{{route('updateAmount')}}" novalidate>
                       @csrf

                        
                        
                      <div class="form-group">
                            <label>Test Name</label>
                            <div>
                        		<input type="hidden" name="id" value="{{$singleAmountInfo->id}}" />
                        		<input type="hidden" name="page" value="{{$page}}" />
                        		
                                <input type="text" name="testName" value="{{$singleAmountInfo->testName}}"  class="form-control parsley-validated" required
                                        data-parsley-required-message="Please Enter Test Name"   placeholder="Test Name"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Test Amount</label>
                            <div>
                                <input type="text" name="testAmount" value="{{$singleAmountInfo->testAmount}}"  class="form-control parsley-validated" required
                                        data-parsley-required-message="Please Enter Test Amount"   placeholder="Test Amount"/>
                            </div>
                        </div>
                        <button  class="btn btn-success" type="submit">Update</button>
                    </form>
                    
                </div> <!-- end card-body-->
            </div> <!-- end card-->
        </div> <!-- end col-->
    </div>


@endsection