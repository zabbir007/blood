@extends('layouts.user')

@section('title')  All Recozation Donor @endsection

@section('content')

<style>
::placeholder {
  color: white;
  opacity: 0.6; /* Firefox */
}

:-ms-input-placeholder { /* Internet Explorer 10-11 */
 color: white;
}

::-ms-input-placeholder { /* Microsoft Edge */
 color: white;
}
.searchBox{
  margin-left: 20%;width:60%;border:1px solid #cce6ff;border-radius:5px;padding: 5px;background-color: #001a33;color:white;
}
</style>
            <div class="row">
                <div class="col-12">
                                
                    <div class="row">
                        <div class="col-lg-12">
                                
                            <div style="margin-top: 20px;">
                                <a href="{{route('createDonorRecozation')}}">
                                 <button type="button" class="btn btn-success  waves-effect waves-light ">Add New Recozation Donor</button>
                                </a> 
                            </div>
                               <br/> 
                                <?php 
                                        $message=Session::get('message');
                                        if($message){

                                            ?>
                                            <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                                <?php
                                                    echo $message;
                                                    Session::put('message','');
                                                ?>
                                            </div>
                                            <?php
                                        
                                    }
                                    ?>              

                                <div class="card-box">
                                    <h4 class="header-title" style="text-align: center">All Recozation Donor</h4>
                                    

                                    <div class="table-responsive">
                                        <table class="table mb-0" id="paymentTable">
                                            <thead class="thead-dark">
                                            <tr>
                                                 <th style="text-align: center;">Name</th>
                                                 <th style="text-align: center;">Age</th>
                                                 <th style="text-align: center;">Blood Group</th>
                                                 <th style="text-align: center;">Phone</th>
                                                 <th style="text-align: center;">Number Of Bag</th>
                                                 <th style="text-align: center;">Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                             @foreach($donorRecozationInfo as $donorRecozation)
                                            <tr>
                                                <td style="text-align: center;">{{$donorRecozation->name}}</td>
                                                <td style="text-align: center;">{{$donorRecozation->age}}</td>
                                                <td style="text-align: center;">
                                                  <?php if($donorRecozation->group=='op'){echo "O+";}else if($donorRecozation->group=='on'){echo "O-";}else if($donorRecozation->group=='abp'){echo "AB+";}else if($donorRecozation->group=='abn'){echo "AB-";}else if($donorRecozation->group=='ap'){echo "A+";}else if($donorRecozation->group=='an'){echo "A-";}else if($donorRecozation->group=='bp'){echo "B+";}else if($donorRecozation->group=='bn'){echo "B-";} ?>
                                                </td>
                                                <td style="text-align: center;">{{$donorRecozation->phone}}</td>
                                                <td style="text-align: center;">{{$donorRecozation->bag}}</td>
                                                <td style="text-align: center;">
                                                    <input type="hidden" name="page" value="{{$donorRecozationInfo->currentPage()}}"/>
                                                    <input type="hidden" name="id" value="{{$donorRecozation->id}}"/>
                                                    <a href="{{route('viewDonorRecozation',[$donorRecozation->id])}}" class="btn btn-success  waves-effect waves-light">View</a>
                                                    <a  href="#" id="{{$donorRecozation->id}}" class="btn btn-danger  waves-effect waves-light btnDonorRecozationDelete">Delete</a>
                                                </td>
                                            </tr>
                                             @endforeach 
                                            </tbody>
                                        </table>
                                    </div> <!-- end table-responsive-->
                                  <br/>
                                   <div id="paginationLink">
                                      {{ $donorRecozationInfo->links() }}
                                   </div>
                                </div> <!-- end card-box -->
                                </div> <!-- end card -->
                            </div><!-- end col-->
                        </div>
                        <!-- end row-->
                       



@endsection