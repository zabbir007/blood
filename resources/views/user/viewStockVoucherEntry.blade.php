@extends('layouts.user')
@section('title') 
    View Receipt Vouchaer
@endsection
@section('content')
<style type="text/css">
    .upOvijog{
        
        background-color: #fff;
    }


    @media print {
      body * {
        margin-top: 5px;
        visibility: hidden;
      }
      
      #printInvoice, #printInvoice * {
        visibility: visible;
      }
      #notPrint, #notPrint *{
        visibility: hidden;
      }
      #searchUpOvijog, #searchUpOvijog *{
        visibility: hidden;
      }
       #addAnotherNewRow, #addAnotherNewRow *{
        visibility: hidden;
      }
      #printInvoice {
        position: absolute;
        left: 0;
        top: 0;
      }



        
     


    }

    
</style>
<div id="printInvoice">
	<div class="container">
		<div class="row" style="margin-top: 5%;">
    <table border="0" width="100%"> 
       <thead> 
        <tr> 
         <th style="width:100%">
            <div class="row">
              <div class="col-md-4"><img class="logo" height="80" width="100" src="{{asset('user/img/1.png')}}"></div>
              <div class="col-md-7">
                <h5 style="color: red;text-align: left;">{{$companyName->companyName}}</h5>
                <h5>Branch : {{$branchName->branchName}}</h5>
                <h5>{{$companyName->description}}</h5>
              </div>
              <div class="col-md-1"><img style="margin-left: -35px;" class="logo" height="80" width="100" src="{{asset('user/img/2.png')}}"></div>
            </div>
         </th> 
       </tr> 
       <tr> 
        <th><hr style="color:#000080"/></th> 
       </tr> 
      </thead> 
      <tbody> 
        <tr> 
          <td width="100%"> 
            <div class="col-md-12">
            	 <?php 
                                        $message=Session::get('message');
                                        if($message){

                                            ?>
                                            <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                                <?php
                                                    echo $message;
                                                    Session::put('message','');
                                                ?>
                                            </div>
                                            <?php
                                        
                                    }
                                    ?>

                                    <?php 
                                        $message=Session::get('messageWarning');
                                        if($message){

                                            ?>
                                             <div class="alert alert-warning alert-dismissible bg-warning text-white border-0 fade show" role="alert">
                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                <?php
                                                    echo $message;
                                                    Session::put('messageWarning','');
                                                ?>
                                            </div>
                                            <?php
                                        
                                    }
                                    ?>
            	<form class="parsley-examples"  novalidate>
            		@csrf
            	<div class="card">
                    <div class="card-header">
                    	<div class="row">
                    		<div class="col-md-8">
                    		</div>
                    		<div class="col-md-4">
                                <div class="row">
                                    <div class="col-md-6">
                                        <span style="">Invoice No :</span>    
                                    </div>
                                    <div class="col-md-6">
                                        <span>{{$singleInfo->invoiceNumber}}</span>    
                                    </div>
                                </div>
                    			<br>
                                <div class="row">
                                    <div class="col-md-6">
                                        <span style="">Date :</span>    
                                    </div>
                                    <div class="col-md-6">
                                        <span> {{$singleInfo->date}}</span>    
                                    </div>
                                </div>
                                <br>
                    			<div class="row">
                                    <div class="col-md-6">
                                        <span style="">Branch Name :</span>   
                                    </div>
                                    <div class="col-md-6">
                                       <span>{{$singleInfo->branchName}}</span>   
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-6">
                                        <span style="">Party Name/Cash/Bank :</span>
                                    </div>
                                    <div class="col-md-6">
                                        <span>{{$singleInfo->partyName}}</span>
                                    </div>
                                </div>
                    			
                    		</div>
                    	</div>
                        <div class="row">
                            <div class="col-md-6">
                                
                            </div>
                            <div class="col-md-6">
                                <span>Balance Type</span>
                                <span>
                                    <select class="form-control parsley-validated" name="balanceType" data-style="btn-secondary">
                                        <option value="stockin" <?php if($singleInfo->balanceType=='stockin'){echo "selected";}  ?> >Stock In</option>
                                        <option value="stockout" <?php if($singleInfo->balanceType=='stockout'){echo "selected";}  ?>>Stock Out</option>
                                    </select>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered" style="width: 100%;" id="addRow">
							<thead>
							  <tr>
							   <th style="text-align: center;">SL</th>
                                <th style="text-align: center;">Item Name</th>
                                <th style="text-align: center;">Quantity</th>
                                <th style="text-align: center;">Rate</th>
                                <th style="text-align: center;">Amount</th>
							  </tr>
							</thead>
							<tbody>
                              <?php
                              $oarishCount=count($sll);
                              for($i=0;$i<$oarishCount;$i++){?>
							  <tr>
							    <td width="20%">
							    	<input required  data-parsley-required-message="Please Enter Serial Number"  data-style="btn-secondary" style="border:0;margin-bottom: 0px; border-bottom: 1px  none; width:100%;margin-left:none;" type="text" name="sl[]" value="{{$sll[$i]}}">
							    </td>
							    <td width="20%">
                                    <input required  data-parsley-required-message="Please Enter Item Name"  data-style="btn-secondary" style="border:0;margin-bottom: 0px; border-bottom: 1px  none; width:100%;margin-left:none;" type="text" name="itemName[]" value="{{$itemName[$i]}}">
                                </td>
                                <td width="20%">
                                    <input required  data-parsley-required-message="Please Enter Quantity"  data-style="btn-secondary" style="border:0;margin-bottom: 0px; border-bottom: 1px  none; width:100%;margin-left:none;" type="text" name="quantity[]" value="{{$quantity[$i]}}">
                                </td>
                                <td width="20%">
                                    <input required  data-parsley-required-message="Please Enter Rate"  data-style="btn-secondary" style="border:0;margin-bottom: 0px; border-bottom: 1px  none; width:100%;margin-left:none;" type="text" name="rate[]" value="{{$rate[$i]}}">
                                </td>
							    <td width="20%">
							    	<input required  data-parsley-required-message="Please Enter Amount"  data-style="btn-secondary" style="border:0;margin-bottom: 0px; border-bottom: 1px  none; width:100%;margin-left:none;" type="text" name="amount[]" value="{{$amount[$i]}}">
							    </td>
							   
							  </tr>
                              <?php
                                 }
                              ?>
							 
							</tbody>
							<tfoot>
							    <tr>
							      <th id="total" colspan="4">Total :</th>
							      <td><input required  data-parsley-required-message="Please Enter Amount"  data-style="btn-secondary" style="border:0;margin-bottom: 0px; border-bottom: 1px  none; width:100%;margin-left:none;" type="text" name="totalAmount" value="{{$singleInfo->totalAmount}}"></td>
							    </tr>
						    </tfoot>
						</table>
						<div class="row">
	                		<div class="col-md-6">
	                			<span>Taka :</span>
	                			<span>
	                				{{$singleInfo->taka}}
	                			</span>
	                		</div>
	                		<div class="col-md-6">
	                		</div>
	                	</div>
	                	<br>
	                	<div class="row">
	                		<div class="col-md-6">
	                			<span>Narration :</span>
	                			<span>
	                				{{$singleInfo->narration}}
	                			</span>
	                		</div>
	                		<div class="col-md-6">
	                		</div>
	                	</div>
	                	<div style="margin-top: 2%;">
                      <a href="{{url()->previous()}}" type="button"  class="btn btn-success printJobNgo" id="notPrint">Back</a>
	                		<button type="button"  class="btn btn-info printJobNgo" id="notPrint"  onclick="printPage()">Print</button>
	                	</div>
	                	 
                    </div>
                </div> <!-- end card-box-->
                </form>
            </div>
        </div>
	</div>
</div>
</td> 
   </tr>
 </tbody> 
</table>
<script>
    
function printPage() {
  window.print();

}
</script>
@endsection