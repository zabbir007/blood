@extends('layouts.user')




@section('title') 

 
        Show Receipt Voucher
   

@endsection


@section('content')

<style type="text/css">
	.indoorDonor{
		
		background-color: #fff;
	}


	@media print {

	  body * {
	  	margin-top: 5px;
	    visibility: hidden;
	  }
	  
	  #printInvoice, #printInvoice * {
	    visibility: visible;
	  }
	  #notPrint, #notPrint *{
	  	visibility: hidden;
	  }
	  #editMode, #editMode *{
	  	visibility: hidden;
	  }
	  #updateMode, #updateMode *{
	  	visibility: hidden;
	  }
	  #searchTestBox, #searchTestBox *{
	  	visibility: hidden;
	  }
	  #searchIndoorDonor, #searchIndoorDonor *{
	  	visibility: hidden;
	  }
	  #printInvoice {
	    position: absolute;
	    left: 0;
	    top: 0;
	  }



	    
	 


	}

</style>

<div id="printInvoice">
	 <div class="container ">

	 	<div id="searchIndoorDonor" style="margin-left: 50px;">
	 		 @if($errors->any())
                    
                <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">
                   

                           <ul>
                               @foreach($errors->all() as $error)
                                    <li>{{$error}}</li>
                               @endforeach
                           </ul>
                       
                   
                </div>
                 @endif

                 <?php 
                    $message=Session::get('message');
                    if($message){

                        ?>
                        <div style="margin-top: 40px;" class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <?php
                                echo $message;
                                Session::put('message','');
                            ?>
                        </div>
                        <?php
                    
                }
                ?>

                <div class="row" style="margin-top: 3px;">
                	<div class="col-3 offset-9">
						
						<a href="{{route('createReceiptVoucher')}}"  class="btn btn-success"  type="submit" >New Receipt Voucher</a>
						
					</div>
                </div>

                  <?php 
                    $messageWarning=Session::get('messageWarning');
                    if($messageWarning){

                        ?>
                        <div style="margin-top: 40px;" class="alert alert-warning alert-dismissible bg-warning text-white border-0 fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <?php
                                echo $messageWarning;
                                Session::put('messageWarning','');
                            ?>
                        </div>
                        <?php
                    
                }
                ?>

	 		
	 	</div>
<div class="row" style="margin-top: 5%;margin-left:5px;" >
	<table border="0" width="100%"> 
	   <thead> 
	    <tr> 
	     <th style="width:100%">
		    <div class="row">
			  <div class="col-md-4"><img class="logo" height="80" width="100" src="{{asset('user/img/1.png')}}"></div>
			  <div class="col-md-7">
			  	<h5 style="color: red;text-align: left;">{{$companyName->companyName}}</h5>
                <h5>Branch : {{$branchName->branchName}}</h5>
                <h5>{{$companyName->description}}</h5>
			  </div>
			  <div class="col-md-1"><img style="margin-left: -35px;" class="logo" height="80" width="100" src="{{asset('user/img/2.png')}}"></div>
			</div>
	     </th> 
	   </tr> 
	   <tr> 
	    <th><hr style="color:#000080"/></th> 
	   </tr> 
	  </thead> 
	  <tbody> 
	    <tr> 
	      <td width="100%"> 
				<div class="col-12 ">
					<form class="parsley-examples" action="{{route('updateReceiptVoucher')}}"  method="post"  >
						@csrf

						<div class="indoorDonor">
						    <br/>
						    <span style="margin-left: 5px; color: green;" id="editMode">Edit Mode</span>
							<span style="display: none;text-align: right; color: red;" id="updateMode">Update Mode</span>
						    <div class="col-12" >
								
								<span style="margin-left: 600px;color: red;">Receipt voucher number:</span>
								<span><input type="text" name="" readonly value="{{$issueVoucherInfo->voucherNumber}}" style=" height: 48px;width: 150px;"></span>
							</div>
						    <br/><br/>
							<div class="col-12">					
							
									<span style="margin-left: 600px;">Date:</span> 
									<span >
									<input name="date" readonly value="{{$issueVoucherInfo->date}}" style="border:0; border-bottom: 1px  dotted; width:20%" type="text"  >
								    </span>
								 <input type="hidden" name="id" value="{{$issueVoucherInfo->id}}">  
							</div>
							<br/>
						    </br>
							<div class="col-12">					
							
									<span style="margin-left: 3px;">Name of the hospital/clinic:</span> 
									<span>
									<input name="referance" readonly value="{{$issueVoucherInfo->referance}}" required  data-parsley-required-message="Please Enter Hospital Or Clinic Name"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:80%" type="text"  >
								    </span>
							</div>
							<br/>
							<div class="col-12">					
							
									<span style="margin-left: 3px;">Ward/cabin number:</span> 
									<span>
									<input name="ward" readonly value="{{$issueVoucherInfo->ward}}" required  data-parsley-required-message="Please Enter Ward No"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:30%" type="text"  >
								    </span>
								    <span style="margin-left: 3px;">Bed number:</span> 
									<span>
									<input name="bed" required  readonly value="{{$issueVoucherInfo->bed}}" data-parsley-required-message="Please Enter Bed Number"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:30%" type="text"  >
								    </span>
							</div>
							
							
							<br/>
							<div class="col-12">					
							
									<span style="margin-left: 3px;">Blood group:</span> 
									
								    <span style="margin-left: 10px;">
									<input type="checkbox" value="op" name="group" readonly="" <?php if ($issueVoucherInfo->group=='op') {echo "checked";}?> >O+
								    </span>
								    <span>
									<input type="checkbox" value="on" name="group" readonly="" <?php if ($issueVoucherInfo->group=='on') {echo "checked";}?> >O-
								    </span>
								    <span style="margin-left: 20px;">
									<input type="checkbox" value="abp" name="group" readonly="" <?php if ($issueVoucherInfo->group=='abp') {echo "checked";}?> >AB+
								    </span>
								    <span>
									<input type="checkbox" value="abn" name="group" readonly="" <?php if ($issueVoucherInfo->group=='abn') {echo "checked";}?> >AB-
								    </span>
								    <span style="margin-left: 20px;">
									<input type="checkbox" value="ap" name="group" readonly="" <?php if ($issueVoucherInfo->group=='ap') {echo "checked";}?> >A+
								    </span>
								    <span>
									<input type="checkbox" value="an" name="group" readonly="" <?php if ($issueVoucherInfo->group=='an') {echo "checked";}?> >A-
								    </span>
								    <span style="margin-left: 20px;">
									<input type="checkbox" value="bp" name="group" readonly="" <?php if ($issueVoucherInfo->group=='bp') {echo "checked";}?> >B+
								    </span>
								    <span>
									<input type="checkbox" value="bn" name="group" readonly="" <?php if ($issueVoucherInfo->group=='bn') {echo "checked";}?> >B-
								    </span>
								    <span style="margin-left: 3px;">Number of blood bag:</span> 
									<span>
									<input name="bagNumber" readonly value="{{$issueVoucherInfo->bagNumber}}" required  data-parsley-required-message="Please Enter Number of blood bag"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:30%" type="text"  >
								    </span>
							</div>


							<br/>
							<div class="col-12">					
							
									<span style="margin-left: 3px;">Number of transfusion set:</span> 
									<span>
									<input name="transfusion" readonly value="{{$issueVoucherInfo->transfusion}}" required  data-parsley-required-message="Please Enter Number of transfusion set"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:30%" type="text"  >
								    </span>
								   
							</div>
							
			                
							<br/><br/>
							<br/><br/>

							<div class="col-12">					
							
									
								    <span style="margin-left: 627px;"> Signature:</span> 
									<span>
									<input name=""  style="border:0; border-bottom: 1px  dotted; width:20%" type="text"  >
								    </span>
								   
							</div>
							<br/><br/>

						</div>

					
				   
				</div> <!-- end col -->
	
    </td> 
   </tr>
 </tbody> 
</table>
	
	<div id="notPrint" style="margin-left: 10px; margin-top: 30px;">
		<a href="{{url()->previous()}}" type="button"  class="btn btn-success printJobNgo" id="notPrint">Back</a>
        <button  class="btn btn-success" id="updateReceiptVoucher" style="display: none;" type="submit" >Update</button>
		<a href="" class="btn btn-warning" id="editReceiptVoucher">Edit</a>
		<a href="" class="btn btn-warning" id="backReceiptVoucher" style="display: none;"   >Back</a>
		<button type="button"  class="btn btn-info" id="printReceiptVoucher" onclick="printPage()">Print</button>
		<a href="" class="btn btn-warning deleteReceiptVoucher"  id="{{$issueVoucherInfo->id}}">Delete</a>
            	
 	</div>

 	</form>

</div>

 
<!-- end row --> 
</div>
</div>


 <script>
    
function printPage() {
  window.print();

}
</script>
@endsection