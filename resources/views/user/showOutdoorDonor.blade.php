@extends('layouts.user')




@section('title') 

 
        Show Outdoor Donor
   

@endsection


@section('content')

<style type="text/css">
	.indoorDonor{
		
		background-color: #fff;
	}


	@media print {

	  body * {
	  	margin-top: 15px;
	    visibility: hidden;
	  }
	  
	  #printInvoice, #printInvoice * {
	    visibility: visible;
	  }
	  #notPrint, #notPrint *{
	  	visibility: hidden;
	  }
	  #searchIndoorDonor, #searchIndoorDonor *{
	  	visibility: hidden;
	  }
	  #printInvoice {
	    position: absolute;
	    left: 0;
	    top: 0;
	  }



	    
	 


	}

</style>

<div id="printInvoice">
	 <div class="container ">

	 	<div id="searchIndoorDonor" style="margin-left: 50px;">
	 		 @if($errors->any())
                    
                <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">
                   

                           <ul>
                               @foreach($errors->all() as $error)
                                    <li>{{$error}}</li>
                               @endforeach
                           </ul>
                       
                   
                </div>
                 @endif

                 <?php 
                    $message=Session::get('message');
                    if($message){

                        ?>
                        <div style="margin-top: 40px;" class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <?php
                                echo $message;
                                Session::put('message','');
                            ?>
                        </div>
                        <?php
                    
                }
                ?>

                <div class="row" style="margin-top: 3px;">
                	<div class="col-3 offset-9">
						
						<a href="{{route('createOutdoorDonor')}}"  class="btn btn-success"  type="submit" >New Outdoor Donor</a>
						
					</div>
                </div>

                  <?php 
                    $messageWarning=Session::get('messageWarning');
                    if($messageWarning){

                        ?>
                        <div style="margin-top: 40px;" class="alert alert-warning alert-dismissible bg-warning text-white border-0 fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <?php
                                echo $messageWarning;
                                Session::put('messageWarning','');
                            ?>
                        </div>
                        <?php
                    
                }
                ?>

	 		
	 	</div>
<div class="row" style="margin-top: 5%;margin-left:5px;" >
	<div class="col-12 ">
		<form class="parsley-examples" action="{{route('updateOutdoorDonor')}}"  method="post"  >
			@csrf

			<div class="indoorDonor">
				<br/>
				<div class="col-12" >
					<h2 style="text-align:center; width: 31%;margin-left: 28%;color: red;">রক্ত কর্মসূচী</h2>
					
				</div>
				<div class="col-12" >
					
					<span style="margin-left: 600px;color: red;">রক্তের ব্যাগ নং:</span>
					<span><input type="text" name="" readonly value="{{$outdoorDonorInfo->id}}" style=" height: 48px;width: 200px;"></span>
				</div>
			
				<div class="col-12" style="margin-top: 5px;">
					
					<span style="margin-left: 561px;color: red;">রক্ত দাতার নিবন্ধন  নং:</span>
					<span><input type="text" name="" readonly value="{{$outdoorDonorInfo->nibondonNo}}" style=" height: 48px;width: 200px;"></span>
					<input type="hidden" name="id" value="{{$outdoorDonorInfo->id}}">
				</div>
				<div class="col-12" style="margin-top: 5px;">
					<h4 style="text-align:center; width: 31%;margin-left: 28%;color: red;">রক্ত দাতার নিবন্ধন ফরম</h4>
					
				</div>
				<br/>
				<div class="col-12">					
				
						<span style="margin-left: 3px;">১.নাম </span> 
						<span>
						<input style="border:0; border-bottom: 1px  dotted; width:93%" type="text" readonly value="{{$outdoorDonorInfo->name}}" >
					    </span>
				</div>

				<br/>
				<div class="col-12">					
				
						<span style="margin-left: 3px;">২.বয়স </span> 
						<span>
						<input name="age" required  data-parsley-required-message="দয়া করে বয়স লিখুন"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:30%" type="text" readonly value="{{$outdoorDonorInfo->age}}" >
					    </span>
					    <span style="margin-left: 3px;">৩. লিঙ্গ :</span> 
						<span id="showSex4">
						<input style="border:0; border-bottom: 1px  dotted; width:30%" type="text" readonly value="<?php if($outdoorDonorInfo->sex=='Male'){echo "পুরুষ";}else if($outdoorDonorInfo->sex=='Female'){echo "মহিলা";} ?>" >
					    </span>
					    <span style="margin-left: 10px;display: none;" id="showSex5">
						<input type="checkbox" value="Male" name="sex">পুরুষ
					    </span>
					    <span id="showSex6" style="display: none;">
						<input type="checkbox" value="Female" name="sex">মহিলা
					    </span>
				</div>
               <br/>
				<div class="col-12">					
				
						<span style="margin-left: 3px;">৪.ফোন </span> 
						<span>
						<input name="phone" required  data-parsley-required-message="দয়া করে ফোন নম্বর লিখুন"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:30%" type="text" readonly value="{{$outdoorDonorInfo->phone}}" >
					    </span>
					   
				</div>

				<br/>
				<div class="col-12">					
				
						<span style="margin-left: 3px;">৫. রক্তের গ্রুপ: <span> 
						<span style="margin-left: 10px;">
						<input type="checkbox" value="op" name="bloodGroup" readonly="" <?php if ($outdoorDonorInfo->bloodGroup=='op') {echo "checked";}?> >ও+
					    </span>
					    <span>
						<input type="checkbox" value="on" name="bloodGroup" readonly="" <?php if ($outdoorDonorInfo->bloodGroup=='on') {echo "checked";}?> >ও-
					    </span>
					    <span style="margin-left: 20px;">
						<input type="checkbox" value="abp" name="bloodGroup" readonly="" <?php if ($outdoorDonorInfo->bloodGroup=='abp') {echo "checked";}?> >এ.বি+
					    </span>
					    <span>
						<input type="checkbox" value="abn" name="bloodGroup" readonly="" <?php if ($outdoorDonorInfo->bloodGroup=='abn') {echo "checked";}?> >এ.বি-
					    </span>
					    <span style="margin-left: 20px;">
						<input type="checkbox" value="ap" name="bloodGroup" readonly="" <?php if ($outdoorDonorInfo->bloodGroup=='ap') {echo "checked";}?> >এ+
					    </span>
					    <span>
						<input type="checkbox" value="an" name="bloodGroup" readonly="" <?php if ($outdoorDonorInfo->bloodGroup=='an') {echo "checked";}?> >এ-
					    </span>
					    <span style="margin-left: 20px;">
						<input type="checkbox" value="bp" name="bloodGroup" readonly="" <?php if ($outdoorDonorInfo->bloodGroup=='bp') {echo "checked";}?> >বি+
					    </span>
					    <span>
						<input type="checkbox" value="bn" name="bloodGroup" readonly="" <?php if ($outdoorDonorInfo->bloodGroup=='bn') {echo "checked";}?> >বি-
					    </span>
				</div>
                
                
				<br/>
				<br/>
				<div class="col-12">					
				
						<span style="margin-left: 3px;">
							আমি স্বজ্ঞানে এবং স্বতঃস্ফর্তভাবে স্বেচ্ছায় রক্তদানে ইচ্ছুক আমি আমার রক্তের প্রয়োজনীয় পরীক্ষা নিরীক্ষা করবার জন্য অনুমতি প্রদান করছি এতদ্বারা আমি রক্ত কর্মসুচীকে আমার রক্তদান সম্পকীয় পূর্ব ও পরবর্তী সকল দায় দায়িত্ব থেকে মুক্ত করছি |
					    </span>
					   
				</div>
				<br/>
				<br/>
				<br/>
				<br/>
				<div class="col-12">					
				
						<span style="margin-left: 3px;">তারিখ:</span> 
						<span >
						<input name="date" style="border:0; border-bottom: 1px  dotted; width:20%" type="text" readonly value="{{$outdoorDonorInfo->date}}" >
					    </span>
					    <span style="margin-left: 96px;">সময় তুমি হার মেনেছ রক্তদানের কাছে </span>
					    <span style="margin-left: 127px;"> স্বাক্ষর:</span> 
						<span>
						<input name=""  style="border:0; border-bottom: 1px  dotted; width:20%" type="text"  >
					    </span>
					   
				</div>
				<div class="col-12">					
				
						<span style="margin-left: 324px;">পাঁচটি মিনিট করলে খরচ একটি জীবন বাঁচে</span> 
						
				</div>


				<br/>
				<br/>

			</div>

			
			<div class="row">
			    <div class="col-sm">
			      <img src="{{asset($outdoorDonorInfo->barCodeImage)}}" alt="{{$outdoorDonorInfo->barCodeImage}}" width="200" height="90">
			    </div>
			    <div class="col-sm">
			      <img src="{{asset($outdoorDonorInfo->barCodeImage)}}" alt="{{$outdoorDonorInfo->barCodeImage}}" width="200" height="90">
			    </div>
			    <div class="col-sm">
			      <img src="{{asset($outdoorDonorInfo->barCodeImage)}}" alt="{{$outdoorDonorInfo->barCodeImage}}" width="200" height="90">
			    </div>
			</div>

		
	   
	</div> <!-- end col -->
	

	
	<div id="notPrint" style="margin-left: 10px; margin-top: 30px;">
        <button  class="btn btn-success" id="updateOutdoorDonor" style="display: none;" type="submit" >Update</button>
		<a href="" class="btn btn-warning" id="editOutdoorDonor">Edit</a>
		<a href="" class="btn btn-warning" id="backOutdoorDonor" style="display: none;"   >Back</a>
		<button type="button"  class="btn btn-info" id="printOutdoorDonor" onclick="printPage()">Print</button>
		<a href="" class="btn btn-warning deleteOutdoorDonor"  id="{{$outdoorDonorInfo->id}}">Delete</a>
		<a href="{{route('showOutdoorTest',[$outdoorDonorInfo->nibondonNo])}}" class="btn btn-primary">Test</a>
            	
 	</div>

 	</form>

</div>

 
<!-- end row --> 
</div>
</div>


 <script>
    
function printPage() {
  window.print();

}
</script>
@endsection