@extends('layouts.user')

@section('title')  Show All Blood Reservation @endsection

@section('content')
<style>
::placeholder {
  color: white;
  opacity: 0.6; /* Firefox */
}

:-ms-input-placeholder { /* Internet Explorer 10-11 */
 color: white;
}

::-ms-input-placeholder { /* Microsoft Edge */
 color: white;
}
.searchBox{
  margin-left: 20%;width:60%;border:1px solid #cce6ff;border-radius:5px;padding: 5px;background-color: #001a33;color:white;
}
</style>
                        
        <div class="row">
            <div class="col-lg-12 offset-lg-12">
            
               <br/>
                <div class="card-box">
                    <h4 class="header-title" style="text-align:center">All Blood Reservation List</h4>
                    
                    <?php 
                      $message=Session::get('message');
                      if($message){

                          ?>
                          <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                              </button>
                              <?php
                                  echo $message;
                                  Session::put('message','');
                              ?>
                          </div>
                          <?php
                      
                  }
                  ?>
                  
                  @if($errors->any())
              
                  <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">
                     

                             <ul>
                                 @foreach($errors->all() as $error)
                                      <li>{{$error}}</li>
                                 @endforeach
                             </ul>
                         
                     
                  </div>
                   @endif


                    <div class="table-responsive">
                        <table class="table mb-0" id="indoorDonorTable">
                            <thead style="background-color: #4281DD;">
                            <tr>
                                
                                <th width="50%" style="color: white; text-align: center;">Blood Group</th>
                                <th width="50%" style="color: white; text-align: center;">Amount Of Bag</th>
                                
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                
                                <td style="border: 2px solid #F7F7F4; text-align: center;">
                                 O+
                                </td>
                                <td style="border: 2px solid #F7F7F4; text-align: center;">
                                  {{$countOp}}
                                  
                                </td>
                                
                               
                            </tr>
                            <tr>
                               <td style="border: 2px solid #F7F7F4; text-align: center;">
                                 O-
                                </td>
                                <td style="border: 2px solid #F7F7F4; text-align: center;">
                                  {{$countOn}}
                                  
                                </td>
                            </tr>
                             <tr>
                               <td style="border: 2px solid #F7F7F4; text-align: center;">
                                 AB+
                                </td>
                                <td style="border: 2px solid #F7F7F4; text-align: center;">
                                  {{$countabp}}
                                  
                                </td>
                            </tr>
                            <tr>
                               <td style="border: 2px solid #F7F7F4; text-align: center;">
                                 AB-
                                </td>
                                <td style="border: 2px solid #F7F7F4; text-align: center;">
                                  {{$countabn}}
                                  
                                </td>
                            </tr>
                            <tr>
                               <td style="border: 2px solid #F7F7F4; text-align: center;">
                                 A+
                                </td>
                                <td style="border: 2px solid #F7F7F4; text-align: center;">
                                  {{$countap}}
                                  
                                </td>
                            </tr>
                            <tr>
                               <td style="border: 2px solid #F7F7F4; text-align: center;">
                                 A-
                                </td>
                                <td style="border: 2px solid #F7F7F4; text-align: center;">
                                  {{$countan}}
                                  
                                </td>
                            </tr>
                            <tr>
                               <td style="border: 2px solid #F7F7F4; text-align: center;">
                                 B+
                                </td>
                                <td style="border: 2px solid #F7F7F4; text-align: center;">
                                  {{$countbp}}
                                  
                                </td>
                            </tr>
                            <tr>
                               <td style="border: 2px solid #F7F7F4; text-align: center;">
                                 B-
                                </td>
                                <td style="border: 2px solid #F7F7F4; text-align: center;">
                                  {{$countbn}}
                                  
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div> <!-- end table-responsive-->
                </div> <!-- end card-box -->
            </div> <!-- end col -->

        </div>
        <!--- end row -->
                       
@endsection