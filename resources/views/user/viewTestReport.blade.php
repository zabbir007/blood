@extends('layouts.user')




@section('title') 

 
        View Test Report
   

@endsection


@section('content')

<style type="text/css">
	.indoorDonor{
		
		background-color: #fff;
	}


	@media print {

	  body * {
	  	margin-top: 5px;
	    visibility: hidden;
	  }

	  #footer {
    display: block; 
    position: fixed; 
    bottom: 0;
  } 
	  
	  #printInvoice, #printInvoice * {
	    visibility: visible;
	  }
	  #notPrint, #notPrint *{
	  	visibility: hidden;
	  }
	  #searchIndoorDonor, #searchIndoorDonor *{
	  	visibility: hidden;
	  }
	  #printInvoice {
	    position: absolute;
	    left: 0;
	    top: 0;
	  }





	}

</style>

<div id="printInvoice">
	 <div class="container ">

<div class="row" style="margin-top: 5%;margin-left:5px;" >
	<table border="0" width="100%"> 
	   <thead> 
	    <tr> 
	     <th style="width:100%">
		    <div class="row">
			  <div class="col-md-4"><img class="logo" height="80" width="100" src="{{asset('user/img/1.png')}}"></div>
			  <div class="col-md-7">
			  	<h5 style="color: red;text-align: left;">{{$companyName->companyName}}</h5>
                <h5>Branch : {{$branchName->branchName}}</h5>
                <h5>{{$companyName->description}}</h5>
			  </div>
			  <div class="col-md-1"><img style="margin-left: -35px;" class="logo" height="80" width="100" src="{{asset('user/img/2.png')}}"></div>
			</div>
	     </th> 
	   </tr> 
	   <tr> 
	    <th><hr style="color:#000080"/></th> 
	   </tr> 
	  </thead> 
	  <tbody> 
	    <tr> 
	      <td width="100%"> 
	         <div class="col-12 ">
					<form class="parsley-examples" action="{{route('saveTest')}}"  method="post"  >
						@csrf
			           
						<div class="indoorDonor">
							
						    <br/><br/>
							<div class="col-12">					
							
									<span style="margin-left: 600px;">Date :</span> 
									<span >
									<input name="date" readonly="" value="{{$testAmountInfo->date}}" style="border:0; border-bottom: 1px  dotted; width:20%" type="text"  >
								    </span>
								   
							</div>
							<br/><br/>
							<div class="col-12">					
							
									<span style="margin-left: 3px;">Test Number :</span> 
									<span>
									<input  readonly="" value="{{$testAmountInfo->testNumber}}" style="border:0; border-bottom: 1px  dotted; width:30%" type="text"  >
								    </span>
							</div>
							<br>
							<div class="col-12">					
							
									<span style="margin-left: 3px;">Name Of Patient :</span> 
									<span>
									<input name="name" readonly="" value="{{$testAmountInfo->name}}" required  data-parsley-required-message="Please Enter Patient Name"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:86%" type="text"  >
								    </span>
							</div>
							
							<br/>
							<div class="col-12">					
							
									<span style="margin-left: 3px;">Age :</span> 
									<span>
									<input name="age" required readonly="" value="{{$testAmountInfo->age}}"  data-parsley-required-message="Please Enter Age"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:30%" type="text"  >
								    </span>
								    <span style="margin-left: 3px;">Sex :</span> 
									<span>
									<input name="sex" required readonly="" value="{{$testAmountInfo->sex}}" data-parsley-required-message="Please Enter Sex"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:30%" type="text"  >
								    </span>
							</div>
							<br/>
							<div class="col-12">					
							
									<span style="margin-left: 3px;">Refd.by/Hospital/Clinic :</span> 
									<span>
									<input name="referance" required  readonly="" value="{{$testAmountInfo->referance}}" data-parsley-required-message="Please Enter Hospital Or Clinic Name"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:82%" type="text"  >
								    </span>
							</div>
							<br/>
							<div class="col-12">					
							
									<span style="margin-left: 3px;">Ward :</span> 
									<span>
									<input name="ward" required readonly="" value="{{$testAmountInfo->ward}}" data-parsley-required-message="Please Enter Ward No"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:30%" type="text"  >
								    </span>
								    <span style="margin-left: 3px;">Bed No :</span> 
									<span>
									<input name="bed" required readonly="" value="{{$testAmountInfo->bed}}" data-parsley-required-message="Please Enter Bed Number"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:30%" type="text"  >
								    </span>
							</div>

							<div class="col-12">
					            <div class="card" >
					               
					                <div class="card-body">
					                   <div class="form-group position-relative mb-3">
					                        <label>Test Required :<span class="text-danger">*</span></label>
					                       <?php
					                            $firstAmount=0;
							                    $pkgCount=count($amount);
							                    for($i=0;$i<$pkgCount;$i++){
							                       
							                        $testAmountInfo=DB::table('test_amount')
													                  ->where('id',$amount[$i])
													                  ->get();
														foreach($testAmountInfo as $te){
												           echo $te->testName.' , ';
												           $firstAmount += $te->testAmount;
												        }
												         
							                    }
						                  
							            	?>
					                       
					                        
					                    </div>
					                   <h2 style="text-align: center;">Report</h2>
					                    
					                    <div class="row">
										    <div class="col-sm-6">
										     <?php
			                                    $pkgCount=count($amount);
							                    for($i=0;$i<$pkgCount;$i++){
							                       
							                        $testAmountInfo=DB::table('test_amount')
													                  ->where('id',$amount[$i])
													                  ->get();
														foreach($testAmountInfo as $te){
												           
			                                               ?>  
												           
													        <h4 style="margin-left: 370px;">{{$te->testName}}</h4>
													       <?php

												           
												        }
												        
												         
							                    }
										     ?>
										    </div>
										    <div class="col-sm-6">
										      <?php
					                           
							                    
							                    foreach($testReport as $test){
												        ?>  
												           
												        <h4 style="margin-left: 50px;">{{$test}}</h4>
												        <?php
												        }
						                  
							            	?>
										    </div>
										 </div>
								   
							            </br>
			                        
					                <!--  <strong>Total Amount:</strong> <h5>{{$firstAmount.'  Taka'}}</h5> -->
					                 </br></br></br>
					                <div class="row" style="padding: 2px 2px 1px 10px;">
										<div class="col-4">
										 
											<span style="margin-left: 10px;">
												<p style="text-align:center;border-bottom: 1px solid #000000; width: 60%;"></p>
												<span style="margin-left:15px;">Technologist's Signature</span>
											</span>
							
										</div>
										
										<div class="col-4">					
										 															
										</div>

										<div class="col-4" style="text-align: center;">
											<span style="margin-left: 10px;">
												<p style="text-align:center;border-bottom: 1px solid #000000; width: 60%;"></p>
												<span style="margin-left:-123px;">Doctor's Signature</span>
											</span>
										</div>	
									</div>	
					                </div>
					            </div> <!-- end card-box-->
					        </div> <!-- end col -->

						</div>
				   
				</div> <!-- end col -->
	      </td> 
	   </tr>
	 </tbody> 
	</table>
	
	

	
	<div id="notPrint" style="margin-left: 10px; margin-top: 30px;">
       <a href="{{url()->previous()}}" type="button"  class="btn btn-success printJobNgo" id="notPrint">Back</a>     	
       <!--  <button  class="btn btn-success"  type="submit" >Save Test</button> -->
        <button type="button"  class="btn btn-info printNewVotar"  onclick="printPage()">Print</button>
            	
 	</div>

 	</form>

</div>

 
<!-- end row --> 
</div>
</div>


 <script>
    
function printPage() {
  window.print();

}
</script>
@endsection