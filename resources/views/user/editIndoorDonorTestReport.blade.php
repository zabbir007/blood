@extends('layouts.user')

@section('title')  Edit Test Report @endsection

@section('content')

<div class="row" style="margin-top: 5%;margin-left:5px;" >
    
	 <div class="col-12">
        <div class="card" >
          <form action="{{route('updateIndoorDonorTestReport')}}"  class="parsley-examples" enctype="multipart/form-data" method="post" novalidate> 
          	@csrf
            <div class="card-body">
            	<?php 
                      $message=Session::get('message');
                      if($message){

                          ?>
                          <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                              </button>
                              <?php
                                  echo $message;
                                  Session::put('message','');
                              ?>
                          </div>
                          <?php
                      
                  }
                  ?>
               <div class="form-group position-relative mb-3">
                   
                   <?php
                       
	                    $pkgCount=count($amount);
	                    for($i=0;$i<$pkgCount;$i++){
	                       ?>
	                        <label>Test Name :<span class="text-danger">*</span></label>
	                       <?php
	                        $testAmountInfo=DB::table('test_amount')
							                  ->where('id',$amount[$i])
							                  ->get();
								foreach($testAmountInfo as $te){
						           echo $te->testName;
						        ?>
						        <label style="margin-top: 5px;">Test Report:</label>
                                 <input type="text" name="report[]" class="form-control parsley-validated" required data-parsley-required-message="Please Enter Report Result"  placeholder="Report"/>
						        <?php
						        }
						         
	                    }
                  
	            	?>
                  
                <label style="margin-top: 5px;">Test Report Done By:</label> 
                <input type="text" name="doctorName" class="form-control parsley-validated" required data-parsley-required-message="Please Enter Doctor Name"  placeholder="Doctor Name"/> 
                <input type="hidden" name="id" value="{{$id}}">
                </div>
            <button type="submit"  class="btn btn-info">Update</button>
            
            </div>
        </form>
        </div> <!-- end card-box-->
    </div> <!-- end col -->

</div>


@endsection
