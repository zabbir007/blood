@extends('layouts.user')




@section('title') 

 
       Show Donor
   

@endsection


@section('content')

<style type="text/css">
	.indoorDonor{
		
		background-color: #fff;
	}


	@media print {

	  body * {
	  	margin-top: 15px;
	    visibility: hidden;
	  }
	  
	  #printInvoice, #printInvoice * {
	    visibility: visible;
	  }
	  #notPrint, #notPrint *{
	  	visibility: hidden;
	  }
	  #editMode, #editMode *{
	  	visibility: hidden;
	  }
	  #updateMode, #updateMode *{
	  	visibility: hidden;
	  }
	  #searchIndoorDonor, #searchIndoorDonor *{
	  	visibility: hidden;
	  }
	  #printInvoice {
	    position: absolute;
	    left: 0;
	    top: 0;
	  }
	}

</style>

<div id="printInvoice">
	 <div class="container ">

	 	<div id="searchIndoorDonor" style="margin-left: 50px;">
	 		 @if($errors->any())
                    
                <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">
                    <ul>
                       @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                       @endforeach
                    </ul>
                </div>
                 @endif

            <?php 
            $message=Session::get('message');
            if($message){
                        ?>
                <div style="margin-top: 40px;" class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <?php
                        echo $message;
                        Session::put('message','');
                    ?>
                </div>
            <?php
            }
            ?>

                <div class="row" style="margin-top: 3px;">
                	<div class="col-3 offset-9">
						
						<a href="{{route('createIndoorDonorEnglish')}}"  class="btn btn-success"  type="submit" >New Indoor Donor</a>
						
					</div>
                </div>
            <?php 
            $messageWarning=Session::get('messageWarning');
            if($messageWarning){
                ?>
                <div style="margin-top: 40px;" class="alert alert-warning alert-dismissible bg-warning text-white border-0 fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <?php
                        echo $messageWarning;
                        Session::put('messageWarning','');
                    ?>
                </div>
            <?php
            }
            ?>
	 	</div>
<div class="row" style="margin-top: 5%;margin-left:5px;" >
	<div class="col-12 ">
		<form class="parsley-examples" action="{{route('updateIndoorDonorEnglish')}}"  method="post"  >
			@csrf
			<div class="indoorDonor">
				<br/>
				<span style="margin-left: 5px; color: green;" id="editMode">Edit Mode</span>
				<span style="display: none;text-align: right; color: red;" id="updateMode">Update Mode</span>
				<div class="col-12" >
					<h2 style="text-align:center; width: 31%;margin-left: 28%;color: red;">Blood Program</h2>
				</div>

				<div class="col-12" >
					<span style="margin-left: 600px;color: red;">Blood Bag No:</span>
					<span><input type="text" name="" readonly value="{{$indoorDonorInfo->id}}" style=" height: 48px;width: 200px;"></span>
				</div>
			
				<div class="col-12" style="margin-top: 5px;">
					<span style="margin-left: 561px;color: red;">Blood Donor Registration No:</span>
					<span><input type="text" name="" readonly value="{{$indoorDonorInfo->nibondonNo}}" style=" height: 48px;width: 200px;"></span>
					<input type="hidden" name="id" value="{{$indoorDonorInfo->id}}">
				</div>
				<div class="col-12" style="margin-top: 5px;">
					<h4 style="text-align:center; width: 31%;margin-left: 28%;color: red;">Blood Donor Registration Form</h4>
				</div>
				<br/>
				<div class="col-12">					
					<span style="margin-left: 3px;">1.Name </span> 
					<span>
					<input name="name" required  data-parsley-required-message="দয়া করে নাম লিখুন"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:93%" type="text" readonly value="{{$indoorDonorInfo->name}}" >
				    </span>
				</div>
				<br/>
				<div class="col-12">					
					<span style="margin-left: 3px;">2.Father/Husband Name</span> 
					<span>
					<input name="fatherOrHusband" required  data-parsley-required-message="দয়া করে পিতা/ স্বামীর নাম লিখুন"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:82%" type="text" readonly value="{{$indoorDonorInfo->fatherOrHusband}}" >
				    </span>
				</div>

				<br/>
				<div class="col-12">					
					<span style="margin-left: 3px;">3.Age</span> 
					<span>
					<input name="age" required  data-parsley-required-message="দয়া করে বয়স লিখুন"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:30%" type="text" readonly value="{{$indoorDonorInfo->age}}" >
				    </span>
				    <span style="margin-left: 3px;">4.Gender: </span> 
					<span id="showSex1">
					<input style="border:0; border-bottom: 1px  dotted; width:30%" type="text" readonly value="<?php if($indoorDonorInfo->sex=='Male'){echo "Male";}else if($indoorDonorInfo->sex=='Female'){echo "Female";} ?>" >
				    </span>
				    <span style="margin-left: 10px;display: none;" id="showSex2">
					<input type="checkbox" value="Male" name="sex">Male
				    </span>
				    <span id="showSex3" style="display: none;">
					<input type="checkbox" value="Female" name="sex">Female
				    </span>
				</div>
               <br/>
				<div class="col-12">					
					<span style="margin-left: 3px;">5.Educational Qualification </span> 
					<span>
					<input name="education" required  data-parsley-required-message="দয়া করে শিক্ষাগত যোগতা লিখুন"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:30%" type="text" readonly value="{{$indoorDonorInfo->education}}" >
				    </span>
				</div>
                
                <br/>
				<div class="col-12">					
					<span style="margin-left: 3px;">6.Current Address (Enter Complete Address)</span> 
					<span>
					<input name="address1" required  data-parsley-required-message="দয়া করে ঠিকানা লিখুন"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:69%" type="text" readonly value="{{$indoorDonorInfo->address1}}" >
				    </span>
				</div>
				<br/>
				<div class="col-12">					
					<span style="margin-left: 3px;">
					<input name="address2" style="border:0; border-bottom: 1px  dotted; width:60%" type="text" readonly value="{{$indoorDonorInfo->address2}}" >
				    </span>
				    <span style="margin-left: 3px;">Mobile </span> 
					<span>
					<input name="phone" required  data-parsley-required-message="দয়া করে ফোন নম্বর লিখুন"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:30%" type="text" readonly value="{{$indoorDonorInfo->phone}}" >
				    </span>
				</div>

				<br/>
				<div class="col-12">					
					<span style="margin-left: 3px;">7.Workplace / Permanent Address</span> 
					<span>
					<input name="address3" required  data-parsley-required-message="দয়া করে স্থায়ী ঠিকানা লিখুন"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:75%" type="text" readonly value="{{$indoorDonorInfo->address3}}" >
				    </span>
				</div>
				<br/>
				<div class="col-12">					
					<span style="margin-left: 3px;">
					<input name="address4" style="border:0; border-bottom: 1px  dotted; width:60%" type="text" readonly value="{{$indoorDonorInfo->address4}}" >
				    </span>
				    <span style="margin-left: 3px;">Mobile </span> 
					<span>
					<input name="phone2" required  data-parsley-required-message="দয়া করে ফোন নম্বর লিখুন"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:30%" type="text" readonly value="{{$indoorDonorInfo->phone2}}" >
				    </span>
				</div>
				<br/>
				<div class="col-12">					
					<span style="margin-left: 3px;">8.What is the prevalence of blood donation in four months? Yes / No when and where</span> 
					<span>
					<input name="description" style="border:0; border-bottom: 1px  dotted; width:41%" type="text" readonly value="{{$indoorDonorInfo->description}}" >
				    </span>
				</div>
				<br/>
				<div class="col-12">					
					<span style="margin-left: 3px;">9.Blood group: <span> 
					<span style="margin-left: 10px;">
					<input type="checkbox" value="op" name="bloodGroup" readonly="" <?php if ($indoorDonorInfo->bloodGroup=='op') {echo "checked";}?> >B+
				    </span>
				    <span>
					<input type="checkbox" value="on" name="bloodGroup" readonly="" <?php if ($indoorDonorInfo->bloodGroup=='on') {echo "checked";}?> >B-
				    </span>
				    <span style="margin-left: 20px;">
					<input type="checkbox" value="abp" name="bloodGroup" readonly="" <?php if ($indoorDonorInfo->bloodGroup=='abp') {echo "checked";}?> >AB+
				    </span>
				    <span>
					<input type="checkbox" value="abn" name="bloodGroup" readonly="" <?php if ($indoorDonorInfo->bloodGroup=='abn') {echo "checked";}?> >AB-
				    </span>
				    <span style="margin-left: 20px;">
					<input type="checkbox" value="ap" name="bloodGroup" readonly="" <?php if ($indoorDonorInfo->bloodGroup=='ap') {echo "checked";}?> >A+
				    </span>
				    <span>
					<input type="checkbox" value="an" name="bloodGroup" readonly="" <?php if ($indoorDonorInfo->bloodGroup=='an') {echo "checked";}?> >A-
				    </span>
				    <span style="margin-left: 20px;">
					<input type="checkbox" value="bp" name="bloodGroup" readonly="" <?php if ($indoorDonorInfo->bloodGroup=='bp') {echo "checked";}?> >B+
				    </span>
				    <span>
					<input type="checkbox" value="bn" name="bloodGroup" readonly="" <?php if ($indoorDonorInfo->bloodGroup=='bn') {echo "checked";}?> >B-
				    </span>
				</div>
				<br/>
				<br/>
				<div class="col-12">					
					<span style="margin-left: 3px;">
						I am willing to voluntarily donate blood willingly and voluntarily, I am allowing my blood test to be examined so that I am relieving the blood program from all liability related to my blood donation before and after.
				    </span>
				</div>
				<br/>
				<br/>
				<br/>
				<br/>
				<div class="col-12">					
					<span style="margin-left: 3px;">Date:</span> 
					<span >
					<input name="date" style="border:0; border-bottom: 1px  dotted; width:20%" type="text" readonly value="{{$indoorDonorInfo->date}}" >
				    </span>
				    <span style="margin-left: 96px;">The time you rate the donation to the blood donation</span>
				    <span style="margin-left: 125px;"> Signature:</span> 
					<span>
					<input name=""  style="border:0; border-bottom: 1px  dotted; width:8%" type="text"  >
				    </span>
				</div>
				<div class="col-12">					
					<span style="margin-left: 324px;">Five minutes cost a life to live</span> 
				</div>
				<br/>
				<br/>
			</div>

			
			<div class="row">
			    <div class="col-sm">
			      <img src="{{asset($indoorDonorInfo->barCodeImage)}}" alt="{{$indoorDonorInfo->barCodeImage}}" width="200" height="90">
			    </div>
			    <div class="col-sm">
			      <img src="{{asset($indoorDonorInfo->barCodeImage)}}" alt="{{$indoorDonorInfo->barCodeImage}}" width="200" height="90">
			    </div>
			    <div class="col-sm">
			      <img src="{{asset($indoorDonorInfo->barCodeImage)}}" alt="{{$indoorDonorInfo->barCodeImage}}" width="200" height="90">
			    </div>
			</div>
	</div> <!-- end col -->
	

	
	<div id="notPrint" style="margin-left: 10px; margin-top: 30px;">
        <a href="{{url()->previous()}}" type="button"  class="btn btn-success printJobNgo" id="notPrint">Back</a>   	
        <button  class="btn btn-success" id="updateIndoorDonor" style="display: none;" type="submit" >Update</button>
		<a href="" class="btn btn-warning" id="editIndoorDonor">Edit</a>
		<a href="" class="btn btn-warning" id="backIndoorDonor" style="display: none;"   >Back</a>
		<button type="button"  class="btn btn-info" id="printIndoorDonor" onclick="printPage()">Print</button>
		<a href="" class="btn btn-warning deleteIndoorDonor"  id="{{$indoorDonorInfo->id}}">Delete</a>
		<a href="{{route('showTest',[$indoorDonorInfo->nibondonNo])}}" class="btn btn-primary">Test</a>
            	
 	</div>

 	</form>

</div>

 
<!-- end row --> 
</div>
</div>


 <script>
    
function printPage() {
  window.print();

}
</script>
@endsection