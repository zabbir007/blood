@extends('layouts.user')

@section('title')  Show All Test Report @endsection

@section('content')
<style>
::placeholder {
  color: white;
  opacity: 0.6; /* Firefox */
}

:-ms-input-placeholder { /* Internet Explorer 10-11 */
 color: white;
}

::-ms-input-placeholder { /* Microsoft Edge */
 color: white;
}
.searchBox{
  margin-left: 20%;width:60%;border:1px solid #cce6ff;border-radius:5px;padding: 5px;background-color: #001a33;color:white;
}
</style>
                        
        <div class="row">
            <div class="col-lg-6 offset-lg-3">
              <div style="margin-top: 20px;">
               
               
          
                  <div class="row">
                    <div class="col-6">
                      </div>
                      <div class="col-6">
                          <div class="input-group m-t-10">
                              <input type="text" class="form-control" id="searchTestReportBoxJ" style=" @media (max-width:969px) and (min-width:1000px) {margin-top: 10px;}​" class="searchBox" placeholder="Search By Test Number Or name or clinic/Hospital Name">
                            <span class="input-group-append">
                                <button type="button" class="btn waves-effect waves-light btn-blue"><i class="fa fa-search mr-1"></i> Search</button>
                            </span>
                          </div>
                      </div>
                  </div>
                 

              </div>
               <br/>
                <div class="card-box">
                    <h4 class="header-title" style="text-align:center">Report List</h4>
                    
                    <?php 
                      $message=Session::get('message');
                      if($message){

                          ?>
                          <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                              </button>
                              <?php
                                  echo $message;
                                  Session::put('message','');
                              ?>
                          </div>
                          <?php
                      
                  }
                  ?>
                  
                  @if($errors->any())
              
                  <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">
                     

                             <ul>
                                 @foreach($errors->all() as $error)
                                      <li>{{$error}}</li>
                                 @endforeach
                             </ul>
                         
                     
                  </div>
                   @endif


                    <div class="table-responsive">
                        <table class="table mb-0" id="testReportTable">
                            <thead style="background-color: #4281DD;">
                            <tr>
                                
                                <th width="30%" style="color: white; text-align: center;">Test Number</th>
                                <th width="30%" style="color: white; text-align: center;">Name</th>
                                <th width="20%" style="color: white; text-align: center;">Clinic Or Hospital Name</th>
                                <th width="20%" style="color: white; text-align: center;">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                             @foreach($showAllTestReport as $showTest) 
                            <tr>
                                
                                <td style="border: 2px solid #F7F7F4; text-align: center;">
                                  {{$showTest->testNumber}}
                                  
                                </td>
                                <td style="border: 2px solid #F7F7F4; text-align: center;">
                                  {{$showTest->name}}
                                  
                                </td>
                                <td style="border: 2px solid #F7F7F4; text-align: center;">
                                  {{$showTest->referance}}
                                  
                                </td>
                                <td style="border: 2px solid #F7F7F4; text-align: center;">
                                 
                                  <a href="{{route('viewTestReport',[$showTest->id])}}" class="action-icon"> <i class="mdi mdi-eye"></i></a> 
                                 
                                  <a href="{{route('editTestReport',[$showTest->id,$showAllTestReport->currentPage()])}}" class="action-icon"><i class="mdi mdi-square-edit-outline"></i></a>
                                
                                 
                                 <input type="hidden" name="id" value="{{$showTest->id}}"/>
                                  <a href="#" id="{{$showTest->id}}" class="action-icon btnTestReportDelete"><i class="mdi mdi-delete"></i></a>
                                </td>
                                
                            </tr>
                            @endforeach
                            </tbody>
                        </table>

                         <table style="display: none;" class="table mb-0" id="searchTestReportTable">
                            <thead style="background-color: #4281DD;">
                            <tr>
                                
                                <th width="30%" style="color: white; text-align: center;">Test Number</th>
                                <th width="30%" style="color: white; text-align: center;">Name</th>
                                <th width="20%" style="color: white; text-align: center;">Clinic Or Hospital Name</th>
                                <th width="20%" style="color: white; text-align: center;">Action</th>
                            </tr>
                            </thead>
                            <tbody>                             
                              <!--LOADING IMAGE DIV START HERE..-->
                              <div id="loadingImg" style="display: none;" ><img src="{{asset('loading_img/Spinner.gif')}}" /></div>
                              <!--LOADING IMAGE DIV END HERE..-->
                            </tbody>
                        </table>
                    </div> <!-- end table-responsive-->
                    <br/>
                   <div id="paginationLink">
                      {{ $showAllTestReport->links() }}
                   </div>

                </div> <!-- end card-box -->
            </div> <!-- end col -->

        </div>
        <!--- end row -->
                       
@endsection