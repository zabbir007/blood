@extends('layouts.user')

@section('title')  All Equipment @endsection

@section('content')

<style>
::placeholder {
  color: white;
  opacity: 0.6; /* Firefox */
}

:-ms-input-placeholder { /* Internet Explorer 10-11 */
 color: white;
}

::-ms-input-placeholder { /* Microsoft Edge */
 color: white;
}
.searchBox{
  margin-left: 20%;width:60%;border:1px solid #cce6ff;border-radius:5px;padding: 5px;background-color: #001a33;color:white;
}
</style>
            <div class="row">
                <div class="col-12">
                                
                    <div class="row">
                        <div class="col-lg-12">
                                
                            <div style="margin-top: 20px;">
                                <a href="{{route('createEquipment')}}">
                                 <button type="button" class="btn btn-success  waves-effect waves-light ">Add New Equipment</button>
                                </a> 
                            </div>
                               <br/> 
                                <?php 
                                        $message=Session::get('message');
                                        if($message){

                                            ?>
                                            <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                                <?php
                                                    echo $message;
                                                    Session::put('message','');
                                                ?>
                                            </div>
                                            <?php
                                        
                                    }
                                    ?>              

                                <div class="card-box">
                                    <h4 class="header-title" style="text-align: center">All Equipment</h4>
                                    

                                    <div class="table-responsive">
                                        <table class="table mb-0" id="paymentTable">
                                            <thead class="thead-dark">
                                            <tr>
                                                 <th style="text-align: center;">Equipment Name</th>
                                                 <th style="text-align: center;">Equipment Price</th>
                                                 <th style="text-align: center;">Additional Price</th>
                                                 <th style="text-align: center;">Quantity</th>
                                                 <th style="text-align: center;">Total Price</th>
                                                 <th style="text-align: center;">Remark</th>
                                                 <th style="text-align: center;">Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                             @foreach($row as $equipmentShow)
                                            <tr>
                                                <td style="text-align: center;">{{$equipmentShow->equipmentName}}</td>
                                                <td style="text-align: center;">{{$equipmentShow->equipmentPrice}}</td>
                                                <td style="text-align: center;">{{$equipmentShow->additionalPrice}}</td>
                                                <td style="text-align: center;">{{$equipmentShow->quantity}}</td>
                                                <td style="text-align: center;">{{$equipmentShow->totalPrice}}</td>
                                                <td style="text-align: center;">{{$equipmentShow->remark}}</td>
                                                <td style="text-align: center;">
                                                    <a href="{{route('editEquipment',[$equipmentShow->id,$row->currentPage()])}}" class="btn btn-warning  waves-effect waves-light">Edit</a>
                                                    <input type="hidden" name="page" value="{{$row->currentPage()}}"/>
                                                    <input type="hidden" name="id" value="{{$equipmentShow->id}}"/>
                                                    <a  href="#" id="{{$equipmentShow->id}}" class="btn btn-danger  waves-effect waves-light btnEquipmentDelete">Delete</a>
                                                </td>
                                            </tr>
                                             @endforeach 
                                            </tbody>
                                        </table>
                                    </div> <!-- end table-responsive-->
                                  <br/>
                                   <div id="paginationLink">
                                      {{ $row->links() }}
                                   </div>
                                </div> <!-- end card-box -->
                                </div> <!-- end card -->
                            </div><!-- end col-->
                        </div>
                        <!-- end row-->



@endsection