@extends('layouts.user')




@section('title') 

 
        Create Test
   

@endsection


@section('content')

<style type="text/css">
	.indoorDonor{
		
		background-color: #fff;
	}


	@media print {

	  body * {
	  	margin-top: 15px;
	    visibility: hidden;
	  }
	  
	  #printInvoice, #printInvoice * {
	    visibility: visible;
	  }
	  #notPrint, #notPrint *{
	  	visibility: hidden;
	  }
	  #searchTestBox, #searchTestBox *{
	  	visibility: hidden;
	  }
	  #searchIndoorDonor, #searchIndoorDonor *{
	  	visibility: hidden;
	  }
	  #printInvoice {
	    position: absolute;
	    left: 0;
	    top: 0;
	  }



	    
	 


	}

</style>

<div id="printInvoice">
	 <div class="container ">

	 	<div id="searchIndoorDonor" style="margin-left: 50px; margin-top: 5px;">
	 		 @if($errors->any())
                    
                <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">
                   

                           <ul>
                               @foreach($errors->all() as $error)
                                    <li>{{$error}}</li>
                               @endforeach
                           </ul>
                       
                   
                </div>
                 @endif

                  <?php 
                    $messageWarning=Session::get('messageWarning');
                    if($messageWarning){

                        ?>
                        <div style="margin-top: 40px;" class="alert alert-warning alert-dismissible bg-warning text-white border-0 fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <?php
                                echo $messageWarning;
                                Session::put('messageWarning','');
                            ?>
                        </div>
                        <?php
                    
                }
                ?>

	 		<form class="parsley-examples" action="{{route('searchTest')}}"  method="post"  >
				@csrf
				<div class="row">
					<div class="col-6">
						<input type="text" class="form-control" name="id" style="width:90%"  required  data-parsley-required-message="please Enter Test Number" placeholder="please Enter Test Number" />
				
					</div>
					<div class="col-3">
						
						<button  class="btn btn-success"  type="submit" >Search  Test</button>
						
					</div>
					
				</div>
	 		
	 		
	 		</form>
	 	</div>
<div class="row" style="margin-top: 5%;margin-left:5px;" >
	<div class="col-12 ">
		<form class="parsley-examples" action="{{route('saveTest')}}"  method="post"  >
			@csrf

			<div class="indoorDonor">
			    <br/><br/>
				<div class="col-12">					
				
						<span style="margin-left: 600px;">Date:</span> 
						<span >
						<input name="date" style="border:0; border-bottom: 1px  dotted; width:20%" type="text"  >
					    </span>
					   
				</div>
				<br/><br/>
				<div class="col-12">					
				
						<span style="margin-left: 3px;">Name Of Patient :</span> 
						<span>
						<input name="name" required  data-parsley-required-message="Please Enter Patient Name"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:86%" type="text"  >
					    </span>
				</div>
				
				<br/>
				<div class="col-12">					
				
						<span style="margin-left: 3px;">Age :</span> 
						<span>
						<input name="age" required  data-parsley-required-message="Please Enter Age"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:30%" type="text"  >
					    </span>
					    <span style="margin-left: 3px;">Sex :</span> 
						<span style="margin-left: 10px;">
						<input type="checkbox" value="Male" name="sex">Male
					    </span>
					    <span>
						<input type="checkbox" value="Female" name="sex">Female
					    </span>
				</div>
				<br/>
				<div class="col-12">					
				
						<span style="margin-left: 3px;">Blood group:</span> 
						<span style="margin-left: 10px;">
						<input type="checkbox" value="op" name="group">O+
					    </span>
					    <span>
						<input type="checkbox" value="on" name="group">O-
					    </span>
					    <span style="margin-left: 20px;">
						<input type="checkbox" value="abp" name="group">AB+
					    </span>
					    <span>
						<input type="checkbox" value="abn" name="group">AB-
					    </span>
					    <span style="margin-left: 20px;">
						<input type="checkbox" value="ap" name="group">A+
					    </span>
					    <span>
						<input type="checkbox" value="an" name="group">A-
					    </span>
					    <span style="margin-left: 20px;">
						<input type="checkbox" value="bp" name="group">B+
					    </span>
					    <span>
						<input type="checkbox" value="bn" name="group">B-
					    </span>
				</div>
			</br>
				<div class="col-12">					
				
						<span style="margin-left: 3px;">Refd.by/Hospital/Clinic :</span> 
						<span>
						<input name="referance" required  data-parsley-required-message="Please Enter Hospital Or Clinic Name"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:82%" type="text"  >
					    </span>
				</div>
				<br/>
				<div class="col-12">					
				
						<span style="margin-left: 3px;">Ward :</span> 
						<span>
						<input name="ward" required  data-parsley-required-message="Please Enter Ward No"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:30%" type="text"  >
					    </span>
					    <span style="margin-left: 3px;">Bed No :</span> 
						<span>
						<input name="bed" required  data-parsley-required-message="Please Enter Bed Number"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:30%" type="text"  >
					    </span>
				</div>

				<div class="col-12">
		            <div class="card"  style="" >
		               
		                <div class="card-body">
		                	 
		                   <div class="form-group position-relative mb-3">

		                       <!--  <label>Chose Test<span class="text-danger">*</span></label></br>
		                        <input type="text" id="searchTestBox" style=" @media (max-width:969px) and (min-width:1000px) {margin-top: 10px;}​" class="searchBox" placeholder="Search Test" /> -->
                                
		                       <!--  <div id="showTotalAmount">
								    
								</div>
                                </br>
								<div id="paidTotalAmount" style="display: none;">
								    <label>Total Paid Amount</label>
								    <input type="Number" name="paidAmount" id="paidAmount">
								</div> -->

								<div class="row">
								    <div class="col-sm-6">
								        <div id="showTotalAmount">
								    
								        </div>
								    </div>
								    <div class="col-sm-6" id="paidTotalAmount" style="display: none;">
								       <label>Total Paid Amount</label>
								       <input type="Number" name="paidAmount" id="paidAmount" class="form-control mb-2">
								    </div>
								</div>

		                        <table class="table mb-0" id="showTest" style="margin-top: 3%">
                            
		                             <tbody> 

		                                @foreach($testAmount as $test)
		                                 <tr>
		                                 	<td>
		                                	<input class="my-activity" type="checkbox" name="test[]" value="{{$test->id}}">Test Name :{{$test->testName}} Test Amount : {{$test->testAmount}}
		                                    </td>
		                                </tr>
		                                @endforeach
		                            </tbody>
		                        </table>
                                
		                        <table style="display: none;" class="table mb-0" id="searchShowTest">
		                           <head>
		                           	 <tr>
		                           	 	
		                           	 </tr>
		                           </head>
		                            <tbody>
		                               
		                            
		                            </tbody>
		                        </table>
		                           
		                               
		                      
		                    </div>
		                <!--  <strong>Total Amount :</strong>: <input type="text" name="amount" id="amount" /> -->

		                </div>
		            </div> <!-- end card-box-->
		        </div> <!-- end col -->

               
				
				<br/>
				<br/>

			</div>

		
	   
	</div> <!-- end col -->
	

	
	<div id="notPrint" style="margin-left: 10px; margin-top: 30px;">
            	
        <button  class="btn btn-success"  type="submit" id="saveTestBtn" >Save Test</button>
        <button type="button"  class="btn btn-info printNewVotar"  onclick="printPage()">Print</button>
            	
 	</div>

 	</form>

</div>

 
<!-- end row --> 
</div>
</div>


 <script>
    
function printPage() {
  window.print();

}
</script>
@endsection