@extends('layouts.user')

@section('title') User Dashboard @endsection
      
@section('content')

	   
        <div class="row" style="margin-top: 3%;">
            <div class="col-md-6 col-xl-3">
                <div class="widget-rounded-circle card-box">
                    <div class="row">
                        
                        <div class="col-12">
                            <div>
                            	<p class="text-muted mb-1 text-truncate" style="text-align: center;">Total Active Donor</p>
                                <h3 class="text-dark mt-1" style="text-align: center;"><span data-plugin="">{{$activeIndoorDonorInfo}}</span></h3>
                               
                            </div>
                        </div>
                    </div> <!-- end row-->
                </div> <!-- end widget-rounded-circle-->
            </div> <!-- end col-->

            <!-- <div class="col-md-6 col-xl-3">
                <div class="widget-rounded-circle card-box" >
                    <div class="row">
                        
                        <div class="col-12">
                            <div>
                            	<p class="text-muted mb-1 text-truncate" style="text-align: center;">Total Active Outdoor Donor</p>
                                <h3 class="text-dark mt-1" style="text-align: center;"><span data-plugin="">{{$activeOutdoorDonorInfo}}</span></h3>
                               
                            </div>
                        </div> -->
                    <!-- </div> --> <!-- end row-->
              <!--   </div> --> <!-- end widget-rounded-circle-->
          <!--   </div>  --><!-- end col-->
          
            <div class="col-md-6 col-xl-3">
                <div class="widget-rounded-circle card-box">
                   <div class="row">
                        
                        <div class="col-12">
                            <div>
                            	<p class="text-muted mb-1 text-truncate" style="text-align: center;">Total Active Normal Patient</p>
                                <h3 class="text-dark mt-1" style="text-align: center;"><span data-plugin="">{{$activeNormalPatientInfo}}</span></h3>
                               
                            </div>
                        </div>
                    </div> <!-- end row-->
                </div> <!-- end widget-rounded-circle-->
            </div> <!-- end col-->

            <div class="col-md-6 col-xl-3">
                <div class="widget-rounded-circle card-box">
                    <div class="row">
                        
                        <div class="col-12">
                            <div>
                            	<p class="text-muted mb-1 text-truncate" style="text-align: center;">Total Active Thalassemia Patient</p>
                                <h3 class="text-dark mt-1" style="text-align: center;"><span data-plugin="">{{$activeThalassemiaPatientInfo}}</span></h3>
                               
                            </div>
                        </div>
                    </div> <!-- end row-->
                </div> <!-- end widget-rounded-circle-->
            </div> <!-- end col-->
            <div class="col-md-6 col-xl-3">
                <div class="widget-rounded-circle card-box">
                    <div class="row">
                        
                        <div class="col-12">
                            <div>
                            	<p class="text-muted mb-1 text-truncate" style="text-align: center;">Donor Success Report</p>
                                <h3 class="text-dark mt-1" style="text-align: center;"><span data-plugin="">{{$indoorDonorPendingReport}}</span></h3>
                               
                            </div>
                        </div>
                    </div> <!-- end row-->
                </div> <!-- end widget-rounded-circle-->
            </div> <!-- end col-->

           <!--  <div class="col-md-6 col-xl-3">
                <div class="widget-rounded-circle card-box">
                    <div class="row">
                        
                        <div class="col-12">
                            <div>
                            	<p class="text-muted mb-1 text-truncate" style="text-align: center;">Outdoor Donor Success Report</p>
                                <h3 class="text-dark mt-1" style="text-align: center;"><span data-plugin="">{{$outdoorDonorPendingReport}}</span></h3>
                               
                            </div>
                        </div> -->
                   <!--  </div>  --><!-- end row-->
               <!--  </div> --> <!-- end widget-rounded-circle-->
           <!--  </div>  --><!-- end col-->

            <div class="col-md-6 col-xl-3">
                <div class="widget-rounded-circle card-box">
                   <div class="row">
                        
                        <div class="col-12">
                            <div>
                            	<p class="text-muted mb-1 text-truncate" style="text-align: center;">Success Test</p>
                                <h3 class="text-dark mt-1" style="text-align: center;"><span data-plugin="">{{$pendingTestReport}}</span></h3>
                               
                            </div>
                        </div>
                    </div> <!-- end row-->
                </div> <!-- end widget-rounded-circle-->
            </div> <!-- end col-->
        </div>
  
@endsection