@extends('layouts.user')

@section('title') Edit Item Name @endsection

@section('content')


<div class="row" style="margin-top: 20px;">
        <div class="col-lg-6 offset-3">
        	<a href="{{route('showItemName')}}">
        	<button class="btn btn-primary" type="submit">Show All Item Name</button>
        	</a>
            <div class="card" style="margin-top: 10px;">
                <div class="card-body">
                	<h4 class="header-title" style="text-align: center">Edit Payment Type</h4>
                    <form class="parsley-examples"  method="post" action="{{route('updateItemName')}}" novalidate>
                       @csrf

                     
                      <div class="form-group">
                            <label>Select Item Group</label>
                            <div>
                                <select class="selectpicker" name="itemGroup" data-style="btn-secondary">
                                    @foreach($accountGroup as $account) 
                                    <option value="{{$account->itemName}}">{{$account->itemName}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div> 
                        
                      <div class="form-group">
                            <label>Item Name</label>
                            <div>
                        		<input type="hidden" name="id" value="{{$singlePaymentInfo->id}}" />
                        		<input type="hidden" name="page" value="{{$page}}" />
                        		
                                <input type="text" name="itemName" value="{{$singlePaymentInfo->itemName}}"  class="form-control parsley-validated" required
                                        data-parsley-required-message="Please Enter Item Name"   placeholder="Item Name"/>
                            </div>
                        </div>
                        <button  class="btn btn-success" type="submit">Update</button>
                    </form>
                    
                </div> <!-- end card-body-->
            </div> <!-- end card-->
        </div> <!-- end col-->
    </div>


@endsection