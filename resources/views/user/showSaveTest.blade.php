@extends('layouts.user')




@section('title') 

 
        Create Test
   

@endsection


@section('content')

<style type="text/css">
	.indoorDonor{
		
		background-color: #fff;
	}


	@media print {

	  body * {
	  	margin-top: 15px;
	    visibility: hidden;
	  }
	  
	  #printInvoice, #printInvoice * {
	    visibility: visible;
	  }
	  #notPrint, #notPrint *{
	  	visibility: hidden;
	  }
	  #searchIndoorDonor, #searchIndoorDonor *{
	  	visibility: hidden;
	  }
	  #printInvoice {
	    position: absolute;
	    left: 0;
	    top: 0;
	  }



	    
	 


	}

</style>

<div id="printInvoice">
	 <div class="container ">

	 	<div id="searchIndoorDonor" style="margin-left: 50px; margin-top: 5px;">
	 		 @if($errors->any())
                    
                <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">
                   

                           <ul>
                               @foreach($errors->all() as $error)
                                    <li>{{$error}}</li>
                               @endforeach
                           </ul>
                       
                   
                </div>
                 @endif

                  <?php 
                    $messageWarning=Session::get('messageWarning');
                    if($messageWarning){

                        ?>
                        <div style="margin-top: 40px;" class="alert alert-warning alert-dismissible bg-warning text-white border-0 fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <?php
                                echo $messageWarning;
                                Session::put('messageWarning','');
                            ?>
                        </div>
                        <?php
                    
                }
                ?>

	 		<form class="parsley-examples" action="{{route('searchTest')}}"  method="post"  >
				@csrf
				<div class="row">
					<div class="col-6">
						<input type="text" class="form-control" name="id" style="width:90%"  required  data-parsley-required-message="please Enter Test Number" placeholder="please Enter Test Number" />
				
					</div>
					<div class="col-3">
						
						<button  class="btn btn-success"  type="submit" >Search  Test</button>
						
					</div>
					
				</div>
	 		
	 		
	 		</form>
	 	</div>
<div class="row" style="margin-top: 5%;margin-left:5px;" >
	<div class="col-12 ">
		<form class="parsley-examples" action="{{route('saveTest')}}"  method="post"  >
			@csrf

			<div class="indoorDonor">
				<h6 style="text-align: right;">Test No- {{$allTestInfo->testNumber}}</h6>
			    <br/><br/>
				<div class="col-12">					
				
						<span style="margin-left: 600px;">তারিখ:</span> 
						<span >
						<input name="date" readonly="" value="{{$allTestInfo->date}}" style="border:0; border-bottom: 1px  dotted; width:20%" type="text"  >
					    </span>
					   
				</div>
				<br/><br/>
				<div class="col-12">					
				
						<span style="margin-left: 3px;">Name Of Patient :</span> 
						<span>
						<input name="name" readonly="" value="{{$allTestInfo->name}}" required  data-parsley-required-message="Please Enter Patient Name"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:86%" type="text"  >
					    </span>
				</div>
				
				<br/>
				<div class="col-12">					
				
						<span style="margin-left: 3px;">Age :</span> 
						<span>
						<input name="age" required readonly="" value="{{$allTestInfo->age}}"  data-parsley-required-message="Please Enter Age"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:30%" type="text"  >
					    </span>
					    <span style="margin-left: 3px;">Sex :</span> 
						<span>
						<input name="sex" required readonly="" value="{{$allTestInfo->sex}}" data-parsley-required-message="Please Enter Sex"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:30%" type="text"  >
					    </span>
				</div>
			</br>

			    <div class="col-12">					
				
						<span style="margin-left: 3px;">Blood group:</span> 
						
					    <span style="margin-left: 10px;">
						<input type="checkbox" value="op" name="group" readonly="" <?php if ($allTestInfo->group=='op') {echo "checked";}?> >O+
					    </span>
					    <span>
						<input type="checkbox" value="on" name="group" readonly="" <?php if ($allTestInfo->group=='on') {echo "checked";}?> >O-
					    </span>
					    <span style="margin-left: 20px;">
						<input type="checkbox" value="abp" name="group" readonly="" <?php if ($allTestInfo->group=='abp') {echo "checked";}?> >AB+
					    </span>
					    <span>
						<input type="checkbox" value="abn" name="group" readonly="" <?php if ($allTestInfo->group=='abn') {echo "checked";}?> >AB-
					    </span>
					    <span style="margin-left: 20px;">
						<input type="checkbox" value="ap" name="group" readonly="" <?php if ($allTestInfo->group=='ap') {echo "checked";}?> >A+
					    </span>
					    <span>
						<input type="checkbox" value="an" name="group" readonly="" <?php if ($allTestInfo->group=='an') {echo "checked";}?> >A-
					    </span>
					    <span style="margin-left: 20px;">
						<input type="checkbox" value="bp" name="group" readonly="" <?php if ($allTestInfo->group=='bp') {echo "checked";}?> >B+
					    </span>
					    <span>
						<input type="checkbox" value="bn" name="group" readonly="" <?php if ($allTestInfo->group=='bn') {echo "checked";}?> >B-
					    </span>
					    
				</div>
				<br/>
				<div class="col-12">					
				
						<span style="margin-left: 3px;">Refd.by/Hospital/Clinic :</span> 
						<span>
						<input name="referance" required  readonly="" value="{{$allTestInfo->referance}}" data-parsley-required-message="Please Enter Hospital Or Clinic Name"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:82%" type="text"  >
					    </span>
				</div>
				<br/>
				<div class="col-12">					
				
						<span style="margin-left: 3px;">Ward :</span> 
						<span>
						<input name="ward" required readonly="" value="{{$allTestInfo->ward}}" data-parsley-required-message="Please Enter Ward No"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:30%" type="text"  >
					    </span>
					    <span style="margin-left: 3px;">Bed No :</span> 
						<span>
						<input name="bed" required readonly="" value="{{$allTestInfo->bed}}" data-parsley-required-message="Please Enter Bed Number"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:30%" type="text"  >
					    </span>
				</div>

				<div class="col-12">
		            <div class="card">
		               
		                <div class="card-body">
		                   <div class="form-group position-relative mb-3">
		                        <label>Test Required :<span class="text-danger">*</span></label>
		                       <?php
		                            $firstAmount=0;
				                    $pkgCount=count($amount);
				                    for($i=0;$i<$pkgCount;$i++){
				                       
				                        $testAmountInfo=DB::table('test_amount')
										                  ->where('id',$amount[$i])
										                  ->get();
											foreach($testAmountInfo as $te){
									           echo $te->testName.' , ';
									           $firstAmount += $te->testAmount;
									        }
									         
				                    }
			                  
				            	?>
		                       
		                        
		                    </div>

		                 <strong>Total Amount:</strong> <h5>{{$firstAmount.'  Taka'}}</h5></br>
		                 <strong>Total Paid Amount:</strong> <h5>{{$allTestInfo->paidAmount}}</h5></br>
		                 <strong>Total Remaining Amount:</strong> <h5>{{$allTestInfo->totalAmount-$allTestInfo->paidAmount}}</h5></br>
		                </div>
		            </div> <!-- end card-box-->
		        </div> <!-- end col -->
               
				
				<br/>
				<br/>

			</div>

			<div class="row">
			    <div class="col-sm">
			      <img src="{{asset($allTestInfo->barCodeImage)}}" alt="{{$allTestInfo->barCodeImage}}" width="200" height="90">
			    </div>
			    <div class="col-sm">
			      <img src="{{asset($allTestInfo->barCodeImage)}}" alt="{{$allTestInfo->barCodeImage}}" width="200" height="90">
			    </div>
			    <div class="col-sm">
			      <img src="{{asset($allTestInfo->barCodeImage)}}" alt="{{$allTestInfo->barCodeImage}}" width="200" height="90">
			    </div>
			</div>

		
	   
	</div> <!-- end col -->
	

	
	<div id="notPrint" style="margin-left: 10px; margin-top: 30px;">
		<a href="{{url()->previous()}}" type="button"  class="btn btn-success printJobNgo" id="notPrint">Back</a>    	
       <!--  <button  class="btn btn-success"  type="submit" >Save Test</button> -->
        <button type="button"  class="btn btn-info printNewVotar"  onclick="printPage()">Print</button>
            	
 	</div>

 	</form>

</div>

 
<!-- end row --> 
</div>
</div>


 <script>
    
function printPage() {
  window.print();

}
</script>
@endsection