@extends('layouts.user')




@section('title') 

 
        Create Outdoor Donor
   

@endsection


@section('content')

<style type="text/css">
	.indoorDonor{
		
		background-color: #fff;
	}


	@media print {

	  body * {
	  	margin-top: 15px;
	    visibility: hidden;
	  }
	  
	  #printInvoice, #printInvoice * {
	    visibility: visible;
	  }
	  #notPrint, #notPrint *{
	  	visibility: hidden;
	  }
	  #searchIndoorDonor, #searchIndoorDonor *{
	  	visibility: hidden;
	  }
	  #printInvoice {
	    position: absolute;
	    left: 0;
	    top: 0;
	  }



	    
	 


	}

</style>

<div id="printInvoice">
	 <div class="container ">

	 	<div id="searchIndoorDonor" style="margin-left: 50px; margin-top: 5px;">
	 		 @if($errors->any())
                    
                <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">
                   

                           <ul>
                               @foreach($errors->all() as $error)
                                    <li>{{$error}}</li>
                               @endforeach
                           </ul>
                       
                   
                </div>
                 @endif

                  <?php 
                    $messageWarning=Session::get('messageWarning');
                    if($messageWarning){

                        ?>
                        <div style="margin-top: 40px;" class="alert alert-warning alert-dismissible bg-warning text-white border-0 fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <?php
                                echo $messageWarning;
                                Session::put('messageWarning','');
                            ?>
                        </div>
                        <?php
                    
                }
                ?>

	 		<form class="parsley-examples" action="{{route('searchOutdoorDonor')}}"  method="post"  >
				@csrf
				<div class="row">
					<div class="col-6">
						<input type="text" class="form-control" name="id" style="width:90%"  required  data-parsley-required-message="please Enter Outdoor Donor Identify Number" placeholder="please Enter Outdoor Donor Identify Number or Mobile Number" />
				
					</div>
					<div class="col-3">
						
						<button  class="btn btn-success"  type="submit" >Search  Indoor Donor</button>
						
					</div>
					
				</div>
	 		
	 		
	 		</form>
	 	</div>
<div class="row" style="margin-top: 5%;margin-left:5px;" >
	<div class="col-12 ">
		<form class="parsley-examples" action="{{route('addOutdoorDonor')}}"  method="post"  >
			@csrf

			<div class="indoorDonor">
				<br/>
				<div class="col-12" >
					<h2 style="text-align:center; width: 31%;margin-left: 28%;color: red;">রক্ত কর্মসূচী</h2>
					
				</div>
				<div class="col-12" >
					
					<span style="margin-left: 600px;color: red;">রক্তের ব্যাগ নং:</span>
					<span><input type="text" name="" readonly="" style=" height: 48px;width: 200px;"></span>
				</div>
			
				<div class="col-12" style="margin-top: 5px;">
					
					<span style="margin-left: 561px;color: red;">রক্ত দাতার নিবন্ধন  নং:</span>
					<span><input type="text" name="" readonly="" style=" height: 48px;width: 200px;"></span>
				</div>
				<div class="col-12" style="margin-top: 5px;">
					<h4 style="text-align:center; width: 31%;margin-left: 28%;color: red;">রক্ত দাতার নিবন্ধন ফরম</h4>
					
				</div>
				<br/>
				<div class="col-12">					
				
						<span style="margin-left: 3px;">১.নাম </span> 
						<span>
						<input name="name" required  data-parsley-required-message="দয়া করে নাম লিখুন"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:93%" type="text"  >
					    </span>
				</div>
				<br/>
				<div class="col-12">					
				
						<span style="margin-left: 3px;">২.বয়স </span> 
						<span>
						<input name="age" required  data-parsley-required-message="দয়া করে বয়স লিখুন"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:30%" type="text"  >
					    </span>
					    <span style="margin-left: 3px;">৩. লিঙ্গ :</span> 
						<span style="margin-left: 10px;">
						<input type="checkbox" value="Male" name="sex">পুরুষ
					    </span>
					    <span>
						<input type="checkbox" value="Female" name="sex">মহিলা
					    </span>
				</div>
               <br/>
				<div class="col-12">					
				
						<span style="margin-left: 3px;">৪.মোবাইল </span> 
						<span>
						<input name="phone" required  data-parsley-required-message="দয়া করে মোবাইল নম্বর লিখুন"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:30%" type="text"  >
					    </span>
					   
				</div>

				<br/>
				<div class="col-12">					
				
						<span style="margin-left: 3px;">৫. রক্তের গ্রুপ: <span> 
						<span style="margin-left: 10px;">
						<input type="checkbox" value="op" name="bloodGroup">ও+
					    </span>
					    <span>
						<input type="checkbox" value="on" name="bloodGroup">ও-
					    </span>
					    <span style="margin-left: 20px;">
						<input type="checkbox" value="abp" name="bloodGroup">এ.বি+
					    </span>
					    <span>
						<input type="checkbox" value="abn" name="bloodGroup">এ.বি-
					    </span>
					    <span style="margin-left: 20px;">
						<input type="checkbox" value="ap" name="bloodGroup">এ+
					    </span>
					    <span>
						<input type="checkbox" value="an" name="bloodGroup">এ-
					    </span>
					    <span style="margin-left: 20px;">
						<input type="checkbox" value="bp" name="bloodGroup">বি+
					    </span>
					    <span>
						<input type="checkbox" value="bn" name="bloodGroup">বি-
					    </span>
				</div>
                
                
				<br/>
				<br/>
				<div class="col-12">					
				
						<span style="margin-left: 3px;">
							আমি স্বজ্ঞানে এবং স্বতঃস্ফর্তভাবে স্বেচ্ছায় রক্তদানে ইচ্ছুক আমি আমার রক্তের প্রয়োজনীয় পরীক্ষা নিরীক্ষা করবার জন্য অনুমতি প্রদান করছি এতদ্বারা আমি রক্ত কর্মসুচীকে আমার রক্তদান সম্পকীয় পূর্ব ও পরবর্তী সকল দায় দায়িত্ব থেকে মুক্ত করছি |
					    </span>
					   
				</div>
				<br/>
				<br/>
				<br/>
				<br/>
				<div class="col-12">					
				
						<span style="margin-left: 3px;">তারিখ:</span> 
						<span >
						<input name="date" style="border:0; border-bottom: 1px  dotted; width:20%" type="date"  >
					    </span>
					    <span style="margin-left: 96px;">সময় তুমি হার মেনেছ রক্তদানের কাছে </span>
					    <span style="margin-left: 127px;"> স্বাক্ষর:</span> 
						<span>
						<input name=""  style="border:0; border-bottom: 1px  dotted; width:20%" type="text"  >
					    </span>
					   
				</div>
				<div class="col-12">					
				
						<span style="margin-left: 324px;">পাঁচটি মিনিট করলে খরচ একটি জীবন বাঁচে</span> 
						
				</div>


				<br/>
				<br/>

			</div>

		
	   
	</div> <!-- end col -->
	

	
	<div id="notPrint" style="margin-left: 10px; margin-top: 30px;">
            	
        <button  class="btn btn-success"  type="submit" >Save Outdoor Donor</button>
        <button type="button"  class="btn btn-info printNewVotar"  onclick="printPage()">Print</button>
            	
 	</div>

 	</form>

</div>

 
<!-- end row --> 
</div>
</div>


 <script>
    
function printPage() {
  window.print();

}
</script>
@endsection