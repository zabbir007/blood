@extends('layouts.user')

@section('title')  Show Request @endsection

@section('content')

<style>
::placeholder {
  color: white;
  opacity: 0.6; /* Firefox */
}

:-ms-input-placeholder { /* Internet Explorer 10-11 */
 color: white;
}

::-ms-input-placeholder { /* Microsoft Edge */
 color: white;
}
.searchBox{
  margin-left: 20%;width:50%;border:1px solid #cce6ff;border-radius:5px;padding: 5px;background-color: #001a33;color:white;
}


}
</style>
  <div class="row">
    <div class="col-12">                
      <div class="row">
        <div class="col-lg-12">
          <div style="margin-top: 20px;">
            
            <div class="row">
                <div class="col-6">
                </div>
                <div class="col-6">
                    <!-- <input type="text" id="searchCustomerOngoingOrderBox" style=" @media (max-width:969px) and (min-width:1000px) {margin-top: 10px;}​" class="searchBox" placeholder="Search By Order Number,Delivery Type And Time,Payment Method,Status"/> -->
                    <div class="input-group m-t-10">
                        <input type="text" class="form-control" id="searchCustomerOngoingOrderBox" style=" @media (max-width:969px) and (min-width:1000px) {margin-top: 10px;}​" class="searchBox" placeholder="Search">
                      <span class="input-group-append">
                          <button type="button" class="btn waves-effect waves-light btn-blue"><i class="fa fa-search mr-1"></i> Search</button>
                      </span>
                    </div>
                </div>
            </div> 
          </div>
           <br/>
            <h1 class="header-title">Donor List</h1>
            <div class="card-box">
               
                
                <?php 
                  $message=Session::get('message');
                  if($message){

                      ?>
                      <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                          </button>
                          <?php
                              echo $message;
                              Session::put('message','');
                          ?>
                      </div>
                      <?php
                  
              }
              ?>
              
              @if($errors->any())
          
              <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">
                 

                         <ul>
                             @foreach($errors->all() as $error)
                                  <li>{{$error}}</li>
                             @endforeach
                         </ul>
                     
                 
              </div>
               @endif


                <div class="table-responsive">
                    <table class="table mb-0" id="customerOngoingOrderTable">
                        <thead  style="background-color: #4281DD;">
                        <tr >
                            
                            <th width="20%" style="color: white;text-align: center;">Name</th>
                            <th width="13%" style="color: white;text-align: center;">Email</th>
                            <th width="20%" style="color: white;text-align: center;">Phone</th>
                            <th width="14%" style="color: white;text-align: center;">Status</th>
                            <th width="15%" style="color: white;text-align: center;">Group</th>
                            <th width="10%" style="color: white;text-align: center;">Action</th>
                        </tr>
                        </thead>
                        <tbody >
                       
                        <tr>
                            
                            <td style="border: 2px solid #F7F7F4;text-align: center;">
                             zabbir
                              
                            </td>
                            <td style="border: 2px solid #F7F7F4;text-align: center;">
                             zabbirhossain729@gmail.com
                              
                            </td>
                            <td style="border: 2px solid #F7F7F4;text-align: center;">
                              01933722564
                            </td>
                            <td style="border: 2px solid #F7F7F4;text-align: center;">
                               
                                  <h5><span class="badge badge-info">not accepted</span></h5>
                                
                            </td>
                            <td style="border: 2px solid #F7F7F4;text-align: center;">
                               
                                  <h5>AB+</h5>
                                
                            </td>
                            <td style="border: 4px solid #F7F7F4;text-align: center;">
                             
                           
                               <a href="" class="action-icon"> <i class="mdi mdi-eye"></i></a>

                             <a href="#" id="" class="action-icon btnCustomerOrderCancel"> <i class="mdi mdi-delete"></i></a>

                      
                            </td>
                            
                        </tr>
                      
                        </tbody>
                    </table>

                     <table style="display: none;" class="table mb-0" id="searchCustomerOngoingOrderTable">
                            <thead  style="background-color: #4281DD;">
                              <tr >
                                  
                                  <th width="20%" style="color: white;">Order Number </th>
                                  <th width="13%" style="color: white;">Delivery Type </th>
                                  <th width="20%" style="color: white;">Payment Method</th>
                                  <th width="14%" style="color: white;">Status</th>
                                  <th width="25%" style="color: white;">Action</th>
                              </tr>
                            </thead>
                            <tbody>                             
                              <!--LOADING IMAGE DIV START HERE..-->
                              <div id="loadingImg" style="display: none;" ><img src="{{asset('loading_img/Spinner.gif')}}" /></div>
                              <!--LOADING IMAGE DIV END HERE..-->
                            </tbody>
                    </table>

                    
                </div> <!-- end table-responsive-->
                <br/>
               
                <div class="row">
                  <div class="col-md-6" id="paginationLink">
                    
                   </div>
                  <div  class="col-md-6">
                    
                  </div>
                   
                </div>

            </div> <!-- end card-box -->
        </div> <!-- end col -->

    </div>
  </div>  
    <!--- end row -->

@endsection