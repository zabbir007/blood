@extends('layouts.user')




@section('title') 

 
        Create Indoor Donor
   

@endsection


@section('content')

<style type="text/css">
	.indoorDonor{
		
		background-color: #fff;
	}


	@media print {

	  body * {
	  	margin-top: 15px;
	    visibility: hidden;
	  }
	  
	  #printInvoice, #printInvoice * {
	    visibility: visible;
	  }
	  #notPrint, #notPrint *{
	  	visibility: hidden;
	  }
	  #searchTestBox, #searchTestBox *{
	  	visibility: hidden;
	  }
	  #searchIndoorDonor, #searchIndoorDonor *{
	  	visibility: hidden;
	  }
	  #printInvoice {
	    position: absolute;
	    left: 0;
	    top: 0;
	  }



	    
	 


	}

</style>

<div id="printInvoice">
	 <div class="container ">

	 	<div id="searchIndoorDonor" style="margin-left: 50px; margin-top: 5px;">
	 		 @if($errors->any())
                    
                <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">
                   

                           <ul>
                               @foreach($errors->all() as $error)
                                    <li>{{$error}}</li>
                               @endforeach
                           </ul>
                       
                   
                </div>
                 @endif

                  <?php 
                    $messageWarning=Session::get('messageWarning');
                    if($messageWarning){

                        ?>
                        <div style="margin-top: 40px;" class="alert alert-warning alert-dismissible bg-warning text-white border-0 fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <?php
                                echo $messageWarning;
                                Session::put('messageWarning','');
                            ?>
                        </div>
                        <?php
                    
                }
                ?>

	 		
	 	</div>
<div class="row" style="margin-top: 5%;margin-left:5px;" >
	<div class="col-12 ">
		<form class="parsley-examples" action="{{route('saveTestAmount')}}"  method="post"  >
			@csrf

			<div class="indoorDonor">
				
				<br/>
				<div class="col-12">					
				
						<span style="margin-left: 3px;">1.Donor Name </span> 
						<span>
						<input name="name" required  data-parsley-required-message="Please Enter Donor Name"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:82%" type="text" value="{{$indoorDonorInfo->name}}" >
					    </span>
				</div>
				<br/>
				<div class="col-12">					
				
						<span style="margin-left: 3px;">2.Donor Registration Number</span> 
						<span>
						<input name="nibondonNo" required  data-parsley-required-message="Please Enter Registration Number"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:74%" type="text" value="{{$indoorDonorInfo->nibondonNo}}" >
					    </span>
				</div>

				<br/>
				<div class="col-12">					
				
						<span style="margin-left: 3px;">3.Age</span> 
						<span>
						<input name="age" required  data-parsley-required-message="Please Enter Age Name"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:30%" type="text" value="{{$indoorDonorInfo->age}}" >
					    </span>
					    <span style="margin-left: 3px;">4.Gender</span> 
						<span>
						<input name="sex" required  data-parsley-required-message="Please Enter Gender Name"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:25%" type="text" value="{{$indoorDonorInfo->sex}}" >
					    </span>
					    <span style="margin-left: 3px;">5. Group</span> 
						<span>
						<input name="group" required  data-parsley-required-message="Please Enter Group Name"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:20%" type="text" value="<?php 
						if($indoorDonorInfo->bloodGroup=='op'){echo "ও+";}
						else if($indoorDonorInfo->bloodGroup=='on'){echo "ও-";}
						else if($indoorDonorInfo->bloodGroup=='abp'){echo "এ.বি+";}
						else if($indoorDonorInfo->bloodGroup=='abn'){echo "এ.বি-";}
						else if($indoorDonorInfo->bloodGroup=='ap'){echo "এ+";}
						else if($indoorDonorInfo->bloodGroup=='an'){echo "এ-";}
						else if($indoorDonorInfo->bloodGroup=='bp'){echo "বি+";}
						else if($indoorDonorInfo->bloodGroup=='bn'){echo "বি-";} ?>" >
					    </span>
				</div>
               <br/>
               <br/>
               <br/>
                <div class="col-12">
		            <div class="card">
		               
		                <div class="card-body">
		                	 
		                   <div class="form-group position-relative mb-3">
		                        <!-- <label>Chose Test<span class="text-danger">*</span></label></br>
		                        <input type="text" id="searchTestBox" style=" @media (max-width:969px) and (min-width:1000px) {margin-top: 10px;}​" class="searchBox" placeholder="Search Test" /> -->
		                        <div class="row">
								    <div class="col-sm-6">
								        <div id="showTotalAmount">
								    
								        </div>
								    </div>
								    <div class="col-sm-6" id="paidTotalAmount" style="display: none;">
								       <label>Total Paid Amount</label>
								       <input type="Number" name="paidAmount" id="paidAmount" class="form-control mb-2">
								    </div>
								</div>
		                        <table class="table mb-0" id="showTest" style="margin-top: 3%">
                            
		                             <tbody> 

		                                @foreach($testAmount as $test)
		                                 <tr>
		                                 	<td>
		                                	<input class="my-activity" type="checkbox" name="test[]" value="{{$test->id}}">Test Name :{{$test->testName}} Test Amount : {{$test->testAmount}}
		                                    </td>
		                                </tr>
		                                @endforeach
		                            </tbody>
		                        </table>

		                        <table style="display: none;" class="table mb-0" id="searchShowTest">
		                           <head>
		                           	 <tr>
		                           	 	
		                           	 </tr>
		                           </head>
		                            <tbody>
		                               
		                            
		                            </tbody>
		                        </table>
		                           
		                               
		                      
		                    </div>
		                <!--  <strong>Total Amount :</strong>: <input type="text" name="amount" id="amount" /> -->

		                </div>
		            </div> <!-- end card-box-->
		        </div> <!-- end col -->

			
				
				

			</div>



		
	   
	</div> <!-- end col -->
	

	
	<div id="notPrint" style="margin-left: 10px; margin-top: 30px;">
		<a href="{{url()->previous()}}" type="button"  class="btn btn-success printJobNgo" id="notPrint">Back</a>    	
       <button  class="btn btn-success" id="saveTestBtn" type="submit" >Save</button>
        <!-- <button type="button"  class="btn btn-info printNewVotar"  onclick="printPage()">Print</button> -->
            	
 	</div>

 	</form>

</div>

 
<!-- end row --> 
</div>
</div>


 <script>
    
function printPage() {
  window.print();

}
</script>
@endsection