@extends('layouts.user')




@section('title') 

 
        Create Donor Recozation
   

@endsection


@section('content')

<style type="text/css">
	.indoorDonor{
		
		background-color: #fff;
	}


	@media print {

	  body * {
	  	margin-top: 15px;
	    visibility: hidden;
	  }
	  
	  #printInvoice, #printInvoice * {
	    visibility: visible;
	  }
	  #notPrint, #notPrint *{
	  	visibility: hidden;
	  }
	  #searchTestBox, #searchTestBox *{
	  	visibility: hidden;
	  }
	  #searchIndoorDonor, #searchIndoorDonor *{
	  	visibility: hidden;
	  }
	  #printInvoice {
	    position: absolute;
	    left: 0;
	    top: 0;
	  }



	    
	 


	}

</style>

<div id="printInvoice">
	 <div class="container ">

	 	<div id="searchIndoorDonor" style="margin-left: 50px; margin-top: 5px;">
	 		 @if($errors->any())
                    
                <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">
                   

                           <ul>
                               @foreach($errors->all() as $error)
                                    <li>{{$error}}</li>
                               @endforeach
                           </ul>
                       
                   
                </div>
                 @endif

                  <?php 
                    $messageWarning=Session::get('messageWarning');
                    if($messageWarning){

                        ?>
                        <div style="margin-top: 40px;" class="alert alert-warning alert-dismissible bg-warning text-white border-0 fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <?php
                                echo $messageWarning;
                                Session::put('messageWarning','');
                            ?>
                        </div>
                        <?php
                    
                }
                ?>

                <?php 
                    $message=Session::get('message');
                    if($message){

                        ?>
                        <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <?php
                                echo $message;
                                Session::put('message','');
                            ?>
                        </div>
                        <?php
                    
                }
                ?>
	 	</div>
<div class="row" style="margin-top: 5%;margin-left:5px;" >
	<div class="col-12 ">
		<form class="parsley-examples" action="{{route('saveDonorRecozation')}}"  method="post"  >
			@csrf

			<div class="indoorDonor">
			    <br/><br/>
				<div class="col-12">					
				
						<span style="margin-left: 600px;">Date:</span> 
						<span >
						<input name="date" style="border:0; border-bottom: 1px  dotted; width:20%" type="date"  >
					    </span>
					   
				</div>
				<br/><br/>
				<div class="col-12">					
				
						<span style="margin-left: 3px;">Name Of Patient :</span> 
						<span>
						<input name="name" required  data-parsley-required-message="Please Enter Patient Name"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:86%" type="text"  >
					    </span>
				</div>
				
				<br/>
				<div class="col-12">					
				
						<span style="margin-left: 3px;">Age :</span> 
						<span>
						<input name="age" required  data-parsley-required-message="Please Enter Age"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:30%" type="text"  >
					    </span>
					    <span style="margin-left: 3px;">Sex :</span> 
						<span style="margin-left: 10px;">
						<input type="checkbox" value="Male" name="sex">Male
					    </span>
					    <span>
						<input type="checkbox" value="Female" name="sex">Female
					    </span>
				</div>
				<br/>
				<div class="col-12">					
				
						<span style="margin-left: 3px;">Needed Blood group:</span> 
						<span style="margin-left: 10px;">
						<input type="checkbox" value="op" name="group">O+
					    </span>
					    <span>
						<input type="checkbox" value="on" name="group">O-
					    </span>
					    <span style="margin-left: 20px;">
						<input type="checkbox" value="abp" name="group">AB+
					    </span>
					    <span>
						<input type="checkbox" value="abn" name="group">AB-
					    </span>
					    <span style="margin-left: 20px;">
						<input type="checkbox" value="ap" name="group">A+
					    </span>
					    <span>
						<input type="checkbox" value="an" name="group">A-
					    </span>
					    <span style="margin-left: 20px;">
						<input type="checkbox" value="bp" name="group">B+
					    </span>
					    <span>
						<input type="checkbox" value="bn" name="group">B-
					    </span>
				</div>
			</br>
				<div class="col-12">					
				
						<span style="margin-left: 3px;">Refd.by/Hospital/Clinic :</span> 
						<span>
						<input name="referance" required  data-parsley-required-message="Please Enter Hospital Or Clinic Name"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:82%" type="text"  >
					    </span>
				</div>
				<br/>
				<div class="col-12">					
				
						<span style="margin-left: 3px;">Phone :</span> 
						<span>
						<input name="phone" required  data-parsley-required-message="Please Enter Phone Number"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:30%" type="text"  >
					    </span>
					    <span style="margin-left: 3px;">Amount Of Blood Bag :</span> 
						<span>
						<input name="bag" required  data-parsley-required-message="Please Enter Number Of Blood Bag"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:30%" type="text"  >
					    </span>
				</div>
				<br/>
				<br/>

			</div>

		
	   
	</div> <!-- end col -->
	

	
	<div id="notPrint" style="margin-left: 10px; margin-top: 30px;">
        <a href="{{url()->previous()}}" type="button"  class="btn btn-success printJobNgo" id="notPrint">Back</a>   	
        <button  class="btn btn-success"  type="submit" id="saveTestBtn" >Save</button>
       
            	
 	</div>

 	</form>

</div>

 
<!-- end row --> 
</div>
</div>


 <script>
    
function printPage() {
  window.print();

}
</script>
@endsection