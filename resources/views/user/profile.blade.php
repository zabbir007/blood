@extends('layouts.user')

@section('title')  Profile @endsection

@section('content')

<!-- Start Content-->
<div class="container-fluid">
	<!-- start page title -->
	<div class="row">
	    <div class="col-12">
	        <div class="page-title-box col-lg-8 offset-lg-2  col-xl-8 offset-xl-2">
	            
	            <h4 class="page-title">Profile</h4>
	        </div>
	    </div>
	</div>
	<!-- end page title -->



                        <div class="row">
                            <div class="col-lg-8 offset-lg-2  col-xl-8 offset-xl-2">
                                <div class="card-box text-center">
                                    
                                     <img src="{{asset( Session::get('userImage') )}}"  alt="{{Session::get('userName')}}" title="{{Session::get('userName')}}" class="img-fluid rounded-circle" width="120"/>
                                    <h4 class="mb-0" style="font-size: 30px;">{{Session::get('userName')}}</h4>
                                    
                                   

                                    <div class="text-left mt-3 col-lg-6 offset-lg-4  col-xl-6 offset-xl-4">

                                    	<?php 
                                        $message=Session::get('message');
                                        if($message){

                                            ?>
                                            <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                                <?php
                                                    echo $message;
                                                    Session::put('message','');
                                                ?>
                                            </div>
                                            <?php
                                        
                                    }
                                    ?>
                                    

                                    	@if($errors->any())
                                    
	                                    <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">
	                                       

	                                               <ul>
	                                                   @foreach($errors->all() as $error)
	                                                        <li>{{$error}}</li>
	                                                   @endforeach
	                                               </ul>
	                                           
	                                       
	                                    </div>
	                                     @endif
                                        <h4 class="font-13 text-uppercase">About Me :</h4>
                                       
                                        <p class="text-muted mb-2 font-13" ><strong>Full Name :</strong> <span class="ml-2">{{$userInfo->name}}</span></p>

                                        <p class="text-muted mb-2 font-13"><strong>Mobile :</strong><span class="ml-2">{{$userInfo->email}}
                                                </span></p>

                                        <p class="text-muted mb-2 font-13"><strong>Email :</strong> <span class="ml-2 ">{{$userInfo->phone}}</span></p>

                                        <p class="text-muted mb-1 font-13"><strong>Status :</strong> <span class="ml-2"><?php if($userInfo->status=='1'){echo "Active";}else{echo "DeActive";} ?></span></p>

                                        

                                       
                                    </div>

                                    
                                </div> <!-- end card-box -->


                                
                                
                        <!-- Add region modal end here...-->

                            </div> <!-- end col-->

                            
                        </div>
                        <!-- end row-->

</div>

@endsection