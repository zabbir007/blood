@extends('layouts.user')




@section('title') 

 
        Create Patient
   

@endsection


@section('content')

<style type="text/css">
	.indoorDonor{
		
		background-color: #fff;
	}


	@media print {

	  body * {
	  	margin-top: 15px;
	    visibility: hidden;
	  }
	  
	  #printInvoice, #printInvoice * {
	    visibility: visible;
	  }
	  #notPrint, #notPrint *{
	  	visibility: hidden;
	  }
	  #searchIndoorDonor, #searchIndoorDonor *{
	  	visibility: hidden;
	  }
	  #printInvoice {
	    position: absolute;
	    left: 0;
	    top: 0;
	  }



	    
	 


	}

</style>

<div id="printInvoice">
	 <div class="container ">

	 	<div id="searchIndoorDonor" style="margin-left: 50px; margin-top: 5px;">
	 		 @if($errors->any())
                    
                <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">
                   

                           <ul>
                               @foreach($errors->all() as $error)
                                    <li>{{$error}}</li>
                               @endforeach
                           </ul>
                       
                   
                </div>
                 @endif

                  <?php 
                    $messageWarning=Session::get('messageWarning');
                    if($messageWarning){

                        ?>
                        <div style="margin-top: 40px;" class="alert alert-warning alert-dismissible bg-warning text-white border-0 fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <?php
                                echo $messageWarning;
                                Session::put('messageWarning','');
                            ?>
                        </div>
                        <?php
                    
                }
                ?>

	 		<form class="parsley-examples" action="{{route('searchPatient')}}"  method="post"  >
				@csrf
				<div class="row">
					<div class="col-6">
						<input type="text" class="form-control" name="id" style="width:90%"  required  data-parsley-required-message="please Enter Patient Registration Number" placeholder="please Enter Patient Registration Number" />
				
					</div>
					<div class="col-3">
						
						<button  class="btn btn-success"  type="submit" >Search  Patient</button>
						
					</div>
					
				</div>
	 		
	 		
	 		</form>
	 	</div>
<div class="row" style="margin-top: 5%;margin-left:5px;" >
	<div class="col-12 ">
		<form class="parsley-examples" action="{{route('addPatient')}}"  method="post"  >
			@csrf

			<div class="indoorDonor">
				<br/>
				<div class="col-12" >
					<h2 style="text-align:center; width: 31%;margin-left: 28%;color: red;">Blood Program</h2>
					
				</div>
				
				<div class="col-12" style="margin-top: 5px;">
					
					<span style="margin-left: 561px;color: red;">Patient Registration No:</span>
					<span><input type="text" name="" readonly="" style=" height: 48px;width: 200px;"></span>
				</div>
				<div class="col-12" style="margin-top: 5px;">
					<h4 style="text-align:center; width: 31%;margin-left: 28%;color: red;">Patient Registration Form</h4>
					
				</div>
				<br/>
				<div class="col-12">					
				
						<span style="margin-left: 3px;">1. Name :</span> 
						<span>
						<input name="name" required  data-parsley-required-message="please Enter Name"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:90%" type="text"  >
					    </span>
				</div>
				<br/>
				<div class="col-12">					
				
						<span style="margin-left: 3px;">2. Email :</span> 
						<span>
						<input name="email" required  data-parsley-required-message="please Enter Email"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:30%" type="text"  >
					    </span>
					    <span style="margin-left: 3px;">3. Mobile :</span> 
						<span>
						<input name="mobile" required  data-parsley-required-message="please Enter Mobile Number"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:25%" type="text"  >
					    </span>
					    <span style="margin-left: 3px;">4. Gender :</span> 
						<span>
						<input name="gender" required  data-parsley-required-message="please Enter Gender"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:15%" type="text"  >
					    </span>
				</div>
				<br/>
				<div class="col-12">					
				
						<span style="margin-left: 3px;">5. Age :</span> 
						<span>
						<input name="age" required  data-parsley-required-message="please Enter Age"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:30%" type="text"  >
					    </span>
					    <span style="margin-left: 3px;">6. Education Qualification :</span> 
						<span>
						<input name="educationQualification" required  data-parsley-required-message="please Enter Education Qualification"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:25%" type="text"  >
					    </span>
				</div>
                <br/>
				<div class="col-12">					
				
						<span style="margin-left: 3px;">7. Present Address :</span> 
						<span>
						<input name="address1" required  data-parsley-required-message="Please Enter Present Address"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:84%" type="text"  >
					    </span>
					   
				</div>
				<br/>
				<div class="col-12">					
				
						
						<span style="margin-left: 3px;">
						<input name="address2" style="border:0; border-bottom: 1px  dotted; width:60%" type="text"  >
					    </span>
					   
				</div>

				<br/>
				<div class="col-12">					
				
						<span style="margin-left: 3px;">8. Permanent Address :</span> 
						<span>
						<input name="address3" required  data-parsley-required-message="Please Enter Permanent Address"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:81%" type="text"  >
					    </span>
					   
				</div>
				<br/>
				<div class="col-12">					
				
						
						<span style="margin-left: 3px;">
						<input name="address4" style="border:0; border-bottom: 1px  dotted; width:60%" type="text"  >
					    </span>
					   
				</div>
				<br/>
				<div class="col-12">					
				    <span style="margin-left: 3px;">9. Blood Group: <span> 
					<span style="margin-left: 10px;">
					<input type="checkbox" value="op" name="bloodGroup">O+
				    </span>
				    <span>
					<input type="checkbox" value="on" name="bloodGroup">O-
				    </span>
				    <span style="margin-left: 20px;">
					<input type="checkbox" value="abp" name="bloodGroup">AB+
				    </span>
				    <span>
					<input type="checkbox" value="abn" name="bloodGroup">AB-
				    </span>
				    <span style="margin-left: 20px;">
					<input type="checkbox" value="ap" name="bloodGroup">A+
				    </span>
				    <span>
					<input type="checkbox" value="an" name="bloodGroup">A-
				    </span>
				    <span style="margin-left: 20px;">
					<input type="checkbox" value="bp" name="bloodGroup">B+
				    </span>
				    <span>
					<input type="checkbox" value="bn" name="bloodGroup">B-
				    </span>
					    
				</div>
				<br/>
				<div class="col-12">					
				    <span style="margin-left: 3px;">10. Requested Blood Group: <span> 
					<span style="margin-left: 10px;">
					<input type="checkbox" value="op" name="requestedBloodGroup">O+
				    </span>
				    <span>
					<input type="checkbox" value="on" name="requestedBloodGroup">O-
				    </span>
				    <span style="margin-left: 20px;">
					<input type="checkbox" value="abp" name="requestedBloodGroup">AB+
				    </span>
				    <span>
					<input type="checkbox" value="abn" name="requestedBloodGroup">AB-
				    </span>
				    <span style="margin-left: 20px;">
					<input type="checkbox" value="ap" name="requestedBloodGroup">A+
				    </span>
				    <span>
					<input type="checkbox" value="an" name="requestedBloodGroup">A-
				    </span>
				    <span style="margin-left: 20px;">
					<input type="checkbox" value="bp" name="requestedBloodGroup">B+
				    </span>
				    <span>
					<input  type="checkbox" value="bn" name="requestedBloodGroup">B-
				    </span>
					    
				</div>
				<br/>
				<div class="col-12">
                    <label>11. Patient Type</label>
                    <div>
                        <select class="form-control parsley-validated" name="type" data-style="btn-secondary">
                            <option value="">---Select Patient Type---</option>
                           
                            <option value="1">Normal Patient</option>
                            <option value="2">Thalassemia Patient</option>
                            
                        </select>
                    </div>
                </div>
                <br/>
				<div class="col-12" style="display: none;" id="normalPatient">				
					<span style="margin-left: 3px;">12. Blood Need Date:</span> 
					<span >
					<input name="bloodNeedDate" style="border:0; border-bottom: 1px  dotted; width:20%" type="text"  >
				    </span>
				</div>
				<br/>
				<div class="col-12" style="display: none;" id="thalassemiaPatient">
				    <span style="margin-left: 3px;">12. Duration Month:</span> 
					<span >
					<input name="durationTime" style="border:0; border-bottom: 1px  dotted; width:20%" type="number"  >
				    </span>					
				</div>
				<br/>
				<br/>
				<br/>
				<br/>
				<div class="col-12">					
				
						<span style="margin-left: 3px;">Date:</span> 
						<span >
						<input name="date" style="border:0; border-bottom: 1px  dotted; width:20%" type="text"  >
					    </span>
					   
					    <span style="margin-left: 320px;">Signature:</span> 
						<span>
						<input name=""  style="border:0; border-bottom: 1px  dotted; width:20%" type="text"  >
					    </span>
					   
				</div>

				<br/>
				<br/>

			</div>

		
	   
	</div> <!-- end col -->
	

	
	<div id="notPrint" style="margin-left: 10px; margin-top: 30px;">
            	
        <button  class="btn btn-success"  type="submit" >Save Patient</button>
        <button type="button"  class="btn btn-info printNewVotar"  onclick="printPage()">Print</button>
            	
 	</div>

 	</form>

</div>

 
<!-- end row --> 
</div>
</div>


 <script>
    
function printPage() {
  window.print();

}
</script>
@endsection