@extends('layouts.user')

@section('title')  Show All Normal Patient @endsection

@section('content')
<style>
::placeholder {
  color: white;
  opacity: 0.6; /* Firefox */
}

:-ms-input-placeholder { /* Internet Explorer 10-11 */
 color: white;
}

::-ms-input-placeholder { /* Microsoft Edge */
 color: white;
}
.searchBox{
  margin-left: 20%;width:60%;border:1px solid #cce6ff;border-radius:5px;padding: 5px;background-color: #001a33;color:white;
}
</style>
                        
        <div class="row">
            <div class="col-lg-12 offset-lg-12">
              <div style="margin-top: 20px;">
               
               
          
                  <div class="row">
                    <div class="col-6">
                      </div>
                      <div class="col-6">
                          <div class="input-group m-t-10">
                              <input type="text" class="form-control" id="searchNormalPatientBox" style=" @media (max-width:969px) and (min-width:1000px) {margin-top: 10px;}​" class="searchBox" placeholder="Search By Name Email Phone Registration Number">
                            <span class="input-group-append">
                                <button type="button" class="btn waves-effect waves-light btn-blue"><i class="fa fa-search mr-1"></i> Search</button>
                            </span>
                          </div>
                      </div>
                  </div>
                 

              </div>
               <br/>
                <div class="card-box">
                    <h4 class="header-title" style="text-align:center">All Normal Patient List</h4>
                    
                    <?php 
                      $message=Session::get('message');
                      if($message){

                          ?>
                          <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                              </button>
                              <?php
                                  echo $message;
                                  Session::put('message','');
                              ?>
                          </div>
                          <?php
                      
                  }
                  ?>
                  
                  @if($errors->any())
              
                  <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">
                     

                             <ul>
                                 @foreach($errors->all() as $error)
                                      <li>{{$error}}</li>
                                 @endforeach
                             </ul>
                         
                     
                  </div>
                   @endif


                    <div class="table-responsive">
                        <table class="table mb-0" id="normalPatientTable">
                            <thead style="background-color: #4281DD;">
                            <tr>
                                
                                <th width="10%" style="color: white; text-align: center;">Name</th>
                                <th width="10%" style="color: white; text-align: center;">Age</th>
                                <th width="10%" style="color: white; text-align: center;">Email</th>
                                <th width="10%" style="color: white; text-align: center;">Education</th>
                                <th width="10%" style="color: white; text-align: center;">Phone</th>
                                <th width="10%" style="color: white; text-align: center;">Blood Need Date</th>
                                <th width="15%" style="color: white; text-align: center;">Registration No</th>
                                <th width="15%" style="color: white; text-align: center;">Status</th>
                                <th width="10%" style="color: white; text-align: center;">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                             @foreach($patientInfo as $patient) 
                            <tr>
                                
                                <td style="border: 2px solid #F7F7F4; text-align: center;">
                                  {{$patient->name}}
                                  
                                </td>
                                <td style="border: 2px solid #F7F7F4; text-align: center;">
                                  {{$patient->age}}
                                  
                                </td>
                                <td style="border: 2px solid #F7F7F4; text-align: center;">
                                  {{$patient->email}}
                                  
                                </td>
                                <td style="border: 2px solid #F7F7F4; text-align: center;">
                                  {{$patient->educationQualification}}
                                  
                                </td>
                                <td style="border: 2px solid #F7F7F4; text-align: center;">
                                  {{$patient->mobile}}
                                  
                                </td>
                                <td style="border: 2px solid #F7F7F4; text-align: center;">
                                  {{$patient->bloodNeedDate}}
                                  
                                </td>
                                <td style="border: 2px solid #F7F7F4; text-align: center;">
                                  {{$patient->nibondonNo}}
                                  
                                </td>
                                <?php
                                 if ($patient->status=='1') {
                                  ?>
                                     <td style="border: 2px solid #F7F7F4; text-align: center;">
                                   
                                         <h5><span class="badge badge-success">Active</span></h5>
                                     </td>
                                <?php
                                 }
                                 else{
                                ?>
                                   <td style="border: 2px solid #F7F7F4; text-align: center;">
                                   
                                          <h5><span class="badge badge-danger">DeActive</span></h5>
                                    </td>

                                <?php
                                  }
                                ?>
                                

                                <td style="border: 2px solid #F7F7F4; text-align: center;">
                                 
                                 
                                 <?php
                                   if ($patient->status=='1') {
                                  ?>
                                   <a href="#" id="{{$patient->id}}" class="action-icon btnNormalPatientDeActive"><span class="mdi mdi-close-circle"></span></a>
                                  <?php
                                   }
                                   else{
                                 ?>
                                  <a href="#" id="{{$patient->id}}" class="action-icon btnNormalPatientActive"><span class="mdi mdi-checkbox-marked-circle"></span></a>
                                <?php
                                  }
                                ?>
                                 
                                 <input type="hidden" name="id" value="{{$patient->id}}"/>
                                  <a href="#" id="{{$patient->id}}" class="action-icon btnNormalPatientDelete"><i class="mdi mdi-delete"></i></a>
                                </td>
                                
                            </tr>
                            @endforeach
                            </tbody>
                        </table>

                         <table style="display: none;" class="table mb-0" id="searchNormalPatientTable">
                            <thead style="background-color: #4281DD;">
                            <tr>
                                
                                <th width="10%" style="color: white; text-align: center;">Name</th>
                                <th width="10%" style="color: white; text-align: center;">Age</th>
                                <th width="10%" style="color: white; text-align: center;">Email</th>
                                <th width="10%" style="color: white; text-align: center;">Education</th>
                                <th width="10%" style="color: white; text-align: center;">Phone</th>
                                <th width="10%" style="color: white; text-align: center;">Blood Need Date</th>
                                <th width="15%" style="color: white; text-align: center;">Registration No</th>
                                <th width="15%" style="color: white; text-align: center;">Status</th>
                                <th width="10%" style="color: white; text-align: center;">Action</th>
                            </tr>
                            </thead>
                            <tbody>                             
                              <!--LOADING IMAGE DIV START HERE..-->
                              <div id="loadingImg" style="display: none;" ><img src="{{asset('loading_img/Spinner.gif')}}" /></div>
                              <!--LOADING IMAGE DIV END HERE..-->
                            </tbody>
                        </table>
                    </div> <!-- end table-responsive-->
                    <br/>
                   <div id="paginationLink">
                      {{ $patientInfo->links() }}
                   </div>

                </div> <!-- end card-box -->
            </div> <!-- end col -->

        </div>
        <!--- end row -->
                       
@endsection