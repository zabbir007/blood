@extends('layouts.user')




@section('title') 

 
        Show Patient

@endsection


@section('content')

<style type="text/css">
	.indoorDonor{
		
		background-color: #fff;
	}


	@media print {

	  body * {
	  	margin-top: 5px;
	    visibility: hidden;
	  }
	  
	  #printInvoice, #printInvoice * {
	    visibility: visible;
	  }
	  #notPrint, #notPrint *{
	  	visibility: hidden;
	  }
	   #editMode, #editMode *{
	  	visibility: hidden;
	  }
	  #updateMode, #updateMode *{
	  	visibility: hidden;
	  }
	  #searchIndoorDonor, #searchIndoorDonor *{
	  	visibility: hidden;
	  }
	  #printInvoice {
	    position: absolute;
	    left: 0;
	    top: 0;
	  }



	    
	 


	}

</style>

<div id="printInvoice">
	 <div class="container ">

	 	<div id="searchIndoorDonor" style="margin-left: 50px;">
	 		 @if($errors->any())
                    
                <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">
                   

                           <ul>
                               @foreach($errors->all() as $error)
                                    <li>{{$error}}</li>
                               @endforeach
                           </ul>
                       
                   
                </div>
                 @endif

                 <?php 
                    $message=Session::get('message');
                    if($message){

                        ?>
                        <div style="margin-top: 40px;" class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <?php
                                echo $message;
                                Session::put('message','');
                            ?>
                        </div>
                        <?php
                    
                }
                ?>

                <div class="row" style="margin-top: 3px;">
                	<div class="col-3 offset-9">
						
						<a href="{{route('createPatient')}}"  class="btn btn-success"  type="submit" >New Patient</a>
						
					</div>
                </div>

                  <?php 
                    $messageWarning=Session::get('messageWarning');
                    if($messageWarning){

                        ?>
                        <div style="margin-top: 40px;" class="alert alert-warning alert-dismissible bg-warning text-white border-0 fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <?php
                                echo $messageWarning;
                                Session::put('messageWarning','');
                            ?>
                        </div>
                        <?php
                    
                }
                ?>

	 		
	 	</div>
<div class="row" style="margin-top: 5%;margin-left:5px;" >
	<div class="col-12 ">
		<form class="parsley-examples" action="{{route('updatePatient')}}"  method="post"  >
			@csrf

			<div class="indoorDonor">
				<br/>
				<span style="margin-left: 5px; color: green;" id="editMode">Edit Mode</span>
				<span style="display: none;text-align: right; color: red;" id="updateMode">Update Mode</span>
				<div class="col-12" >
					<h2 style="text-align:center; width: 31%;margin-left: 28%;color: red;">Blood Program</h2>
					
				</div>
				
				<div class="col-12" style="margin-top: 5px;">
					
					<span style="margin-left: 561px;color: red;">Patient Registration No:</span>
					<span><input type="text" name="" readonly="" value="{{$patientInfo->nibondonNo}}" style=" height: 48px;width: 200px;"></span>
				</div>
				<div class="col-12" style="margin-top: 5px;">
					<h4 style="text-align:center; width: 31%;margin-left: 28%;color: red;">Patient Registration Form</h4>
					
				</div>
				<br/>
				<div class="col-12">					
				
						<span style="margin-left: 3px;">1. Name :</span> 
						<span>
						<input name="name" required readonly="" value="{{$patientInfo->name}}" data-parsley-required-message="please Enter Name"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:90%" type="text"  >
						<input type="hidden" name="id" value="{{$patientInfo->id}}">
					    </span>
				</div>
				<br/>
				<div class="col-12">					
				
						<span style="margin-left: 3px;">2. Email :</span> 
						<span>
						<input name="email" required readonly="" value="{{$patientInfo->email}}" data-parsley-required-message="please Enter Email"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:30%" type="text"  >
					    </span>
					    <span style="margin-left: 3px;">3. Mobile :</span> 
						<span>
						<input name="mobile" required readonly="" value="{{$patientInfo->mobile}}" data-parsley-required-message="please Enter Mobile Number"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:25%" type="text"  >
					    </span>
					    <span style="margin-left: 3px;">4. Gender :</span> 
						<span>
						<input name="gender" required readonly="" value="{{$patientInfo->gender}}" data-parsley-required-message="please Enter Gender"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:15%" type="text"  >
					    </span>
				</div>
				<br/>
				<div class="col-12">					
				
						<span style="margin-left: 3px;">5. Age :</span> 
						<span>
						<input name="age" required readonly="" value="{{$patientInfo->age}}" data-parsley-required-message="please Enter Age"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:30%" type="text"  >
					    </span>
					    <span style="margin-left: 3px;">6. Education Qualification :</span> 
						<span>
						<input name="educationQualification" required readonly="" value="{{$patientInfo->educationQualification}}" data-parsley-required-message="please Enter Education Qualification"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:25%" type="text"  >
					    </span>
				</div>
                <br/>
				<div class="col-12">					
				
						<span style="margin-left: 3px;">7. Present Address :</span> 
						<span>
						<input name="address1" required readonly="" value="{{$patientInfo->address1}}" data-parsley-required-message="Please Enter Present Address"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:84%" type="text"  >
					    </span>
					   
				</div>
				<br/>
				<div class="col-12">					
				
						
						<span style="margin-left: 3px;">
						<input name="address2" readonly="" value="{{$patientInfo->address2}}" style="border:0; border-bottom: 1px  dotted; width:60%" type="text"  >
					    </span>
					   
				</div>

				<br/>
				<div class="col-12">					
				
						<span style="margin-left: 3px;">8. Permanent Address :</span> 
						<span>
						<input name="address3" required readonly="" value="{{$patientInfo->address3}}" data-parsley-required-message="Please Enter Permanent Address"  data-style="btn-secondary" style="border:0; border-bottom: 1px  dotted; width:81%" type="text"  >
					    </span>
					   
				</div>
				<br/>
				<div class="col-12">					
				
						
						<span style="margin-left: 3px;">
						<input name="address4" readonly="" value="{{$patientInfo->address4}}" style="border:0; border-bottom: 1px  dotted; width:60%" type="text"  >
					    </span>
					   
				</div>
				<br/>
				<div class="col-12">					
				    <span style="margin-left: 3px;">9. Blood Group: <span> 
					<span style="margin-left: 10px;">
					<input type="checkbox" value="op" name="bloodGroup" readonly="" <?php if ($patientInfo->bloodGroup=='op') {echo "checked";}?> >O+
				    </span>
				    <span>
					<input type="checkbox" value="on" name="bloodGroup" readonly="" <?php if ($patientInfo->bloodGroup=='on') {echo "checked";}?> >O-
				    </span>
				    <span style="margin-left: 20px;">
					<input type="checkbox" value="abp" name="bloodGroup" readonly="" <?php if ($patientInfo->bloodGroup=='abp') {echo "checked";}?> >AB+
				    </span>
				    <span>
					<input type="checkbox" value="abn" name="bloodGroup" readonly="" <?php if ($patientInfo->bloodGroup=='abn') {echo "checked";}?> >AB-
				    </span>
				    <span style="margin-left: 20px;">
					<input type="checkbox" value="ap" name="bloodGroup" readonly="" <?php if ($patientInfo->bloodGroup=='ap') {echo "checked";}?> >A+
				    </span>
				    <span>
					<input type="checkbox" value="an" name="bloodGroup" readonly="" <?php if ($patientInfo->bloodGroup=='an') {echo "checked";}?> >A-
				    </span>
				    <span style="margin-left: 20px;">
					<input type="checkbox" value="bp" name="bloodGroup" readonly="" <?php if ($patientInfo->bloodGroup=='bp') {echo "checked";}?> >B+
				    </span>
				    <span>
					<input type="checkbox" value="bn" name="bloodGroup" readonly="" <?php if ($patientInfo->bloodGroup=='bn') {echo "checked";}?> >B-
				    </span>
					    
				</div>
				<br/>
				<div class="col-12">					
				    <span style="margin-left: 3px;">10. Requested Blood Group: <span> 
					<span style="margin-left: 10px;">
					<input type="checkbox" value="op" name="requestedBloodGroup" readonly="" <?php if ($patientInfo->requestedBloodGroup=='op') {echo "checked";}?> >O+
				    </span>
				    <span>
					<input type="checkbox" value="on" name="requestedBloodGroup" readonly="" <?php if ($patientInfo->requestedBloodGroup=='on') {echo "checked";}?> >O-
				    </span>
				    <span style="margin-left: 20px;">
					<input type="checkbox" value="abp" name="requestedBloodGroup" readonly="" <?php if ($patientInfo->requestedBloodGroup=='abp') {echo "checked";}?> >AB+
				    </span>
				    <span>
					<input type="checkbox" value="abn" name="requestedBloodGroup" readonly="" <?php if ($patientInfo->requestedBloodGroup=='abn') {echo "checked";}?> >AB-
				    </span>
				    <span style="margin-left: 20px;">
					<input type="checkbox" value="ap" name="requestedBloodGroup" readonly="" <?php if ($patientInfo->requestedBloodGroup=='ap') {echo "checked";}?> >A+
				    </span>
				    <span>
					<input type="checkbox" value="an" name="requestedBloodGroup" readonly="" <?php if ($patientInfo->requestedBloodGroup=='an') {echo "checked";}?> >A-
				    </span>
				    <span style="margin-left: 20px;">
					<input type="checkbox" value="bp" name="requestedBloodGroup" readonly="" <?php if ($patientInfo->requestedBloodGroup=='bp') {echo "checked";}?> >B+
				    </span>
				    <span>
					<input type="checkbox" value="bn" name="requestedBloodGroup" readonly="" <?php if ($patientInfo->requestedBloodGroup=='bn') {echo "checked";}?> >B-
				    </span>
					    
				</div>
				<br/>
				<div class="col-12" id="patientType" style="display: hidden;">
                    <label>11. Patient Type</label>
                    <div>
                        <select class="form-control parsley-validated" name="type" data-style="btn-secondary">
                            <option value="">---Select Branch---</option>
                           
                            <option value="1" <?php if($patientInfo->type=='1') {echo "selected";} ?> >Normal Patient</option>
                            <option value="2" <?php if($patientInfo->type=='2') {echo "selected";} ?> >Thalassemia Patient</option>
                            
                        </select>
                    </div>
                </div>

				<div class="col-12" id="showPatientType">
				    <span style="margin-left: 3px;">11. Patient Type :</span> 
					<span >
					<input name="" style="border:0; border-bottom: 1px  dotted; width:20%" type="text" readonly="" value="<?php if($patientInfo->type=='1') {echo "Normal Patient";}else{echo "Thalassemia Patient";} ?>" >
				    </span>					
				</div>
				<br/>
				<div class="col-12" style="display: none;" id="normalPatient">				
					<span style="margin-left: 3px;">12. Blood Need Date:</span> 
					<span >
					<input name="bloodNeedDate" readonly="" value="{{$patientInfo->bloodNeedDate}}" style="border:0; border-bottom: 1px  dotted; width:20%" type="text"  >
				    </span>
				</div>
				<br/>
				<div class="col-12" style="display: none;" id="thalassemiaPatient">
				    <span style="margin-left: 3px;">12. Duration Month:</span> 
					<span >
					<input name="durationTime" readonly="" value="{{$patientInfo->durationTime}}" style="border:0; border-bottom: 1px  dotted; width:20%" type="number"  >
				    </span>					
				</div>
				
				<br/>
				<br/>
				<br/>
				<br/>
				<div class="col-12">					
				
						<span style="margin-left: 3px;">Date:</span> 
						<span >
						<input name="date" readonly="" value="{{$patientInfo->date}}" style="border:0; border-bottom: 1px  dotted; width:20%" type="text"  >
					    </span>
					   
					    <span style="margin-left: 320px;">Signature:</span> 
						<span>
						<input name=""  style="border:0; border-bottom: 1px  dotted; width:20%" type="text"  >
					    </span>
					   
				</div>

				<br/>
				<br/>

			</div>
            
            </br>
            </br>
            <div class="row">
			    <div class="col-sm">
			      <img src="{{asset($patientInfo->barCodeImage)}}" alt="{{$patientInfo->barCodeImage}}" width="200" height="90">
			    </div>
			    <div class="col-sm">
			      <img src="{{asset($patientInfo->barCodeImage)}}" alt="{{$patientInfo->barCodeImage}}" width="200" height="90">
			    </div>
			    <div class="col-sm">
			      <img src="{{asset($patientInfo->barCodeImage)}}" alt="{{$patientInfo->barCodeImage}}" width="200" height="90">
			    </div>
			</div>
		
	   
	</div> <!-- end col -->
	

	
	<div id="notPrint" style="margin-left: 10px; margin-top: 30px;">
		<a href="{{url()->previous()}}" type="button"  class="btn btn-success printJobNgo" id="notPrint">Back</a>
        <button  class="btn btn-success" id="updatePatient" style="display: none;" type="submit" >Update</button>
		<a href="" class="btn btn-warning" id="editPatient">Edit</a>
		<a href="" class="btn btn-warning" id="backPatient" style="display: none;"   >Back</a>
		<button type="button"  class="btn btn-info" id="printPatient" onclick="printPage()">Print</button>
		<a href="" class="btn btn-warning deletePatient"  id="{{$patientInfo->id}}">Delete</a>
            	
 	</div>

 	</form>

</div>

 
<!-- end row --> 
</div>
</div>


 <script>
    
function printPage() {
  window.print();

}
</script>
@endsection