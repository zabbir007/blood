<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Donor Register</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!-- App favicon -->
        <link rel="shortcut icon" href="assets/images/favicon.ico">

        <!-- Sweet Alert-->
        <link href="{{asset('admin/assets/libs/sweetalert2/sweetalert2.min.css')}}" rel="stylesheet" type="text/css" />

          <!--Date css-->
          <!-- Plugins css -->
        <link href="{{asset('admin/assets/libs/flatpickr/flatpickr.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('admin/assets/libs/bootstrap-colorpicker/bootstrap-colorpicker.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('admin/assets/libs/clockpicker/bootstrap-clockpicker.min.css')}}" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        
        <!-- Plugins css  for select option-->
       <!-- Plugins css -->
        <link href="{{asset('admin/assets/libs/jquery-nice-select/nice-select.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('admin/assets/libs/switchery/switchery.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('admin/assets/libs/multiselect/multi-select.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('admin/assets/libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('admin/assets/libs/bootstrap-select/bootstrap-select.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('admin/assets/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.css')}}" rel="stylesheet" type="text/css" />
        <!-- End Plugins css  for select option-->

        <!-- Plugins css -->
        <link href="{{asset('admin/assets/libs/dropzone/dropzone.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('admin/assets/libs/dropify/dropify.min.css')}}" rel="stylesheet" type="text/css" />

        <!-- Plugins css  for image upload file-->

        <!-- Plugins css -->
        <link href="{{asset('admin/assets/libs/flatpickr/flatpickr.min.css')}}" rel="stylesheet" type="text/css" />

        

        <!-- third party data-table css -->
        <link href="{{asset('admin/assets/libs/datatables/dataTables.bootstrap4.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('admin/assets/libs/datatables/responsive.bootstrap4.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('admin/assets/libs/datatables/buttons.bootstrap4.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('admin/assets/libs/datatables/select.bootstrap4.css')}}" rel="stylesheet" type="text/css" />
        <!-- third party data-table css end -->

        <!-- App css -->
        <link href="{{asset('admin/assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('admin/assets/css/icons.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('admin/assets/css/app.min.css')}}" rel="stylesheet" type="text/css" />
        <style type="text/css">
        	body {
				 background-image: url('{{asset('donor/assets/images/tomattos_backr.jpg')}}');
                 height: 100%;
                 width: 100%;
				 background-repeat: no-repeat;
                 background-size: cover;
                 
				}
        </style>
    </head>

    <body class="" >
        <?php 
            $message=Session::get('message');
            if($message){

                ?>
                <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <?php
                        echo $message;
                        Session::put('message','');
                    ?>
                </div>
                <?php
            
        }
        ?>
       
		<div class="account-pages mt-5 mb-5">
			<div class="container" >

                <div class="row justify-content-center">
                    <div class="col-md-12 col-lg-12 col-xl-12" >
                        <div class="card bg-pattern" style="" >

                            <div class="card-body p-4">


                                
                                 <?php 
                                        $message=Session::get('message');
                                        if($message){

                                            ?>
                                            <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                                <?php
                                                    echo $message;
                                                    Session::put('message','');
                                                ?>
                                            </div>
                                            <?php
                                        
                                    }
                                    ?>

                                    <?php 
                                        $message=Session::get('messageWarning');
                                        if($message){

                                            ?>
                                             <div class="alert alert-warning alert-dismissible bg-warning text-white border-0 fade show" role="alert">
                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                <?php
                                                    echo $message;
                                                    Session::put('messageWarning','');
                                                ?>
                                            </div>
                                            <?php
                                        
                                    }
                                    ?>
                                    

                                        @if($errors->any())
                                    
                                        <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">
                                           

                                                   <ul>
                                                       @foreach($errors->all() as $error)
                                                            <li>{{$error}}</li>
                                                       @endforeach
                                                   </ul>
                                               
                                           
                                        </div>
                                         @endif


                                
                                <div class="text-center w-75 m-auto">
                                    <a href="index.html">
                                        <span><img src="{{asset('driver/assets/images/logo-light.png')}}" alt="" height="22"></span>
                                    </a>
                                    
                                </div>
                                
                                <form action="{{route('savePatient')}}" method="post" enctype="multipart/form-data" class="parsley-examples">
                                    @csrf
                                        <div class="form-group">
                                            <label>Name</label>
                                            <div>
                                                <input type="text" name="name"  class="form-control parsley-validated" required
                                                        data-parsley-required-message="Please Enter Your Name"  placeholder="Name"/>
                                            </div>
                                        </div>

                                        

                                        <div class="form-group">
                                            <label>Phone</label>
                                            <div>
                                                <input type="text" data-parsley-type="digits" name="phone"  id="mobileNumber" data-parsley-minlength="11" data-parsley-maxlength="11"  class="form-control parsley-validated" required
                                                        data-parsley-required-message="Please Enter Your Contact Number"  data-parsley-type-message="Please Enter Only Digits" data-parsley-minlength-message="Please Enter Only 11 Digits" data-parsley-maxlength-message="Please Enter Only 11 Digits" placeholder="Phone Number"/>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label>Email</label>
                                            <div>
                                                <input type="email" name="email" id="email" class="form-control parsley-validated" required
                                                        data-parsley-required-message="Please Enter Your Email"  data-parsley-type-message="Please Provide Valid Email"  placeholder="Email"/>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label>Password</label>
                                            <div>
                                                <input type="password" name="password" id="password" class="form-control parsley-validated" required
                                                       data-parsley-minlength="8" data-parsley-maxlength="16" data-parsley-required-message="Please Enter Your Password" data-parsley-minlength-message="Password must be between 8 to 16 character" data-parsley-maxlength-message="Password must be between 8 to 16 character"  placeholder="Password"/>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label>Confirm Password</label>
                                            <div>
                                                <input type="password" name="confirmPassword"  data-parsley-equalto="#password" class="form-control parsley-validated" required
                                                       data-parsley-minlength="8" data-parsley-maxlength="16" data-parsley-equalto-message="Password Not Matched!!" data-parsley-required-message="Please Enter Your Password" data-parsley-minlength-message="Password must be between 8 to 16 character" data-parsley-maxlength-message="Password must be between 8 to 16 character"  placeholder="Confirm Password"/>
                                            </div>
                                        </div>

                                       
                                        <div class="form-group">
                                            <label>Select Branch</label>
                                            <div>
                                                <select class="selectpicker" name="branchId" data-style="btn-secondary">
                                                  
                                                    <option value="1">Dhaka</option>
                                                     <option value="2">Tangail</option>
                                                   
                                                </select>
                                            </div>
                                        </div>

                                         

                                         <div class="form-group">
                                            <label>Donor Image</label>
                                            <div class="mt-3">
                                                
                                                <input type="file" name="image" class="dropify" required data-parsley-required-message="Please Select Driver Picture"  data-parsley-type-message="Please Select Driver Picture"  />
                                            </div>
                                        </div>

                                         <div class="form-group mb-0 text-center">
                                            <button class="btn btn-success btn-block" type="submit" id="signUp"> Sign Up </button>
                                        </div>
                                </form>
                                
                                 <div class="text-center" style="margin-top: 15px;">
                                    <p class="text-white-50" style="color: black;"><b style="color: black;">Already have account?</b>  <a href="{{route('patientLogin')}}" class="text-white ml-1"><b style="color: green;">Sign In</b></a></p>
                                </div>
                               

                            </div> <!-- end card-body -->
                        </div>
                        <!-- end card -->

                        
                        <!-- end row -->

                    </div> <!-- end col -->

                </div>
                <!-- end row -->
           
 
             </div>
            <!-- end container -->
         
        
        <!-- end page -->
           

           <footer class="footer footer-alt">
           <!--  Copy Right by <a href="" class="text-white-50"></a>  -->
        </footer>

       <!-- Right bar overlay-->
        <div class="rightbar-overlay"></div>

        <!-- Vendor js -->
        <script src="{{asset('admin/assets/js/vendor.min.js')}}"></script>

       <!-- Plugins js-->
        <script src="{{asset('admin/assets/libs/flatpickr/flatpickr.min.js')}}"></script>
        <script src="{{asset('admin/assets/libs/jquery-knob/jquery.knob.min.js')}}"></script>
        <script src="{{asset('admin/assets/libs/jquery-sparkline/jquery.sparkline.min.js')}}"></script>
        <script src="{{asset('admin/assets/libs/flot-charts/jquery.flot.js')}}"></script>
        <script src="{{asset('admin/assets/libs/flot-charts/jquery.flot.time.js')}}"></script>
        <script src="{{asset('admin/assets/libs/flot-charts/jquery.flot.tooltip.min.js')}}"></script>
        <script src="{{asset('admin/assets/libs/flot-charts/jquery.flot.selection.js')}}"></script>
        <script src="{{asset('admin/assets/libs/flot-charts/jquery.flot.crosshair.js')}}"></script>

         <!-- third party data table js -->
        <script src="{{asset('admin/assets/libs/datatables/jquery.dataTables.js')}}"></script>
        <script src="{{asset('admin/assets/libs/datatables/dataTables.bootstrap4.js')}}"></script>
        <script src="{{asset('admin/assets/libs/datatables/dataTables.responsive.min.js')}}"></script>
        <script src="{{asset('admin/assets/libs/datatables/responsive.bootstrap4.min.js')}}"></script>
        <script src="{{asset('admin/assets/libs/datatables/dataTables.buttons.min.js')}}"></script>
        <script src="{{asset('admin/assets/libs/datatables/buttons.bootstrap4.min.js')}}"></script>
        <script src="{{asset('admin/assets/libs/datatables/buttons.html5.min.js')}}"></script>
        <script src="{{asset('admin/assets/libs/datatables/buttons.flash.min.js')}}"></script>
        <script src="{{asset('admin/assets/libs/datatables/buttons.print.min.js')}}"></script>
        <script src="{{asset('admin/assets/libs/datatables/dataTables.keyTable.min.js')}}"></script>
        <script src="{{asset('admin/assets/libs/datatables/dataTables.select.min.js')}}"></script>
        <script src="{{asset('admin/assets/libs/pdfmake/pdfmake.min.js')}}"></script>
        <script src="{{asset('admin/assets/libs/pdfmake/vfs_fonts.js')}}"></script>
        <!-- third party data table js ends -->

        <!-- select option link here-->
        <script src="{{asset('admin/assets/libs/jquery-nice-select/jquery.nice-select.min.js')}}"></script>
        <script src="{{asset('admin/assets/libs/switchery/switchery.min.js')}}"></script>
        <script src="{{asset('admin/assets/libs/multiselect/jquery.multi-select.js')}}"></script>
        <script src="{{asset('admin/assets/libs/select2/select2.min.js')}}"></script>
        <script src="{{asset('admin/assets/libs/jquery-mockjax/jquery.mockjax.min.js')}}"></script>
        <script src="{{asset('admin/assets/libs/autocomplete/jquery.autocomplete.min.js')}}"></script>
        <script src="{{asset('admin/assets/libs/bootstrap-select/bootstrap-select.min.js')}}"></script>
        <script src="{{asset('admin/assets/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js')}}"></script>
        <script src="{{asset('admin/assets/libs/bootstrap-maxlength/bootstrap-maxlength.min.js')}}"></script>

        <!-- Init js-->
        <!-- <script src="{{asset('admin/assets/js/pages/form-advanced.init.js')}}"></script> -->
        <!-- select option link end here-->

      <!-- Plugins css  for image upload file-->

       <!-- Sweet Alerts js -->
        <script src="{{asset('admin/assets/libs/sweetalert2/sweetalert.min.js')}}"></script>

        <!-- Sweet alert init js-->
        
        <script src="{{asset('admin/assets/js/pages/sweet-alerts.init.js')}}"></script>

        <!-- Sweet Alerts js -->

     <!-- Plugins js -->
        <script src="{{asset('admin/assets/libs/dropzone/dropzone.min.js')}}"></script>
        <script src="{{asset('admin/assets/libs/dropify/dropify.min.js')}}"></script>

         <!-- Init js-->
        <script src="{{asset('admin/assets/js/pages/form-fileuploads.init.js')}}"></script>

         <!-- Date Plugin-->
         <!-- Plugins js-->
        <script src="{{asset('admin/assets/libs/flatpickr/flatpickr.min.js')}}"></script>
        <script src="{{asset('admin/assets/libs/bootstrap-colorpicker/bootstrap-colorpicker.min.js')}}"></script>
        <script src="{{asset('admin/assets/libs/clockpicker/bootstrap-clockpicker.min.js')}}"></script>
         <!-- Init js-->
        <script src="{{asset('admin/assets/js/pages/form-pickers.init.js')}}"></script>

 <!-- Plugins css  for image upload file-->

       


        <!-- Datatables init -->
        <script src="{{asset('admin/assets/js/pages/datatables.init.js')}}"></script>

        <!-- Dashboar 1 init js-->
        <script src="{{asset('admin/assets/js/pages/dashboard-1.init.js')}}"></script>

   <!-- Plugin js-->
        <script src="{{asset('admin/assets/libs/parsleyjs/parsley.min.js')}}"></script>
        <!-- Validation init js-->
        <script src="{{asset('admin/assets/js/pages/form-validation.init.js')}}"></script>



        <!-- App js-->
        <script src="{{asset('admin/assets/js/app.min.js')}}"></script>

</body>
</html>