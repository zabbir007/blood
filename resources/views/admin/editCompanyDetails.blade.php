@extends('layouts.admin')

@section('title') Edit Company Details @endsection

@section('content')


<div class="row" style="margin-top: 20px;">
        <div class="col-lg-6 offset-3">
        	<a href="{{route('showCompanyDetails')}}">
        	<button class="btn btn-primary" type="submit">Show All Company Details</button>
        	</a>
            <div class="card" style="margin-top: 10px;">
                <div class="card-body">
                	<h4 class="header-title" style="text-align: center">Edit Company Details</h4>
                    <form class="parsley-examples"  method="post" action="{{route('updateCompanyDetails')}}" novalidate>
                       @csrf

                        
                        
                      <div class="form-group">
                            <label>Payment Type</label>
                            <div>
                        		<input type="hidden" name="id" value="{{$singleDetailsInfo->id}}" />
                        		<input type="hidden" name="page" value="{{$page}}" />
                        		
                                <input type="text" name="companyName" value="{{$singleDetailsInfo->companyName}}"  class="form-control parsley-validated" required
                                        data-parsley-required-message="Please Enter Company Name"   placeholder="Company Name"/>
                            </div>
                            <label>Company Details</label>
                            <div>
                        
                                <input type="text" value="{{$singleDetailsInfo->description}}" name="companyDetails"  class="form-control parsley-validated" required
                                        data-parsley-required-message="Please Enter Company Details"   placeholder="Company Details"/>
                            </div>
                            <label>Status</label>
                            <div>
                                <select class="form-control parsley-validated" name="active" data-style="btn-secondary">
                                    <option value="1" <?php if($singleDetailsInfo->active=='1'){echo "selected";} ?> >Active</option>
                                    <option value="0"  <?php if($singleDetailsInfo->active=='0'){echo "selected";} ?> >DeActive</option>
                                </select>
                            </div>
                        </div>
                        <button  class="btn btn-success" type="submit">Update</button>
                    </form>
                    
                </div> <!-- end card-body-->
            </div> <!-- end card-->
        </div> <!-- end col-->
    </div>


@endsection