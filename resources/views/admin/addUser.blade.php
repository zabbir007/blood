@extends('layouts.admin')

@section('title') User Register @endsection

@section('content')

   <div class="account-pages mt-5 mb-5">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8 col-lg-10 col-xl-10">
                        <div class="card bg-pattern">

                            <div class="card-body p-4">


                                
                                 <?php 
                                        $message=Session::get('message');
                                        if($message){

                                            ?>
                                            <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                                <?php
                                                    echo $message;
                                                    Session::put('message','');
                                                ?>
                                            </div>
                                            <?php
                                        
                                    }
                                    ?>

                                    <?php 
                                        $message=Session::get('messageWarning');
                                        if($message){

                                            ?>
                                             <div class="alert alert-warning alert-dismissible bg-warning text-white border-0 fade show" role="alert">
                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                <?php
                                                    echo $message;
                                                    Session::put('messageWarning','');
                                                ?>
                                            </div>
                                            <?php
                                        
                                    }
                                    ?>
                                    

                                        @if($errors->any())
                                    
                                        <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">
                                           

                                                   <ul>
                                                       @foreach($errors->all() as $error)
                                                            <li>{{$error}}</li>
                                                       @endforeach
                                                   </ul>
                                               
                                           
                                        </div>
                                         @endif


                                
                                <div class="text-center w-75 m-auto">
                                    <a href="index.html">
                                        <span><img src="{{asset('driver/assets/images/logo-light.png')}}" alt="" height="22"></span>
                                    </a>
                                    
                                </div>
                                
                                <form action="{{route('adminSaveUser')}}" method="post" enctype="multipart/form-data" class="parsley-examples">
                                    @csrf
                                        <div class="form-group">
                                            <label>Name</label>
                                            <div>
                                                <input type="text" name="name"  class="form-control parsley-validated" required
                                                        data-parsley-required-message="Please Enter Your Name"  placeholder="Name"/>
                                            </div>
                                        </div>

                                        

                                        <div class="form-group">
                                            <label>Phone</label>
                                            <div>
                                                <input type="text" data-parsley-type="digits" name="phone"  id="mobileNumber" data-parsley-minlength="11" data-parsley-maxlength="11"  class="form-control parsley-validated" required
                                                        data-parsley-required-message="Please Enter Your Contact Number"  data-parsley-type-message="Please Enter Only Digits" data-parsley-minlength-message="Please Enter Only 11 Digits" data-parsley-maxlength-message="Please Enter Only 11 Digits" placeholder="Phone Number"/>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label>Email</label>
                                            <div>
                                                <input type="email" name="email" id="email" class="form-control parsley-validated" required
                                                        data-parsley-required-message="Please Enter Your Email"  data-parsley-type-message="Please Provide Valid Email"  placeholder="Email"/>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label>Password</label>
                                            <div>
                                                <input type="password" name="password" id="password" class="form-control parsley-validated" required
                                                       data-parsley-minlength="8" data-parsley-maxlength="16" data-parsley-required-message="Please Enter Your Password" data-parsley-minlength-message="Password must be between 8 to 16 character" data-parsley-maxlength-message="Password must be between 8 to 16 character"  placeholder="Password"/>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label>Confirm Password</label>
                                            <div>
                                                <input type="password" name="confirmPassword"  data-parsley-equalto="#password" class="form-control parsley-validated" required
                                                       data-parsley-minlength="8" data-parsley-maxlength="16" data-parsley-equalto-message="Password Not Matched!!" data-parsley-required-message="Please Enter Your Password" data-parsley-minlength-message="Password must be between 8 to 16 character" data-parsley-maxlength-message="Password must be between 8 to 16 character"  placeholder="Confirm Password"/>
                                            </div>
                                        </div>

                                       
                                        <div class="form-group">
                                            <label>Select Branch</label>
                                            <div>
                                                <select class="selectpicker" name="branchId" data-style="btn-secondary">
                                                  
                                                    <option value="">---Select Branch---</option>
                                                    @foreach($showBranch as $branch) 
                                                    <option value="{{$branch->id}}">{{$branch->branchName}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                         

                                         <div class="form-group">
                                            <label>User Image</label>
                                            <div class="mt-3">
                                                
                                                <input type="file" name="image" class="dropify" required data-parsley-required-message="Please Select Driver Picture"  data-parsley-type-message="Please Select Driver Picture"  />
                                            </div>
                                        </div>

                                         <div class="form-group mb-0 text-center">
                                            <button class="btn btn-success btn-block" type="submit" id="signUp"> Sign Up </button>
                                        </div>
                                </form>

                               

                            </div> <!-- end card-body -->
                        </div>
                        <!-- end card -->

                      
                        <!-- end row -->

                    </div> <!-- end col -->
                </div>
                <!-- end row -->
            </div>
            <!-- end container -->
        </div>

@endsection