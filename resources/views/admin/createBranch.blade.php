@extends('layouts.admin')

@section('title') Create Branch @endsection
@section('content')




<div class="row" style="margin-top: 20px;">
        <div class="col-lg-6 offset-3">
            <a href="{{route('showBranch')}}">
            <button class="btn btn-success" type="submit">Show All Branch</button>
            </a>
            <div class="card" style="margin-top: 10px;">
                <div class="card-body">
                    <h4 class="header-title" style="text-align: center">Add Branch </h4>
                    <form class="parsley-examples"  method="post" action="{{route('saveBranch')}}" novalidate>
                       @csrf

                        <?php 
                            $message=Session::get('message');
                            if($message){

                                ?>
                                <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <?php
                                        echo $message;
                                        Session::put('message','');
                                    ?>
                                </div>
                                <?php
                            
                        }
                        ?>
                        
                        @if($errors->any())
                    
                        <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">
                           

                                   <ul>
                                       @foreach($errors->all() as $error)
                                            <li>{{$error}}</li>
                                       @endforeach
                                   </ul>
                               
                           
                        </div>
                         @endif
                        
                      <div class="form-group">

                            <div class="form-group position-relative mb-3">
                                <label for="regionName">Region Name<span class="text-danger">*</span></label>
                                <select class="form-control" data-live-search="true" required  data-parsley-required-message="Please Enter Region Name"  data-style="btn-secondary" name="regionId">
                                    <option value="">----Select Region---- </option>
                                    @foreach($regionInfo as $region)
                                        <option value="{{$region->id}}">{{$region->regionName}}</option>
                                    @endforeach    
                                </select>
                            </div>

                            <div class="form-group position-relative mb-3" id="districtSelectBox" style="display:none;">
                                <label for="DistrictName">District Name<span class="text-danger">*</span></label>
                                <select   class="form-control" data-live-search="true" id="districtId" required  data-parsley-required-message="Please Enter District Name"  data-style="btn-secondary" name="districtId">
                                      
                                </select>
                            </div>

                            <div class="form-group" id="areaBox" style="display:none;">
                                <label>Branch Name</label>
                                <div>
                                    
                                    <input type="text" name="areaName"  class="form-control parsley-validated" required
                                            data-parsley-required-message="Please Enter Area Name"  placeholder="Branch Name"/>
                                </div>
                            </div>
                            <div id="loadingImg" style="display: none;" ><img src="{{asset('loading_img/Spinner.gif')}}" /></div>
                            
                        </div>
                        <button id="btnAreaSave" class="btn btn-success" type="submit" style="display: none;">Save</button>
                    </form>
                    
                </div> <!-- end card-body-->
            </div> <!-- end card-->
        </div> <!-- end col-->
    </div>

@endsection