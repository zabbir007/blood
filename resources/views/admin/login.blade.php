<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Admin Login</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!-- App favicon -->
        <link rel="shortcut icon" href="assets/images/favicon.ico">

        <!-- App css -->
        <link href="{{asset('admin/assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('admin/assets/css/icons.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('admin/assets/css/app.min.css')}}" rel="stylesheet" type="text/css" />
        <style type="text/css">
        	body {
				 background-image: url('{{asset('admin/assets/images/tomattos_backr.jpg')}}');
                 height: 100%;
                 width: 100%;
				 background-repeat: no-repeat;
                 background-size: cover;
                 
				}
        </style>
    </head>

    <body class="" >
        <?php 
            $message=Session::get('message');
            if($message){

                ?>
                <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <?php
                        echo $message;
                        Session::put('message','');
                    ?>
                </div>
                <?php
            
        }
        ?>
       
		<div class="account-pages mt-5 mb-5" style="margin-top: 10%;">
			<div class="container"  style="margin-top: 10%;">

                <div class="row justify-content-center" style="margin-top: 10%;">
                    <div class="col-md-8 col-lg-6 col-xl-5"  style="margin-top: 10%;">
                        <div class="card bg-pattern" style=""  style="margin-top: 10%;">

                            <div class="card-body p-5">
                                <form action="{{route('adminSignIn')}}" method="post" class="parsley-examples">

                                    <br/>
                                    <?php 
                                        $message=Session::get('message');
                                        if($message){

                                            ?>
                                            <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                                <?php
                                                    echo $message;
                                                    Session::put('message','');
                                                ?>
                                            </div>
                                            <?php
                                        
                                    }
                                    ?>
                                    


                                    @if($errors->any())
                                    
                                    <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">
                                       

                                               <ul>
                                                   @foreach($errors->all() as $error)
                                                        <li>{{$error}}</li>
                                                   @endforeach
                                               </ul>
                                           
                                       
                                    </div>
                                     @endif
                                    
                                        @csrf
                                       
                                        <h1 style="text-align: center;">Admin Login</h1>
                                        <div class="form-group">
                                            <label>Email</label>
                                            <div>
                                                <input type="text" name="email" class="form-control parsley-validated" required
                                                        data-parsley-required-message="Please Enter Your Email"  data-parsley-type-message="Please Provide Valid Email"  placeholder="Email" style="border-radius: 9px;"/>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label>Password</label>
                                            <div>
                                                <input type="password" name="password" required class="form-control parsley-validated" 
                                                       data-parsley-minlength="8" data-parsley-maxlength="16" data-parsley-required-message="Please Enter Your Password" data-parsley-minlength-message="Password must be between 8 to 16 character" data-parsley-maxlength-message="Password must be between 8 to 16 character"  placeholder="Password" style="border-radius: 9px;"/>
                                            </div>
                                        </div>

                                        <div class="form-group mb-0 text-center">
                                            <div class="wrapper" style="text-align: center;">
                                               <button type="submit" class="btn btn-primary waves-effect waves-light" style="background-color: #21B68C;border: none;height: 39px;width: 137px;">
                                                    Sign In
                                                </button>
                                            </div>
                                        </div>
                                </form>

                                

                            </div> <!-- end card-body -->
                        </div>
                        <!-- end card -->

                        
                        <!-- end row -->

                    </div> <!-- end col -->

                </div>
                <!-- end row -->
           
 
             </div>
            <!-- end container -->
         
        
        <!-- end page -->
           

           <footer class="footer footer-alt">
           <!--  Copy Right by <a href="" class="text-white-50"></a>  -->
        </footer>

       <!-- Right bar overlay-->
        <div class="rightbar-overlay"></div>

        <!-- Vendor js -->
        <script src="{{asset('admin/assets/js/vendor.min.js')}}"></script>

        <!-- Plugin js-->
        <script src="{{asset('admin/assets/libs/parsleyjs/parsley.min.js')}}"></script>

        <!-- Validation init js-->
        <script src="{{asset('admin/assets/js/pages/form-validation.init.js')}}"></script>

        <!-- App js -->
        <script src="{{asset('admin/assets/js/app.min.js')}}"></script>
</body>
</html>