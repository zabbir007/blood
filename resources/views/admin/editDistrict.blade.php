@extends('layouts.admin')

@section('title') Edit District @endsection

@section('content')


<div class="row" style="margin-top: 20px;">
        <div class="col-lg-6 offset-3">
        	<a href="{{route('showDistrict')}}">
        	<button class="btn btn-success" type="submit">Show All District</button>
        	</a>
            <div class="card" style="margin-top: 10px;">
                <div class="card-body">
                	<h4 class="header-title" style="text-align: center">Edit District</h4>
                    <form class="parsley-examples"  method="post" action="{{route('updateDistrict')}}" novalidate>
                       @csrf

                        
                        
                      <div class="form-group">
                            <div>
                        		<input type="hidden" name="id" value="{{$singleDistrictInfo->id}}" />
                        		<input type="hidden" name="page" value="{{$page}}" />
                        		<div class="form-group position-relative mb-3">
                                <label for="DistrictName">Select Region Name<span class="text-danger">*</span></label>
                                <select class="selectpicker" data-live-search="true"  data-style="btn-secondary" name="regionId">
                                    <option value="">----Select Region---- </option>
                                    @foreach($regionResult as $showRegion)
                                        <?php
                                           if ($singleDistrictInfo->regionId==$showRegion->id) {
                                        ?>
                                        <option value="{{$showRegion->id}}" selected>{{$showRegion->regionName}}</option>
                                        <?php   	
                                           	
                                           }
                                           else
                                           {
                                        ?>
                                        <option value="{{$showRegion->id}}">{{$showRegion->regionName}}</option>
                                        <?php
                                           }
                                        ?>
                                    @endforeach    
                                </select>
                               </div>
                        	    <div class="form-group position-relative mb-3">
                                <input type="text" name="districtName" value="{{$singleDistrictInfo->districtName}}"  class="form-control parsley-validated" required
                                        data-parsley-required-message="Please Enter District Name"   placeholder="District Name"/>
                                </div>        
                            </div>
                        </div>
                        <button  class="btn btn-success" type="submit">Update</button>
                    </form>
                    
                </div> <!-- end card-body-->
            </div> <!-- end card-->
        </div> <!-- end col-->
    </div>


@endsection