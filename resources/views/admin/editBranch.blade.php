@extends('layouts.admin')

@section('title') Edit Branch @endsection

@section('content')

<div class="row" style="margin-top: 20px;">
        <div class="col-lg-6 offset-3">
            <a href="{{route('showBranch')}}">
            <button class="btn btn-success" type="submit">Show All Branch</button>
            </a>
            <div class="card" style="margin-top: 10px;">
                <div class="card-body">
                    <h4 class="header-title" style="text-align: center">Edit Branch </h4>
                    <form class="parsley-examples"  method="post" action="{{route('updateBranch')}}" novalidate>
                       @csrf

                      
                        
                      <div class="form-group">

                            <div class="form-group position-relative mb-3">
                                <label for="regionName">Region Name<span class="text-danger">*</span></label>
                                <select class="form-control" data-live-search="true" required  data-parsley-required-message="Please Enter Region Name"  data-style="btn-secondary" name="regionId">
                                    <option value="">----Select Region---- </option>
                                    @foreach($regionInfo as $region)
                                    	<?php
                                    		if($region->id==$regionId){
                                    			?>

                                    			<option value="{{$region->id}}" selected="selected">{{$region->regionName}}</option>
                                    			<?php
                                    		}else{
                                    	?>
                                        <option value="{{$region->id}}">{{$region->regionName}}</option>
                                    <?php } ?>
                                    @endforeach    
                                </select>
                            </div>

                            <div class="form-group position-relative mb-3" id="districtSelectBox" >
                                <label for="DistrictName">District Name<span class="text-danger">*</span></label>
                                <select   class="form-control" data-live-search="true" id="districtId" required  data-parsley-required-message="Please Enter District Name"  data-style="btn-secondary" name="districtId">
                                      <option value="">----Select Region---- </option>
                                    @foreach($districtInfoForSelectedRegion as $district)
                                    	<?php
                                    		if($district->id==$districtId){
                                    			?>

                                    			<option value="{{$district->id}}" selected="selected">{{$district->districtName}}</option>
                                    			<?php
                                    		}else{
                                    	?>
                                        <option value="{{$district->id}}">{{$district->districtName}}</option>
                                    <?php } ?>
                                    @endforeach  

                                </select>
                            </div>

                            <div class="form-group" id="areaBox" >
                                <label>Branch</label>
                                <div>
                                	<input type="hidden" name="id" value="{{$areaId}}" />
                                	<input type="hidden" name="page" value="{{$page}}" />
                                    <input type="text" name="areaName" value="{{$singleAreaInfo->branchName}}"  class="form-control parsley-validated" required
                                            data-parsley-required-message="Please Enter Branch Name"  placeholder="Branch"/>
                                </div>
                            </div>
                            
                        </div>
                        <button id="btnAreaSave" class="btn btn-success" type="submit" >Update</button>
                    </form>
                    
                </div> <!-- end card-body-->
            </div> <!-- end card-->
        </div> <!-- end col-->
    </div>

@endsection