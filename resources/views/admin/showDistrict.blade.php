@extends('layouts.admin')

@section('title')  District @endsection

@section('content')
<style>
::placeholder {
  color: white;
  opacity: 0.6; /* Firefox */
}

:-ms-input-placeholder { /* Internet Explorer 10-11 */
 color: white;
}

::-ms-input-placeholder { /* Microsoft Edge */
 color: white;
}
.searchBox{
  margin-left: 20%;width:60%;border:1px solid #cce6ff;border-radius:5px;padding: 5px;background-color: #001a33;color:white;
}
</style>
                <div class="row">
                    <div class="col-8 offset-2">                
                        <div class="row">
                        <div class="col-lg-12">
                            <div style="margin-top: 20px;">
                                <a href="{{route('createDistrict')}}">
                                 <button type="button" class="btn btn-success  waves-effect waves-light ">Add New District</button>
                                </a>

                                
                                 <div class="row">
                                        <div class="col-6">
                                        </div>
                                        <div class="col-6">
                                            <div class="input-group m-t-10">
                                                <input type="text" class="form-control" id="searchDistrictBox" style=" @media (max-width:969px) and (min-width:1000px) {margin-top: 10px;}​" class="searchBox" placeholder="Search District By Area Name, District Name">
                                              <span class="input-group-append">
                                                  <button type="button" class="btn waves-effect waves-light btn-blue"><i class="fa fa-search mr-1"></i> Search</button>
                                              </span>
                                            </div>
                                        </div>
                                      </div> 
                            </div>
                               <br/>   
                               
                                    <?php 
                                        $message=Session::get('message');
                                        if($message){

                                            ?>
                                            <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                                <?php
                                                    echo $message;
                                                    Session::put('message','');
                                                ?>
                                            </div>
                                            <?php
                                        
                                    }
                                    ?>
            

                                <div class="card-box">
                                    <h4 class="header-title" style="text-align: center;">All District</h4>
                                    

                                    <div class="table-responsive">
                                        <table class="table mb-0" id="districtTable">
                                            <thead style="background-color: #4281DD;">
                                            <tr>
                                                 
                                                 <th style="color: white; text-align: center;">District Name</th>
                                                 <th style="color: white; text-align: center;">Region Name</th>
                                                 <th style="color: white; text-align: center;">Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                              @foreach($districtResult as $showDistrict)  
                                                <tr>
                                                    
                                                    <td style="border: 2px solid #F7F7F4; text-align: center;">{{$showDistrict->districtName}}</td>
                                                    <td style="border: 2px solid #F7F7F4; text-align: center;">{{$showDistrict->regionName}}</td>
                                                    <td style="border: 2px solid #F7F7F4; text-align: center;">
                                                        <a href="{{route('editDistrict',[$showDistrict->id,$districtResult->currentPage()])}}" class="action-icon"><i class="mdi mdi-square-edit-outline"></i></a>
                                                        <input type="hidden" name="page" value="{{$districtResult->currentPage()}}"/>
                                                        <input type="hidden" name="id" value="{{$showDistrict->id}}"/>
                                                        <a href="#" id="{{$showDistrict->id}}" class="action-icon btnDistrictDelete"><i class="mdi mdi-delete"></i></a>
                                                    </td>
                                                  
                                                </tr>
                                            @endforeach   
                                            </tbody>
                                        </table>

                                        <table style="display: none;" class="table mb-0" id="searchDistrictTable">
                                            <thead style="background-color: #4281DD;">
                                              <tr>
                                                   
                                                   <th style="color: white; text-align: center;">District Name</th>
                                                   <th style="color: white; text-align: center;">Region Name</th>
                                                   <th style="color: white; text-align: center;">Action</th>
                                              </tr>
                                            </thead>
                                            <tbody>
                                           
                                              <div id="loadingImg" style="display: none;" ><img src="{{asset('loading_img/Spinner.gif')}}" /></div>
                                            </tbody>
                                        </table>
                                    </div> <!-- end table-responsive-->
                                     <br/>
                                   <div id="paginationLink">
                                      {{ $districtResult->links() }}
                                   </div>
        
                                </div> <!-- end card-box -->
                                </div> <!-- end card -->
                            </div><!-- end col-->
                        </div>
                        <!-- end row-->



                       
@endsection