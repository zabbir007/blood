@extends('layouts.admin')

@section('title') Create Company Details @endsection

@section('content')
   <div class="row" style="margin-top: 20px;">
        <div class="col-lg-6 offset-3">
        	<a href="{{route('showCompanyDetails')}}">
        	<button class="btn btn-primary" type="submit">Show All Company Details</button>
        	</a>
            <div class="card" style="margin-top: 10px;">
                <div class="card-body">
                	<h4 class="header-title" style="text-align: center">Add Details</h4>
                    <form class="parsley-examples"  method="post" action="{{route('saveCompanyDetails')}}" novalidate>
                    	<?php 
                            $message=Session::get('message');
                            if($message){

                                ?>
                                <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <?php
                                        echo $message;
                                        Session::put('message','');
                                    ?>
                                </div>
                                <?php
                            
                        }
                        ?>
                        @if($errors->any())
                                    
                                    <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">
                                       

                                               <ul>
                                                   @foreach($errors->all() as $error)
                                                        <li>{{$error}}</li>
                                                   @endforeach
                                               </ul>
                                           
                                       
                                    </div>
                                    @endif
                       @csrf
                      <div class="form-group">
                            <label>Company Name</label>
                            <div>
                        
                                <input type="text" name="companyName"  class="form-control parsley-validated" required
                                        data-parsley-required-message="Please Enter Company Name"   placeholder="Company Name"/>
                            </div>
                            <label>Company Details</label>
                            <div>
                        
                                <input type="text" name="companyDetails"  class="form-control parsley-validated" required
                                        data-parsley-required-message="Please Enter Company Details"   placeholder="Company Details"/>
                            </div>
                            <label>Status</label>
                            <div>
                                <select class="form-control parsley-validated" name="active" data-style="btn-secondary">
                                    <option value="1">Active</option>
                                    <option value="0">DeActive</option>
                                </select>
                            </div>
                        </div>
                        <button class="btn btn-success" type="submit">Add</button>
                    </form>
                    
                </div> <!-- end card-body-->
            </div> <!-- end card-->
        </div> <!-- end col-->
    </div>

@endsection