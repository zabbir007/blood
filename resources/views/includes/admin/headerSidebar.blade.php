 <!-- ========== Left Sidebar Start ========== -->
            <div class="left-side-menu">

                <div class="slimscroll-menu">

                    <!--- Sidemenu -->
                    <div id="sidebar-menu">

                        <ul class="metismenu" id="side-menu">

                            <li class="menu-title">DashBoard</li>

                           
                             <li>
                                <a href="javascript: void(0);">
                                    <i class="fe-user"></i>
                                    <span> User </span>
                                    <span class="menu-arrow"></span>
                                </a>
                                <ul class="nav-second-level" aria-expanded="false">
                                    <li>
                                        <a href="{{route('adminAddUser')}}">Add User</a>
                                    </li>
                                    <li>
                                        <a href="{{route('adminShowAllUser')}}">All User</a>
                                    </li>
                                    
                                </ul>
                            </li>

                            <li>
                                <a href="javascript: void(0);">
                                    <i class="fe-user"></i>
                                    <span>Add Address</span>
                                    <span class="menu-arrow"></span>
                                </a>
                                <ul class="nav-second-level" aria-expanded="false">
                                    <li>
                                        <a href="{{route('showRegion')}}">Region</a>
                                    </li>
                                    <li>
                                        <a href="{{route('showDistrict')}}">District</a>
                                    </li>
                                   
                                </ul>
                            </li>

                            <li>
                                <a href="javascript: void(0);">
                                    <i class="fe-user"></i>
                                    <span>Add Branch </span>
                                    <span class="menu-arrow"></span>
                                </a>
                                <ul class="nav-second-level" aria-expanded="false">
                                    <li>
                                        <a href="{{route('showBranch')}}">Branch</a>
                                    </li>
                                   
                                </ul>
                            </li>

                            <li>
                                <a href="javascript: void(0);">
                                    <i class="fe-user"></i>
                                    <span>Company Details</span>
                                    <span class="menu-arrow"></span>
                                </a>
                                <ul class="nav-second-level" aria-expanded="false">
                                    <li>
                                        <a href="{{route('showCompanyDetails')}}">Add Details</a>
                                    </li>
                                   
                                </ul>
                            </li>
                            

                            
                        </ul>

                    </div>
                    <!-- End Sidebar -->

                    <div class="clearfix"></div>

                </div>
                <!-- Sidebar -left -->

            </div>
            <!-- Left Sidebar End -->