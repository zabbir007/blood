 <!-- ========== Left Sidebar Start ========== -->
            <div class="left-side-menu">

                <div class="slimscroll-menu">

                    <!--- Sidemenu -->
                    <div id="sidebar-menu">

                        <ul class="metismenu" id="side-menu">

                            <a href="{{route('dashboard')}}">
                                    <span > Dashboard </span>
                                    
                                </a>

                            <li>
                                <a href="javascript: void(0);">
                                    <i class="fe-user"></i>
                                    <span> Donor Management</span>
                                    <span class="menu-arrow"></span>
                                </a>
                                <ul class="nav-second-level" aria-expanded="false">
                                   <!--  <li>
                                        <a href="{{route('donorRequest')}}">Donor Request</a>
                                    </li> -->
                                    <li>
                                        <a href="{{route('createIndoorDonor')}}">Add Donor Bangla</a>
                                    </li>

                                    <li>
                                        <a href="{{route('createIndoorDonorEnglish')}}">Add Donor English</a>
                                    </li>
                                    
                                   <!--  <li>
                                        <a href="{{route('createOutdoorDonor')}}">Add Outdoor Donor</a>
                                    </li> -->
                                   
                                    <li>
                                        <a href="{{route('showAllIndoorDonor')}}">Show Donor</a>
                                    </li>
                                   <!--  <li>
                                        <a href="{{route('showAllOutdoorDonor')}}">Show Outdoor Donor</a>
                                    </li> -->
                                    
                                </ul>
                            </li>

                             <li>
                                <a href="javascript: void(0);">
                                    <i class="fe-user"></i>
                                    <span> Donor Recruitment</span>
                                    <span class="menu-arrow"></span>
                                </a>
                                <ul class="nav-second-level" aria-expanded="false">
                                   <!--  <li>
                                        <a href="{{route('donorRequest')}}">Donor Request</a>
                                    </li> -->
                                   
                                    <li>
                                        <a href="{{route('showIndoorDonorTestReport')}}">Donor Test Report</a>
                                    </li>

                                    <li>
                                        <a href="{{route('showDonorRecozation')}}">Donor Recozation</a>
                                    </li>
                                    
                                   <!--  <li>
                                        <a href="{{route('showOutdoorDonorTestReport')}}">Outdoor Donor Test Report</a>
                                    </li> -->
                                    
                                </ul>
                            </li>
                            
                             <li>
                                <a href="javascript: void(0);">
                                    <i class="fe-user"></i>
                                    <span>Laboratory Management</span>
                                    <span class="menu-arrow"></span>
                                </a>
                                <ul class="nav-second-level" aria-expanded="false">
                                    <li>
                                        <a href="{{route('showAmount')}}">Add Amount</a>
                                    </li>
                                    <li>
                                        <a href="{{route('addTest')}}">Add Test</a>
                                    </li>
                                    <li>
                                        <a href="{{route('testReport')}}">Test Report</a>
                                    </li>
                                    
                                </ul>
                            </li>
                            
                            <li>
                                <a href="javascript: void(0);">
                                    <i class="fe-user"></i>
                                    <span>Patient Registration</span>
                                    <span class="menu-arrow"></span>
                                </a>
                                <ul class="nav-second-level" aria-expanded="false">
                                    <li>
                                        <a href="{{route('createPatient')}}">Add Patient</a>
                                    </li>
                                    <li>
                                        <a href="{{route('showAllNormalPatient')}}">Show Normal Patient</a>
                                    </li>
                                   
                                </ul>
                            </li>

                            <li>
                                <a href="javascript: void(0);">
                                    <i class="fe-user"></i>
                                    <span>Blood Receipt And Distribution</span>
                                    <span class="menu-arrow"></span>
                                </a>
                                <ul class="nav-second-level" aria-expanded="false">
                                    <li>
                                        <a href="{{route('createReceiptVoucher')}}">Receipt Voucher</a>
                                    </li>
                                    <li>
                                        <a href="{{route('createIssueVoucher')}}">Issue Voucher</a>
                                    </li>
                                    <li>
                                        <a href="{{route('totalBloodReservation')}}">Total Blood Reservation</a>
                                    </li>
                                    
                                </ul>
                            </li>

                            <li>
                                <a href="javascript: void(0);">
                                    <i class="fe-user"></i>
                                    <span>Thalassemia Module</span>
                                    <span class="menu-arrow"></span>
                                </a>
                                <ul class="nav-second-level" aria-expanded="false">
                                    <li>
                                        <a href="{{route('createPatient')}}">Add Thalassemia Patient</a>
                                    </li>
                                    <li>
                                        <a href="{{route('showAllThalassemiaPatient')}}">Show Thalassemia Patient</a>
                                    </li>
                                    
                                </ul>
                            </li>

                            <li>
                                <a href="javascript: void(0);">
                                    <i class="fe-user"></i>
                                    <span>Store Managenemt</span>
                                    <span class="menu-arrow"></span>
                                </a>
                                <ul class="nav-second-level" aria-expanded="false">
                                    <li>
                                        <a href="{{route('showItem')}}">Item Group</a>
                                    </li>
                                    <li>
                                        <a href="{{route('showItemName')}}">Item Name</a>
                                    </li>
                                    <li>
                                        <a href="{{route('createEquipment')}}">Add Store Equipments</a>
                                    </li>
                                    
                                   <!--  <li>
                                        <a href="{{route('showStockReport')}}">Stock Report</a>
                                    </li> -->

                                    <li>
                                        <a href="{{route('showStockVoucherEntry')}}">Add Store Voucher Entry</a>
                                    </li>
                                </ul>
                            </li>

                            <li>
                                <a href="javascript: void(0);">
                                    <i class="fe-user"></i>
                                    <span>Accounts Module</span>
                                    <span class="menu-arrow"></span>
                                </a>
                                <ul class="nav-second-level" aria-expanded="false">
                                    <li>
                                        <a href="{{route('showPaymentVouchaer')}}">Payment Vouchaer</a>
                                    </li>
                                    <li>
                                        <a href="{{route('showReceiptVouchaer')}}">Receipt Vouchaer</a>
                                    </li>
                                    <li>
                                        <a href="{{route('showPaymentType')}}">Account Group</a>
                                    </li>
                                    <li>
                                        <a href="{{route('showAccountLeager')}}">Account Leager</a>
                                    </li>
                                    <li>
                                        <a href="{{route('showJournalVouchaer')}}">Journal Vouchaer</a>
                                    </li>
                                </ul>
                            </li>

                            
                           <!--  <li>
                                <a href="javascript: void(0);">
                                    <i class="fe-user"></i>
                                    <span>Blood Group</span>
                                    <span class="menu-arrow"></span>
                                </a>
                                <ul class="nav-second-level" aria-expanded="false">
                                    <li>
                                        <a href="#">Add New</a>
                                    </li>
                                    
                                </ul>
                            </li> -->

                            
                        </ul>

                    </div>
                    <!-- End Sidebar -->

                    <div class="clearfix"></div>

                </div>
                <!-- Sidebar -left -->

            </div>
            <!-- Left Sidebar End -->