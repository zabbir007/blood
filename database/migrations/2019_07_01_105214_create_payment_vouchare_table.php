<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentVouchareTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_vouchare', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('userId')->nullable();
            $table->string('vouchareNo')->nullable();
            $table->string('date')->nullable();
            $table->string('branchName')->nullable();
            $table->string('paymentTypeId')->nullable();
            $table->string('sl')->nullable();
            $table->string('leager')->nullable();
            $table->string('amount')->nullable();
            $table->string('totalAmount')->nullable();
            $table->string('taka')->nullable();
            $table->string('narration')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_vouchare');
    }
}
