<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patients', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable();
            $table->string('userId')->nullable();
            $table->string('age')->nullable();
            $table->string('gender')->nullable();
            $table->string('educationQualification')->nullable();
            $table->string('email')->nullable();
            $table->string('mobile')->nullable();
            $table->string('address1')->nullable();
            $table->string('address2')->nullable();
            $table->string('address3')->nullable();
            $table->string('address4')->nullable();
            $table->string('bloodGroup')->nullable();
            $table->string('requestedBloodGroup')->nullable();
            $table->string('type')->nullable();
            $table->string('durationTime')->nullable();
            $table->string('bloodNeedDate')->nullable();
            $table->string('date')->nullable();
            $table->string('nibondonNo')->nullable();
            $table->string('barCodeImage')->nullable();
            $table->string('status')->nullable();
            $table->string('messageDate')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patients');
    }
}
