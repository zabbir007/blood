<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReceiptVoucherTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('receipt_voucher', function (Blueprint $table) {
            $table->bigIncrements('id');
             $table->string('voucherNumber')->nullable();
            $table->string('date')->nullable();
            $table->string('userId')->nullable();
            $table->string('referance')->nullable();
            $table->string('ward')->nullable();
            $table->string('bed')->nullable();
            $table->string('group')->nullable();
            $table->string('bagNumber')->nullable();
            $table->string('transfusion')->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('receipt_voucher');
    }
}
