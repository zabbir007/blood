<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJournalVouchaerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('journal_vouchaer', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('date')->nullable();
            $table->string('userId')->nullable();
            $table->string('branchName')->nullable();
            $table->string('vouchaerNo')->nullable();
            $table->string('taka')->nullable();
            $table->string('narration')->nullable();
            $table->string('leagerName')->nullable();
            $table->string('dr')->nullable();
            $table->string('cr')->nullable();
            $table->string('drAmount')->nullable();
            $table->string('crAmount')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('journal_vouchaer');
    }
}
