<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIssueVoucherTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('issue_voucher', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('voucherNumber')->nullable();
            $table->string('date')->nullable();
            $table->string('userId')->nullable();
            $table->string('name')->nullable();
            $table->string('referance')->nullable();
            $table->string('ward')->nullable();
            $table->string('bed')->nullable();
            $table->string('group')->nullable();
            $table->string('bagNumber')->nullable();
            $table->string('transfusion')->nullable();
            $table->string('donorNumber')->nullable();
            $table->string('receiptNumber')->nullable();
            $table->string('receiverName')->nullable();
            $table->string('receiverAdd')->nullable();
            $table->string('receiverTel')->nullable();
            $table->string('receiverEm')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('issue_voucher');
    }
}
