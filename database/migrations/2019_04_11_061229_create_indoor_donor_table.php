<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIndoorDonorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('indoor_donor', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('userId')->nullable();
            $table->string('name')->nullable();
            $table->string('fatherOrHusband')->nullable();
            $table->string('age')->nullable();
            $table->string('sex')->nullable();
            $table->string('education')->nullable();
            $table->string('address1')->nullable();
            $table->string('address2')->nullable();
            $table->string('phone')->nullable();
            $table->string('address3')->nullable();
            $table->string('address4')->nullable();
            $table->string('phone2')->nullable();
            $table->string('description')->nullable();
            $table->string('date')->nullable();
            $table->string('barCodeImage')->nullable();
            $table->string('nibondonNo')->nullable();
            $table->string('bloodGroup')->nullable();
             $table->integer('status')->default($value = 1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('indoor_donor');
    }
}
