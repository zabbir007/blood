<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoreOutdoorTestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('store_outdoor_test', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('donorReg');
            $table->string('testId');
            $table->string('testReport')->nullable();
            $table->string('testDoctorName');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('store_outdoor_test');
    }
}
