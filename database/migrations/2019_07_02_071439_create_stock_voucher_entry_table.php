<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStockVoucherEntryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock_voucher_entry', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('userId')->nullable();
            $table->string('invoiceNumber')->nullable();
            $table->string('balanceType')->nullable();
            $table->string('date')->nullable();
            $table->string('branchName')->nullable();
            $table->string('partyName')->nullable();
            $table->string('sl')->nullable();
            $table->string('itemName')->nullable();
            $table->string('quantity')->nullable();
            $table->string('rate')->nullable();
            $table->string('amount')->nullable();
            $table->string('totalAmount')->nullable();
            $table->string('taka')->nullable();
            $table->string('narration')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock_voucher_entry');
    }
}
