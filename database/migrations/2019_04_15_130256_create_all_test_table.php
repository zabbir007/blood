<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAllTestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('all_test', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('date')->nullable();
            $table->string('userId')->nullable();
            $table->string('name')->nullable();
            $table->string('age')->nullable();
            $table->string('sex')->nullable();
            $table->string('group')->nullable();
            $table->string('referance')->nullable();
            $table->string('ward')->nullable();
            $table->string('bed')->nullable();
            $table->string('testId')->nullable();
            $table->string('remainingAmount')->nullable();
            $table->string('totalAmount')->nullable();
            $table->string('paidAmount')->nullable();
            $table->string('testReport')->nullable();
            $table->string('testNumber')->nullable();
            $table->string('barCodeImage')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('all_test');
    }
}
